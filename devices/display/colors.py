import netpp
import time
from colorcurves import *

from lut import *

def write_frame(r):
	x = r.Stream.X.get()
	y = r.Stream.Y.get()
	c = 0
	buf = ""
	for i in range(y):
		for j in range(x):
			buf += chr((j+c) & 0xff)
			buf += chr((i+c) & 0xff)

	r.Stream.Data.set(buffer(buf))
	time.sleep(0.1)

hs = 0xc000 / 24


SUNRISE = [
	(0x0, (0x5ff, 0, 255)),
	(0x2000, (0x200, 100, 255)),
	(0x3000,    (0x500, 127, 100)),
	(0x5000, (0x400, 200, 0)),
	(0x6000, (0x200, 230, 0)),
	(0x8000, (0x000, 100, 50)),
	(0x10000, (0x200, 180, 0)),
]

ALL_CURVES = [
	AUREABOREALIS, GREENSIES, SPRINGSIES
]

def make_lut16(curve):
	val = 0
	buf = ""
	cur, hwbpoint = curve[0]
	oh, ow, ob = hwbpoint
	for i in curve[1:]:
		pos, hwbpoint = i
		step = float(1.0) / float(pos - cur)
		f = 0.0
		for j in range(cur, pos):
			nh, nw, nb = hwbpoint
			
			h = oh + f * (nh-oh)
			w = ow + f * (nw-ow)
			b = ob + f * (nb-ob)
			f += step
			hwb = (int(h), int(w), int(b))

			r, g, b = hwb2rgb(hwb)
			buf += struct.pack("BBB", r, g, b)

		oh, ow, ob = nh, nw, nb

		cur = j + 1

	return buffer(buf)


def convert_warm(v, a):
	v >>= 4
	if v < 0x600:
		hwb = (v / 4, 200 - v / 8, v / 12)
	elif v < 0x0c00:
		v -= 0x600
		hwb = (0x600 / 4 + v / 4, 200-0x600/8 + v / 12, 128-v/12)
	else:
		v -= 0xc00
		hwb = (0x4ff - v / 2, 0, v / 12)
		
	return hwb2rgb(hwb)


def disp_config(disp, x, y):
	disp.Stream.Stop.set(1)
	disp.Stream.BitsPerPixel.set(16)
	disp.Mode.set(disp.Mode.Indexed16.get())
	disp.Stream.X.set(x)
	disp.Stream.Y.set(y)


import sys



disp = netpp.connect("localhost:2008")
r = disp.sync()

disp_config(r, 256, 256)
r.Stream.Start.set(1)

default = r.ColorTable.Preset.Pseudocolor.get()
custom = r.ColorTable.Preset.Custom.get()
# r.ColorTable.Preset.set(default)
# r.ColorTable.Gamma.set(0.7)
# r.ColorTable.Update.set(1) # Update LUT
# write_frame(r)
# time.sleep(1.0)
r.ColorTable.Preset.set(custom)

if len(sys.argv) > 1:
	curve = eval(sys.argv[1])
	lut = make_lut16(curve)
	r.ColorTable.CustomData.set(lut)
	r.ColorTable.Update.set(1) # Update LUT
	write_frame(r)

else:
	for curve in ALL_CURVES:
		lut = make_lut16(curve)
		r.ColorTable.CustomData.set(lut)
		r.ColorTable.Update.set(1) # Update LUT
		write_frame(r)
		time.sleep(2)


