/** \file property_types.h 
 *
 * Private Property data types
 *
 * This file contains the token encodings. Since tokens are do not a
 * priori have to be a static entity, the encoding can change over time.
 * It is entirely up to the slave to define the token arrangements and
 * internal decoding structure.
 * Changing the token structure will NOT break netpp compatibility.
 *
 * However, note that there are reserved tokens which can never be used
 * in user space, see devlib_types.h
 *
 */

#ifndef DCPROPERTY_TYPES_H_INCLUDED_260872
#define DCPROPERTY_TYPES_H_INCLUDED_260872

#include "devlib_types.h"

#include "dlistpool.h"

/* This is how the tokens (32 bit) are composed */

#define DYNAMIC_PROPERTY       0x80000000
#define DEVICE_MASK            0x70000000
#ifdef CONFIG_PROXY
/* Proxies have a index field */
#define INDEX_MASK             0x0f000000
#define ACCESS_MASK            0x00fff000
#else
#define ACCESS_MASK            0x0ffff000
#endif
#define PROP_MASK              0x00000fff

/** Maximum possible array index */
#define MAX_INDEX              128

/** Maximum possible property name length */
#ifndef NAMELENGTH
#define NAMELENGTH 64
#endif

/** Build a token from root node and index
 * \param parent   The root node
 * \param prop     The property's index
 *
 * Just for internal use. Handle with care!
 */
#define BUILD_TOKEN(parent, prop)  \
	( (parent & ~PROP_MASK) | ((prop) & PROP_MASK) )

#define BUILD_ARRAYTOKEN(parent, index) \
	( (parent) | ( (index) << 12) )


/** Get the property table index from token */
#define INDEX_FROMTOKEN(t)   ( t & PROP_MASK )
/** Get the array offset from token */
#define OFFSET_FROMTOKEN(t)  (( t & ACCESS_MASK ) >> 12)
/** Create a device root node token. 'x' = device index */
#define DEVICE_TOKEN(x)      ( (TOKEN) x << 28 )

#define DEVICE_FROMTOKEN(x)  ( (TOKEN) x >> 28 )

enum loc_enum {
	DC_ADDR,                        // value in flat address space
	DC_HANDLER,                     // get/set value by handler
	DC_STATIC,                      // static value
	DC_VAR,                         // Use pointer variable (BUFFER, STRING)
	DC_CUSTOM,                      // Custom dispatcher
	DC_USERDEFINED                  // User defined dispatcher
};
#ifdef TINYCPU
typedef uint8_t Location;
#else
typedef enum loc_enum Location;
#endif

// Return true when property is a buffer or string type
#define IS_BUFFER(x)      (x == DC_BUFFER || x == DC_STRING)

// Events descriptors are children nodes of a property and are 'fired'
// under certain conditions. When a code 'PROPERTY_EVENT' is returned
// from dcDevice_SetProperty(), the property's event nodes must be queried
// and an appropriate action on the event target must be taken

typedef enum {
	DC_ENABLE,                      // Enable property
	DC_DISABLE,                     // Disable property
	DC_UPDATE,                      // Update property
	DC_DYNAMIC,                     // Re-build dynamic property
} EventType;

// Getter/Setter function pointers
typedef int (*FPGetter)(DEVICE, DCValue *out); 
typedef int (*FPSetter)(DEVICE, DCValue *in); 

typedef uint32_t  BITMASK;  // Bitmask

#ifdef TINYCPU
#ifdef DEBUG
#warning "Number of properties limited to 254!"
#endif
typedef unsigned char  PROPINDEX;
typedef unsigned char  BSIZE;
#else
typedef unsigned short PROPINDEX;
typedef uint16_t       BSIZE;    // register or block size
#endif

typedef unsigned short ADDR_OFFSET;

#define LAST_BIT 31
#define _1 (BITMASK) (~0)

#define CREATEMASK(from, to)  (( _1 << from ) & ( _1 >> (LAST_BIT - to)))

// Property atoms:

// DC_HANDLER
struct pa_handler {
	FPGetter get;
	FPSetter set;
};

// DC_ARRAY
struct pa_array {
	short          banksize; // register bank size
	ADDR_OFFSET    offset;   // register offset, reserved
};

// DC_REGISTER
struct pa_register {
	ADDRESS        id;       // Memory or Register address 
	unsigned char  lsb;      // Bitrange LSB
	unsigned char  msb;      // Bitrange MSB
	BSIZE          size;     // register or block size
};

// case DC_BUFFER, DC_STRING
struct pa_buffer {
	unsigned char *p;        // Buffer pointer     
	size_t         size;
};

struct pa_string {
	char           *p;        // String pointer     
	size_t         size;
};

struct pa_event {
	EventType                  s_evttype; // Event type
#if defined(__cplusplus) || defined(ALLOW_DYNAMIC_PROPERTIES)
	PROPINDEX                  target;    // property target
#else
	MAYBE_CONST PROPINDEX      target;    // property target
#endif
};

// New custom handler for in and out, specific to a property
// context (void *p)
// This is the default handler type for a dynamic property
struct pa_custom {
	int (*handler)(void *p, int rw, DCValue *inout); 
	void *p;                  // User pointer
};

union pa_access {
	struct pa_handler 	func;
	struct pa_array     array;
	// case DC_ROOT
	unsigned short      base;     // inheritance property root index
	struct pa_register  reg;
	struct pa_string    str;
	struct pa_buffer    buf;
	struct pa_custom  	custom;   // custom dispatcher

	// VARIABLE pointers.
	// They may adapt to the architecture's int size.
	MAYBE_VOLATILE pint_t             *varp_int;
	MAYBE_VOLATILE puint_t            *varp_uint;
	MAYBE_VOLATILE pmode_t            *varp_mode;
	MAYBE_VOLATILE uint8_t            *varp_bool;
	MAYBE_VOLATILE float              *varp_float;

	// DC_STATIC
	MAYBE_CONST int32_t       s_int;    // Force 32 bit static
	MAYBE_CONST uint32_t      s_uint;   // Force 32 bit static
	MAYBE_CONST float         s_float;
	MAYBE_CONST char         *s_string;

	// DC_EVENT
	struct pa_event     event;

};

////////////////////////////////////////////////////////////////////////////

/** static Property descriptor */

/* Warning: Changing the order requires changing proplist.xsl */
typedef struct _propertydesc {
	MAYBE_CONST char *name;
	uint16_t          flags;    ///< flags (shared public and private)

	PropertyType      type;     ///< Property type
	Location          where;    ///< Property domain
	union pa_access   access;   ///< Access descriptor

	MAYBE_CONST PROPINDEX *members;  ///< Member list (static only)

} PropertyDesc;

/* Dynamic nodes */

typedef TOKEN DynPropToken;

/** Dynamic property node type */
typedef enum {
	DC_NULL,    ///< NULL node (invalid)
	DC_HUB,     ///< Hub
	DC_PORT,    ///< Port
	DC_DEVICE,  ///< RemoteDevice
	DC_DPROP    ///< Dynamic property property :-)
} PNodeType;

/** Dynamic property descriptor
 *
 * Needed for parameters changing during the entire netpp session.
 * When ALLOW_DYNAMIC_PROPERTIES defined, allow dynamic properties
 * as well. They have a TOKEN with MSB set in this implementation.
 *
 */

typedef struct dyn_prop_t {
	INDEX next;
	INDEX prev;
	char name[NAMELENGTH];
	PNodeType type;             ///< Node type 
	union {
		/** A Hub structure */
		struct {
			DynPropToken ports;               ///< Port children list
			const
			struct protocol_methods *methods; ///< Hub methods
		} hub;
		/** A port just can have a state on some protocols */
		struct {
#ifdef CONFIG_PROXY
			DynPropToken proxy;               ///< Proxy hub
#endif
			uint16_t state;
		} port;
#ifdef CONFIG_CACHE
		struct {
			TOKEN proptoken;
			INDEX rank;
			INDEX prio;
		} cache;
#endif
#ifdef ALLOW_DYNAMIC_PROPERTIES
		/** A dynamic property */
		struct {
			PropertyDesc *desc;
			INDEX first;    ///< index to first child
			INDEX last;     ///< index to last child
		} prop;
#endif
		struct DCDevice *device;      ///< Pointer to Remote device
	};
} DynPropertyDesc;

/** An internal device descriptor.
 * This is generated by the XML processor and contains all the necessary
 * information for the device control. It is typically instanced in proplist.c.
 */
typedef struct device_desc_t {
	const PropertyDesc *root;     ///< Pointer to property list
	const int major;              ///< Backend revision major
	const int minor;              ///< Backend revision minor
	const int nproperties;        ///< Number of properties
} DeviceDesc;

// Internal helper function:
//

TOKEN follow_link(const PropertyDesc **cur);
	const PropertyDesc *getProperty_ByToken(TOKEN t);
TOKEN nextVisibleToken(TOKEN parent,
	const PropertyDesc *cur, int i, char visible);

// const PropertyDesc *getProperty_ByToken(TOKEN t);

#endif  // DCPROPERTY_TYPES_H_INCLUDED_260872
