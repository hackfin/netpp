# Platform specific make rules and flags


# Some common flags for the kirkwood ARM architecture:
KIRKWOOD_CFLAGS = -mtune=marvell-f -mtune=xscale -march=armv5te -march=armv4t \
				  -msoft-float

OMAP3_CFLAGS = -march=armv7-a -mtune=cortex-a8 -mfloat-abi=softfp \
               -mfpu=neon -ffast-math -mthumb

ifeq ($(PLATFORM),bfin)
	ARCH = bfin-uclinux-
endif

ifeq ($(PLATFORM),net7026)
	ARCH = arm-none-eabi-
	NONET = yes
	CFLAGS += -DSTANDALONE -DTINYCPU
	CFLAGS += -DNO_MALLOC
	CFLAGS += -mcpu=arm7tdmi -fno-common
	LDFLAGS += -Wl,--no-warn-mismatch -nostdlib -mcpu=arm7tdmi
endif

ifeq ($(PLATFORM),aria)
	ARCH = arm-linux-gnueabi-
	CFLAGS += -mcpu=arm7tdmi
	LDFLAGS += -mcpu=arm7tdmi
endif

ifeq ($(PLATFORM),pyps)
	ARCH = mips-elf-
	CFLAGS += -DHASNOT_STDINT -DSTANDALONE -DTINYCPU
	CFLAGS += -msoft-float -fno-builtin -mips16
endif

ifeq ($(PLATFORM),mais)
	ARCH = mips-elf-
	CFLAGS += -DHASNOT_STDINT -DSTANDALONE -DTINYCPU
	CFLAGS += -msoft-float -fno-builtin
endif

ifeq ($(PLATFORM),wrtnode)
	ARCH = mipsel-openwrt-linux-uclibc-
	CFLAGS += -fPIC # Needed when making use of .so's
	LDFLAGS += -ldl
endif

ifeq ($(PLATFORM),microblaze)
	ARCH = microblaze-elf-
	CFLAGS += -DHASNOT_STDINT -DSTANDALONE -DTINYCPU
	CFLAGS += -msoft-float -fno-builtin
endif

ifeq ($(PLATFORM),ez430)
	ARCH = msp430-
	CUSTOM_ERRHANDLER = yes
	WITHOUT_CRC = yes
	CFLAGS += -DSTANDALONE -DTINYCPU -DNO_LOGGING -mmcu=$(CPU)
	LDFLAGS += -Wl,-Map -Wl,symbols.map -mmcu=$(CPU)
endif

ifeq ($(PLATFORM),esp8266)
	ARCH = xtensa-lx106-elf-
	CUSTOM_LIBRARY = $(LIBNAME)_irom0
	CFLAGS += -DNO_MALLOC
	CFLAGS += -DSTANDALONE -DTINYCPU -DUSER_INIT
	# Newer toolchain has stdint:
	# -DHASNOT_STDINT
	CFLAGS += -DNO_COLOR
	CFLAGS += -Wl,-EL -fno-inline-functions
	CFLAGS += -nostdlib -mlongcalls -mtext-section-literals -DICACHE_FLASH
	CFLAGS += -D__ets__
	LDFLAGS += -nostdlib -u call_user_start
	LDFLAGS += -Wl,-static
endif

ifeq ($(PLATFORM),mingw32)
	EXE = .exe
	ifndef ARCH
		ARCH = i686-w64-mingw32-
	endif
else
	EXE = 
endif

ifeq ($(PLATFORM),omap3)
	CFLAGS += $(OMAP3_CFLAGS) -fPIC
	ARCH = arm-linux-gnueabi-
endif

ifeq ($(PLATFORM),kirkwood)
	CFLAGS += $(KIRKWOOD_CFLAGS)
	ARCH = arm-linux-gnueabi-
endif

ifeq ($(PLATFORM),kirkwood-openwrt)
	CFLAGS += $(KIRKWOOD_CFLAGS)
	ARCH = arm-openwrt-linux-uclibcgnueabi-
endif

ifeq ($(PLATFORM),zpu)
	ARCH = zpu-elf-
	CUSTOM_ERRHANDLER = yes
	CFLAGS += -DNO_MALLOC -DNO_FLOAT
	CFLAGS += -DHASNOT_STDINT -D__ZPU__ -DSTANDALONE -DTINYCPU
	# Compile for ZPUng:
	LDFLAGS = -Wl,--defsym -Wl,ZPU_ID=0xcafe1020
	LDFLAGS += -nostdlib
	LDFLAGS += -Wl,--relax -Wl,--gc-sections
	LDFLAGS += -Wl,-Map -Wl,zpu.map
	LDFLAGS += -Wl,--cref 
	LDFLAGS += -Wl,-T -Wl,$(LINKERSCRIPT)
	LDFLAGS += -nostdlib
endif

ifeq ($(PLATFORM),avr)
	CPU=atmega16
	ARCH = avr-
	CFLAGS += -DSTANDALONE -DTINYCPU -mmcu=$(CPU) -Os
	LDFLAGS += -Wl,-Map -Wl,symbols.map -mmcu=$(CPU) -Wl,-N
endif

ifeq ($(PLATFORM),atmega32u4)
	CPU=atmega32u4
	ARCH = avr-
	WITHOUT_CRC = yes
	CFLAGS += -DNO_MALLOC
	CFLAGS += -DSTANDALONE -DTINYCPU -mmcu=$(CPU) -Os
	LDFLAGS += -Wl,-Map -Wl,symbols.map -mmcu=$(CPU) -Wl,-N
	LDFLAGS += -Wl,--relax -Wl,--gc-sections
endif

ifeq ($(PLATFORM),Darwin)
  # Find editline headers from 'libedit' brew package:
  CFLAGS += -I/usr/local/opt/libedit/include
  CFLAGS += -I/usr/local/Cellar/python@2/2.7.15_1/Frameworks/Python.framework/Versions/2.7/include/python2.7
  CPPFLAGS += -I/usr/local/opt/libedit/include
  LDFLAGS += -L/usr/local/opt/libedit/lib -ledit
  LDFLAGS += -L/usr/local/Cellar/python@2/2.7.15_1/Frameworks/Python.framework/Versions/2.7/lib/ -lpython2.7
endif

ifdef PREFIX

CC = $(PREFIX)/$(ARCH)gcc
CXX = $(PREFIX)/$(ARCH)g++
AS = $(PREFIX)/$(ARCH)as
AR = $(PREFIX)/$(ARCH)ar
LD = $(PREFIX)/$(ARCH)ld
RUNTIME = $(PREFIX)/$(ARCH)runtime
STRIP = $(PREFIX)/$(ARCH)strip
OBJCOPY = $(PREFIX)/$(ARCH)objcopy

else

CC = $(ARCH)gcc
CXX = $(ARCH)g++
AS = $(ARCH)as
AR = $(ARCH)ar
LD = $(ARCH)ld
RUNTIME = $(ARCH)runtime
STRIP = $(ARCH)strip
OBJCOPY = $(ARCH)objcopy

endif

# Allow to add platform root dir for cross compiling against target libs:

ifdef PLATFORM_ROOT
CFLAGS += -I$(PLATFORM_ROOT)/usr/include
LDFLAGS += -L$(PLATFORM_ROOT)/usr/lib
endif
