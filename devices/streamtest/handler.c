
////////////////////////////////////////////////////////////////////////////
//
// (c) 2005, 2006 section5 // Martin Strubel <hackfin@section5.ch>
//
// Handler for storing a byte stream on the client ("slave")
//
////////////////////////////////////////////////////////////////////////////

#include "netpp.h"
#include "streamtest/device_error.h"
#include "streamtest/streamtest.h"
#include "property_protocol.h" // netpp_log()

#include <stdlib.h>


struct gcontext
context = {
	.peerdev = NULL,
	.thread = 0,
	.client = "",
	.mode = 0,
	.file = NULL,
	.blocksize = 0,
	.buffer = NULL,
	.cursize = 0
};


int get_stream_blocksize(DEVICE d, DCValue *out)
{
	out->value.i = context.blocksize;
	return 0;
}

int set_stream_blocksize(DEVICE d, DCValue *in)
{
	int n = in->value.i;
	if (context.blocksize != n) {
		if (context.buffer) free(context.buffer);
		context.buffer = (char *) malloc(n * sizeof(char));
		if (!context.buffer) {
			return DCERR_MALLOC;
		}
		context.blocksize = in->value.i;
	}
	return 0;
}

void *feeder(void *arg)
{
	int error = 0;
	static char buf[] = "Wie war das nochmal?\n";
	DCValue val;

	val.type = DC_STRING;
	val.value.p = buf;
	val.len = sizeof(buf);

	while (error == 0 && context.mode != 0) {
		error = dcDevice_SetProperty(context.peerdev, context.t_data, &val);
	}

	netpp_log(DCLOG_NOTICE, "Thread terminated: '%s'",
		dcGetErrorString(error));

	return NULL;
}

int set_stream_start(DEVICE d, DCValue *in)
{
	FILE *f;
	int error = 0;

	if (context.mode != 0) {
		error = dcDeviceOpen(context.client, &context.peerdev);
		if (error < 0) {
			context.peerdev = NULL;
		}

		error = dcProperty_ParseName(context.peerdev,
			"Stream.DynamicData", &context.t_data);
		if (error == 0) {
			error = pthread_create(&context.thread, NULL, &feeder, NULL);
			if (error) {
				printf("Failed to create thread\n");
				error = DCERR_STREAM_START;
			}
		}
	} else {
		printf("Open test.dat for writing\n");
		f = fopen("test.dat", "wb");
		if (!f) {
			return DCERR_STREAM_START;
		}
		context.file = f;
	}
	return error;
}

int set_stream_stop(DEVICE d, DCValue *in)
{
	void *ret;
	if (context.file) {
		printf("Close test.dat\n");
		fclose(context.file);
		context.file = NULL;
	}
	if (context.buffer) {
		printf("CLOSE buffer\n");
		free(context.buffer);
		context.buffer = NULL;
	}
	if (context.peerdev) {
		context.mode = 0; // Set this to exit thread
		pthread_join(context.thread, &ret);
		netpp_log(DCLOG_VERBOSE, "Finished feeder thread");
		dcDeviceClose(context.peerdev);
		context.peerdev = NULL;
	}
	context.blocksize = 0;
	return 0;
}

/* This function initializes the buffer for gathering the data
 *
 * If in->type is DC_BUFFER, owner must initialize buffer structure and
 * validate length of data that peer desires to send.
 * If in->type is DC_COMMAND, the buffer is flushed.
 */

int set_stream_data(DEVICE d, DCValue *in)
{
	size_t n;

	if (in->type == DC_COMMAND) {
		// Note that in->len no longer contains the original length
		// at this point.
		n = fwrite((char *) context.buffer, 1, context.cursize, context.file);
		printf("Wrote %lu bytes to file\n", n);
		if (n != context.cursize) {
			return DCERR_STREAM_WRITE;
		}
	} else if (in->type == DC_BUFFER) {
		if (in->len > context.blocksize) {
			in->len = context.blocksize;
			return DCERR_PROPERTY_SIZE_MATCH;
		}
		in->value.p = context.buffer;
		// Store length
		context.cursize = in->len;
	} else
		return DCERR_PROPERTY_TYPE_MATCH;

	return 0;
}
		
int set_resx(DEVICE d, DCValue *in)
{
	printf("Setting X resol ignored\n");
	return 0;
}

int set_resy(DEVICE d, DCValue *in)
{
	printf("Setting Y resol ignored\n");
	return 0;
}

