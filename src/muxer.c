/** Somewhat generic channel muxer layer for underlying devices
 *
 * EXPERIMENTAL
 *
 * * Has proxy functionality
 * * Allows simultaneous master/slave role
 *
 * (c) 2016 <strubel@section5.ch>
 *
 */

#include <stdlib.h>
#include <string.h>
#include "devlib.h"
#include "proxy.h"
// #include "queue.h"

#ifndef MAX_PACKET_SIZE
#define MAX_PACKET_SIZE 128
#endif

// unixdevice.c / rawio.c
int device_poll(int fd, int timeout);

// Default timeout to check for a request (outside protocol)
#define TIMEOUT_CHECK       10 

// Default timeout to wait for a response inside protocol
#define TIMEOUT_WAIT      2000

int device_open(const char *name, const char *param);
int dev_read(int fd, char *buf, int len);
int dev_write(int fd, const char *buf, int len);



#define HDR_SIZE sizeof(PropertyPacketHdr)

#define PP_EXTENSION_CHANNEL 0x8000

static short g_pktchunksize = MAX_PACKET_SIZE - HDR_SIZE;

PortBay g_users;

#if 0
static
int handle_muxerror(int error, const char *txt)
{
	netpp_log(DCLOG_ERROR,
			"Device error [%s]", txt);
	error = DCERR_COMM_GENERIC;
	return error;
}
#endif

PortToken g_mux_users;

// XXX HACK:
int g_fd = -1;

static
int peer_init(Peer *p)
{
	p->sendbuf = (char *) malloc(MAX_PACKET_SIZE);
	if (!p->sendbuf) return DCERR_MALLOC;

	p->recvtimeout_check =  TIMEOUT_CHECK;
	p->recvtimeout_wait  =  TIMEOUT_WAIT;

	p->recvbuf = (char *) malloc(MAX_PACKET_SIZE);
	if (!p->recvbuf) {
		free(p->sendbuf);
		return DCERR_MALLOC;
	}
	p->packetsize = MAX_PACKET_SIZE - HDR_SIZE;
	return 0;
}


int muxdev_init(RemoteDevice *devices, const char *dev)
{
	int error;
	Peer *p;
	PortToken hub;
	char name[32];
	char *param;

	error = scan_into(dev, ':', name, sizeof(name));
	if (error < 0) return DCERR_PROPERTY_SIZE_MATCH;
	param = &name[error]; // String after ':'

	devices[0].flags = F_PROXY;
	devices[1].flags = F_PROXY;

	p = &devices[0].peer;
	
	p->proxy.flags = PP_EXTENSION_CHANNEL; // Mark as channel 1

	g_fd = device_open(name, param);
	if (g_fd < 0) {
		netpp_log(DCLOG_ERROR, "Could not open device %s", name);
		return DCERR_COMM_INIT;
	}
	p->fd = g_fd;

	devices[0].localdata = NULL;

	hub = Hub(GET_PORTROOT(), MUX_PEERS_NAME, 0);
	if (!IS_VALIDTOKEN(hub)) return DCERR_MALLOC;
	g_mux_users = hub;
	portbay_append(GET_PORTROOT(), hub);


	hub = Hub(GET_PORTROOT(), MUX_SERVER_NAME, &mux_methods);
	if (!IS_VALIDTOKEN(hub)) return DCERR_MALLOC;

	p->hub = desc_fromtoken(hub);
	if (!p->hub) {
		close(p->fd);
		return DCERR_MALLOC;
	}

	// Abuse these buffers for m/s and s/m queue
	peer_init(p);

	p->proxy.flags = 0;

	return 0;
}

#if 0
int mux_server_init(const char *dev, ServerContext *serv)
{
	serv->flags = F_SERV_TCP;
	// Reset user count
	serv->usercount = 0;
	serv->peer_init = 0; // peer_init;
	serv->slave_handler = slave_handler;
 	serv->hub = Hub(GET_PORTROOT(), "_MUXserv", &mux_methods);
	if (!IS_VALIDTOKEN(serv->hub)) return DCERR_MALLOC;
	// Reserve device list:
	serv->peerdevs = Hub(GET_PORTROOT(), "_MUXpeers", 0);
	if (!IS_VALIDTOKEN(serv->peerdevs)) return DCERR_MALLOC;
	// But we don't append this to the global portbay.
	serv->devices =
		portbay_fromtoken(desc_fromtoken(serv->peerdevs)->hub.ports);
	return 0;
}
#endif

#if 0
static
int muxdev_handle_rxq(Peer *slave, Peer *master)
{
	struct pollfd pfd;
	unsigned short type;
	PropertyPacketHdr h;
	char *pkt;
	int ret;
	int n;
	PortBay *users;
	Peer *p = slave;

	pfd.fd = p->fd;
	pfd.events = POLLIN | POLLPRI;

	n = poll(&pfd, 1, 1);
	if (n < 0) {
		return handle_muxerror(n, "Polling RX");
	} else if (n == 0) return n;

	n = dev_read(p->fd, (char *) &h, sizeof(h));
 	type = ntohs(h.type);
	if (n > 0) {
		// If channel flag set, put into respective queue
		if ((type & PP_EXTENSION_CHANNEL)) {
			p = master;
		}
		if (p) {
			PacketQueue *rxq = (PacketQueue *) p->recvbuf;
			ret = queue_ptr(rxq, (void **) &pkt);
			memcpy(pkt, &h, sizeof(h)); // Copy header
			pkt += sizeof(h);
			n = get_packet_length((char *) &h);
			// If len > maximum possible payload size, prune!
			if (n > g_pktchunksize) {
				n = g_pktchunksize;
			}
			n = dev_read(p->fd, pkt, n);
			if (n < 0) return n;
			queue_commit(rxq, n + sizeof(PropertyPacketHdr));
		}
	}

	return 0;
}

static
int muxdev_handle_txq(Peer *slave, Peer *master)
{
	int ret;
	int n;
	PacketQueue *txq;
	int fd;
	char *buf;

	// Transmit queue:
	txq = (PacketQueue *) slave->sendbuf;
	ret = queue_get(txq, (void **) &buf, &n);
	fd = slave->fd;
	if (ret > 0) {
		printf("Write from Queue 0 -> out\n");
		n = dev_write(fd, buf, n);
		queue_pop(txq);
	}

	if (master) {
		txq = (PacketQueue *) master->sendbuf;
		ret = queue_get(txq, (void **) &buf, &n);
		if (ret > 0) {
			printf("Write from Queue 1 -> out\n");
			n = dev_write(fd, buf, n);
			queue_pop(txq);
		}
	}


	return 0;
}

#endif

int muxdev_handle(RemoteDevice *devices)
{
	int ret;
#if 0
	DynPropertyDesc *pd;
	PortToken walk;
	PortBay *users;
	Peer *p = 0;

	users = portbay_fromtoken(desc_fromtoken(g_mux_users)->hub.ports);
	walk = ITERATOR_FIRST(users);
	if (ITERATOR_VALID(walk)) {
		pd = desc_fromtoken(walk);
		p = &pd->device->peer;
		// walk = ITERATOR_NEXT(users, pd);
	}

	ret = muxdev_handle_rxq(&devices[0].peer, p);
	if (ret < 0) return ret;
	ret = muxdev_handle_txq(&devices[0].peer, p);
#endif
	ret = slave_handler(&devices[0]);
	// ret = 0;

	return ret;
}

static
int mux_create(PortToken hub, const char *name, PortToken *portid)
{
	PortToken port;
	// DynPropertyDesc *hubd;

// TODO Don't allow to create multiple connections or arbitrary
// MUX:n entries.
	name = skip_delimiter(name, ':');
	if (!name) return DCERR_COMM_INIT;

	netpp_log(DCLOG_DEBUG, "Create MUX entry with name '%s'", name);

	port = Port(hub, name, DC_PORT);
	if (!IS_VALIDTOKEN(port)) return DCERR_MALLOC;
	hub_append(hub, port);
	// hubd = desc_fromtoken(hub);
	*portid = port;
	return 0;
}

int muxdev_exit(RemoteDevice *devices)
{
	free(devices[0].peer.recvbuf);
	free(devices[0].peer.sendbuf);

	return 0;
}

int dev_probe(DynPropertyDesc *hub);

static
int mux_probe(DynPropertyDesc *hub)
{
	PortToken p;
 	PortBay *bay = portbay_fromtoken(hub->hub.ports);
 	p = PortNode(bay, "<path>", DC_PORT);
 	portbay_append(bay, p);
	// dev_probe(hub);
	return 0;
}

/** This function gets called in master mode only, when a connection
 * is made from the MUX hub.
 *
 */

static
int mux_open(RemoteDevice *d, int portindex)
{
	int error;

	// char name[8];

	char *name;

	name = getname(&d->peer, portindex);

	// Hack for master only mode:
	if (g_fd < 0) {
		netpp_log(DCLOG_VERBOSE, "Open MUX device %s", name);
		error = proxy_open(d, "DEV:", name);
		if (error < 0) return error;
		g_fd = d->peer.fd; // Grab fd from created proxy
	} else {
		// Not allowed to open another one when slave is enabled.
		return DCERR_COMM_INIT;
	}

	// We already opened the device when initializing the muxer,
	// but we have to assign a packet queue to the new peer structure:
	peer_init(&d->peer);
	d->peer.proxy.flags = 0;

// 	users = portbay_fromtoken(desc_fromtoken(g_mux_users)->hub.ports);
// 	if (portindex > 2 || portindex < 0) return DCERR_COMM_INIT;
// 	printf("Open MUX device index %d\n", portindex);
// 	sprintf(name, "MUX:%d", portindex + 1);
// 	p = PortNode(users, name, DC_DEVICE);
// 	portbay_append(users, p);
// 	desc_fromtoken(p)->device = d;
	return 0;
}

int mux_close(RemoteDevice *d)
{
	proxy_close(d);
	portbay_remove(d->portid);
	free(d->peer.sendbuf);
	free(d->peer.recvbuf);
	return 0;
}

#if 1
static
int send_slice(Peer *peer, PropertyPacketHdr *h, const char *data,
		unsigned short len)
{
 	// printf("Send slice fd %d\n", peer->fd);
 	dev_write(peer->fd, (char *) h, sizeof(*h));
 	dev_write(peer->fd, data, len);
	return 0;
}

#else

static
int send_slice(Peer *peer, PropertyPacketHdr *h, const char *data,
		unsigned short len)
{
	int error;
 	PacketQueue *txq = (PacketQueue *) peer->sendbuf;
 	char *pkt;
 
 	error = queue_ptr(txq, (void **) &pkt);
 	memcpy(pkt, h, sizeof(PropertyPacketHdr)); pkt += sizeof(PropertyPacketHdr);
 	memcpy(pkt, data, len);
 	queue_commit(txq, len + sizeof(PropertyPacketHdr));
// 	printf("Send slice fd %d\n", peer->fd);
// 	dev_write(peer->fd, (char *) h, sizeof(*h));
// 	dev_write(peer->fd, data, len);
	return 0;
}

#endif

static
int mux_send_packet(Peer *peer, PropertyPacketHdr *h,
		const char *data, size_t len)
{
	int error;
	unsigned short type = ntohs(h->type);

	// Insert channel mux field into type:
	type |= peer->proxy.flags;

// 	netpp_log(DCLOG_DEBUG, "Send packet fd: %x, type: %x len: %ld",
// 			peer->fd, peer->proxy.flags, len);

	// Now send packet fragments
	int i = 0;

	h->id = htons(i);

	while (len > g_pktchunksize) {
		h->type = htons(type | PP_FLAG_FRAG);
		netpp_log(DCLOG_DEBUG, "Send slice (%d left)", len);
		error = send_slice(peer, h, data, g_pktchunksize);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Failed to send slice");
			return error;
		}
		len -= g_pktchunksize;
		data += g_pktchunksize;

		// Update length field
		h->sub.set.len = htonl((unsigned long) len);

		i++;
		h->id = htons(i);
	}

	h->type = htons(type);

	// netpp_log(DCLOG_DEBUG, "Last slice (%d left)", len);
	error = send_slice(peer, h, data, (unsigned short) len);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Failed to send slice");
		return error;
	}

	return error;
}

#if 1
int mux_poll_packet(Peer *p, PropertyPacketHdr **h, unsigned short timeout)
{
	char *buf;
	int n;

	n = device_poll(p->fd, timeout);
	if (n <= 0) return n;

	// HACK, this is ok, because PacketQueue is > packet buffer
	buf = p->recvbuf;

	n = dev_read(p->fd, buf, sizeof(PropertyPacketHdr));
	n = get_packet_length(buf);
	*h = (PropertyPacketHdr *) buf;
	buf += sizeof(PropertyPacketHdr);
	if (n > g_pktchunksize) {
		n = g_pktchunksize;
	}

	n = dev_read(p->fd, buf, n);
	return n + sizeof(PropertyPacketHdr);
}

#else

int mux_poll_packet(Peer *p, PropertyPacketHdr **h, unsigned short timeout)
{
	int n;
	int ret;

	PacketQueue *rxq = (PacketQueue *) p->recvbuf;

	ret = queue_get(rxq, (void **) h, &n);
	if (ret == 0) return 0;


	// Belongs to release function:
	// queue_pop(rxq);

	return n;
}

#endif

#if 1

int mux_release_packet(Peer *p)
{
	return 0;
}

#else

int mux_release_packet(Peer *p)
{
	PacketQueue *rxq = (PacketQueue *) p->recvbuf;
	return queue_pop(rxq);
}
#endif

const
struct protocol_methods mux_methods = {
	.probe   = &mux_probe,
	.create  = &mux_create,
	.open    = &mux_open,
	.close   = &mux_close,
	.send    = &mux_send_packet,
	.recv    = &mux_poll_packet,
	.release = &mux_release_packet
};

