/* Pin I/O modes */

extern
struct debug {
	unsigned long lasttoken;
	int lastindex;
} g_debug;

enum {
	PINMODE_DIGITALIN,
	PINMODE_DIGITALOUT,
	PINMODE_ANALOGIN,
	PINMODE_ANALOGOUT,
	PINMODE_ALT
};
