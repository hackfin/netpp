#include <jpeglib.h>


void *jpeg_decoder_init(void);

int jpeg_readhdr(void *handle);

int jpeg_decode_frame(void *handle,
	unsigned char *dest,
	unsigned char *stream, unsigned int len);

void jpeg_decoder_exit(void *handle);

