#include <stdio.h>
#include <windows.h>
#include "driver.h"
#include <conio.h>

int input_poll(HANDLE fd, char *buf, int len)
{
	DWORD n;
	int i = 0;
#ifdef CONSOLE_INPUT
	while (len--) {
		if(!ReadConsole(fd, buf, 1, &n, NULL)) {
			return 0;
		}
		i += n;
	}
#else
	while (len--) {
		*buf++ = _getch();
		i++;
	}
#endif
	return i;
}


HANDLE input_init(int ch)
{
	HANDLE fd = 0;
#ifdef CONSOLE_INPUT
	DWORD fdm;
	fd = GetStdHandle(STD_INPUT_HANDLE);
	GetConsoleMode(fd, &fdm);
	fdm |= ENABLE_WINDOW_INPUT;
	FlushConsoleInputBuffer(fd);
	SetConsoleMode(fd, fdm);
#endif
	return fd;
}


int input_exit(HANDLE fd)
{
	return 0;
}
