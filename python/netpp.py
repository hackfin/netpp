"""Simple python wrapper around the raw C module
These classes allow some kind of easier access.

A short example:

	import netpp
	dev = netpp.connect("TCP:localhost:2010")
	root = dev.sync()
	root.Enable.set(1)
	

# (c) 2009 section5 <hackfin@section5.ch>
"""

import sys
sys.path.append("../Debug")

import device
import pickle
import os
import collections

Types = device.Types
Flags = device.Flags
ConnectError = device.ConnectError

# Default cache prefix
try:
	CACHE_PREFIX = os.environ["HOME"] + "/.netpp/cache/"
except:
	CACHE_PREFIX = None

# Special reserved token
try:
	TOKEN_CHECKSUM = device.ReservedTokens["CHECKSUM"]
	TOKEN_NPROPS = device.ReservedTokens["NPROPS"]
except:
	sys.stderr.write("You need to upgrade your netpp device module.\n")
	sys.exit(-1)

class Property:
	"""The property wrapper class"""

	def __init__(self, token, name, tp = 0, fl = None):
		self._token = token
		self._children = collections.OrderedDict()
		self._name = name
		# Cache for type and flags:
		self._type = tp
		self._flags = fl
		self._debug = False

	def token(self):
		"Retrieve raw token value"
		return self._token.value

	def name(self):
		"Retrieve the property's name"
		return self._name

	def get(self, val = None):	
		"""Get property value from peer and return it. If 'val' is specifed,
modify this reference and return the netpp return code. If not given,
a NEW property reference is returned."""
		if val:
			return self._token.get(val)
		else:
			return self._token.get()

	def set(self, value):	
		"Set property value on peer"
		return self._token.set(value, self._type)

	def type(self):
		return self._type

	def flags(self):
		return self._flags

	def __repr__(self):
		return "<Property: '%s' %s>" % (self._name, repr(self._token))

	def __str__(self):
		return "<Property: '%s'>" % self._name

	def __lt__(self, other):
		return self._token < other._token

	def prettyprint(self, maxdepth = 5, depth = 0):
		"Print out property tree. Debugging only"
		pre = depth * "  "
		print pre, "'%s' : [%08x]" % (self._name, self._token.value)
		d = self._children
		if depth >= maxdepth:
			return
		for i in d.values():
			i.prettyprint(maxdepth, depth + 1)

	def children(self):
		c = self._children.values()
		return c

	def delete(self):
		p = self.parent()
		del p._children[self.name()]

	def find(self, tval):
		for i in self._children.values():
			t = i.token()
			if tval == t:
				return i
			p = i.find(tval)
			if p:
				return p

		return None

	def reference(self, p):
		i = p.get()
		return self.find(i)

	def parent(self):
		return self._parent
	
	def getHidden(self, name):
		s = "." + name
		t = self._token.getitem(s)
		return Property(t, name)

	def update(self):
		c = self._token.child()
		while c:
			n = c.name()
			if not self._children.has_key(n):
				p = Property(c, n)
				p._parent = self
				p.scan(True)
				if self._debug:
					sys.stderr.write("Update: %s\n" % p.children())
				self._children[n] = p
			c = c.next()

	def scan(self, include_type = False, recursive = True):
		"""Scan name space on peer. This only needs to be done when properties
have changed."""
		if include_type:
			self._type, self._flags = self._token.type()
		c = self._token.child()
		self._children = collections.OrderedDict()
		while c:
			n = c.name()
			if self._debug:
				sys.stderr.write("Query prop %s\n" % n)
			p = Property(c, n)
			p._parent = self
			if recursive:
				p.scan(include_type)
			self._children[n] = p
			c = c.next()


	def map(self):
		d = collections.OrderedDict()
		for k in self._children.keys():
			p = self._children[k]
			d[k] = p.map()
			
		return (self._token.value, self._type, self._flags, d)

	def desc(self):
		"Return description dictionary from property"
		d = collections.OrderedDict()
		d[self._name] = self.map()
		return d

	def __nonzero__(self):
		return True	

	def __dir__(self):
		return self.children()

	def __getattr__(self, name):
		if self.__dict__['_children']:
			return self._children[name]
		else:
			return self.getHidden(name)

	def __len__(self):
		s = ".Size"
		t = self._token.getitem(s)
		return t.get()

	def __getitem__(self, n):
		s = ".[%d]" % n
		t = self._token.getitem(s)
		p = Property(t, self._name)
		p.scan(True)
		return p

class Hub:
	"""A Hub is a 'port bay'. If a device is docked somewhere into an
abstract interface, it will be shown in the root node tree returned by
the scan() method"""
	def __init__(self, d):
		self.device = d

	def scan(self, name = "PortRoot"):
		t = self.device.getToken("")
		root = Property(t, name)
		root.scan()
		return root
		
class Device:
	"""A device wrapper class.
It handles the automatic querying of the
remote device properties as well as propertly list caching and
synchronization.  The cache file is stored in the current working
directory under the name stored in `self.cachename`
"""

	def __init__(self, d):
		self.device = d
		# Get root node
		t = d.getToken("")
		self.name = t.name()
		if CACHE_PREFIX != None:
			try:
				os.stat(CACHE_PREFIX)
				self.prefix = CACHE_PREFIX
			except:
				print CACHE_PREFIX + " not found, using PWD for storage"
				self.prefix = ".cache_" 
		else:
				self.prefix = ".cache_"

		self.cachename = self.name + ".pyp"
		self.checksum = 0

	def scan(self, include_type = False):
		"Scan Device property list explicitely. See also sync()"

		t = self.device.getToken(TOKEN_NPROPS)
		self.num_properties = t.get()

		t = self.device.getToken("") # This function returns a typed token
		p = Property(t, self.name)
		p.scan(include_type)
		return p

	def buildnode(self, name, node):
		t = self.device.getToken(node[0]) # Builds a untyped token
		p = Property(t, name, node[1], node[2])
		children = node[3]
		for k in children.keys():
			d = self.buildnode(k, children[k])
			p._children[k] = d
			d._parent = p
		
		return p

	def build(self, root):
		"Build property tree from pickled cache"
		k = root.keys()[0]
		return self.buildnode(k, root[k])

	def loadcache(self):
		try:
			f = open(self.prefix + self.cachename, "r")
			(p, self.checksum) = pickle.load(f)
			root = self.build(p)
			f.close()
		except IOError:
			return None
		return root
	
	def pickle(self, root):
		"Store property tree in cache"
		fname = self.prefix + self.cachename
		try:
			f = open(fname, "w")
			properties = (root.desc(), self.checksum)
			pickle.dump(properties, f)
			f.close()
		except IOError:
			sys.stderr.write("Unable to write to %s\n" % fname)


	def sync(self, force = False, include_type = True):
		"""Sync device property list. If a cache file is found, it is used.
Otherwise, a scan() is executed and the results pickled in the cache."""

		root = self.loadcache()
		if not root or force:
			print "Scanning device..."
			root = self.scan(include_type)
			t = self.device.getToken(TOKEN_CHECKSUM)
			try:
				self.checksum = t.get()
				self.pickle(root)
				print "done."
			except TypeError:
				print "Proxied device, no checksum"
			except SystemError:
				print "No checksum"
		else:
			# Do the checksum test. If properties have changed, the
			# Checksum has, too.
			t = self.device.getToken(TOKEN_CHECKSUM)
			try:
				chk = t.get();
				if chk != self.checksum:
					sys.stderr.write("Device properties have changed, rescanning...\n")
					root = self.scan(include_type)
					self.checksum = chk
					self.pickle(root)
					print "done."
			except TypeError:
				print "Proxied device, no checksum"
			except SystemError:
				print "Failure to sync, maybe remove cache file ('*.pyp')"

		# Store reference so we can not crash if associated device
		# is deleted:
		root._device = self.device
		return root

def connect(dest):
	return Device(device.open(dest))

def localports():
	"Returns a dummy property containing the local port hub structure"
	local = Hub(device.open())
	return local.scan()

def Buffer(size):
	"A netpp buffer type of given size"
	return device.buffer(size)

if __name__ == '__main__':
	dev = connect("localhost")

	print 70 * '#'

	r = dev.sync()
	r.prettyprint()

	print 70 * '-'

# Access variables the easy way:
	x = r.Stream.X

	v = x.get()
	print v
	x.set(v + 1)
	print x.get()

	i = r.Integer1

	# Get possible values from Integer 1 property:
	print i.Min.get()
	print i.Max.get()

