/* File : example.i */
%include "std_string.i"
%include "typemaps.i"
%javaconst(1);
%include "enumtypeunsafe.swg"


%module jnetpp

%{
#include "../include/devlib_types.h"
#include "../include/netppplus.h"
#include "roothub.h"
%}

typedef unsigned short uint16_t;
typedef unsigned long  uint32_t;
typedef          long   int32_t;

const char *dcGetErrorString(int error);

%ignore getName();
%ignore operator int();
%ignore operator float();
%ignore operator ==;
%ignore Value(long i);
%rename(asLong) operator long();
%rename(asBool) operator bool();
%rename(asString) operator const char * const();
%rename(device) operator struct DCDevice*();

%extend Value {
    Value(std::string str) {
        Value *v = new Value(str.c_str(), str.length());
        printf("Construct string from '%s'\n", str.c_str());
        return v;
    };
    std::string String() {
        std::string s((const char *) $self->value.p);
        return s;
    }
};

%extend Property {
    std::string Property::name() {
        std::string s;
        $self->getName(s);
        return s;
    }
    int setValue(bool b) {
        Value v(b); return $self->setValue(v);
    }
    int setValue(int i) {
        Value v(i); return $self->setValue(v);
    }
    int setValue(float f) {
        Value v(f); return $self->setValue(v);
    }
    int setValue(short s) {
        Value v(s); return $self->setValue(v);
    }
    Value *getValue(void) {
        Value *v = new Value(); $self->getValue(*v);
        return v;
    }
};

%exception getChild {
    try {
        $action
    } catch (int &e) {
        jclass cls = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(cls, dcGetErrorString(e));
        return $null;
    }
}

/* Let's just grab the original header file here */
%include "../include/devlib_error.h"
%include "../include/devlib_types.h"
%include "../include/netppplus.h"
%include "roothub.h"



