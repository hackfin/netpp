/* Wrapper for different protocols
 *
 *
 */

#include "devlib_error.h"
#include "property_protocol.h"
// Debugging:

#ifdef DEBUG

const char *debug_pktype(PropertyPacketHdr *h)
{
	const char *type;
	static char s[256];

	switch (ntohs(h->type)) {
		case PP_STATUS:
			type = "STQ";
			break;
		case PP_GETROOT:
			type = "ROO";
			break;
		case PP_SELECT:
			type = "SEL";
			break;
		case PP_NAME | PP_FLAG_PROTO:
			type = "NMQ";
			break;
		case PP_NAME:
			type = "NAM";
			break;
		case PP_BULK:
			type = "BLK";
			break;
		case PP_GET:
			type = "GET";
			break;
		case PP_FLAG_PROTO | PP_GET:
		case PP_FLAG_PROTO | PP_SET:
			type = "TYP";
			break;
		case PP_SET:
			type = "SET";
			break;
		default:
			type = "invalid";
	}
	sprintf(s, "%s %08x", type, ntohl(h->sub.status.token));
	return s;
}

#endif

// Workaround to disable LWIP macros
#if LWIP_COMPAT_SOCKETS
#undef send
#undef recv
#endif

int send_packet(Peer *p, PropertyPacketHdr *h, const char *data, size_t len)
{
#ifdef DEBUG
	netpp_log(DCLOG_VERBOSE, "Send packet type %s", debug_pktype(h));
#endif
	return p->hub->hub.methods->send(p, h, data, len);
}

int poll_packet(Peer *p, PropertyPacketHdr **h, int mode)
{
	unsigned short timeout;
	int error;

	switch (mode) {
		case POLL_WAIT:
			timeout = p->recvtimeout_wait;
			break;
		case POLL_CHECK:
			timeout = p->recvtimeout_check;
			break;
		default:
			timeout = p->recvtimeout_wait;
	}

	error = p->hub->hub.methods->recv(p, h, timeout);
#ifdef DEBUG
	if (error > 0)
		netpp_log(DCLOG_VERBOSE, "Got packet type %s", debug_pktype(*h));
#endif
	return error;
}

int release_packet(Peer *p)
{
	int error = 0;
	int (*releasefunc)(Peer *p);

	releasefunc = p->hub->hub.methods->release;
	if (releasefunc) error = releasefunc(p);
	return error;
}
