import sys
sys.path.append("../../python")
sys.path.append("../../Debug")

import struct
import time

import netpp

from lut import *


GAMMA = 0.2

def convert_thresh(v):
	if v > 0x8000:
		return 255, 255, 255
	else:
		return 0, 0, 0

def convert_8(v):
	return v >> 8, v >> 8, v >> 8


def gen_markers(markers):
	buf = ""
	for i in markers:
		buf += struct.pack("<HH", i[0], i[1])

	return buffer(buf)

def disp_config(disp, x, y):
	disp.Stream.Stop.set(1)
	disp.Stream.BitsPerPixel.set(16)
	disp.Mode.set(disp.Mode.Indexed16.get())
	disp.Stream.X.set(x)
	disp.Stream.Y.set(y)

def write_frames(r):
	x = r.Stream.X.get()
	y = r.Stream.Y.get()
	c = 0
	buf = ""
	for i in range(y):
		for j in range(x):
			buf += chr((j+c) & 0xff)
			buf += chr((i+c) & 0xff)

	print "Buffer length", len(buf)

	markers = [ (10, 10), (128, 20), (41, 77), (200, 18) ]


	for i in range(15):
		o = 16 + i * 16
		markers[1] = (o, o)
		m = gen_markers(markers[: 1 + i / 2 % 3])
		r.Marker.set(m)

		r.Stream.Data.set(buffer(buf))
		time.sleep(0.1)

	# clear markers:
	m = buffer("")
	r.Marker.set(m)
	r.Stream.Data.set(buffer(buf))
	

def run_demo(r, pr):

	print "Use built in table"
	default = r.ColorTable.Preset.Pseudocolor.get()
	r.ColorTable.Preset.set(default)
	r.ColorTable.Gamma.set(0.7)
	r.ColorTable.Update.set(1) # Update LUT

	write_frames(r)

	r.Stream.Stop.set(1)
	r.Stream.Start.set(1)

	print "Generate LUT..."
	lut = gen_lut16(convert_gamma, GAMMA)

	default = r.ColorTable.Preset.Custom.get()
	r.ColorTable.Preset.set(default)
	r.ColorTable.CustomData.set(lut)

	write_frames(r)



def create_range_data(r):
	x = r.Stream.X.get()
	y = r.Stream.Y.get()
	c = 0
	buf = ""
	for i in range(y / 2):
		for j in range(x):
			buf += struct.pack("<H", c)
			c += 1
			c &= 0xffff

	c = 0x8000
	for i in range(y / 2):
		for j in range(x):
			buf += struct.pack("<H", c)
			c += 1
			c &= 0xffff

	print "Buffer length", len(buf)
	return buf


def convert_custom(v):
	if v < 0x100:
		hwb = (0, v, 255-v )
	elif v > 0x8000: # negative?
		h = 0x800 - (v >> 5)
		b = (0x10000 - v) >> 8
		hwb = (h, 127 - b / 2, b)
	else:
		v = int(0x500 * (v / 32767.0) ** 0.4)
		hwb = (v, 0, 0)
		
	return hwb2rgb(hwb)

def convert_one(v, arg):
	return convert_custom(v)

def exp_lut(r):
	lut = gen_lut16(convert_one, 1.0)

	default = r.ColorTable.Preset.Custom.get()
	r.ColorTable.Preset.set(default)
	r.ColorTable.CustomData.set(lut)
	buf = create_range_data(r)
	r.Stream.Data.set(buffer(buf))

# Open first screen:
d0 = netpp.connect("localhost:2008")
r0 = d0.sync()
d1 = netpp.connect("localhost:2008")
r1 = d1.sync()

disp_config(r0, 256, 256)

r0.Stream.Start.set(1)
exp_lut(r0)

run_demo(r0, 0)
r0.Stream.Stop.set(1)
disp_config(r1, 256, 128)
r1.Stream.Start.set(1)
run_demo(r1, 0)
r0.Stream.Stop.set(1)
r1.Stream.Stop.set(1)



