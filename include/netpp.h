/** \file netpp.h
 *
 * This is the main header to include. It makes sure, that all the
 * correct headers from the device control library are included.
 */

#include "devlib.h"
#include "devlib_types.h"
#include "devlib_error.h"

