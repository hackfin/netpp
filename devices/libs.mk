# Generic rules to make master / slave / transport libs
#
#

# All common objects for the slave library
#
COMMONSRCDIR = $(NETPP)/common
PROTOSRCDIR = $(NETPP)/src

Q ?= @

COMMONSRCS = logging.c dlistpool.c
# Pure master code:
ifdef IS_MASTER
# Local (non-RPC) API?
ifdef CONFIG_LOCALAPI
CFLAGS += -DCONFIG_LOCALHUB
COMMONSRCS += prophandler.c local_api.c 
else
COMMONSRCS += propapi.c 
PROTOSRCS   = packet.c wrapper.c
endif
# Slave build:
else
CFLAGS += -DCONFIG_SLAVE
ifdef DEFAULT_DEVICE_INDEX
CFLAGS += -DDEFAULT_DEVICE_INDEX=$(DEFAULT_DEVICE_INDEX)
endif
# Important: Proxy requires the property API
ifdef PROXY
COMMONSRCS += propapi.c
else
COMMONSRCS += local_api.c
endif
COMMONSRCS += prophandler.c 
PROTOSRCS   = packet.c wrapper.c
endif

ifndef PLATFORM
PLATFORM=$(shell "uname")
endif

RELEASE_OPT_FLAGS ?= -O

# TODO: Some day we turn all the hacky config variables into proper
# menuconfigured CONFIG_XXX items.

ifdef USB
	CFLAGS += -DCONFIG_USBFIFO
	PROTOSRCS += usb_fifo.c
endif

ifdef UDP
	CFLAGS += -DCONFIG_UDP
	PROTOSRCS += udp.c
	NETWORKING = yes
endif

ifdef TCP
	CFLAGS += -DCONFIG_TCP
	PROTOSRCS += tcp.c
	NETWORKING = yes
endif

ifdef DYNPROPERTIES
	PROTOSRCS += dynprops.c
	CFLAGS += -DALLOW_DYNAMIC_PROPERTIES
endif

ifdef NETWORKING
	PROTOSRCS += ipcommon.c
endif

PROTOSRCS += ports.c
# INI support for configureable proxies:
PROTOSRCS += conffile.c
	
# Default relative path:
NETPP ?= ../..

INCLUDES += -I$(NETPP)/include
INCLUDES += -I$(NETPP)/src
INCLUDES += -I.

CFLAGS += $(INCLUDES) $(OPTIMIZATIONS) 
CFLAGS += -Wall -MD
CFLAGS += -DVERSION_STRING=\"$(VERSION)-$(LIBNAME)\"
# Use CRC16 by default:
ifndef WITHOUT_CRC
CFLAGS += -DUSE_CRC16
COMMONSRCS += crc16.c
endif

# Make guesses depending on PLATFORM= specification
include $(NETPP)/devices/platform.mk

ifndef CUSTOM_ERRHANDLER
	COMMONSRCS += errorhandler.c
endif

# Special hook:
ifeq ($(ARCH),bfin-uclinux-)
		LDFLAGS = -Wl,-elf2flt
endif

ifdef RELEASE
	CONFIG_PREFIX = Release
else
	CONFIG_PREFIX = Debug
endif

CONFIG = $(ARCH)$(CONFIG_PREFIX)

ifndef RELEASE
	CFLAGS += $(DEBUGOPTS)
else
	CFLAGS += $(RELEASE_OPT_FLAGS)
endif

ifdef DEBUG
	CFLAGS += -DDEBUG
endif

ifdef CUSTOM_LIBRARY
LDFLAGS += -L$(CONFIG) -l$(CUSTOM_LIBRARY)
else
LDFLAGS += -L$(CONFIG) -l$(LIBNAME)
endif

# Windows DLL stuff:
DLL = $(CONFIG)/$(LIBNAME).dll
LIB = $(CONFIG)/$(LIBNAME).lib     # The import library
DEFFILE = $(LIBNAME).def

# Linux static lib:
STATICLIB = $(CONFIG)/lib$(LIBNAME).a

ifeq (CYGWIN_NT-5.1,$(PLATFORM))
	LDFLAGS += -lwsock32
endif

ifeq (mingw32,$(PLATFORM))
	LDFLAGS += -lws2_32
	# Hack to avoid import symbols
	CFLAGS += -DDLL_EXPORTS
	DLLDEPS += $(LDFLAGS)
	# Linux cross compiler:
	DLLTOOL = $(ARCH)dlltool
	DLLWRAP = $(ARCH)dllwrap
	DUTIES = $(DLL) $(DEFFILE)
else
ifdef UNIXDEVICE
	CFLAGS += -DCONFIG_DEVICE
	PROTOSRCS += unixdevice.c
endif
ifdef TCP
	ifdef PROXY
		CFLAGS += -DCONFIG_PROXY
		PROTOSRCS += proxy_generic.c
		PROTOSRCS += proxy_tcp.c
	endif
endif
ifdef TUNNEL
	CFLAGS += -DCONFIG_DEVICEMUX
	CSRCS += muxer.c
endif
endif

ifeq (Linux,$(PLATFORM))
	CFLAGS += -fPIC
endif

SOURCE = $(shell [ -e $(NETPP)/src ] || echo nosrcdist)

# Transport protocol objects:
PROTOOBJS = $(PROTOSRCS:%.c=$(CONFIG)/%.o)
# Extra user objects (handler, property list):
OBJS = $(CSRCS:%.c=$(CONFIG)/%.o)
# Common slave objects:
LIBOBJS = $(COMMONSRCS:%.c=$(CONFIG)/%.o) $(PROTOOBJS) $(OBJS)

$(CONFIG)/%.o: $(COMMONSRCDIR)/%.c
	$(Q)$(CC) -o $@ -c $< $(CFLAGS) 

$(CONFIG)/%.o: $(PROTOSRCDIR)/%.c
	$(Q)$(CC) -o $@ -c $< $(CFLAGS)

$(CONFIG)/%.o: %.c
	$(Q)$(CC) -o $@ -c $< $(CFLAGS)

default: all

include $(NETPP)/xml/prophandler.mk

ifneq ($(SOURCE),nosrcdist)
	MAYBE_STATICLIB = $(STATICLIB)
	MAYBE_LIBCLEAN = maybe_libclean
endif

$(STATICLIB): $(CONFIG) $(LIBOBJS)
	@$(AR) rsv $@ $(LIBOBJS)

show:
	@echo ---------------------------------------------------------------
	@echo Compiling for platform \'$(PLATFORM)\'
	@echo ---------------------------------------------------------------
	@echo SOURCE: $(SOURCE)
	@echo LIB: $(MAYBE_STATICLIB)
	@echo LIBOBJS: $(LIBOBJS)
	@echo DUTIES: $(DUTIES)

lib: $(MAYBE_STATICLIB) $(DUTIES)

.PHONY: lib

$(DEFFILE): $(STATICLIB)
	$(DLLTOOL) $(STATICLIB) --output-def $(DEFFILE) -k

# Make mingw/cygwin compatible DLL for internal use.
$(DLL): $(CONFIG) $(LIBOBJS) $(DEFFILE)
	@echo ">> creating $(DLL)..."
	$(DLLWRAP) -k -o $(DLL) --target=i386-mingw32 \
	    --dllname $(LIBNAME).dll --def $(DEFFILE) \
		$(LIBOBJS) \
		$(DLLDEPS) 
	@echo ">> creating MinGW import library $(LIB)"
	@$(DLLTOOL) --dllname $(LIBNAME).dll -a --def $(DEFFILE) --output-lib $(LIB) 

dll: $(DLL)
	echo $(DLL)

# Make DLL directory, if it does not exist
$(CONFIG):
	[ -e $(CONFIG) ] || mkdir $(CONFIG)

maybe_libclean:
	rm -fr $(ARCH)Debug/ $(ARCH)Release/

libclean: $(MAYBE_LIBCLEAN)

clean:: libclean
	rm -f proplist.c register.h
	rm -f *.d
	rm -fr __pycache__

TARGETDIR = $(DESTDIR)/$(LIBNAME)

install:: $(INSTALL_TASKS)
	# Install common stuff:
	install -d $(TARGETDIR)/$(CONFIG)
	install -m 644 $(STATICLIB) $(TARGETDIR)/$(CONFIG)
	install -m 644 Makefile $(TARGETDIR)
