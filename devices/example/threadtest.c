/* Multithreading test with netpp
 *
 * Case, where several threads need to poll the status of a remote device.
 * Not a recommended solution.
 *
 */

#include <pthread.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "devlib.h"
#include "devlib_types.h"
#include "devlib_error.h"


struct tokens {
	TOKEN data;
	TOKEN count;
} g_tokens;

#define MEMBER(m)  offsetof(struct tokens, m) / sizeof(TOKEN)

static struct _tdesc {
	unsigned short off;
	char *name;
} init_tokens[] = 
{
	{ MEMBER(data), "Stream.HandledData" },
	{ MEMBER(count), "Stream.Count" },
	{ 0 }
};

void handle_error(int error)
{
	printf("Error: %d\n", error);
	// printf("Error: %s\n", device_strerr(error));
}


int token_init(DEVICE d)
{
	int error;
	struct _tdesc *t = init_tokens;
	
	while (t->name) {
		error = dcProperty_ParseName(d, t->name, &( (TOKEN *) &g_tokens)[t->off]);
		if (error < 0) {
			handle_error(error);
			return 0;
		}
		t++;
	}
	return 1;
}

static short g_sem;

void *poll_thread(void *arg)
{
	DEVICE d = (DEVICE) arg;
	DCValue val;

	val.len = 0x80000;
	val.value.p = malloc(val.len);

	while (g_sem) {
		dcDevice_GetProperty(d, g_tokens.data, &val);
	}

	free(val.value.p);
	return 0;
}


int thread_test(DEVICE d)
{
	pthread_t thread;
	void *ret;
	DCValue val;
	int error;

	val.value.i = 0; val.type = DC_INT;
	dcDevice_SetProperty(d, g_tokens.count, &val);

	g_sem = 1;

	error = pthread_create(&thread, NULL, &poll_thread, d);

	while (1) {
		dcDevice_GetProperty(d, g_tokens.count, &val);
		printf("Current Frame: %d\n", val.value.i);
		usleep(20000);
		if (val.value.i > 2000) break;
	}
		
	g_sem = 0;

	printf("Waiting for thread to end...\n");
	pthread_join(thread, &ret);

	return 0;
}


int main(int argc, char **argv)
{
	int error;

	DEVICE d;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <netpp target>\n", argv[0]);
		return -1;
	}
	
	error = dcDeviceOpen(argv[1], &d);
	if (error < 0) {
		handle_error(error);
		return -1;
	}

	if (token_init(d)) {
		thread_test(d);
	} else {
		printf("Failed to initialize Property tokens. Wrong backend?\n");
	}

	dcDeviceClose(d);
	return error;
}
