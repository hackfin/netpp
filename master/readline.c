#include "shell.h"
#include <stdio.h>
#include <editline.h>

static struct {
	int empty;
	char buf[256];
	unsigned short pos;
} s_readline_context = {
	.empty = 1
};

int copy_string(char *dest, const char *src, int n)
{
	while (n--) {
		*dest++ = *src;
		if (*src == '\0') { return 0; }
		src++;
	}
	return 1;
}

int input_readline(const char *prompt)
{
	const char *rl;
	int ret;

	rl = readline(prompt);
	if (rl == NULL) {
		return -1;
	}
	ret = copy_string(s_readline_context.buf, rl,
		sizeof(s_readline_context.buf));
	if (ret) {
		printf("OVERFLOW\n"); return -1;
	}
	add_history(rl);
	s_readline_context.pos = 0;
	s_readline_context.empty = 0;
	return 0;
}

int input_poll(int fd, char *buf, int n)
{
	int ret;

	if (!s_readline_context.empty) {
		ret = copy_string(buf,
			&s_readline_context.buf[s_readline_context.pos], n);
		if (ret == 0) {
			s_readline_context.empty = 1;
			s_readline_context.pos = 0;
		} else {
			s_readline_context.pos += n;
		}
		return n;
	}
	return 0;
}


int input_init(int ch)
{
	return 0;
}

int input_exit(int fd)
{
	return 0;
}
