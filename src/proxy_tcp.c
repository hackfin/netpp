/**
 *
 * TCP proxy for demonstration. Note: This is 'master side' code, like
 * the tcp layer. The proxy slave must be implemented on the slave side
 * according to the example proxy code in devices/proxy/.
 *
 * (c) 2014 Martin Strubel <hackfin@section5.ch>
 *
 */

#include <stdio.h>
#include "proxy.h"
#ifndef LWIP_COMPAT_SOCKETS
#include <errno.h>
#endif

// DO NOT CHANGE. Changing this, makes the protocol incompatible to
// previous version
#ifndef TINYCPU
#define MAX_PACKET_SIZE   65536
#endif

// Default timeout to check for a request (outside protocol) in ms
#define TIMEOUT_CHECK         1

// Default timeout to wait for a response inside protocol in ms
#define TIMEOUT_WAIT       5000

#define HDR_SIZE sizeof(PropertyPacketHdr)
#define MAX_PAYLOAD_SIZE (MAX_PACKET_SIZE - HDR_SIZE)

#ifdef DEBUG
#define DEB(x) x
#else
#define DEB(x)
#endif


struct protocol_methods prx_methods;
int tcp_sock_init(SOCKET *sock, SockAddrIn *sin);
int tcp_poll_packet(Peer *p, PropertyPacketHdr **h, unsigned short timeout);
int tcp_send_packet(Peer *peer, PropertyPacketHdr *h,
		const char *data, size_t len);

static
int peer_init(Peer *p)
{
	p->sendbuf = buf_alloc(MAX_PACKET_SIZE * 2);
	if (!p->sendbuf) return DCERR_MALLOC;
	p->recvbuf = (char *) &p->sendbuf[MAX_PACKET_SIZE];

	p->recvtimeout_check =  TIMEOUT_CHECK;
	p->recvtimeout_wait  =  TIMEOUT_WAIT;

	p->packetsize = MAX_PACKET_SIZE - HDR_SIZE;
	return 0;
}

// Only call from server (slave). Never from master!

int prx_sock_init(SOCKET *sock, SockAddrIn *sin)
{
	int error = 0;
	int option;
	SOCKET s;

	s = (SOCKET) socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); 
	if (s == INVALID_SOCKET) { 
		netpp_log(DCLOG_ERROR, "Error initializing PRX socket: %s",
#ifdef LWIP_COMPAT_SOCKETS
			"FIXME"
#else
			strerror(errno)
#endif
		);
		return DCERR_COMM_INIT;
	}

	option = 1;
	setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *) &option,
		sizeof(option));

	sin->sin_addr.s_addr    = INADDR_ANY;
	sin->sin_family         = AF_INET;

	*sock = s;

	return error;
}

////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////

static
int servercontext_init(ServerContext *serv)
{
	// Reset user count
	serv->flags = F_SERV_TCP | F_SERV_PROXY;
	serv->usercount = 0;
	serv->peer_init = peer_init;
	serv->slave_handler = slave_handler;
 	serv->hub = Hub(GET_PORTROOT(), PRX_SERVER_NAME, &prx_methods);
	if (!IS_VALIDTOKEN(serv->hub)) {
		return DCERR_MALLOC;
	}
	// Reserve device list:
	serv->peerdevs = Hub(GET_PORTROOT(), PRX_PEERS_NAME, 0);
	// Append to root node for debugging:
	portbay_append(GET_PORTROOT(), serv->peerdevs);

	serv->cleanup = NULL;

	serv->devices =
		portbay_fromtoken(desc_fromtoken(serv->peerdevs)->hub.ports);
	return 0;
}


int prx_server_init(int listen_port, ServerContext *serv)
{
	int error;
	int flag = 1;
	SockAddrIn sin;
	int sock;

	error = servercontext_init(serv);
	if (error < 0) return error;

	// Set up socket for server port
	sin.sin_port = htons(listen_port);
	error = tcp_sock_init(&sock, &sin);
	if (error < 0) return DCERR_COMM_INIT;

	setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(int));

	error = bind(sock, (SockAddr *) &sin, sizeof(SockAddr));
	if (error < 0) return DCERR_COMM_INIT;

	error = listen(sock, 2);
	if (error < 0) return DCERR_COMM_INIT;

	serv->listener = sock;

	return add_server_entry(serv, listen_port);
}


int prx_slave_mainloop(ServerContext *serv)
{
	int error = 0;
	while (1) {
		// First, we serve users
		net_serve_users(serv);
		error = tcp_listen_incoming(serv);
		if (error) break;
	}
	return error;
}

int prx_server_exit(ServerContext *serv)
{
	int error;
	error = shutdown(serv->listener, SHUT_RDWR);
	close(serv->listener);

	portdesc_free(serv->hub);
	portdesc_free(serv->peerdevs);
	
	return error;
}

////////////////////////////////////////////////////////////////////////////
// TCP port methods

/** Open proxy.
 *
 * This can be called in several ways:
 *
 * #- The proxy way: 'PRX:localhost:2014': This opens the proxy directly.
 * #- The endpoint way: 'PRX:localhost:2014+USB:usb0': Opens the proxy and
 *    the specified end point.
 */

int prx_open(RemoteDevice *rd, int portindex)
{
	int error;

	// The `rd` remote device is the device handle that the caller receives.
	// Our local Device d is the actual proxy. When we open the proxy,
	// the proxy's GetRoot() does the redirection to the proxied device.
	char *name;
	name = getname(&rd->peer, portindex);

	// We're a TCP proxy:
	error = proxy_open(rd, "TCP:", name);

	return error;
}

int prx_close(DEVICE d)
{
	proxy_close(d);
	netpp_tcp_close(d);
	return 0;
}

int prx_create(PortToken hub, const char *name, PortToken *out)
{
	return net_create(hub, name, out);
}

int proxy_iterate(PortBay *bay, DEVICE d)
{
	PortToken dev;
	char name[32];
	DCValue val;
	int error;

	val.len = sizeof(name);

	// '0' initiates scan:
	val.value.i = 0; val.type = DC_INT;
	error = dcDevice_SetProperty(d, THIS_DEVICE_TOKEN(d, scan), &val);
	while (error == 0) {
		val.type = DC_STRING;
		val.value.p = name; //  val.len = sizeof(name);
		error = dcDevice_GetProperty(d, THIS_DEVICE_TOKEN(d, peerid), &val);
		if (error > 0) 
			dev = PortNode(bay, (char *) val.value.p, DC_DEVICE);
		else
			dev = PortNode(bay, (char *) val.value.p, DC_PORT);
		portbay_append(bay, dev);
		// '1' iterates over list:
		val.value.i = 1; val.type = DC_INT;
		error = dcDevice_SetProperty(d, THIS_DEVICE_TOKEN(d, scan), &val);
	}
	return error;
}

/* This function finds registered peers on the proxy side and appends
 * them into a separate hub. This doesn't bother us, even while we are
 * changing the local port structures.
 */
int secondary_probe(const char *port, PortToken proxy)
{
	int error = 0;
	struct proxy_tokens tokens;

	DEVICE d;
	netpp_log(DCLOG_VERBOSE, "Probing proxy '%s'", port);
	error = dcDeviceOpen(port, &d);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Failed to open"); return error;
	}

	DEB(netpp_log(DCLOG_VERBOSE, "Token init.."));
	error = proxytokens_init(d, &tokens);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Probe: Failed to initialize tokens");
		return error;
	}


	PortToken proxyhub = hub_reuse(port, 0);
	if (proxyhub == TOKEN_INVALID) {
		return DCERR_EXCEED_POOL;
	}
	PortBay *ports = portbay_fromtoken(desc_fromtoken(proxyhub)->hub.ports);
	// Temporarily assign tokens:
	d->localdata = &tokens;

	// Clear port bay entries of the proxy:
	portbay_purge(desc_fromtoken(proxyhub)->hub.ports);

	error = proxy_iterate(ports, d);
	d->localdata = 0;
	if (error < 0) return error;

	portbay_append(GET_PORTROOT(), proxyhub);

	error = dcDeviceClose(d);

	return error;
}

#ifdef PROXY
// Proxy servers do not probe themselves
int prx_probe(DynPropertyDesc *hub)
{
	return 0;
}
#else

int prx_probe(DynPropertyDesc *hub)
{
	DynPropertyDesc *pd;
	PortBay *bay = portbay_fromtoken(hub->hub.ports);
	PortToken proxy;
	int error = 0;
	char port[64];

	// Note, we are called client-side.
	// Proxies are both client and server (Master/Slave).

	// This finds the available proxies. On return, they are stored
	// in the PRX hub port list.
	net_probe(hub);

	proxy = ITERATOR_FIRST(bay);

	// Walk through the proxies and probe each the secondary way:
 	while (ITERATOR_VALID(proxy) && error == 0) {
	 	pd = desc_fromtoken(proxy);
	 	snprintf(port, sizeof(port), "PRX:%s", pd->name);
	 	error = secondary_probe(port, proxy);
	 	proxy = ITERATOR_NEXT(bay, pd);
	}

	// Now after this, we have populated the local ports list with
	// proxy server entries plus possible proxy targets/endpoints:
	//
	// Examples:
	//
	// Proxy entry:
	//
	//   Child: (H) [80000002] 'PRX'
	//      Child: (P) [80030000] '192.168.1.11:2014'
	//      Child: (P) [80030001] '192.168.0.2:2014'
	//
	// Proxy target (endpoints):
	//
	//   Child: (H) [80000005] 'PRX:192.168.1.11:2014'
	//      Child: (P) [80060000] 'SWP:31'
	//   Child: (H) [80000004] 'PRX:192.168.0.2:2014'
	//      Child: (P) [80050000] 'SWP:31'

	return 0;
}

#endif

struct protocol_methods prx_methods = {
	.probe   = &prx_probe,
	.create  = &prx_create,
	.open    = &prx_open,
	.close   = &prx_close,
	.send    = &tcp_send_packet,
	.recv    = &tcp_poll_packet
};

