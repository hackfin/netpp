/* This contains the stubs for the local API to access local registers,
 * etc.
 *
 */

#include "netpp.h"
#include "slave.h"
#include <stdio.h>

TOKEN local_getroot(DEVICE d)
{
	// Note: we have no dynamic property support in here (yet)
	return DEVICE_TOKEN(1);
}

// 16 bit register table, little endian

static unsigned char g_reg[32] = {
	0xad, 0xde, 0xef, 0xbe,
	0xbb, 0xaa, 0xdd, 0xcc,
	0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00,
	0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff,
};

int device_read(RemoteDevice *d,
		uint32_t addr, unsigned char *buf, unsigned long size)
{
	netpp_log(DCLOG_VERBOSE, "Try to read %d bytes from addr %04x", size, addr);
	// Just dummy-read in little endian format:
	unsigned char *reg = &g_reg[(addr & 0xf) << 1];
	switch (size) {
		case 2:
			buf[1] = reg[1];
			printf("%02X ", reg[1]);
		case 1:
			buf[0] = reg[0];
			printf("%02X ", reg[0]);
			break;
		default:
			return DCERR_COMM_FRAME;
	}
	printf("\n");
	return 0;
}

int device_write(RemoteDevice *d,
		uint32_t addr, const unsigned char *buf,
		unsigned long size)
{
	netpp_log(DCLOG_VERBOSE, "Write %d bytes to addr %04x", size, addr);
	unsigned char *reg = &g_reg[(addr & 0xf) << 1];
	switch (size) {
		case 2:
			netpp_log(DCLOG_VERBOSE, "> %02X %02X", buf[0], buf[1]);
			reg[0] = buf[0]; reg[1] = buf[1];
			break;
		case 1:
			netpp_log(DCLOG_VERBOSE, "> %02X", buf[0]);
			reg[0] = buf[0];
			break;
		default:
			return DCERR_COMM_FRAME;
	}
	return 0;
}
