
#define NIL (PortDesc *) 0

#define PORTHANDLE void *

/** Port types
 */


typedef enum {
	DC_BAY,
	DC_HUB,
	DC_PORT
} PortType;

struct _portdesc;

// A list with start and end pointers
typedef struct {
	struct _portdesc *first, *last;
} PortBay;

/* Protocol methods
 */

struct protocol_methods {
	int (*probe)(PortyBay *self);
	int (*handler)(DEVICE d, int entrystate, TOKEN t, DCValue *val);
};

/** Port I/O methods 
 */

struct port_methods {
	int (*port_open)(PORTHANDLE h);
	int (*port_close)(PORTHANDLE h);
	int (*port_send)(PORTHANDLE h, const char *data, size_t len);
	int (*port_recv)(PORTHANDLE h, char *data, size_t len);
};


typedef struct _portdesc {
	const char   *name;
	PortType type;

	union {
		struct {
			PORTHANDLE h;
			PROPINDEX *props;
		} port;
		struct {
			struct protocol_methods *proto_methods;
			struct port_methods *port_methods;
			FPHubProbe probe;
			struct PortBay ports;
		} hub;

	} access;

	struct _portdesc *prev, *next;

} PortDesc;

void portbay_append(PortBay *b, PortDesc *d);
void portbay_remove(PortBay *b, PortDesc *d);
void portbay_free(PortBay *b);


