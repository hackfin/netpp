/** Helper macros to initialize property list
 *
 */

#ifdef C99_COMPLIANT
// No macros yet to define
#else

#define INIT_NONE { 0, 0 }

#define INIT_REG(which, _id, _lsb, _msb, _size)  \
	{ which.id = _id; which.lsb = _lsb; which.msb = _msb; \
	  which.size = _size; }

#define INIT_FUNC(which, _get, _set) \
	{ which.get = _get; which.set = _set; }
#define INIT_BUF(which, _ptr, _size) \
	{ which.p = _ptr; which.size = _size; }
#define INIT_STATIC(which, _val) \
	{ which = _val; }
#define INIT_VARP(which, _var) \
	{ which = &_var; }

#endif
