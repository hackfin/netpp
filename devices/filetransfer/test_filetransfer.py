#!/usr/bin/env python

import sys
import netpp
import pytest
import time
import os
import filecmp
import subprocess

data = """
Ballmer argues against Linux
Timing of customer e-mail notable

By TODD BISHOP
SEATTLE POST-INTELLIGENCER REPORTER

Microsoft Corp. Chief Executive Steve Ballmer reiterated the company's case against the open-source Linux operating system yesterday in an e-mail message to customers that was notable in part for its timing.

The 2,600-word message cited research reports and case studies to support Microsoft's longstanding assertion that Windows is a better choice than Linux in such areas as security, total ownership cost and indemnification from intellectual-property claims. It primarily reinforced themes that Microsoft has presented in a campaign it calls "Get the Facts on Windows and Linux."

The message came as Microsoft partner Dell Inc. announced an expanded relationship with Linux vendor Novell to broaden Dell's offerings of the open-source operating system on computer servers.

The timing was also significant because Microsoft plans to stop supporting an older version of its Windows operating system for servers, Windows NT Server 4.0, at the end of this year. Ballmer's message could help sway an executive considering upgrading from NT to either Linux or a newer version of Windows, said Jupiter Research analyst Joe Wilcox.

"It's in Microsoft's interest to get the message out," Wilcox said.

Open-source advocates and others have criticized Microsoft's "Get the Facts" campaign for relying heavily on research that the company funds. Ballmer's e-mail noted that some of the studies were commissioned by the company, but others weren't. Regardless of the funding, he wrote, Microsoft wanted "truly independent, factual information."

The message also addressed the proprietary Unix operating system, but the main focus was Linux, which is available freely over the Internet.

Among other things, Ballmer commented on the popular belief that Linux is more secure because many different developers around the world view and work collaboratively on the software code. "While this has some validity, it is not necessarily the best way to develop secure software," he wrote. "We believe in the effectiveness of a structured software engineering process that includes a deep focus on quality, technology advances and vigorous testing to make software more secure."

Ballmer and Bill Gates issue such e-mails periodically. Past subjects have included spam and security.

Yesterday's message appeared, in part, to be an effort by Microsoft to define the issues for people weighing Windows against open-source, said Michael Cherry, an analyst at Directions on Microsoft, a Kirkland-based research firm. "They're framing the debate," he said, "and I don't think that's a bad thing for them to do."
"""

def test_file():
	buffer_data = buffer(data)
	root.File.Name.set("/tmp/test.dat")
	root.File.Open.set(1)
	root.File.Data.set(buffer_data)
	root.File.Close.set(1)

	root.File.Open.set(0)
	content = buffer("")
	while 1:
		chunk = root.File.Data.get()
		l = len(chunk)
		print("Got %d bytes" % l)
		if not l: break
		content += chunk

	print("File size:", root.File.Size.get())
	root.File.Close.set(1)
	if content != buffer_data:
		raise ValueError("File data does not match")

############################################################################
# pytest tests:
#

def test_file_protocol():
	request = [os.getcwd() + "/master/master",
		"localhost:5001", "File", "LICENSE"]
	master = subprocess.Popen(request, cwd="/tmp")
	master.wait()

	assert filecmp.cmp("LICENSE", "/tmp/LICENSE")


@pytest.yield_fixture(autouse=True, scope='session')
def test_cleanup():
	global root
	slave = subprocess.Popen(["devices/filetransfer/slave", "--port=5001"])
	time.sleep(0.5)
	dev = netpp.connect("TCP:localhost:5001")

	root = dev.sync()

	yield
	slave.terminate()

if __name__ == '__main__':
	pass
