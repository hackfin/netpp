import sys
import netpp
import random
import time
import subprocess
import pytest

@pytest.yield_fixture(autouse=True, scope='session')
def test_cleanup():
	global slave, root, dev
	slave = subprocess.Popen(["devices/example/slave", "--deviceindex=-1", "--port=5008"])
	time.sleep(0.5)
	dev = netpp.connect("TCP:localhost:5008")

	root = dev.sync()

	yield
	if slave:
		slave.terminate()
	time.sleep(1.0)

############################################################################
# Dynamic property tests:

def test_dynprop():
	obj0 = root.Obj0
	obj1 = root.Obj1

	assert obj0.Count.get() == 0
	assert obj1.Count.get() == 1

	obj0.User.set("blabla")

	s = obj0.User.get()

	print(len(s), len("blabla"))

	assert s == "blabla"


