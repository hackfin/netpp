/** Remote display server library header
 *
 * (c) 2005-2011, Martin Strubel <strubel@section5.ch>
 *
 */
#include <stdint.h>
#include "vkit/videomodes.h"

#include "extern.h"

typedef struct my_config {
	int             x;
	int             y;
	unsigned short  bpp;
	unsigned short  mode;
	unsigned short  flags;  // display mode / flags
	float           gamma;
} Config;

#define MODE_RAW       VIDEOMODE_RAW
#define MODE_GRAY      VIDEOMODE_8BIT
#define MODE_INDEXED   VIDEOMODE_INDEXED
#define MODE_BAYER     VIDEOMODE_8BIT_BAYER
#define MODE_JPEG_RAW  VIDEOMODE_JPEG_RAW
#define MODE_JPEG      VIDEOMODE_JPEG
#define MODE_UYVY      VIDEOMODE_UYVY
#define MODE_YUYV      VIDEOMODE_YUYV

// Lookup table from 16 bit to RGB colours
#define LUT16_SIZE (0x10000 * 3)



/** Number of maximum VFBs supported */
#define MAX_VFBS 1
/** JPEG or other compressed image header */
#define HEADER_SIZE 1024

struct SDL_Surface;

typedef
struct virtfb_t {
	unsigned short flags;
	struct SDL_Surface *screen;
	struct SDL_Surface *marker_bmp;
	uint16_t marker[256 * 2];
	unsigned short nmarkers;
	void           *rawdata; // raw image data
	unsigned short  mode;    // Data format / Video mode
	unsigned short  bpp;
	unsigned int    size;    // Full data buffer size
	unsigned int    len;     // compressed image length
	// Special image header, sent normally once:
	unsigned char *header;
	unsigned long  headerlen;
	unsigned short  frameno;  ///< Frame number
	short width, height;
	unsigned long npixels;
	unsigned short preset;
	unsigned char *lut;     ///< lookup table for indexed mode
	/** Update callback */
	int (*update)(struct virtfb_t *vfb);
	void *context;           ///< User Context
} VirtFB;

#define F_SURFACE 0x0001
#define F_OVERLAY 0x0002   ///< use overlay (no flip after draw)
#define F_SHARED  0x0008   ///< 

int draw_frame(struct virtfb_t *vfb);
int draw_frame_indexed(struct virtfb_t *vfb);
int draw_frame_bayer(struct virtfb_t *vfb);
int draw_frame_uyvy(struct virtfb_t *vfb);
int draw_frame_nbit(struct virtfb_t *vfb);
void draw_histo(struct virtfb_t *vfb, uint32_t *bins);


int display_init(struct virtfb_t *vfb);
int display_exit(struct virtfb_t *vfb);
int display_set_current(unsigned short which, struct virtfb_t *vfb);
int display_set_callback(unsigned short which, int (*cb)(VirtFB *p), void *c);

void lut_update(VirtFB *vfb);

int init_drawcallback(VirtFB *vfb);

// Exported variables:
//

int sdl_update(struct virtfb_t *vfb);

extern Config g_config;


extern
char *g_videomodes[];
