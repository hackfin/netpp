/** \file py_device.c
 *
 * (c) 2007-2009, Martin Strubel <hackfin@section5.ch>
 *
 * TODO: Add custom netpp exceptions
 *
 */

#include "Python.h"
#include "devlib.h"
#include "devlib_error.h"
#include "property_protocol.h"

#ifdef DEBUG
#	include <stdio.h>
#endif

#define CONCAT(x, y)   x##y
#define INITMODULE(x)  CONCAT(init, x)
#define RESOLVE(x)     #x
#define STRINGIFY(x)   RESOLVE(x)

static PyObject  *s_restokens;
static PyObject  *s_typecodes;
static PyObject  *s_flags;

static PyObject
*newTokenObject(DEVICE device, TOKEN parent, TOKEN prop);

////////////////////////////////////////////////////////////////////////////
// AUX

#define HANDLE_ERROR(err, msg) handleError(err, __LINE__)

static PyObject *ConnectError;

PyObject *
handleError(int err, int line)
{
	char s[256];
	PyObject *exctype = PyExc_SystemError;

	sprintf(s, "%s:%d: %s", __FILE__, line, dcGetErrorString(err));
	// catch some special errors
	if (err < 0) { // Errors
		switch (err) {
			case DCERR_MALLOC:
			case DCERR_BADPTR:
				exctype = PyExc_MemoryError;
				break;
			case DCERR_COMM_INIT:
			case DCERR_COMM_NAK:
				exctype = ConnectError;
				break;
			case DCERR_COMM_GENERIC:
			case DCERR_COMM_TIMEOUT:
			case DCERR_COMM_FRAME:
			case DCERR_COMM_OVERRUN:
			case DCERR_COMM_ONBOARD:
			case DCERR_COMM_DISCONNECT:
				exctype = PyExc_IOError;
				break;
			case DCERR_MODULE_SYMBOL:
			case DCERR_MODULE_LOAD:
				exctype = PyExc_ImportError;
				break;
			case DCERR_PROPERTY_RANGE:
			case DCERR_PROPERTY_SIZE_MATCH:
				exctype = PyExc_ValueError;
				break;
			case DCERR_PROPERTY_TYPE_UNKNOWN:
			case DCERR_PROPERTY_TYPE_MATCH:
			case DCERR_PROPERTY_ACCESS:
				exctype = PyExc_TypeError;
				break;
		}
		PyErr_SetString(exctype, s);
	} else { // Warnings:
		PyErr_WarnEx(PyExc_UserWarning, s, 1);
	}
	return NULL;
}

// converts DeviceValue into a Python Object
PyObject *
ValueToPyObject(DCValue *val)
{
	PyObject *p = NULL;

	switch (val->type) {
		case DC_EVENT:
		case DC_REGISTER:
			p = PyLong_FromUnsignedLong(val->value.i & 0xffffffff);
			break;
		case DC_MODE:
		case DC_INT:
		case DC_COMMAND:
			p = PyInt_FromLong(val->value.i);
			break;
		case DC_BOOL:
			if (val->value.i) {
				p = Py_True;
			} else {
				p = Py_False;
			}
			Py_INCREF(p);
			break;
		case DC_FLOAT:
			p = PyFloat_FromDouble(val->value.f);
			break;
		case DC_STRING:
			p = PyString_FromStringAndSize(val->value.p, val->len - 1);
			break;
		case DC_BUFFER:
			p = PyBuffer_New(0);
			break;
		case DC_STRUCT:
			p = Py_BuildValue("s", "<struct>");
		default:
			netpp_log(DCLOG_ERROR, "Unsupported data type %d", val->type);
			return HANDLE_ERROR(DCERR_PROPERTY_TYPE_UNKNOWN, "ValueToPyObject");
	}
	return p;
}


int
ValueFromPyObject(DCValue *val, PyObject *arg, char buffer_write)
{
	Py_ssize_t buflen;
	int ret = 0;

	if (PyInt_Check(arg)) {
		val->value.i = PyInt_AsLong(arg);
		val->type = DC_INT;
	} else
	if (PyFloat_Check(arg)) {
		val->value.f = PyFloat_AsDouble(arg);
		val->type = DC_FLOAT;
	} else
	if (PyString_Check(arg)) {
		val->value.p = PyString_AsString(arg);
		val->type = DC_STRING;
		val->len = PyString_Size(arg) + 1;
	} else
	if (PyBuffer_Check(arg)) {
		if (buffer_write) {
			ret = PyObject_AsWriteBuffer(arg, &val->value.p, &buflen);
		} else {
			ret = PyObject_AsReadBuffer(arg, (const void **) &val->value.p,
					&buflen);
		}
		val->len = buflen;
		val->type = DC_BUFFER;
	} else {
		PyErr_SetString(PyExc_TypeError,
				"bad type for setProperty()");
		return 0;
	}
	if (ret < 0) {
		// PyErr_SetString(PyExc_TypeError,
				// "Could not get pointer to buffer object");
		return 0;
	}
	return 1;
}

int send_buffer(DEVICE d, TOKEN prop, DCValue *val)
{
	int error = 0;
	char *p;

	int len = val->len;

	// First, try sending the entire buffer
	error = dcDevice_SetProperty(d, prop, val);
	switch (error) {
		// We failed, because server doesn't like our blocksize
		case DCERR_COMM_OVERRUN:
		case DCERR_PROPERTY_SIZE_MATCH:
			netpp_log(DCLOG_ERROR, "Property size mismatch");
			p = val->value.p;
			break;
		// It worked, but only n bytes were taken
		case DCWARN_PROPERTY_MODIFIED:
			netpp_log(DCLOG_ERROR, "Only partial buffer taken");
			p = ( (char *) val->value.p + val->len);
			break;
		default:
			return error;
	}

	error = dcProperty_GetProto(d, prop, val);
	if (error < 0) return error;

	netpp_log(DCLOG_WARN,
			"Adjusting block length: %ld", val->len);

	// now send object in chunks:		

	while (len > 0) {
		val->value.p = p;
		if (len < val->len) val->len = len;
		error = dcDevice_SetProperty(d, prop, val);
		p += val->len; len -= val->len;
		if (error < 0) break;
	}	

	return error;
}

static PyObject *
getProperty(DEVICE device, TOKEN prop, PyObject *pval)
{
	DCValue val;
	PyObject *p = NULL;
	int ret;
	Py_ssize_t buflen;
	int size;
	char *cp = 0;

	// If we specified a variable reference, do not initialize a new
	// variable but initialize reference.

	if (pval) {
		DEB(netpp_log(DCLOG_DEBUG, "Use buffer reference"));
		if (!ValueFromPyObject(&val, pval, 1)) return NULL;
		size = val.len; // Total expected buffer size
		DEB(netpp_log(DCLOG_DEBUG, "Buffer size %d", size));
		cp = (char *) val.value.p;
	} else {
		val.value.p = (char *) ""; val.len = 0;
		val.type = DC_UNDEFINED;
	}
	
	ret = dcDevice_GetProperty(device, prop, &val);

	// If we happen to query a string/buffer (thus didn't have storage
	// space), reserve memory and retry:
	if ((ret == DCERR_PROPERTY_SIZE_MATCH || ret == DCERR_BADPTR)) {

		DEB(netpp_log(DCLOG_DEBUG, "python: alloc buf[%d], got ret %d", val.len, ret));
		// If we were a buf reference, return effective length:
		if (pval) return PyInt_FromLong(val.len);
		switch (val.type) {
			case DC_STRING:
				// Create string without terminating \0:
				p = PyString_FromStringAndSize(NULL, val.len - 1);
				val.value.p = PyString_AsString(p);
				break;
			case DC_BUFFER:
				p = PyBuffer_New(val.len);
				ret = PyObject_AsWriteBuffer(p, &val.value.p, &buflen);
				if (ret < 0) return NULL;
				val.len = buflen;
				break;
			default:
				netpp_log(DCLOG_ERROR, "Unexpected type: %d", val.type);
				return HANDLE_ERROR(ret, "getProperty");
		}
		// And try again:
		DEB(netpp_log(DCLOG_NOTICE, "request %d", val.len));
		ret = dcDevice_GetProperty(device, prop, &val);
		// Now, we can fail a second time, if the backend still decided
		// it run out of data (when it might not know, a priori).
		// In this case it can ONLY return a DCWARN_PROPERTY_MODIFIED.
		// This only works on a returned buffer, not a passed reference!!
		if (!pval && ret == DCWARN_PROPERTY_MODIFIED) {
			// Create new buffer with effectively returned length:
			PyObject *tmp;
			switch (val.type) {
				case DC_BUFFER:
					tmp = PyBuffer_FromReadWriteObject(p, 0, val.len);
					Py_DECREF(p);
					p = tmp;
					break;
				case DC_STRING:
					// Oh yes, this is a -1, because python strings
					// don't include the 0 termination
					tmp = PyString_FromStringAndSize(val.value.p, val.len - 1);
					Py_DECREF(p);
					p = tmp;
					break;
				default:
					return HANDLE_ERROR(ret, "getProperty");
			}

		}

		DEB(netpp_log(DCLOG_NOTICE, "Left : %d, ret: %d", val.len, ret));
	} else if (ret == 0 && (val.type == DC_BUFFER || val.type == DC_STRING) && cp) {
		// We passed a prereserved writeable buffer
		// In this case, we poll the peer until we receive all
		// bytes. This can hang forever, if the handler is not correctly
		// implemented.
		// From the previous GetProperty, we got the effectively read
		// bytes in val.len.
		size -= val.len;
		while (size > 0) {
			cp += val.len; // Increment pointer
			netpp_log(DCLOG_NOTICE, "Poll for more: %d bytes", size);
			val.len = size;
			val.value.p = cp;
			ret = dcDevice_GetProperty(device, prop, &val);
			if (ret < 0 || val.len == 0) break;
			size -= val.len;
		}
	}

	if (ret < 0) {
		return HANDLE_ERROR(ret, "getProperty");
	}

	if (pval) {
		p = PyInt_FromLong(val.len); // Return effective length
	} else 
	if (p == NULL) {
		p = ValueToPyObject(&val);
	}

	return p;
}

static PyObject *
setProperty(DEVICE device, TOKEN prop, PyObject *pval, int type)
{
	DCValue val;
	int default_type;
	int ret;

	if (!ValueFromPyObject(&val, pval, 0))
		return NULL;

	// If type was not specified, try guessing:
	if (type == DC_INVALID && (val.type == DC_INT)) {
		netpp_log(DCLOG_VERBOSE,
			"Unspecified type of [%08x], try guessing", prop);
		DCValue v;
		v.value.p = NULL;
		ret = dcProperty_GetProto(device, prop, &v);
		if (ret < 0) return HANDLE_ERROR(ret, "Property_GetType");
		switch (v.type) {
			case DC_REGISTER:
			case DC_MODE:
			case DC_COMMAND:
			case DC_BOOL:
				val.type = v.type;
				break;
			default:
				if ((int) v.type < 0)
					return HANDLE_ERROR(ret, "setProperty::GetType");
				break;
		}
	} else {
		default_type = type; // Override
	}

	// Buffer need special treatment:
	if (val.type == DC_BUFFER || val.type == DC_STRING) {
		ret = send_buffer(device, prop, &val);
		if (ret < 0) {
			return HANDLE_ERROR(ret, "setProperty (Buffer)");
		}
	} else {
		val.type = default_type;
		ret = dcDevice_SetProperty(device, prop, &val);
		if (ret < 0) {
			return HANDLE_ERROR(ret, "setProperty");
		}
	}

	return Py_BuildValue("i", ret);
}

////////////////////////////////////////////////////////////////////////////
// Token object
//
// A Token is a TOKEN wrapped with a few getattr/setattr functions.
// This allows us to build a python class from the netpp description.

typedef struct token_object {
	PyObject_HEAD
	DEVICE          device;
	TOKEN           parent;
	TOKEN           token;
} TokenObject;

staticforward PyTypeObject DeviceType;

// Methods:

static PyObject *
Token_get(TokenObject *self, PyObject *args)
{
	PyObject *arg = NULL;

	PyArg_ParseTuple(args, "|O", &arg);

	return getProperty(self->device, self->token, arg);
}

static PyObject *
Token_set(TokenObject *self, PyObject *args)
{
	PyObject *arg;
	int type = DC_INVALID;

	if (!PyArg_ParseTuple(args, "O|i", &arg, &type))
		return NULL;

	return setProperty(self->device, self->token, arg, type);
}

static PyObject *
Token_name(TokenObject *self, PyObject *args)
{
	int ret;
	static DCValue val;
	PyObject *p;
	static char propname[64];

	// Allocate string:
	val.len = sizeof(propname);
	val.value.p = propname;

	ret = dcProperty_GetName(self->device, self->token, &val);
	if (ret < 0) {
		return HANDLE_ERROR(ret, "name");
	}

	// val.value.p may have changed, thus use this:
	p = PyString_FromStringAndSize(val.value.p, val.len - 1);
	
	return p;
}

static PyObject *
Token_child(TokenObject *self, PyObject *args)
{
	int ret;
	TOKEN child;

	ret = dcProperty_Select(self->device,
			self->token, 
			self->token,  &child);

	if (ret < 0) 
		return HANDLE_ERROR(ret, "child");

	if (ret) {
		Py_INCREF(Py_None);
		return Py_None;
	}

	return newTokenObject(self->device, self->token, child);
}

static PyObject *
Token_type(TokenObject *self, PyObject *args)
{
	int ret;
	DCValue val;
	val.len = 0;

	ret = dcProperty_GetProto(self->device, self->token, &val);
	if (ret < 0 && ret != DCERR_PROPERTY_SIZE_MATCH) 
		return HANDLE_ERROR(ret, "child");
	return Py_BuildValue("(ii)", val.type, val.flags);
}

static PyObject *
Token_next(TokenObject *self, PyObject *args)
{
	int ret;
	TOKEN next;

	ret = dcProperty_Select(self->device,
			self->parent, 
			self->token,  &next);

	if (ret < 0) 
		return HANDLE_ERROR(ret, "next");

	if (ret) {
		Py_INCREF(Py_None);
		return Py_None;
	}

	return newTokenObject(self->device, self->parent, next);
}

static PyObject *
Token_getitem(TokenObject *self, PyObject *args)
{
	int ret;
	char *name;
	TOKEN token = self->token;
	
	// TODO: Type check for non-container tokens

	if (!PyArg_ParseTuple(args, "s", &name))
		return NULL;

	ret = dcProperty_ParseName(self->device, name, &token);
	if (ret < 0) {
		return HANDLE_ERROR(ret, "getitem");
	}
	
	return newTokenObject(self->device, self->token, token);
}

static PyMethodDef Token_methods[] = 
{
	// BASIC ACCESS
	{"get",            (PyCFunction) Token_get,            METH_VARARGS},
	{"set",            (PyCFunction) Token_set,            METH_VARARGS},
	{"name",           (PyCFunction) Token_name,           METH_VARARGS},
	{"type",           (PyCFunction) Token_type ,          METH_VARARGS},
	{"child",          (PyCFunction) Token_child,          METH_VARARGS},
	{"next",           (PyCFunction) Token_next,           METH_VARARGS},
	{"getitem",        (PyCFunction) Token_getitem,        METH_VARARGS},
	{NULL,		NULL}		/* sentinel */
};

static void
Token_dealloc(TokenObject *self)
{
	PyObject_Del(self);
}


static PyObject *
Token_getattr(TokenObject *self, char *name)
{
	if (strcmp(name, "value") == 0) {
		return PyLong_FromUnsignedLong(self->token);
	} else
	return Py_FindMethod(Token_methods, (PyObject *)self, name);
}

PyObject *Token_repr(TokenObject *self)
{
	return PyString_FromFormat("<Token: %08x>", self->token);
}

int Token_compare(TokenObject *self, TokenObject *other)
{
	if (self->token < other->token) return -1;
	if (self->token > other->token) return 1;
	return 0;
}

statichere PyTypeObject NetppTokenType = {
	/* The ob_type field must be initialized in the module init function
	 * to be portable to Windows without using C++. */
	PyObject_HEAD_INIT(NULL)
	0,			/*ob_size*/
	"device.Token",		/*tp_name*/
	sizeof(TokenObject),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor) Token_dealloc, /*tp_dealloc*/
	0,          /*tp_print*/
	(getattrfunc) Token_getattr, /*tp_getattr*/
	0, // (setattrfunc) Token_setattr, /*tp_setattr*/
	(cmpfunc) Token_compare, /*tp_compare*/
	(reprfunc) Token_repr,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
        0,                      /*tp_call*/
        0,                      /*tp_str*/
        0,                      /*tp_getattro*/
        0,                      /*tp_setattro*/
        0,                      /*tp_as_buffer*/
        Py_TPFLAGS_DEFAULT,     /*tp_flags*/
        0,                      /*tp_doc*/
        0,                      /*tp_traverse*/
        0,                      /*tp_clear*/
        0,                      /*tp_richcompare*/
        0,                      /*tp_weaklistoffset*/
        0,                      /*tp_iter*/
        0,                      /*tp_iternext*/
        0,                      /*tp_methods*/
        0,                      /*tp_members*/
        0,                      /*tp_getset*/
        0,                      /*tp_base*/
        0,                      /*tp_dict*/
        0,                      /*tp_descr_get*/
        0,                      /*tp_descr_set*/
        0,                      /*tp_dictoffset*/
        0,                      /*tp_init*/
        0,                      /*tp_alloc*/
        0,                      /*tp_new*/
        0,                      /*tp_free*/
        0,                      /*tp_is_gc*/
};

#define TokenObject_Check(v)	((v)->ob_type == &NetppTokenType)

static PyObject *
newTokenObject(DEVICE device, TOKEN parent, TOKEN prop)
{
	TokenObject *self;
	self = PyObject_New(TokenObject, &NetppTokenType);
	if (self == NULL)
		return NULL;
	self->device = device;    // device port handle
	self->parent = parent;
	self->token = prop;
	return (PyObject *) self;
}


////////////////////////////////////////////////////////////////////////////
//
// Device object
//
////////////////////////////////////////////////////////////////////////////

#define FLAGS_VALID 0x0001

typedef struct {
	PyObject_HEAD
	DEVICE          device;
	unsigned int    flags;
} DeviceObject;

staticforward PyTypeObject DeviceType;

#define DeviceObject_Check(v)	((v)->ob_type == &DeviceType)

////////////////////////////////////////////////////////////////////////////
//
// Device object method functions
//
////////////////////////////////////////////////////////////////////////////


static PyObject *
Device_close(DeviceObject *self, PyObject *args)
{
	if (!PyArg_ParseTuple(args, ""))
		return NULL;

	if (self->device != DC_PORT_ROOT) {
		dcDeviceClose(self->device);
	}
	self->device = 0;

	Py_INCREF(Py_None);
	return Py_None;
}


static PyObject *
Device_setProperty(DeviceObject *self, PyObject *args)
{
	TOKEN prop;
	PyObject *arg;
	char *name;
	int type = DC_INVALID;

	int ret;

	if (!PyArg_ParseTuple(args, "sO|i", &name, &arg, &type))
		return NULL;

	ret = dcProperty_ParseName(self->device, name, &prop);
	if (ret < 0) {
		return HANDLE_ERROR(ret, "setProperty");
	}

	return setProperty(self->device, prop, arg, type);
}

static PyObject *
Device_getProperty(DeviceObject *self, PyObject *args)
{
	int ret;
	TOKEN prop;
	PyObject *arg = NULL;

	char *name;
	if (!PyArg_ParseTuple(args, "s|O", &name, &arg))
		return NULL;

	ret = dcProperty_ParseName(self->device, name, &prop);
	if (ret < 0) {
		return HANDLE_ERROR(ret, "getProperty");
	}

	return getProperty(self->device, prop, arg);
}

static PyObject *
Device_getToken(DeviceObject *self, PyObject *args)
{
	int ret;
	PyObject *arg;
	TOKEN prop;
	TOKEN token = INVALID_TOKEN;

	char *name;

	if (!PyArg_ParseTuple(args, "O", &arg))
		return NULL;


	if (PyLong_Check(arg)) {
		token = PyLong_AsUnsignedLong(arg);
		return newTokenObject(self->device, token, token);
	} else
	if (PyInt_Check(arg)) {
		token = PyInt_AsLong(arg);
		return newTokenObject(self->device, token, token);
	} else 
	if (PyString_Check(arg)) {
		name = PyString_AsString(arg);
	} else {
		PyErr_SetString(PyExc_TypeError,
				"bad argument type to getToken()");
		return NULL;
	}

	// If empty string, we return root token
	if (strlen(name) == 0) {
		ret = dcDevice_GetRoot(self->device, &prop);
	} else {
		ret = dcProperty_ParseName(self->device, name, &prop);
	}

	if (ret < 0) {
		return HANDLE_ERROR(ret, "getToken");
	}

	return newTokenObject(self->device, prop, prop);
}


static PyMethodDef Device_methods[] = 
{
	// BASIC ACCESS
	{"close",        (PyCFunction) Device_close,          METH_VARARGS},
	{"set",          (PyCFunction) Device_setProperty,    METH_VARARGS},
	{"get",          (PyCFunction) Device_getProperty,    METH_VARARGS},
	{"getToken",     (PyCFunction) Device_getToken,       METH_VARARGS},
	{NULL,		NULL}		/* sentinel */
};

////////////////////////////////////////////////////////////////////////////
// Device type object standard methods
//


static void
Device_dealloc(DeviceObject *self)
{
	if (self->device && self->device != DC_PORT_ROOT)
		dcDeviceClose(self->device);
	PyObject_Del(self);
}


static PyObject *
Device_getattr(DeviceObject *self, char *name)
{
	return Py_FindMethod(Device_methods, (PyObject *)self, name);
}

// finally, the object type definition

statichere PyTypeObject DeviceType = {
	/* The ob_type field must be initialized in the module init function
	 * to be portable to Windows without using C++. */
	PyObject_HEAD_INIT(NULL)
	0,			/*ob_size*/
	"device.Device",		/*tp_name*/
	sizeof(DeviceObject),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor) Device_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc) Device_getattr, /*tp_getattr*/
	(setattrfunc) 0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
        0,                      /*tp_call*/
        0,                      /*tp_str*/
        0,                      /*tp_getattro*/
        0,                      /*tp_setattro*/
        0,                      /*tp_as_buffer*/
        Py_TPFLAGS_DEFAULT,     /*tp_flags*/
        0,                      /*tp_doc*/
        0,                      /*tp_traverse*/
        0,                      /*tp_clear*/
        0,                      /*tp_richcompare*/
        0,                      /*tp_weaklistoffset*/
        0,                      /*tp_iter*/
        0,                      /*tp_iternext*/
        0,                      /*tp_methods*/
        0,                      /*tp_members*/
        0,                      /*tp_getset*/
        0,                      /*tp_base*/
        0,                      /*tp_dict*/
        0,                      /*tp_descr_get*/
        0,                      /*tp_descr_set*/
        0,                      /*tp_dictoffset*/
        0,                      /*tp_init*/
        0,                      /*tp_alloc*/
        0,                      /*tp_new*/
        0,                      /*tp_free*/
        0,                      /*tp_is_gc*/
};



//
// Create new device Python object and return it
//
static DeviceObject *
newDeviceObject(DEVICE d)
{
	DeviceObject *self;
	self = PyObject_New(DeviceObject, &DeviceType);
	if (self == NULL)
		return NULL;
	self->device = d;    // device port handle
	self->flags = 0;
	return self;
}

////////////////////////////////////////////////////////////////////////////
//
// Device module definitions
//
////////////////////////////////////////////////////////////////////////////

static PyObject *
device_open(PyObject *self, PyObject *args)
{
	char *portname = NULL;
	DEVICE dev;
	int error;
	int raw = 0;

	DeviceObject *d;

	if (!PyArg_ParseTuple(args, "|si", &portname, &raw)) {
		return 0;
	}

	if (portname) {
		error = dcDeviceOpen(portname, &dev);

		if (error == 0 || (raw && error > 0)) {
			d = newDeviceObject(dev);
			// if no error or warning, device is valid
			if (error == 0) d->flags |= FLAGS_VALID;
		} else {
			// Warnings don't automatically close the device, so
			// we do it
			if (error > 0) {
				dcDeviceClose(dev);
			}
			d = (DeviceObject *) HANDLE_ERROR(error, "device_open");
		} 
	} else {
		// Return port root dummy:
		dev = DC_PORT_ROOT;
		d = newDeviceObject(dev);
	} 
	return (PyObject *) d;
}

static PyObject *
device_newbuffer(PyObject *self, PyObject *args)
{
	int len = 0;

	if (!PyArg_ParseTuple(args, "i", &len)) return NULL;

	return PyBuffer_New(len);
}

static PyMethodDef device_methods[] = {
	{"open",         device_open,                         METH_VARARGS },
	{"buffer",       device_newbuffer,                    METH_VARARGS },
	{NULL, NULL}
};


#ifdef __WIN32__
	__declspec(dllexport)
#endif


void
INITMODULE(MODULENAME)(void)
{
	PyObject *m;
	// XXX
	// this only for windows portability
	// might be removed when using g++ or any other C++ compiler
	DeviceType.ob_type = &PyType_Type;
	//
	m = Py_InitModule("device", device_methods);

#define ENUM_ENTRY(x) { #x, DC_##x }

	static
	struct enums {
		char *id;
		enum property_type_enum code;
	} list[] = {
        ENUM_ENTRY(ROOT), ENUM_ENTRY(INT), ENUM_ENTRY(FLOAT), ENUM_ENTRY(BOOL),
		ENUM_ENTRY(MODE), ENUM_ENTRY(STRING), ENUM_ENTRY(BUFFER),
		ENUM_ENTRY(STRUCT), ENUM_ENTRY(ARRAY),
		ENUM_ENTRY(COMMAND),
        ENUM_ENTRY(EVENT),
        ENUM_ENTRY(UINT),
        { NULL }
	};

	s_typecodes = PyDict_New();
	s_flags = PyDict_New();
	s_restokens = PyDict_New();
	struct enums *c = list;
	while (c->id) {
		PyDict_SetItemString(s_typecodes, c->id, PyInt_FromLong(c->code));
		c++;
	}

	// Userdefined Exceptions:
	ConnectError = PyErr_NewException("netpp.ConnectError", NULL, NULL);
	Py_INCREF(ConnectError);
	PyModule_AddObject(m, "ConnectError", ConnectError);	

	// Now the flags:
	PyDict_SetItemString(s_flags, "F_WO", PyInt_FromLong(F_WO));
	PyDict_SetItemString(s_flags, "F_RO", PyInt_FromLong(F_RO));
	PyDict_SetItemString(s_flags, "F_VOLATILE", PyInt_FromLong(F_VOLATILE));

	PyDict_SetItemString(s_restokens, "CHECKSUM",
		PyInt_FromLong(TOKEN_CHECK));
	PyDict_SetItemString(s_restokens, "NPROPS",
		PyInt_FromLong(TOKEN_NPROPS));
	PyDict_SetItemString(s_restokens, "PKTSIZE",
		PyInt_FromLong(TOKEN_PKTSIZE));

	PyModule_AddObject(m, "Types", s_typecodes);
	PyModule_AddObject(m, "Flags", s_flags);
	PyModule_AddObject(m, "ReservedTokens", s_restokens);
}
