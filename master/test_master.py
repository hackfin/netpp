#!/usr/bin/env python

import sys
import time
import netpp
import pytest
import subprocess
import difflib
import filecmp


SLAVE = "devices/example/slave"
slave = None

@pytest.yield_fixture(autouse=True, scope='session')
def test_cleanup():
	global slave, dev, root
	slave = subprocess.Popen([SLAVE, "--port=2008"])
	time.sleep(0.5)
	dev = netpp.connect("TCP:localhost:2008")
	root = dev.sync()
	yield
	slave.terminate()


def run_dump(dumpfilename):
	log = open(dumpfilename, "w")
	master = subprocess.Popen(["master/netpp-dump", "TCP:localhost:2008"], stdout=log)
	master.wait()
	log.close()

def test_dump():
	run_dump("testbench/dump.ini")
	assert filecmp.cmp("testbench/reference.ini", "testbench/dump.ini")

def test_config():
	master = subprocess.Popen(["master/netpp-config", "TCP:localhost:2008",
	"testbench/dump.ini"])
	master.wait()
	run_dump("testbench/dump2.ini")

	difference = """--- 
+++ 
@@ -31,11 +31,11 @@
 HandledArray[6] = "item_6"
 HandledArray[7] = "item_7"
 # Update is writeonly (WO)
-# (READONLY) LogBuffer = "<0000> This is a fake log message"
+# (READONLY) LogBuffer = "<idle>"
 # (READONLY) Monitor.Zoom = 5.000000e-01
 # (READONLY) Monitor.ControlReg = 21930 # 0x55aa
 Thermo.Threshold = 2.700000e+01
-# (READONLY) Thermo.Value = 2.436000e+01
+# (READONLY) Thermo.Value = 2.393000e+01
 Container.Test = "0"
 # Array 'Container.TestArray' size 16
 Container.TestArray[0] = 0
"""

	fa = open("testbench/reference.ini", "r")
	fb = open("testbench/dump2.ini", "r")
	a = fa.read()
	b = fb.read()
	fa.close()
	fb.close()

	delta = ''.join(difflib.unified_diff(a.splitlines(1), b.splitlines(1)))
	df = open("/tmp/delta.txt", "w")
	df.write(delta)
	df.close()

	assert delta == difference
