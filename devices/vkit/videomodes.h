/** \file videomodes.h
 * Videomodes header
 */

#ifndef GENERATE_MODE_STRINGS
#	define PREFIX  enum
#	define MODE_DEF(name, x) VIDEOMODE_##name x
#else
#	define PREFIX
#	define MODE_DEF(name, x) #name
#endif

#define MODES_BEGIN() PREFIX {
#define MODES_END() };

/** Video modes */

MODES_BEGIN()
	MODE_DEF( RAW,  ),            ///< RAW 16 bit mode
	MODE_DEF( 8BIT, ),            ///< 8 Bit raw
	MODE_DEF( 8BIT_BAYER, ),      ///< 8 Bit bayer
	MODE_DEF( YUV, ),             ///< Cr Y Cb Y interpolation mode
	MODE_DEF( YUYV, ),            ///< YUYV order
	MODE_DEF( JPEG_RAW, = 8 ),    ///< Raw JPEG data with separate header
	MODE_DEF( JPEG, ),            ///< Full JPEG block
	MODE_DEF( JPEG_MONO, ),       ///< JPEG encoding, monochrome
	MODE_DEF( PREPROCESS, = 16 ), ///< Generic preprocessing mode
	MODE_DEF( INDEXED, ),         ///< Indexed 16 bit display mode
	// Future:
	MODE_DEF( RLE, ),             ///< Run length encoded
MODES_END()

// Legacy (compatibility):
#define VIDEOMODE_UYVY VIDEOMODE_YUV

////////////////////////////////////////////////////////////////////////////
// Input pixel formats

#ifndef GENERATE_MODE_STRINGS

/** Internal pixel formats. */

enum {
	PIXELFORMAT_MONO,
	PIXELFORMAT_YUYV,
	PIXELFORMAT_JPEG_RAW,
	PIXELFORMAT_MJPG,
};

#endif

#undef PREFIX
#undef MODE_DEF

