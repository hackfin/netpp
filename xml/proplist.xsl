<?xml version="1.0" encoding="ISO-8859-1"?>

<!--

$Id: proplist.xsl 334 2015-02-28 10:20:53Z strubi $

Transformation style sheet DEVDESC-XML -> PropertyList C source

(c) 2005, section5 // Martin Strubel <strubel@section5.ch>

-->

<xsl:stylesheet version="1.0" 
	xmlns:my="http://www.section5.ch/dclib/schema/devdesc"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

	<xsl:import href="registermap.xsl"/>

	<xsl:output method="text" encoding="ISO-8859-1"/>	

	<!-- This key is needed for register referencing -->

	<xsl:key name="regkey" match="my:register" use="@id"/>
	<xsl:param name="attribute">const</xsl:param>

	<!-- Bit ranges -->
	<xsl:template match="my:bitfield"><xsl:value-of select="@lsb"/>,<xsl:value-of select="@msb"/></xsl:template>

	<xsl:template match="my:register" mode="ref">
		<xsl:param name="which"/><xsl:value-of select="my:bitfield[@name=$which]/@lsb"/>, <xsl:value-of select="my:bitfield[@name=$which]/@msb"/></xsl:template>

	<xsl:template match="my:register" mode="refRegsize">
		<xsl:choose>
			<xsl:when test="@size"><xsl:value-of select="@size"/></xsl:when>
			<xsl:otherwise>1</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="my:regref">
		<xsl:choose>
			<xsl:when test="@bits">
				<xsl:apply-templates select="key('regkey', @ref)" mode="ref">
					<xsl:with-param name="which" select="@bits"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>0, LAST_BIT</xsl:otherwise>
		</xsl:choose>, <xsl:apply-templates select="key('regkey', @ref)" mode="refRegsize"/>

	</xsl:template>

	<!-- encode endianness PER register -->
	<xsl:template match="my:register" mode="endiancode">
		<xsl:if test="../@endian = 'BIG'"> | F_BIG</xsl:if>
	</xsl:template>

	<!-- Needs to be retrieved via the key mechanism -->
	<xsl:template match="my:regref" mode="endiancode">
		<xsl:apply-templates select="key('regkey', @ref)" mode="endiancode"/>
	</xsl:template>

	<!-- Enumeration of all static entities -->

	<xsl:template match="my:property|my:struct|my:array" mode="enum">
		<xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<!-- Boundary variable handling -->

	<xsl:template match="my:min|my:max|my:size" mode="var_enum">
		<xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<!-- Access to programming language variables -->
	<xsl:template name="var_lang">
		<xsl:choose>
			<xsl:when test="@type = 'BUFFER'">DC_VAR, { .buf = { (unsigned char *) <xsl:value-of select="./my:variable"/>, sizeof(<xsl:value-of select="./my:variable"/>) } },
			</xsl:when>
			<xsl:when test="@type = 'STRING'">DC_VAR, { .str = { (char *) <xsl:value-of select="./my:variable"/>, sizeof(<xsl:value-of select="./my:variable"/>) } },
			</xsl:when>
			<xsl:when test="@type = 'BOOL'">DC_VAR, { .varp_bool = &amp;<xsl:value-of select="./my:variable"/> },
			</xsl:when>
			<xsl:when test="@type = 'REGISTER'">DC_VAR, { .varp_uint = &amp;<xsl:value-of select="./my:variable"/> },
			</xsl:when>
			<xsl:when test="@type = 'INT'">DC_VAR, { .varp_int = &amp;<xsl:value-of select="./my:variable"/> },
			</xsl:when>
			<xsl:when test="@type = 'MODE'">DC_VAR, { .varp_mode = &amp;<xsl:value-of select="./my:variable"/> },
			</xsl:when>
			<xsl:when test="@type = 'FLOAT'">DC_VAR, { .varp_float = &amp;<xsl:value-of select="./my:variable"/> },
			</xsl:when>
			<xsl:otherwise>DC_ADDR, { 0 }
#error "No variable support for this datatype"
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Access to programming language dynamic heap variables -->
	<xsl:template name="var_heap">
		<xsl:variable name="container" select="../@name"/>
		<xsl:text>DC_VAR, { .s_uint = STRUCT_EXPORT(</xsl:text><xsl:value-of select="$container"/>, <xsl:value-of select="./my:variable"/><xsl:text>) },
			</xsl:text>
	</xsl:template>

	<xsl:template name="boundary_value">
		<xsl:choose>
			<xsl:when test="./my:regref">
		DC_ADDR, { .reg = { Reg_<xsl:value-of select="./my:regref/@ref"/>, <xsl:apply-templates select="./my:regref"/>} },
			</xsl:when>
			<xsl:when test="./my:variable"><xsl:call-template name="var_lang"/></xsl:when>
			<xsl:when test="./my:value"><xsl:call-template name="static_limit"/></xsl:when>
			<xsl:otherwise>DC_HANDLER, { .func = { get_<xsl:value-of select="./my:handler"/>, 0 } }
</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Choice/Item handling -->

	<xsl:template match="my:item" mode="item_enum">
		<xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<xsl:template match="my:item" mode="item_init">
	{ "<xsl:value-of select="@name"/>" /* <xsl:value-of select="generate-id()"/> */, F_RO,
		DC_MODE,
		DC_STATIC, { .s_int = <xsl:value-of select="./my:value"/> }, 0 },
	</xsl:template>

	<!-- Limits (Min/Max) handling -->

	<xsl:template match="my:min|my:max" mode="limits_enum">
		<xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<xsl:template name="generic_handler">

		<xsl:choose>
			<xsl:when test="./my:handler/@type = 'DYNAMIC'">
			<xsl:text>DC_CUSTOM, { .custom = { handle_</xsl:text><xsl:value-of select="my:handler"/>, NULL } },
</xsl:when>
			<xsl:otherwise>DC_HANDLER, { .func = { <xsl:call-template name="func_handler"/> } },
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="static_limit">
		<xsl:choose>
			<xsl:when test="../my:size"> DC_STATIC, { .s_int = <xsl:value-of select="my:value"/> }, 0 </xsl:when>
			<xsl:when test="../../@type = 'INT'"> DC_STATIC, { .s_int = <xsl:value-of select="my:value"/> }, 0</xsl:when>
			<xsl:when test="../../@type = 'FLOAT'"> DC_STATIC, { .s_float = <xsl:value-of select="my:value"/> }, 0</xsl:when>
			<xsl:otherwise test="../../@type = 'INT'"> DC_STATIC, { .s_int = <xsl:value-of select="my:value"/> }, 0</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- static values: -->
	<xsl:template match="my:value">
		<xsl:choose>
			<xsl:when test="../@type = 'REGISTER'">DC_STATIC, { .s_uint = <xsl:value-of select="."/> },
			</xsl:when>
			<xsl:when test="../@type = 'INT'">DC_STATIC, { .s_int = <xsl:value-of select="."/> },
			</xsl:when>
			<xsl:when test="../@type = 'FLOAT'">DC_STATIC, { .s_float = <xsl:value-of select="."/> },
			</xsl:when>
			<xsl:when test="../@type = 'STRING'">DC_STATIC, { .s_string = "<xsl:value-of select="."/>" },
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="my:min" mode="limits_init">
	{ "Min" /* <xsl:value-of select="generate-id()"/>: "<xsl:value-of select="../../@name"/>
		<xsl:text>" */, F_RO</xsl:text>
		<xsl:apply-templates select="my:regref" mode="endiancode"/>
		<xsl:text>,
		DC_</xsl:text>
			<xsl:value-of select="../../@type"/>, <xsl:call-template name="boundary_value"/> },
	</xsl:template>

	<xsl:template match="my:max" mode="limits_init">
	{ "Max" /* <xsl:value-of select="generate-id()"/>: "<xsl:value-of select="../../@name"/>
		<xsl:text>" */, F_RO</xsl:text>
		<xsl:apply-templates select="my:regref" mode="endiancode"/>
		<xsl:text>,
		DC_</xsl:text><xsl:value-of select="../../@type"/>, <xsl:call-template name="boundary_value"/> },
	</xsl:template>

	<xsl:template match="my:limits/my:min|my:limits/my:max|my:event|my:choice/my:item" mode="prop_children">
		<xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<xsl:template match="my:property" mode="prop_children">
		<xsl:if test="./my:limits or ./my:choice or ./my:event">
<xsl:value-of select="$attribute"/> static PROPINDEX
g_children_<xsl:value-of select="generate-id()"/>[] = {
	<xsl:apply-templates select="my:limits/my:min|my:limits/my:max|my:event|my:choice/my:item" mode="prop_children"/>
	<xsl:text>INVALID_INDEX
};

</xsl:text>
	</xsl:if>
	</xsl:template>


	<!-- 
		Handling of array size
		 -->

	<xsl:template match="my:size" mode="var_init">
	{ "Size" /* "<xsl:value-of select="../@name"/>
		<xsl:text>" */, F_RO</xsl:text>
		<xsl:apply-templates select="my:regref" mode="endiancode"/>
		<xsl:text>,
		DC_INT, </xsl:text><xsl:call-template name="boundary_value"/> },
	</xsl:template>

	<!-- array element size calculation -->
	<xsl:template match="my:array" mode="calcsize">
		<xsl:if test="my:property/my:regref">
			<xsl:apply-templates select="my:value"/>
		</xsl:if>
	</xsl:template>

	<!-- handler functions -->

	<xsl:template name="func_handler">
		<xsl:choose>
			<xsl:when test="name(..) = 'struct'">
				<xsl:choose>
					<xsl:when test="not(@access) or @access = 'RW' or @access = 'RO'">get_<xsl:value-of select="./my:handler"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>, <xsl:choose>
				<xsl:when test="not(@access) or @access = 'RW' or @access = 'WO'">set_<xsl:value-of select="./my:handler"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
						
			</xsl:when>

			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="not(@access) or @access = 'RW' or @access = 'RO'">get_<xsl:value-of select="./my:handler"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>, <xsl:choose>
				<xsl:when test="not(@access) or @access = 'RW' or @access = 'WO'">set_<xsl:value-of select="./my:handler"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
	
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- handler proto -->
	<xsl:template name="handler_proto_get">
	<xsl:choose>
		<xsl:when test="@type = 'DYNAMIC'">DECLARE_CUSTOM_HANDLER(handle_<xsl:value-of select="."/>)
</xsl:when>
		<xsl:otherwise>DECLARE_PROTO_GETTER(get_<xsl:value-of select="."/>)
</xsl:otherwise>
	</xsl:choose>
	</xsl:template>

	<xsl:template name="handler_proto_set">
	<xsl:choose>
		<xsl:when test="@type = 'DYNAMIC'">DECLARE_CUSTOM_HANDLER(handle_<xsl:value-of select="."/>)
</xsl:when>
		<xsl:otherwise>DECLARE_PROTO_SETTER(set_<xsl:value-of select="."/>)
</xsl:otherwise>
	</xsl:choose>
	</xsl:template>


	<xsl:template match="my:min/my:handler|my:max/my:handler|my:size/my:handler" mode="handler_proto">
		<xsl:call-template name="handler_proto_get"/>
	</xsl:template>

	<xsl:template match="my:property/my:handler" mode="handler_proto">
		<xsl:if test="not(../my:regref) and not(../my:value)">
		<xsl:choose>
			<xsl:when test="name(../..) = 'struct'">
		<xsl:text>// HANDLER for struct '</xsl:text><xsl:value-of select="../../@name"/>.<xsl:value-of select="../@name"/>
		<xsl:text>'
</xsl:text>
				<xsl:choose>
					<xsl:when test="not(../@access) or ../@access = 'RW' or ../@access = 'RO'">
		<xsl:call-template name="handler_proto_get"/>
					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="not(../@access) or ../@access = 'RW' or ../@access = 'WO'">
		<xsl:call-template name="handler_proto_set"/>
					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
				</xsl:choose>
						
			</xsl:when>

			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="not(../@access) or ../@access = 'RW' or ../@access = 'RO'">
						<xsl:text>// HANDLER for '</xsl:text>
							<xsl:value-of select="../@name"/>
						<xsl:text>'
</xsl:text>
		<xsl:call-template name="handler_proto_get"/>

					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="not(../@access) or ../@access = 'RW' or ../@access = 'WO'">
		<xsl:call-template name="handler_proto_set"/>
					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
				</xsl:choose>
	
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
	</xsl:template>


	<!-- Event descriptor initialization -->

	<xsl:template match="my:event" mode="evt_enum">
		<xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<xsl:template match="my:event" mode="evt_init">
	{ "EVT_" "<xsl:value-of select="@target"/>", /* <xsl:value-of select="generate-id()"/> */ F_RO,
		DC_EVENT,
		DC_STATIC, { .event = { DC_<xsl:value-of select="@type"/>, TOKEN_<xsl:value-of select="@target"/> } }
	},
	</xsl:template>


	<!-- direct access token definitions -->
	<xsl:template match="my:property|my:struct" mode="deftoken">
	<xsl:if test="@id">#define TOKEN_<xsl:value-of select="@id"/> BUILD_TOKEN(DEVICE_TOKEN(<xsl:value-of select="count(ancestor::my:device/preceding-sibling::my:device)"/>), <xsl:value-of select="generate-id()"/>)
</xsl:if>
	</xsl:template>
	<!-- direct access via external globals -->
	<xsl:template match="my:property|my:struct" mode="exttoken">
		<xsl:if test="@id">TOKEN_DEFINE(<xsl:value-of select="@id"/>
		<xsl:text>)
</xsl:text>
		</xsl:if>
	</xsl:template>

	<!-- Struct declaration:
	     This creates a sequence of member id's (statically)
		 -->

	<xsl:template match="my:struct/my:property|my:struct/my:array" mode="memberlist">
		 <xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<xsl:template match="my:struct" mode="struct_decl">
// Struct "<xsl:value-of select="@name"/>"
<xsl:value-of select="$attribute"/> static PROPINDEX
g_struct_<xsl:value-of select="generate-id()"/>[] = {
	<xsl:apply-templates select="./my:property|./my:array" mode="memberlist"/>
	<xsl:text>INVALID_INDEX
};
</xsl:text>
	</xsl:template>

	<xsl:template match="my:group[not(@hidden='true')]/my:array|my:struct/my:array" mode="array_decl">
// Array "<xsl:value-of select="@name"/>"
<xsl:value-of select="$attribute"/> static PROPINDEX
g_array_<xsl:value-of select="generate-id()"/>[] = {
	<xsl:value-of select="generate-id(./my:size)"/> /* Size */,
	<xsl:choose>
		<xsl:when test="./my:array"><xsl:value-of select="generate-id(./my:array)"/></xsl:when>
		<xsl:when test="./my:struct"><xsl:value-of select="generate-id(./my:struct)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="generate-id(./my:property)"/></xsl:otherwise>
	</xsl:choose>,
	<xsl:text>INVALID_INDEX
};
</xsl:text>
	</xsl:template>

	<!-- Initialization of Property tables -->

	<xsl:template match="my:property" mode="prop_init">
	{ "<xsl:value-of select="@name"/>" /* <xsl:value-of select="generate-id()"/>
		<xsl:text> */, </xsl:text>
		<xsl:if test="@volatile">F_VOLATILE | </xsl:if>
		<xsl:if test="@hidden = 'true'">F_PRIVATE | </xsl:if>
		<xsl:if test="./my:event">F_ACTION | </xsl:if>
		<xsl:choose>
			<xsl:when test="@access">F_<xsl:value-of select="@access"/></xsl:when>
			<xsl:otherwise>F_RW</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="./my:regref"><xsl:apply-templates select="./my:regref" mode="endiancode"/></xsl:if>
		<xsl:text>,
		DC_</xsl:text>
	 <xsl:value-of select="@type"/>, 	
		<xsl:choose>
			<xsl:when test="./my:regref">DC_ADDR, { .reg = { Reg_<xsl:value-of select="./my:regref/@ref"/>, <xsl:apply-templates select="./my:regref"/> } },
			</xsl:when>
			<xsl:when test="./my:variable[not(@member='true')]"><xsl:call-template name="var_lang"/></xsl:when>
			<xsl:when test="./my:variable[@member='true']"><xsl:call-template name="var_heap"/></xsl:when>
			<xsl:when test="./my:value"><xsl:apply-templates select="./my:value"/></xsl:when>
			<xsl:otherwise><xsl:call-template name="generic_handler"/></xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<!--
			<xsl:when test="./my:limits">g_children<xsl:value-of select="generate-id(./my:limits)"/></xsl:when>
			<xsl:when test="./my:choice">g_choice_<xsl:value-of select="generate-id(./my:choice)"/></xsl:when>
			-->
			<xsl:when test="./my:choice or ./my:event or ./my:limits">g_children_<xsl:value-of select="generate-id()"/></xsl:when>

			<xsl:when test="@type = 'BUFFER'"><xsl:text>		0</xsl:text></xsl:when>
			<xsl:otherwise><xsl:text>		NULL_CHILDREN</xsl:text></xsl:otherwise>
		</xsl:choose>
	<xsl:text> },
</xsl:text>
	</xsl:template>
	<xsl:template match="my:struct" mode="prop_init">
	{ "<xsl:value-of select="@name"/>" /* <xsl:value-of select="generate-id()"/>
		<xsl:text> */, </xsl:text>
		<xsl:if test="@hidden = 'true'">F_PRIVATE | </xsl:if>
		<xsl:choose>
			<xsl:when test="@access">F_<xsl:value-of select="@access"/></xsl:when>
			<xsl:otherwise>F_RW</xsl:otherwise>
		</xsl:choose>,
		DC_STRUCT, 
		DC_HANDLER, { .func = {0, 0} },
			g_struct_<xsl:value-of select="generate-id()"/>, },
	</xsl:template>

	<!-- Unit mapping when no offset specified: -->
	<xsl:template match="my:group" mode="unit_map">
	<xsl:text>/* Unit mapping defines, when UNIT_MAP specified */
</xsl:text>
	<xsl:text>#define UNIT_ADDR_SHIFT RESOLVE_BITFIELD(</xsl:text>
	<xsl:apply-templates select="my:property/my:regref"/>
	<xsl:text>)
</xsl:text>

	<xsl:apply-templates select="my:property/my:choice/my:item" mode="unit_map" />

	</xsl:template>



	<xsl:template match="my:group[not(@hidden='true')]/my:array|my:struct/my:array" mode="prop_init">
	{ "<xsl:value-of select="@name"/>" /* <xsl:value-of select="generate-id()"/>
		<xsl:text> */, </xsl:text>
		<xsl:if test="@hidden = 'true'">F_PRIVATE | </xsl:if>
		<xsl:choose>
			<xsl:when test="@access">F_<xsl:value-of select="@access"/></xsl:when>
			<xsl:otherwise>F_RW</xsl:otherwise>
		</xsl:choose>,
		DC_ARRAY, 
		DC_HANDLER,
		<xsl:choose>
			<xsl:when test="./my:regref">{ .array = { <xsl:apply-templates select="key('regkey', ./my:regref/@ref)" mode="refRegsize"/>, (ADDR_OFFSET) Reg_<xsl:value-of select="./my:regref/@ref"/> } }, 
			</xsl:when>
			<xsl:otherwise>{ .array = { 1, 0x0000 } },</xsl:otherwise>
		</xsl:choose> g_array_<xsl:value-of select="generate-id()"/> },
		<!-- 
		<xsl:choose>
			<xsl:when test="./my:struct">g_struct_<xsl:value-of select="generate-id(./my:struct)"/></xsl:when>
			<xsl:when test="./my:array">g_array_<xsl:value-of select="generate-id(./my:array)"/></xsl:when>
			<xsl:otherwise>g_array_<xsl:value-of select="generate-id(./my:property)"/></xsl:otherwise>
		</xsl:choose> },
		-->
	</xsl:template>

	<!-- handle device types -->
	<xsl:template match="my:device" mode="dev_enum">{ g_props_<xsl:value-of select="@id"/>, <xsl:value-of select="./my:revision/my:major"/>, <xsl:value-of select="./my:revision/my:minor"/>,  MAXPROP_<xsl:value-of select="@id"/> },
	</xsl:template>

	<!-- device index enumeration -->
	<xsl:template match="my:device" mode="dev_index">g_index_<xsl:value-of select="@id"/>,
	</xsl:template>

<!-- Warning: The templates 'groupenum' and 'groupprops' must ensure that
     enumeration and the index match. Otherwise, the code will fail
     miserably. Don't touch these, unless you really have to. -->

	<xsl:template match="my:group" mode="groupenum">
/* Group '<xsl:value-of select="@name"/>' */
	<xsl:apply-templates select=".//my:property|.//my:struct|.//my:array" mode="enum">
		<!-- NO more sorting
		<xsl:sort select="@name" order="ascending"/>
-->
	</xsl:apply-templates>
	// Items:
	<xsl:apply-templates select=".//my:item" mode="item_enum"/>
	// Limits
	<xsl:apply-templates select=".//my:min|.//my:max" mode="var_enum"/>
	// Sizes
	<xsl:apply-templates select=".//my:size" mode="var_enum"/>
	<!-- TODO: Variable size array support -->
	// Events
	<xsl:apply-templates select=".//my:event" mode="evt_enum"/>

	</xsl:template>

	<xsl:template match="my:group" mode="groupprops">
////////////////////////////////////////////////////////////////////////////
/* Group '<xsl:value-of select="@name"/>' */

	/* Properties */
	<xsl:apply-templates select=".//my:property|.//my:struct|.//my:array" mode="prop_init">
		<!-- NO more sorting
		<xsl:sort select="@name" order="ascending"/>
-->
	</xsl:apply-templates>
	/* Items */
	<xsl:apply-templates select=".//my:item" mode="item_init"/>
	/* Limits */
	<xsl:apply-templates select=".//my:min|.//my:max" mode="limits_init"/>
	/* Size variables */
	<xsl:apply-templates select=".//my:size" mode="var_init"/>
	/* Event descriptors */
	<xsl:apply-templates select=".//my:event" mode="evt_init"/>
	</xsl:template>

	<xsl:template match="my:group" mode="grouplist">
////////////////////////////////////////////////////////////////////////////
/* Group '<xsl:value-of select="@name"/>' */
////////////////////////////////////////////////////////////////////////////
// Struct lists:
<xsl:apply-templates select=".//my:struct" mode="struct_decl"/>

////////////////////////////////////////////////////////////////////////////
// Array lists:
<xsl:apply-templates select=".//my:array" mode="array_decl"/>

////////////////////////////////////////////////////////////////////////////
// Children lists:
<xsl:apply-templates select=".//my:property" mode="prop_children"/>

	</xsl:template>

	<xsl:template match="my:group" mode="groupindex">
/* Group '<xsl:value-of select="@name"/>' */
	<xsl:apply-templates select="./my:property" mode="enum">
		<!-- NO more sorting
		<xsl:sort select="@name" order="ascending"/>
-->
	</xsl:apply-templates>
	<xsl:apply-templates select="./my:struct" mode="enum"/>
	<xsl:apply-templates select="./my:array" mode="enum"/>

	</xsl:template>

	<xsl:template match="my:device" mode="dev_init">
////////////////////////////////////////////////////////////////////////////
// DEVICE: <xsl:value-of select="@name"/>, ID: <xsl:value-of select="@id"/>
////////////////////////////////////////////////////////////////////////////


// PROPERTY KEYS
enum {
	<xsl:value-of select="@id"/>, // Device Root node
	<xsl:apply-templates select=".//my:group[not(@hidden='true')]" mode="groupenum"/>
	MAXPROP_<xsl:value-of select="@id"/> // = Number of Properties
};

	<xsl:apply-templates select=".//my:group[not(@hidden='true')]" mode="grouplist"/>


////////////////////////////////////////////////////////////////////////////
// Register defines

<xsl:apply-templates select="my:group[@name='UNIT_MAP']" mode="unit_map"/>
<xsl:apply-templates select="my:registermap" mode="reg_decl"/>

////////////////////////////////////////////////////////////////////////////
// Feature tables

// ROOT LIST (Top level nodes)

<xsl:value-of select="$attribute"/> static
PROPINDEX
g_root_<xsl:value-of select="@id"/>[] = {
	<xsl:apply-templates select=".//my:group[not(@hidden='true')]" mode="groupindex"/>
	INVALID_INDEX
};

<xsl:value-of select="$attribute"/> static
PropertyDesc
g_props_<xsl:value-of select="@id"/>[] = {

	/* ROOT NODE (always on top of the list!) */

	{ "<xsl:value-of select="@name"/>", F_RO <xsl:if test="@baseclass"><xsl:value-of select="@size"/> | F_LINK</xsl:if>,
		DC_ROOT, DC_STATIC, { .base = <xsl:choose>
			<xsl:when test="@baseclass">g_index_<xsl:value-of select="@baseclass"/></xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose> } , g_root_<xsl:value-of select="@id"/> },
	<xsl:apply-templates select=".//my:group[not(@hidden='true')]" mode="groupprops"/>
	{ 0 }
};

</xsl:template>

<xsl:template match="my:header">
<xsl:if test="@language = 'C' or not(@language)">
<xsl:value-of select="."/>
</xsl:if>
</xsl:template>

<xsl:template match="/">
/**************************************************************************
 *
 * This file was generated by dclib/netpp. Modifications to this file will
 * be lost.
 * Stylesheet: proplist.xsl     (c) 2005-2011 section5
 *
 * Version: <xsl:value-of select="my:devdesc/my:device/my:revision/my:major"/>.<xsl:value-of select="my:devdesc/my:device/my:revision/my:minor"/><xsl:value-of select="my:devdesc/my:device/my:revision/my:extension"/>
 *
 **************************************************************************/

#include "property_types.h"

<xsl:apply-templates select=".//my:header"/>
#define NULL_CHILDREN  0  /* Property has no children */

#ifndef REGISTERMAP_OFFSET
#define REGISTERMAP_OFFSET(x) (x)
#endif

#ifndef TOKEN_DEFINE
#define TOKEN_DEFINE(id) TOKEN g_t_##id = TOKEN_##id;
#endif

#ifndef BEGIN_EXTERNAL_TOKEN_DEFINITION
#define BEGIN_EXTERNAL_TOKEN_DEFINITION
#endif

#ifndef END_EXTERNAL_TOKEN_DEFINITION
#define END_EXTERNAL_TOKEN_DEFINITION
#endif

#define RESOLVE_BITFIELD(x, y, z) (x)

#define DECLARE_CUSTOM_HANDLER(func)  int func(void *p, int rw, DCValue *inout);
#define DECLARE_PROTO_GETTER(func)    int func(DEVICE d, DCValue *out);
#define DECLARE_PROTO_SETTER(func)    int func(DEVICE d, DCValue *in);

#define STRUCT_EXPORT(container, member) \
	offsetof(container, member)


////////////////////////////////////////////////////////////////////////////
// LIBRARY VERSION CONTROL

<xsl:value-of select="$attribute"/> char g_vendor[]   = "<xsl:value-of select="my:devdesc/my:vendor"/>";

<xsl:value-of select="$attribute"/> int g_version[2] = { <xsl:value-of select="my:devdesc/my:revision/my:major"/>, <xsl:value-of select="my:devdesc/my:revision/my:minor"/> };

<xsl:value-of select="$attribute"/> char g_build[]   = "<xsl:value-of select="my:devdesc/my:revision/my:extension"/>";

////////////////////////////////////////////////////////////////////////////
// DIRECT PROPERTY ACCESS

<xsl:apply-templates select=".//my:struct" mode="deftoken"/>
<xsl:apply-templates select=".//my:property" mode="deftoken"/>

////////////////////////////////////////////////////////////////////////////
// PROTOTYPES

<xsl:apply-templates select=".//my:property/my:handler" mode="handler_proto"/>
<xsl:apply-templates select=".//my:min/my:handler|.//my:max/my:handler|.//my:size/my:handler" mode="handler_proto"/>
////////////////////////////////////////////////////////////////////////////
// DEVICE DECLARATIONS


enum {
	<xsl:apply-templates select="//my:device" mode="dev_index"/>
	N_DEVICES
};

<xsl:apply-templates select="//my:device" mode="dev_init"/>

BEGIN_EXTERNAL_TOKEN_DEFINITION // {

<xsl:apply-templates select=".//my:struct" mode="exttoken"/>

<xsl:apply-templates select=".//my:property" mode="exttoken"/>
END_EXTERNAL_TOKEN_DEFINITION // }

////////////////////////////////////////////////////////////////////////////
// SUPPORTED DEVICES


<xsl:value-of select="$attribute"/> DeviceDesc g_devices[] = {
	<xsl:apply-templates select="//my:device" mode="dev_enum"/>
};

<xsl:value-of select="$attribute"/> int g_ndevices = N_DEVICES;

////////////////////////////////////////////////////////////////////////////

<xsl:text>
</xsl:text>
	</xsl:template>
</xsl:stylesheet>
