/* Slave dispatcher packet queue
 *
 * (c) 2013, 2014, Martin Strubel <hackfin@section5.ch>
 *
 */

#include <string.h>
#include <stdlib.h>
#include "property_protocol.h"
#include "devlib_error.h"
#include "queue.h"

#ifndef ENTER_CRITICAL_PUT
#define ENTER_CRITICAL_PUT ENTER_CRITICAL
#endif

#ifndef LEAVE_CRITICAL_PUT
#define LEAVE_CRITICAL_PUT LEAVE_CRITICAL
#endif

#ifndef ENTER_CRITICAL

#define ENTER_CRITICAL(p) \
	if (mutex_lock(&p->mutex) < 0) return DCERR_MUTEX;

#define LEAVE_CRITICAL(p) \
	if (mutex_unlock(&p->mutex) < 0) return DCERR_MUTEX;
#endif

// Must be power of two:

#ifndef NO_MALLOC
PacketQueue *queue_new(void)
{
	PacketQueue *q;

	q = (PacketQueue *) malloc(sizeof(PacketQueue));
	if (q) {
		queue_init(q);
	}
	return q;
}
#endif

int queue_put(PacketQueue *pq, void *p, int len)
{
	void *dst;
	int ret = 0;
	int i;

	i = pq->head;
	dst = (void *) &pq->q[i];

	pq->size[i] = len;
	DEB(netpp_log(DCLOG_VERBOSE, "Queue packet @%p[%d] %d", dst, len, i));
	if (len > 0) memcpy(dst, p, len);
	if (pq->head == pq->tail) ret = 1; // Overrun

	ENTER_CRITICAL_PUT(pq);
	pq->head = (pq->head + 1) & (Q_SIZE - 1);
	LEAVE_CRITICAL_PUT(pq);
	return ret;
}

void queue_init(PacketQueue *pq)
{
	pq->state = Q_IDLE;
	pq->head = 
	pq->tail = 0;
	MAYBE(mutex_init(&pq->mutex));
}

void queue_exit(PacketQueue *pq)
{
	MAYBE(mutex_exit(&pq->mutex));
}

int queue_ptr(PacketQueue *pq, void **p)
{
	*p = &pq->q[pq->head];
	return 0;
}

int queue_commit(PacketQueue *pq, int len)
{
	int i;
	i = (pq->head + 1) & (Q_SIZE - 1);
	ENTER_CRITICAL(pq);
	pq->size[pq->head] = len;
	pq->head = i;
	if (i == pq->tail) i = 1; // Almost full
	else               i = 0;
	LEAVE_CRITICAL(pq);
	return i;
}

int queue_get(PacketQueue *pq, void **p, int *len)
{
	int ret = 0;
	int i;
	ENTER_CRITICAL(pq);
	i = pq->tail;
	if (i != pq->head) {
		*p = &pq->q[i];
		ret = pq->size[i];
		if (ret >= 0) {
			*len = ret;
		}
	}
	LEAVE_CRITICAL(pq);
	return ret;
}

#ifndef SWAPTYPES_DEFINED
// Not supported for SWAP:
int queue_set(PacketQueue *pq, void *p, int len)
{
	int ret = 0;
	int i;

	i = pq->head;

	pq->q[i] = p;
	pq->size[i] = len;

	if (pq->head == pq->tail) ret = 1; // Overrun

	ENTER_CRITICAL_PUT(pq);
	pq->head = (pq->head + 1) & (Q_SIZE - 1);
	LEAVE_CRITICAL_PUT(pq);
	return ret;
}
#endif

int queue_pop(PacketQueue *pq)
{
	int i;
	ENTER_CRITICAL(pq);
		i = pq->tail;
		i = (i + 1) & (Q_SIZE - 1);
		pq->tail = i;
	LEAVE_CRITICAL(pq);
	return 0;
}
