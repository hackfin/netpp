////////////////////////////////////////////////////////////////////////////
//
// - Platform dependent settings plus configuration of exported
// symbols
// 
// (c) 2005, section5
//
// $Id: platform.h 334 2015-02-28 10:20:53Z strubi $
//
////////////////////////////////////////////////////////////////////////////


#ifndef PLATFORM_H_INCLUDED
#define PLATFORM_H_INCLUDED

#ifdef __MSP430__
#define ADDRESS uint16_t
#else
#define ADDRESS uint32_t
#endif


// Allow to allocate some local chunks not on the stack
#ifdef TINYCPU
#define MAYBE_STATIC static
#else
#define MAYBE_STATIC
#endif

#define MAYBE_VOLATILE volatile

#ifdef DISABLE_CONST
#define MAYBE_CONST
#else
#define MAYBE_CONST const
#endif

#ifndef HASNOT_STDINT
#include <stdint.h>
#elif defined(PROPRIETARY_CTYPES)
#include <c_types.h>
#else
#include "arch.h"
#endif

#if defined(WIN32) || defined(__CYGWIN__)
#	define WIN32_LEAN_AND_MEAN		// Exclude crap
#	define NO_COLOR
#	include <windows.h>
#endif
#ifndef BYTE
#define BYTE unsigned char
#endif
#ifndef NULL
#	define NULL 0
#endif

#ifdef TINYCPU
typedef unsigned int   puint_t;
typedef int            pint_t;
typedef short          pmode_t;
#else
typedef uint32_t       puint_t;
typedef int32_t        pint_t;
typedef uint16_t       pmode_t;
#endif


/* Macros for platforms that have malloc() */
#ifdef NO_MALLOC
char *buf_alloc(int n);
void buf_free(char *buf);
struct DCDevice *rdev_alloc(int n);
void rdev_dealloc(struct DCDevice *r);
#else
#include <stdlib.h>
#define rdev_alloc(x)   (RemoteDevice *) malloc(x * sizeof(RemoteDevice))
#define rdev_dealloc(x)                  free(x)
#define buf_alloc(x)            (char *) malloc(x)
#define buf_free(x)                      free(x)
#endif

#if defined(__linux__) || defined(__APPLE__) || defined(__QNX__)
#	ifndef HANDLE
#		define HANDLE int
#	endif
#	define DWORD unsigned long
#	include <stddef.h> // offsetof
#	include <unistd.h>
#	define Sleep(x) usleep(1000 * x)
#endif

#if defined(STANDALONE)
void Sleep(int ms); // External
#endif

#if defined(__QNX__)
#	include <strings.h>
#endif

#if defined(WIN32) && !defined(__CYGWIN__)
#	define strcasecmp _stricmp
#	define strncasecmp _strnicmp
#else
#	include <string.h>
#endif

#ifdef WIN32
#	define DLLHANDLE  HINSTANCE
#	define COMMHANDLE HANDLE
#else 
#	define DLLHANDLE  void *
#	define COMMHANDLE int
#endif

#ifdef WIN32
#define in_addr_t unsigned long
#endif

#endif // PLATFORM_H_INCLUDED
