/** Handler example code.
 *
 * All handlers (getters and setters) start with get_ respectively with
 * set_.
 *
 * Note that for readonly/writeonly properties, only the relevant handler
 * functions need to be specified.
 *
 */

#include "devlib.h"
#include "devlib_error.h"
#include "example.h"
#include "property_protocol.h"
#ifndef __ZPU__
#include <stdio.h>           // For debugging only

#include <stdlib.h>           // For debugging only
#endif

// Global variables exposed to property access:

struct my_globals g_globals = {
	.zoom = 0.5,
#ifndef TINYCPU
	.buffer = { 0x0de, 0xad, 0xbe, 0xef, 0x00,  },
	.bufsize = BUFSIZE,
#endif
	.string = "Default",
	.mode = 0,
	.count = 0,
	.flag = 0,
	.demo = 42
};

int32_t g_array[16] = {
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
};

#ifndef TINYCPU
ImgBuffer     g_imgbuf;
#endif

// Size check makros:

#define SIZE_CHECK_GT(inout, bufvar)  \
	if (inout->len > sizeof(bufvar)) { \
		inout->len = sizeof(bufvar); \
		return DCERR_PROPERTY_SIZE_MATCH; \
	}

#define SIZE_CHECK_LT(inout, bufvar)  \
	if (inout->len < sizeof(bufvar)) { \
		inout->len = sizeof(bufvar); \
		return DCERR_PROPERTY_SIZE_MATCH; \
	}


static int g_progress = 100;

/* Handler for property 'Start' */
	
int set_stream_start(DEVICE d, DCValue *in)
{
	netpp_log(DCLOG_VERBOSE, "Set 'stream_start' x: %d y: %d",
		g_imgbuf.x, g_imgbuf.y);

	// Couple with progress bar
	g_progress = 0;
	return DCWARN_PROPERTY_EVENT;
}
		
/* Handler for property 'Stop' */
	
int set_stream_stop(DEVICE d, DCValue *in)
{
	netpp_log(DCLOG_VERBOSE, "Set 'stream_stop'");
	return 0;
}
		
#ifndef TINYCPU

/** Buffer read handler
 *
 * A buffer read handler typically reacts to two mechanisms:
 *
 * - Address query (out->type == DC_BUFFER): The property handler
 *   wants to know where to fetch the data.
 * - Buffer release (in->type == DC_COMMAND): The property handler has
 *   sent all data and notifies the owner (via this handler) that all data
 *   was transferred.
 *
 */

	
int get_buffer(DEVICE d, DCValue *out)
{
	int warn = 0;

	switch (out->type) {
		case DC_COMMAND:  // This is a buffer update action
			netpp_log(DCLOG_VERBOSE, "Release buffer");
			g_globals.count++;
			break;
		case DC_UNDEFINED:
		case DC_BUFFER:
			// You must do a buffer size check here:
			netpp_log(DCLOG_VERBOSE, "Get buffer, len %d", out->len);
	
			// Set data gathering pointer:
			out->value.p = g_globals.buffer;

			// We could use this macro, IF we had a static buffer size:
			// SIZE_CHECK_LT(out, buffer)
			if (out->len < g_globals.bufsize) {
				// In this scenario, we do not tolerate a request of
				// less than `bufsize` bytes. So we report back the correct
				// size and a size mismatch:
				out->len = g_globals.bufsize;
				return DCERR_PROPERTY_SIZE_MATCH;
			} else if (out->len > g_globals.bufsize) {
				out->len = g_globals.bufsize;
				// This warning explicitely reports to the caller,
				// that the request was served, but something has changed.
				// So, we tolerate a request of more data than available.
				warn = DCWARN_PROPERTY_MODIFIED;
			}

			break;
		default:
			return DCERR_PROPERTY_TYPE_MATCH;
	}
	return warn;
}

/** Buffer write handler
 *
 * A buffer write handler typically reacts to two mechanisms:
 *
 * - Address query (in->type == DC_BUFFER): The property handler
 *   wants to know where to store the incoming data.
 * - Buffer update (in->type == DC_COMMAND): The property handler has
 *   received all data and notifies the handler that all data has
 *   been transferred.
 */

int set_buffer(DEVICE d, DCValue *in)
{
	switch (in->type) {
		case DC_COMMAND:  // This is a buffer update action
			// Fill in update code
			netpp_log(DCLOG_VERBOSE, "Update buffer len %d", in->len);
			break;
		case DC_INVALID:
		case DC_BUFFER:
			// You must do a buffer size check here:
			netpp_log(DCLOG_VERBOSE, "Set buffer len %d", in->len);
			if (in->len > g_globals.bufsize) {
				in->len = g_globals.bufsize;
				return DCERR_PROPERTY_SIZE_MATCH;
			}

			// Tell engine where the data will go to:
			in->value.p = g_globals.buffer;
			break;
		default:
			return DCERR_PROPERTY_TYPE_MATCH;
	}

	return 0;
}

#else

int get_buffer(DEVICE d, DCValue *out)
{
	return DCERR_PROPERTY_HANDLER;
}

int set_buffer(DEVICE d, DCValue *in)
{
	return DCERR_PROPERTY_HANDLER;
}

#endif

static float g_val = 3.1415926;

int set_floatval(DEVICE d, DCValue *in)
{
	g_val = in->value.f;
	netpp_log(DCLOG_VERBOSE, "Set float value to %f", g_val);
	return 0;
}

int get_floatval(DEVICE d, DCValue *out)
{
	netpp_log(DCLOG_VERBOSE, "Get float value %f", g_val);
	out->value.f = g_val;
	return 0;
}


int get_dyndata(DEVICE d, DCValue *out)
{
	int l;
	const char *str = "<This string is always reset>";
	// Note: After exit of this function, the local variables don't live
	// anymore. But the string above does, even if not declared static.
	// So the following assignment is ok.
	out->value.p = (void *) str;
	l = strlen(str) + 1;
	// Always feed back length:
	out->len = l;

	if (out->len < l) {
		fprintf(stderr, "Report client side buffer requirement: %lu bytes\n",
			out->len);
		return DCERR_PROPERTY_SIZE_MATCH;
	}
	return 0;
}

int get_dyn_max(DEVICE d, DCValue *out)
{
	out->value.i = g_globals.demo;
	return 0;
}

#ifndef TINYCPU

/** This is an example how to stream temporar dynamic data */
int set_dyndata(DEVICE d, DCValue *in)
{
	int size;

	size = in->len;

	static char *data = 0;

	switch (in->type) {
		case DC_COMMAND:
			if (!data) break;
			printf("String size: %d\n", size);
			printf("\n------ begin -----\n");
			puts(data);
			printf("\n------ end -------\n");
			free(data); data = NULL;
			break;
		case DC_STRING:
			if (data) free(data);

			data = (char *) malloc(size);
			if (!data) return DCERR_MALLOC;

			in->value.p = data;
			break;
		default:
			return DCERR_PROPERTY_TYPE_MATCH;
	}
	return 0;
}

#else

int set_dyndata(DEVICE d, DCValue *in)
{
	return DCERR_PROPERTY_HANDLER;
}

#endif

char tmp_str[32];

// Dynamic Array size:
int get_size(DEVICE d, DCValue *out)
{
	out->value.i = 8;
	netpp_log(DCLOG_VERBOSE, "get dynamic size");
	return 0;
}

// Array item handler:
int set_item(DEVICE d, DCValue *in)
{
	switch (in->type) {
		case DC_STRING:
			// Tell engine where string should go
			in->value.p = tmp_str;
			// ..and how long it can be:
			in->len = sizeof(tmp_str);
			// Note: This string must not live on the stack!
			// Otherwise -> BOOM
			break;
		default:
			printf("Set item[%d] to %s\n", in->index, tmp_str);
	}
	return 0;
}

static
char g_str[16];

int get_item(DEVICE d, DCValue *out)
{
	int n;
	int error = 0;
	/* Warning: Stuff on the stack does not live past function
     * exit! Therefore: Only do this with static strings. */
	// Python hook to report size:
	n = strlen("item00_");
	if (out->len < n) {
		out->len = n;
		error = DCERR_PROPERTY_SIZE_MATCH;
	} else
	if (out->type == DC_STRING) {
		sprintf(g_str, "item_%d", out->index);
		out->type = DC_STRING;
		n = strlen(g_str)+1;
		if (out->len > n) {
			printf("Notify client that we send less than requested\n");
			error = DCWARN_PROPERTY_MODIFIED;
		}
		out->len = n;
		out->value.p = g_str;
	}
	return error;
}

/** Dynamic error handling. Implement yourself :-) */
int get_retcode_code(DEVICE d, DCValue *out)
{
	netpp_log(DCLOG_VERBOSE, "Get 'retcode_code'");
	return 0;
}

int set_retcode_code(DEVICE d, DCValue *in)
{
	netpp_log(DCLOG_VERBOSE, "Set 'retcode_code'");
	return 0;
}
		
/* Handler for property 'Description' */
	
int get_retcode_description(DEVICE d, DCValue *out)
{
	netpp_log(DCLOG_VERBOSE, "Get 'retcode_description'");
	return 0;
}

int get_pin_func(DEVICE d, DCValue *out)
{
	// Fake pin mode according to index:
	if (out->index < 4) {
		out->value.u = 1;
	} else {
		out->value.u = 3;
	}
	return 0;
}

static char *s_pinmode[4] = {
	"IN", "OUT", "ANALOG", "ALTERNATE"
};

static uint16_t port_dir = 0;
static uint16_t port_val = 0;

int get_pin_val(DEVICE d, DCValue *out)
{
	out->value.i = (port_val >> out->index) & 1;
	return 0;
}

int set_pin_val(DEVICE d, DCValue *in)
{
	uint16_t val = port_val;

	if (in->value.i) {
		val |= (1 << in->index);
	} else {
		val &= ~(1 << in->index);
	}
	netpp_log(DCLOG_NOTICE, "Set Pin[%d] => %04x", in->index, val);
	port_val = val;
	return 0;
}

int set_pin_func(DEVICE d, DCValue *in)
{
	uint16_t val = port_dir;

	switch (in->value.i) {
		case 0:
			val &= ~(1 << in->index);
			port_dir = val;
			break;
		case 1:
			val |= (1 << in->index);
			port_dir = val;
			break;
		case 2:
		case 3:
			break;
		default:
			return DCERR_PROPERTY_HANDLER;

	}
	netpp_log(DCLOG_NOTICE, "Pin[%d] => %s",
		in->index, s_pinmode[in->value.i & 3]);
	return 0;
}


int set_retcode_description(DEVICE d, DCValue *in)
{
	switch (in->type) {
		case DC_COMMAND:  // This is a buffer update action
			// Fill in update code
			break;
		case DC_STRING:
		case DC_BUFFER:
			// Here you have to obtain the pointer to the 
			// (static) description
			break;
		default:
			return DCERR_PROPERTY_TYPE_MATCH;
	}

	return 0;
}


int dump_hub(PortToken hub, int level)
{
	PortBay *base = GET_PORTROOT();
	DynPropertyDesc *pd, *child;
	int error;
	const char delim[] = "########################################";

	PortToken next = PORTTOKEN_NIL;

	while (ITERATOR_VALID(hub)) {
		pd = desc_fromtoken(hub);
		puts(delim);
		printf("#  Hub Node[%08x]: %s\n", hub, pd->name);
		error = port_select(hub, next, &next);
		while (error == 0) {
			child = desc_fromtoken(next);
			if (child->type == DC_PORT) {
				printf("[P %s  ]\n", child->name);
			} else {
				printf("[D %s ]\n", child->name);
			}
			error = port_select(hub, next, &next);
		}
		hub = ITERATOR_NEXT(base, pd);
	}
	puts(delim);
	return 0;
}

int set_dump_users(DEVICE d, DCValue *in)
{
	// Retrieve peer device token:
	PortToken hub;

	// Hack to get parenting hub from node ID:
	hub = MAKE_PORTID(0, HUBINDEX_FROMTOKEN(d->peer.user.node) - 1);
	netpp_log(DCLOG_DEBUG, "Peer node: %08x\n", hub);

	if (in->type == DC_COMMAND) {
		return dump_hub(hub, in->value.i);
	} else return DCERR_PROPERTY_TYPE_MATCH;


}

int set_update(DEVICE d, DCValue *in)
{
	return DCWARN_PROPERTY_EVENT;
}


/** This is a complicated one.
 *
 * The log buffer is 80 bytes maximum. However, when writing a string,
 * the effective length can be less.
 * A client requesting less than the log buffer is told to reserve
 * at minimum the size of the log buffer.
 * Upon next call however, less may be returned.
 */

int get_log(DEVICE d, DCValue *out)
{
	int error = 0;
	static char logbuf[80];
	size_t len;
	static int count = 0;
	switch (out->type) {
		case DC_COMMAND:  // This is a buffer update action
			// Fill in update code
			break;
		case DC_STRING:
			if (count % 5 == 0) {
				sprintf(logbuf, "<%04x> This is a fake log message",
					count & 0xffff);
				out->value.p = logbuf;
			} else {
				out->value.p = "<idle>";
			}
			// Get effective len:
			len = strlen(out->value.p) + 1;
			// If we reserved too much, warn:
			if (out->len < sizeof(logbuf)) {
				len = sizeof(logbuf);
				error = DCERR_PROPERTY_SIZE_MATCH;
			} else
			if (out->len > len) {
				error = DCWARN_PROPERTY_MODIFIED;
 				count++;
			} else {
				count++;
			}
			out->len = len;
			break;
		default:
			error = DCERR_PROPERTY_TYPE_MATCH;
	}
	return error;
}

int16_t random_value_between(int lower, int upper)
{
	int range = upper - lower;
	float tmp;
	unsigned int r;
	if (range < 0) {
		netpp_log(DCLOG_ERROR, "Negative range!");
		return 0;
	}
	// Value between -RAND_MAX/2 .. RAND_MAX/2:
	r = rand();

	tmp = (float) r * (float) range;
	tmp /= (float) RAND_MAX;
	return lower + (int16_t) tmp;
}

#define AMPL  200

static 
struct thermostat {
	int16_t centigrade;
	int16_t upper;
	int16_t lower;
} g_thermo = {
	2300, -AMPL, AMPL
};

int get_temperature(DEVICE d, DCValue *out)
{
	int16_t delta;
	// Create random
	if (g_thermo.centigrade < -800) {
		g_thermo.lower = g_thermo.centigrade + 800 + 4;
	} else
	if (g_thermo.centigrade > 12000) {
		g_thermo.upper = 12000 - g_thermo.centigrade + 4;
	} else {
		g_thermo.lower = -AMPL;
		g_thermo.upper = AMPL;
	}

	delta = random_value_between(g_thermo.lower, g_thermo.upper);
	g_thermo.centigrade += delta;

	out->value.f = (float) g_thermo.centigrade / 100.0;
	return 0;
}


int get_progress(DEVICE d, DCValue *out)
{
	int ret = DCWARN_PROPERTY_INACTIVE;
	if (g_progress < 100) {
		ret = DCWARN_PROPERTY_MODIFIED;
		g_progress += 10;
	}
	out->value.u = g_progress;
	netpp_log(DCLOG_VERBOSE, "Get progress");
	return ret;
}

#ifdef ALLOW_DYNAMIC_PROPERTIES
/** Netpp custom handler */
int handle_obj_count(void *p, int write, DCValue *val)
{
	struct Object *obj = (struct Object *) p;
	val->value.i = obj->count;
	return 0;
}

/** Netpp custom handler */
int handle_obj_user(void *p, int write, DCValue *val)
{
	struct Object *obj = (struct Object *) p;
	if (val->type == DC_STRING) {
		// Initialize proxy:
		val->value.p = obj->name;

		if (write) {
			if (val->type == DC_STRING) {
				SIZE_CHECK_GT(val, obj->name);
			}
		} else {
			
			// Not the most effective strategy:
			int l = strlen(obj->name) + 1;
			
			// If we reserved more space then needed, just return warn code:
			if (val->len > l) {
				val->len = l;
				return DCWARN_PROPERTY_MODIFIED;
			} else
			// Mandatory for clients not reserving enough space
			// (like python) in order to query the real length:
			if (val->len < l) {
				val->len = l; // report real length
				return DCERR_PROPERTY_SIZE_MATCH;
			}
		}
	} else {
		netpp_log(DCLOG_VERBOSE, "Commit string");
	}
	return 0;
}

#else

// Hack to satisfy build without dynamic property support

int handle_obj_count(void *p, int write, DCValue *val) { return 0; } 

int handle_obj_user(void *p, int write, DCValue *val) { return 0; } 

#endif
