#!/usr/bin/env python

# Test script to test stream server

import sys
import time
import netpp
import pytest
import subprocess

if __name__ == '__main__':
	SLAVE = "./test"
else:
	SLAVE = "devices/libslave/test"


@pytest.yield_fixture(autouse=True, scope='session')
def test_cleanup():
	global slave, dev, root
	slave = subprocess.Popen([SLAVE, "--port=2008"])
	time.sleep(0.5)
	dev = netpp.connect("TCP:localhost:2008")
	root = dev.sync()
	yield
	slave.terminate()

def test_slave():
	a = root.Butze.get()
	a = a + 1
	root.Knarf.set(a)
	b = root.Fnarf.get()
	print(b)
	assert b == 15

############################################################################
# Should be the last test, as it terminates the slave:
#
def test_terminate():
	if slave:
		slave.terminate()
		try:	
			# When slave terminated, this MUST fail
			dev = netpp.connect("TCP:localhost:5000")
			assert False
		except:
			# Failure is expected!
			pass


if __name__ == '__main__':
	test_slave()
	slave.terminate()
