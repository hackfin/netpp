# Test script to verify proper ref counts:
import netpp

d = netpp.connect("localhost")
d1 = netpp.connect("localhost")
r = d.sync()

del d # delete the associated device
print r.Extra.get() # Root node should still be valid
del r

r = d1.sync()
r.DumpUsers.set(1)
print r.desc()

