// Dummies, needed for symmetry:

#include "devlib.h" // Should leave
#include "devlib_error.h"
#include "devlib_types.h"
#include "property_protocol.h"

EXTERN_C_BEGIN

int
property_set(DEVICE d, TOKEN t, DCValue *val)
{
	return 0;
}

int
property_get(DEVICE d, TOKEN t, DCValue *val)
{
	return 0;
}

TOKEN property_parsename(DEVICE d, const char *propname, TOKEN token)
{
	return 0;
}

TOKEN property_select(DEVICE d, TOKEN parent, TOKEN prev)
{
	return 0;
}

int property_getdesc(DEVICE d, TOKEN t, DCValue *val)
{
	netpp_log(DCLOG_ERROR, "Not implemented on master side\n");
	return DCERR_PROPERTY_HANDLER;
}

int         property_getname(DEVICE d, TOKEN t, DCValue *value)
{
	netpp_log(DCLOG_ERROR, "Not implemented on master side\n");
	return 0;
}

TOKEN local_getroot(DEVICE d)
{
	return 0;
}
