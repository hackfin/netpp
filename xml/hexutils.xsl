<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"> 

<xsl:template name="str-pad">
	<xsl:param name="string" />
	<xsl:param name="pad-char" />
	<xsl:param name="str-length" />

	<xsl:choose>
		<xsl:when test="string-length($string) > $str-length">
			<xsl:value-of select="substring($string,1,20)" />   
		</xsl:when>
		<xsl:when test="string-length($string) = $str-length">
			<xsl:value-of select="$string" />   
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="str-pad">
				<xsl:with-param name="string" select="concat($pad-char,$string)" />
				<xsl:with-param name="pad-char" select="$pad-char" />
				<xsl:with-param name="str-length" select="$str-length" />
			</xsl:call-template>
		</xsl:otherwise>     
	</xsl:choose>   
</xsl:template>

<xsl:template name="hex2dec">
    <xsl:param name="num" />
    <xsl:param name="hex" select="translate($num,'abcdef','ABCDEF')"/>
    <xsl:param name="acc" select="0" />
    <xsl:choose>
        <xsl:when test="string-length($hex)">
            <xsl:call-template name="hex2dec">
                <xsl:with-param name="hex" select="substring($hex,2,string-length($hex))" />
                <xsl:with-param name="acc" select="$acc * 16 + string-length(substring-before('0123456789ABCDEF',substring($hex,1,1)))" />
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$acc" />
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="dec2hex">
    <xsl:param name="dec" />
    <xsl:if test="$dec >= 16">
        <xsl:call-template name="dec2hex">
            <xsl:with-param name="dec" select="floor($dec div 16)" />
        </xsl:call-template>
    </xsl:if>
    <xsl:value-of select="substring('0123456789ABCDEF', ($dec mod 16) + 1, 1)" />
</xsl:template>

<xsl:template name="shiftleft">
    <xsl:param name="val" />
    <xsl:param name="i" />
    <xsl:choose>
        <xsl:when test="$i > 0">
        <xsl:call-template name="shiftleft">
            <xsl:with-param name="val" select="$val + $val" />
            <xsl:with-param name="i" select="$i - 1" />
        </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
			<xsl:value-of select="$val" />
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="calcaddr">
    <xsl:param name="base" />
    <xsl:param name="offs" />
    <xsl:param name="padded-length" />

	<xsl:variable name="offset">
		<xsl:call-template name="hex2dec">
			<xsl:with-param name="num" select="substring($offs, 3)"/>
		</xsl:call-template>
	</xsl:variable>

	<xsl:variable name="mapoffset">
		<xsl:call-template name="hex2dec">
			<xsl:with-param name="num" select="substring($base, 3)"/>
		</xsl:call-template>
	</xsl:variable>

	<xsl:variable name="mmr_absolute">
		<xsl:call-template name="hex2dec">
			<xsl:with-param name="num" select="substring($mmr_base, 3)"/>
		</xsl:call-template>
	</xsl:variable>

	<xsl:variable name="hexval">
		<xsl:call-template name="dec2hex">
			<xsl:with-param name="dec" select="$mmr_absolute + $offset + $mapoffset" />
		</xsl:call-template>
	</xsl:variable>

	<xsl:call-template name="str-pad">
		<xsl:with-param name="string" select="$hexval"/>
		<xsl:with-param name="pad-char" select="0"/>
		<xsl:with-param name="str-length" select="$padded-length"/>
	</xsl:call-template>

</xsl:template>


</xsl:stylesheet> 
