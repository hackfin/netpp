/* Master utility functions
 *
 *
 */

#include "netpp.h"
#include <stdio.h>
int list_children(DEVICE d, TOKEN t, int recursive);
int print_type(DCValue *v);
int my_add_group(void *c, const char *name);
int my_set_property(void *c, const char *name, const char *val);
int handleError(int error);
int query_prop(DEVICE d, TOKEN t, DCValue *val);
int value_from_string(DEVICE d, TOKEN p, const char *str);

int dump_children(FILE *f, DEVICE d, TOKEN t, char *fullname, char *pos,
	int left, int mode);

