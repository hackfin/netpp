#include "property_types.h"

typedef struct netpp_cache {
	DlistPool pool;
	TOKEN root;
	int index;
	int n;
} Cache;


int cache_init(Cache *cache, int n);
int cache_clear(Cache *c);

struct netpp_cache *cache_new(int n);
#define cache_free(c) free(c)
int cache_add(struct netpp_cache *c, const char *id, TOKEN token);
TOKEN cache_find(struct netpp_cache *c, const char *id);
