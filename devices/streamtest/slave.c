/** \file slave.c
 *
 * Slave property dispatcher
 * Property protocol back end (embedded system, etc.)
 *
 * (c) 2006 Martin Strubel <hackfin@section5.ch>
 *
 */

#ifdef DEBUG
#define DEB(x) x
#endif

#include "netpp.h"
#include "slave.h"
#include "streamtest.h"
#include <stdio.h>



////////////////////////////////////////////////////////////////////////////
//

int device_write(DEVICE d,
		uint32_t addr, const unsigned char *buf, unsigned long size)
{
	return 0;
}

int device_read(DEVICE d,
		uint32_t addr, unsigned char *buf, unsigned long size)
{
	int error = 0;

	return error;
}

////////////////////////////////////////////////////////////////////////////
//

int main(int argc, char **argv)
{
	int error;

	// Important: Register property list
	register_proplist(g_devices, g_ndevices);
//
// This function must be called for example from MSVC
#ifndef C99_COMPLIANT
	init_properties();
#endif

	error = start_server(argc, argv);
	if (error)
		netpp_log(DCLOG_ERROR, "Server terminated with error:\n"
			"'%s'\n", dcGetErrorString(error));
	return error;
}

