/** \file netppplus.h
 *
 * C++ wrapper API for netpp
 */

/**
 * \defgroup Wrappers     C++ wrapper
 *
 * The C++ wrapper is a rather thin layer around the netpp C library.
 * It introduces three classes, listed with their C equivalents:
 *
 * -# Value: DCValue
 * -# Property: (DEVICE,TOKEN) compound
 * -# Device: DEVICE handle
 *
 * Some functions return an error code, some don't, but throw exceptions.
 * The reason for this difference and a somewhat missing beauty of the API:
 * Functions that can cause an exception are normally called at initialization
 * of the program, for example when the GUI is initialized. Thus, each GUI
 * element (text controller, etc.) is assigned a Property, or the property
 * token. If a failure occurs during this initialization, it is mostly
 * considered a reason to break early.
 *
 * During the program, Property::setValue() and Property::getValue()
 * actions normally occur. Therefore, these return error codes instead of
 * throwing exceptions, as these errors are less critical. Thus, for
 * stability over longer terms, it is rather desired to evaluate errors
 * instead of using an exception. This is debatable.
 *
 * Note that for scripting functionality, a python wrapper is available.
 * See python/ folder of the netpp distribution.
 *
 */

#include "netpp.h"

#include <string>

class Device;

/**
 * \addtogroup Wrappers
 * \{
 */

/** A runtime value class.
 * This is a basic wrapper around the DCValue union struct.
 *
 * You can assign a typical value directly to a Value, like:
 *
 * Value b = false;
 */

class Value : public DCValue
{
	friend class Device;
public:
	Value()                        { type = DC_INVALID; len = 0; }
	Value(bool b)                  { type = DC_BOOL;    len = 0;  value.i = b; }
	Value(short i)                 { type = DC_MODE;    len = 0;  value.i = i; }
	Value(unsigned int i)          { type = DC_UINT;    len = 0;  value.u = i; }
	Value(int i)                   { type = DC_INT;     len = 0;  value.i = i; }
	Value(long i)                  { type = DC_INT;     len = 0;  value.i = i; }
	Value(float f)                 { type = DC_FLOAT;   len = 0;  value.f = f; }
	Value(char c)                  { type = DC_COMMAND; len = 0;  value.i = c; }
	Value(const char *c)           { type = DC_STRING;  len = strlen(c) + 1; 
                                     value.p = (void *) c; }
	Value(const char *c, size_t n) { type = DC_STRING;  len = n;
                                     value.p = (void *) c; }
	Value(unsigned char *c, unsigned int n) 
	                               { type = DC_BUFFER;  len = n;
                                     value.p = (void *) c; }
	Value(char *c, int n)          { type = DC_BUFFER;  len = n;
                                     value.p = (void *) c; }

	float          asFloat()                 { return value.f; }
	unsigned int   asUInt()                  { return value.u; }
	int            asInt()                   { return value.i; }
	int           *asIntP()                  { return (int *) value.p; }
	int            toString(std::string &s);
	const char    *asConstString()           { return (const char *) value.p; }
	operator bool()                          { return asInt(); }
	operator long()                          { return asInt(); }
	operator int()                           { return asInt(); }
	operator float()                         { return asFloat(); }
};

/** Property class.
 * This consists of a device handle and a property token
 *
 */

class Property 
{
protected:
	DEVICE             device;
	TOKEN              token;

	void               blank()   { device = 0; token = TOKEN_INVALID; }
	Property           select(Property *previous);
public:
	// Auto-cleanup port root when no longer needed:
	~Property()         { if (device == DC_PORT_ROOT && token == 0)
	                      dcDeviceClose(device); }
	Property()         { device = 0; token = TOKEN_INVALID; }
	Property(DEVICE d, TOKEN t)   { device = d; token = t; }
	Property(class Device *d, TOKEN t);

	/** Returns device token */
	TOKEN              getToken()           { return token; }
	/** Returns associated device */
	DEVICE             getDevice()          { return device; }
	/** Retrieves property local name into string s */
	int                getName(std::string &s);
	/** Returns first child of this property
	 * \return A property which must be validated against a NIL property
	 *         using isValid().
	 */
	Property           getChild(void)          { return select(this); }
	/** Returns following sibling of given property
	 * \param prev     The previously selected property in the children
	 *                 hierarchy. 
	 * \return         A property which must be validated against a NIL
	 *                 property using isValid().
	 *
	 * \exception      A netpp error code
	 */
	Property           getNext(Property &prev) { return select(&prev); }
	/** Get Value prototype from peer. Fills in data types and lengths of
	 * value, but does not yet read the value. Used to determine the data
	 * type.
	 *
	 * \return         A netpp error code
	 */

	/** Finds a child by name in a property hierarchy
	 * \return         A netpp error code
	 */
	int                findChild(const char *name, Property &child);

	/** Gets target property for an event */
	Property           getTarget(Value &val);

	int                getProto(Value &val);
	/** Retrieves the property value.
	 * Note that in case of a DC_BUFFER or DC_STRING property, the Value
	 * must be preinitialized.
	 * \return         A netpp error code
	 */
	int                getValue(Value &val);
	/** Sets the property value
	 * \param val      The preinitialized Value
	 */
	int                setValue(Value &val);

	int                parseValue(Value &val, const char *s);

	/** Returns true, if the Property is valid. */
	bool               isValid()            { return (token == INVALID_TOKEN) ?
	                                          false : true; }

	bool operator==(const Property &other) 
	{ return (token == other.token && device == other.device); }

};

/** The NETPP device class.
 *
 * A device is normally allocated on the heap and opened/closed using the
 * Device::open() and Device::close() methods.
 *
 */

class Device 
{
	DEVICE device;
public:
	Device(DEVICE d = 0);
	~Device();

	/** Open the remote device on the specified port name.
	 * See also dcDeviceOpen() */
	int                open(const char *port_name);
	/** Close the remote device. See also dcDeviceClose()*/
	int                close();


	int                getChecksum(void);

	/** Retrieve root node. See dcDevice_GetRoot()
	 * \return         The device root property
	 * \exception      A netpp error code
	 * */
	Property           getRoot(void);
	/** Obtain property identifier from device. See dcProperty_ParseName().
	 *
	 * \return         The requested property id or:
	 * \exception      A netpp error code
	 */
	Property           getProperty(const char *name);
	/** Get device name
	 * \return         error code, if string too long
	 */
	int                getName(std::string &s);
	DEVICE             getDEVICE(void)  { return device; }
	operator DEVICE()  { return device; }
};

/* \} */

////////////////////////////////////////////////////////////////////////////

