/** \file conffile.h
 *
 * Configuration file support
 *
 */

#include <stdio.h>

/** \defgroup Conffile      Configuration File handling
 *
 * A configuration file serves the purpose to store device properties
 * in a human readable, standard INI file format.
 *
For example:
 *
 *\verbatim 
[Blackfin]
irsize = 5                # Size of Instruction register
idcmd  = 2                # Command for requesting IDCODE
desc   = "Blackfin BF527" # Description
id     = 0x027e00cb       # IDCODE
idmask = 0x0fffffff       # Mask (ANDed with IDCODE before comparison)
\endverbatim
 *
 * For example, for  each device in a JTAG chain, an entry like the
 * above needs to be specified in the file.
 *
 * The main function to read the INI file is conf_parse(). Before parsing
 * the configuration, a BufStream structure and a ConfHandler structure
 * need to be instantiated in the caller.
 *
 * The ConfHandler::add_group and ConfHandler::set_property members are
 * callback function pointers and must be initialized to appropriate functions.
 * These are called by conf_parse().
 *
 */

/** \addtogroup Conffile
 * @{ */


/** Buffered stream structure */

typedef struct {
	FILE *file;                  ///< File handle
	char *buf[2];                ///< Buffer queue
	unsigned short bufsize;      ///< Buffer size
	char *pos;                   ///< Current position
	char *end;                   ///< End of buffer
	unsigned short cur;          ///< Current buffer
} BufStream;

/** Configuration handler.
 *
 * Handler structure passed on to configuration file parser.
 */

typedef struct {
	/** Add group '[group]' */
	int (*add_group)(void *user, const char *name);
	/** Set Property in current group */
	int (*set_property)(void *user, const char *name, const char *val); 
	/** User field for context structures */
	void *user;
} ConfHandler;

/** Parses configuration from BufStream s using ConfHandler h */
int conf_parse(BufStream *s, ConfHandler *h);

/* @} */

