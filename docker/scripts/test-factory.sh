#!/bin/bash

export LD_LIBRARY_PATH=/tmp/modbus/usr/local/lib
export PYTHONPATH=$HOME/netpp/python:$HOME/netpp/Debug
cd netpp/devices/factory
make all
python test.py $HOME/libmodbus/tests/unit-test-server

