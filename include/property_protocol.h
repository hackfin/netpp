/** \file property_protocol.h
 * Network property protocol specific definitions and APIs
 *
 * (c) 2005-2011, Martin Strubel <strubel@section5.ch>
 *
 *
 */
 
/**
 * \example server.c
 *
 * An example of a netpp server main loop. If the CONFIG_* variables
 * are defined, the server listens on both USB and TCP ports.
 * Note that concurrent property accesses must be handled by the user.
 * The netpp library does not implement locking.
 *
 */

/**
 * \example slave_test.c
 *
 * An example of a very simple server using a generic static libslave library.
 *
 */

#include "netpp_platform.h"
#include "property_types.h"
#include "ports.h"

// The target specific config file in devices/<target>

#include <stdarg.h>

/**
 * \defgroup Server NETPP server functionality
 *
 * A netpp server must initialize its properties and call a few functions
 * within its main loop to listen to property protocol requests on
 * various interfaces. Currently, TCP and some USB devices are implemented.
 *
 * If you don't have a source release of the NETPP library, you can link
 * your netpp server code against the libslave library (Linux or standalone:
 * libslave.a or Windows: libslave.lib).
 *
 * See \ref slave_test.c and associated \ref handler.c on how to implement a
 * netpp server.
 *
 */

#ifndef NETPP_PROTOCOL_TESTSUITE
/** Netpp protocol version */
#define NETPP_PROTOCOL_VERSION     0x02
#else
/** Netpp protocol version for non-release tests */
#define NETPP_PROTOCOL_VERSION     0x00
// If version 0 is returned, use this define for versioning tests
#define NETPP_PROTOCOL_TESTING     0x55
#endif

#ifndef DEFAULT_LISTEN_PORT
#define DEFAULT_LISTEN_PORT 2008
#endif

/** Port for netpp discovery */
#define DISCOVERY_PORT      7208

/* Packet types */
#define PP_STATUS        0x0000    // Status packet (slave only)
#define PP_REQUEST       0x0000    // Request packet (master only)
#define PP_GETROOT       0x0001    // GetRoot request (v0 LEGACY)
#define PP_SELECT        0x0002    // Select  request
#define PP_NAME          0x0003    // Query local namespace
#define PP_BULK          0x0008    // Bulk packet
#define PP_GET           0x0010    // Get property
#define PP_SET           0x0011    // Set property

#define PP_MASK_TYPE     0x00ff    // Type mask

#define PP_FLAG_FRAG     0x0100    // Packet stream fragment
#define PP_FLAG_PROTO    0x0200    // Prototype query

#define PP_DESC          (PP_GET | PP_FLAG_PROTO)

/* Request types: */

#define REQUEST_VERSION         0x00000000

// Reserved:
#define REQUEST_PKTSIZE         0x00000001

#define REQUEST_PROBE           0x26087223
#define REQUEST_PROBE_ANSWER    0xdeadface

/* These requests are only used for connectionless protocols */
#define REQUEST_SESSION_BEGIN   0x00000010
#define REQUEST_SESSION_END     0x00000011

/* Protocol definition
 *
 * Basically, the host files a request and waits for the answer.
 * Normally, this is an Acknowledge including a return code.
 * In case of a GetProperty() command, the target answers with a Set.
 *
 * See Scheme below:
 *
 * (Q: Request, A: Ack, G: Get, S: Set)
 *
 * Host     [Q]     ..[S]    ..[G]    ..
 *             \   /    \   /    \   /
 * Target       [A]      [A]      [S]
 *
 * The protocol is symmetrical, so target and host can be swapped.
 *
 * If the master initiates the protocol sequence with a STATUS packet,
 * it is considered a special query or message.
 *
 *
 */

#if defined(__cplusplus) && !defined(__BORLANDC__)
#define UNION_NAME u
#else
#define UNION_NAME
#endif

#if defined(LWIP) && !LWIP_COMPAT_SOCKETS
struct pktqueue;
struct tcp_pcb;
#endif

#if defined(CUSTOM_MAC)
// We have our own packet queue:
struct pktqueue;
#endif

typedef struct peer {
#ifdef HAVE_SOCKET
	SOCKET           fd;  // Generic file descriptor
#else
	int              fd;  // Generic file descriptor
#endif
	/** This is a protocol specific user pointer. Some protocol
     * implementations use it. For example, the IP protocol servers stores the
     * Port descriptor token of the client in this field.
     * This can again be used by a device handler (setter/getter) to determine
     * device specific contexts */
	union {
		void            *p;
		PortToken        node;
	} user;
	DynPropertyDesc *hub; // Pointer to hub (methods)
	union {
		struct net {
			uint32_t ipaddr;
			uint16_t dport;
			uint16_t sport;
		} ip;
		struct usb {
			uint16_t flags;
		} usb;
		struct custom {
			uint16_t addr;
			uint16_t state;
			uint16_t flags;
			uint8_t  nonce;
		} proxy;
	} UNION_NAME;
		
#if defined(LWIP) && !LWIP_COMPAT_SOCKETS
	struct pktqueue *queue;
	struct tcp_pcb  *pcb;
#elif defined(CUSTOM_MAC)
	const char      *rxptr;
	int              rxlen;
	char             macaddr[6]; // ethernet MAC address
	unsigned long    idle_ticks; // Idle time in user ticks
#else
	char            *sendbuf;
	char            *recvbuf;
#endif
	unsigned short   recvtimeout_wait;
	unsigned short   recvtimeout_check;
	unsigned long    packetsize;        // Payload max packet size supported
} Peer;

/** Remote device structure */

typedef struct DCDevice {
	Peer peer;
#ifdef CONFIG_PTHREAD
	NETPP_MUTEX mutex;
#endif
	PortToken portid;
	void *localdata;       ///<  User supplied data for local structures
#ifdef CONFIG_PROXY
	// Not needed, used localdata for now:
	// DEVICE proxy;          ///<  Proxy device handle
	unsigned int refcount; ///< Reference count for proxied device
#endif

/** This function pointer is called on disconnect, when not zero. */
	int          (*cleanup)(struct DCDevice *d);
	short          proto_version;
	uint16_t       flags;
	uint16_t       errcount;
} RemoteDevice;

// Internal error count limit on temporarely tolerable errors (UDP):
#define ERRCOUNT_LIMIT  5

#define F_PROXY   0x0001
#define F_EPOPEN  0x0002

// Polling modes
enum {
	POLL_WAIT,
	POLL_CHECK
};

#define PPKT_HDR struct property_packet_hdr *
int handle_ppkt(struct property_packet_hdr *h, Peer *peer, int n);

// Ensure packing alignment
#if defined(_MSC_VER) || defined(__BORLANDC__)
#	pragma pack(push, _netpp)
#	pragma pack(2)
#endif

typedef struct property_packet_hdr {
	uint16_t type;
	uint16_t id;             // Packet id
	union {
		struct __PACKED {
			uint32_t token;  // Token
			uint16_t code;   // Status/Return code
			uint16_t len;    // PROTOCOL v2: Length field (for packet size)
			uint16_t stat;   // Special status field
		} status;
		struct __PACKED {
			uint32_t token;  // Token
			uint32_t len;    // length of data
		} get;
		struct __PACKED {
			uint32_t token;  // Token
			uint32_t len;    // length of data
			uint16_t type;   // data type
		} set;
		struct __PACKED {
			uint32_t parent;
			uint32_t prev;
		} select;
		// Namespace query:
		struct __PACKED {
			uint32_t token;
			uint16_t len;
		} name;
		// LEGACY v0 -- DEPRECATED.
		struct __PACKED {
			uint16_t len;
		} parse;
	} sub;

} PropertyPacketHdr;

// New 1.0 wrapper for full zerocopy embedded option:

#ifndef TINYCPU
#define REQUEST_PACKET_HDR(p, x, len) \
	PropertyPacketHdr header; memset(&header, 0, sizeof(PropertyPacketHdr)); \
	x = &header;

#define REQUEST_PACKET(p, x, payload, len) \
	REQUEST_PACKET_HDR(p, x, len); \
	char tmp_payload_buf[4]; payload = tmp_payload_buf;
#else
// Needs to be specified by packet queue layer:
void request_packet_header(Peer *peer, PropertyPacketHdr **h, unsigned int len);
// No safety check in netpp < 1.0:
#define SAFETY_CHECK(x)
#define REQUEST_PACKET_HDR(p, x, len) \
	request_packet_header(p, &x, len + sizeof(PropertyPacketHdr)); \
	SAFETY_CHECK(x); \

#define REQUEST_PACKET(p, x, payload, len) \
	REQUEST_PACKET_HDR(p, x, len); \
	payload = &x[1];
#endif

#if defined(_MSC_VER) || defined(__BORLANDC__)
#	pragma pack(pop, _netpp)
#endif

////////////////////////////////////////////////////////////////////////////

EXTERN_C_BEGIN

int poll_packet(Peer *p, PropertyPacketHdr **h, int mode);
int send_packet(Peer *p, PropertyPacketHdr *h, const char *data, size_t len);
int get_status_packet(struct peer *peer, TOKEN *p);
int release_packet(Peer *p);

// Device iteration:
TOKEN       device_select(TOKEN d);


// If we're configured as proxy, include the property_() as well as the
// local_() API.

#ifdef PROXY
#	define PROXY_MODE
#	include "property_api.h"
#	undef PROXY_MODE
#endif

#ifdef CONFIG_PROXY
// When changing the proxy protocol, change this, too (and handle it)
#define PROXY_PROTOCOL_VERSION 0
#endif

// Property interface:

#include "property_api.h"

/** Register property list with netpp backend (Slave side)
 */
void register_proplist(struct device_desc_t *d, int n);

/**
 * \defgroup NetCommon Slave common networking code
 *
 * This contains common networking functions on Slave side only!
 *
 */


/**
 * \addtogroup NetCommon
 * \{
 */

/** Handle Socket error */
int handle_sockerr(int error, const char *txt);

/** Send network packet */
int net_send_packet(Peer *peer, PropertyPacketHdr *h,
		const char *data, size_t len);

/** Open remote connection by name */
int net_openbyname(RemoteDevice *d, const char *name,
	int (*con_init)(Peer *p, int bufsize));


/* \} */


/**
 * \addtogroup Server
 * \{
 */

/** Macro returning the "local device". Normally, netpp passes on the
 * <b>remote</b> device as first parameter (DEVICE d). In rare cases
 * when netpp is used locally, this macro should be used to return a
 * <b>local</b> device descriptor. Make sure to ONLY use this macro,
 * the definition may change in future to support the local access via
 * the generic netpp API.
 */
#define LOCAL_DEVICE() (DEVICE) DC_LOCAL_DEVICE

/** Get Root node token
 *
 * A netpp server must implement this function to return its device root
 * node. When using a single device which is the standard case, this
 * function must return 0. When a backend implements more than one device
 * functionality, it must select and return the appropriate root node.
 */
TOKEN local_getroot(DEVICE d);

/** Enters the slave handler reacting to a property protocol request.
 * This handler exits after a certain protocol specific timeout, if
 * no protocol command phase was initiated. This function is only to be used
 * directly by protocols that do not need to initiate a connection (like USB).
 * For TCP/UDP connections, see net_serve_users().
 *
 * \param d            Remote device handle
 * \return             Error code or 0 if no error
 */
int slave_handler(RemoteDevice *d);

/** Device backend write register or memory function
 *
 * \param d      Device handle
 * \param addr   Raw register or memory address
 * \param buf    Buffer pointer
 * \param size   Size of data to write to device
 */
int device_write(DEVICE d, uint32_t addr,
            const unsigned char *buf, unsigned long size);

/** Device backend read register
 */
int device_read(DEVICE d, uint32_t addr,
		unsigned char *buf, unsigned long size);

/** Device descriptor structure. See proplist.c (generated from XML)
 */
struct device_desc_t;


int handle_request(DEVICE d, TOKEN t, DCValue *value);
int handle_setproperty(DEVICE d, TOKEN t, DCValue *value);


/* \} */


////////////////////////////////////////////////////////////////////////////
// PROTOCOL AUX FUNCTIONS

// Internal function to retrieve packet length (payload information)
int get_packet_length(const char *buf);


/** Convert IP long to string */
int str_fromip(char name[], uint32_t ip, unsigned short port);


////////////////////////////////////////////////////////////////////////////
// protocol specific stuff

/** Internal protocol request */
int protocol_request(RemoteDevice *d, int request);


char *getname(Peer *p, int portindex);

// Send logical packet slice
int net_send_slice(Peer *peer, PropertyPacketHdr *h, const char *data,
		unsigned short len);

/** Master: Explicit opening of a TCP connection
 *
 * \param d       Remote device structure, can be uninitialized.
 * \param addr    Hostname address (currently in IP notation)
 *                This can be of the form "<ip>:<port>"
 */
int tcp_openbyname(RemoteDevice *d, const char *addr);

/** Get remote IP from TCP
 * \param d     Remote device handle
 * \param ip    Pointer to IP address returned by this function
 * \param port  Pointer to Port number returned by this function
 *
 */
int tcp_getremoteaddr(RemoteDevice *d,
	unsigned long *ip, unsigned short *port);

/** Clean up TCP connection
 * This simply closes the socket and is the default cleanup function
 * if a Peer has disconnected */
int tcp_cleanup(RemoteDevice *d);

/** Set TCP cleanup function.
 * This function is called on a disconnect (from peer), in general:
 * when the connection is lost.
 */
void tcp_set_disconnect_cleanup(RemoteDevice *d, int (*cleanup)(DEVICE d));

////////////////////////////////////////////////////////////////////////////

/** Event logging callback function pointer */
typedef void (*LogCallback)(const char *fmt, va_list argp);

/** Log function pointer
 *
 *
 * \param log_level      one of #DCLOG_ERROR, #DCLOG_DEBUG, #DCLOG_VERBOSE
 * \param fmt            printf compatible string format
 * \param ...            printf compatible arguments
 */

#ifdef NO_LOGGING
#define netpp_log(log_level, fmt, ...)
#elif defined(SPARSE_LOGGING)
#include <stdio.h>
#define netpp_log(log_level, fmt, ...) \
	printf(__FILE__ ": %d\n", __LINE__)
#else
#ifdef STANDALONE
extern int (*console_printf)(const char *fmt, ...);
#define netpp_log(log_level, fmt, ...) console_printf(fmt "\n", ##__VA_ARGS__)
#else
extern
void (*netpp_log)(int log_level, const char *fmt, ...);
#endif
#endif

EXTERN_C_END

#ifndef STANDALONE
// Mutex introduction for masters accessing one device from several
// threads.
int mutex_init(NETPP_MUTEX *m);
int mutex_exit(NETPP_MUTEX *m);
int mutex_lock(NETPP_MUTEX *m);
int mutex_unlock(NETPP_MUTEX *m);
#endif

uint16_t crc16(unsigned char *buf, int len, uint16_t cksum);

int user_device_init(RemoteDevice *rdev, PortToken p);
