/**
 *
 * TCP packet layer for property protocol
 *
 * (c) 2006 Martin Strubel <hackfin@section5.ch>
 *
 */

#include <stdio.h> // DEBUGGING
#include "devlib.h"
#include "devlib_error.h"
#include "property_protocol.h"
#ifndef LWIP_COMPAT_SOCKETS
#include <errno.h>
#endif

// Warning: Packet size must, on some platforms, be a multiple of four
// or even eight.

// DO NOT CHANGE. Changing this, makes the protocol incompatible to
// previous version
// Protocol v1.1 will be able to query the packet size using TOKEN_PKTSIZE.
#ifndef TINYCPU
#define MAX_PACKET_SIZE   65536
#endif

// Default timeout to check for a request (outside protocol) in ms
#define TIMEOUT_CHECK         1

// Default timeout to wait for a response inside protocol in ms
#define TIMEOUT_WAIT       5000

#define HDR_SIZE sizeof(PropertyPacketHdr)
#define MAX_PAYLOAD_SIZE (MAX_PACKET_SIZE - HDR_SIZE)

#ifdef DEBUG
#define DEB(x) x
#else
#define DEB(x)
#endif

static int packet_recv(int fd, char *buf, int len)
{
	int i, n;
	n = 0;
	while (len > 0) {
		i = recv(fd, buf, len, 0);
		if ((i <= 0)) {
			if (i == 0) {
				return DCERR_COMM_DISCONNECT;
			}
#ifndef LWIP_COMPAT_SOCKETS
			else if (errno == EAGAIN) continue;
#endif
			handle_sockerr(i, "reading from socket");
			return DCERR_COMM_GENERIC;
		}
		buf += i; n += i; len -= i;
	}
	return n;
}

int read_packet(Peer *p)
{
	unsigned int len;
	int error;

	error = packet_recv(p->fd, p->recvbuf, HDR_SIZE);
	if (error < 0 ) return error;

	len = get_packet_length(p->recvbuf);
	if (len > p->packetsize) len = p->packetsize;

	error = packet_recv(p->fd, &p->recvbuf[HDR_SIZE], len);
	if (error < 0) return error;

	return error + HDR_SIZE;
}

#if defined(__WIN32__) || defined(LWIP_COMPAT_SOCKETS)

/** Poll/wait for a packet. This function is called when we expect a packet
 * within a protocol command sequence 
 */

int tcp_poll_packet(Peer *p, PropertyPacketHdr **h, unsigned short timeout)
{
	int n = 0;
	fd_set fds;
	struct timeval tv;

	FD_ZERO(&fds);
	FD_SET(p->fd, &fds); // select() on socket

	tv.tv_sec = timeout / 1000; tv.tv_usec = (timeout % 1000) * 1000;

	select(p->fd + 1, &fds, NULL, NULL, &tv);
	if (FD_ISSET(p->fd, &fds)) {
		n = read_packet(p);
		*h = (PropertyPacketHdr *) p->recvbuf;
		if (n == 0) n = -1;
	}
	return n;
}

#elif defined (__linux__) || defined(__APPLE__)

int tcp_poll_packet(Peer *p, PropertyPacketHdr **h, unsigned short timeout)
{
	int n;

	struct pollfd pfd;

	pfd.fd = p->fd;
	pfd.events = POLLIN | POLLPRI;

	n = poll(&pfd, 1, timeout);
	if (n < 0) {
		return handle_sockerr(n, "Polling socket");
	} else if (n == 0) return n;

	n = read_packet(p);
	*h = (PropertyPacketHdr *) p->recvbuf;
	return n;
}


#endif

int tcp_send_packet(Peer *peer, PropertyPacketHdr *h,
		const char *data, size_t len)
{
	int error;
	unsigned int n;
	unsigned short type = ntohs(h->type);
	// netpp_log(DCLOG_DEBUG, "Send packet dest: %08lx, dport: %d len: %ld",
	//		peer->ip.ipaddr, peer->ip.dport, len);

	// Now send packet fragments
	int i = 0;

	h->id = htons(i);
	n = peer->packetsize;

	while (len > n) {
		h->type = htons(type | PP_FLAG_FRAG);
		// netpp_log(DCLOG_DEBUG, "Send slice (%d left)", len);
		error = net_send_slice(peer, h, data, n);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Failed to send slice");
			return error;
		}
		len -= n;
		data += n;

		// Update length field
		h->sub.set.len = htonl((unsigned long) len);

		i++;
		h->id = htons(i);
	}

	h->type = htons(type);

	error = net_send_slice(peer, h, data, (unsigned short) len);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Failed to send slice");
	}

	return error;
}

static
int peer_init(Peer *p)
{
	p->sendbuf = buf_alloc(MAX_PACKET_SIZE * 2);
	if (!p->sendbuf) return DCERR_MALLOC;
	p->recvbuf = (char *) &p->sendbuf[MAX_PACKET_SIZE];

	p->recvtimeout_check =  TIMEOUT_CHECK;
	p->recvtimeout_wait  =  TIMEOUT_WAIT;

	p->packetsize = MAX_PACKET_SIZE - HDR_SIZE;
	return 0;
}

static
int peer_exit(Peer *p)
{
	if (p->sendbuf) buf_free(p->sendbuf);
	return 0;
}

// Only call from server (slave). Never from master!


int tcp_sock_init(SOCKET *sock, SockAddrIn *sin)
{
	int error = 0;
	int option;
	SOCKET s;

	s = (SOCKET) socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); 
	if (s == INVALID_SOCKET) { 
		netpp_log(DCLOG_ERROR, "Error initializing TCP socket: %s",
#ifdef LWIP_COMPAT_SOCKETS
			"FIXME"
#else
			strerror(errno)
#endif
		);
		return DCERR_COMM_INIT;
	}

	option = 1;
	setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *) &option,
		sizeof(option));

	sin->sin_addr.s_addr    = INADDR_ANY;
	sin->sin_family         = AF_INET;

	*sock = s;

	return error;
}

static
int servercontext_init(ServerContext *serv)
{
	serv->flags = F_SERV_TCP;
	// Reset user count
	serv->usercount = 0;
	serv->peer_init = peer_init;
	serv->slave_handler = slave_handler;
 	serv->hub = Hub(GET_PORTROOT(), TCP_SERVER_NAME, &tcp_methods);
	if (!IS_VALIDTOKEN(serv->hub)) return DCERR_MALLOC;
	// Reserve device list:
	serv->peerdevs = Hub(GET_PORTROOT(), TCP_PEERS_NAME, 0);
	if (!IS_VALIDTOKEN(serv->peerdevs)) return DCERR_MALLOC;
	// But we don't append this to the global portbay.
	serv->devices =
		portbay_fromtoken(desc_fromtoken(serv->peerdevs)->hub.ports);
	serv->cleanup = NULL;
	return 0;
}

#if defined (__WIN32__)

static
int connection_init(Peer *p, int bufsize)
{
	int error;
	SockAddrIn sin;
	socklen_t size = (socklen_t) sizeof(int);
	int flag = 1;
	WSADATA data;
	SOCKET sock;

	error = peer_init(p);
	if (error < 0) return error;

	error = WSAStartup(MAKEWORD(2, 2), &data);
	if (error != 0) {
		return DCERR_COMM_INIT;
	}

	// Set up socket for local source port
	sin.sin_port = htons(p->ip.sport);
	error = tcp_sock_init(&sock, &sin);
	if (error < 0) {
		error = handle_sockerr(error, "initializing TCP socket");
		WSACleanup();
		return error;
	}

	setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, (char *) &flag, size);
	setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, size);

	// Connect to peer
	sin.sin_addr.s_addr = p->ip.ipaddr;
	sin.sin_port        = htons(p->ip.dport);

	error = connect(sock, (SockAddr *) &sin, sizeof(sin));
	if (error < 0) {
		error = handle_sockerr(error, "connecting to socket");
		WSACleanup();
		return error;
	}

	p->fd = sock; // just copy

	return 0;
}

static
int connection_exit(Peer *p)
{
	if (shutdown(p->fd, SD_SEND) == SOCKET_ERROR) {
		netpp_log(DCLOG_ERROR, "Could not shutdown socket");
	}
	while (recv(p->fd, p->recvbuf, MAX_PACKET_SIZE, 0) > 0);
	closesocket(p->fd);
	WSACleanup();
	peer_exit(p);
	return 0;
}


int tcp_server_init(int listen_port, ServerContext *serv)
{
	int error;
	SockAddrIn sin;
	WSADATA data;
	int flag = 1;
	SOCKET sock;

	error = servercontext_init(serv);
	if (error < 0) return error;

	// Set up socket for server port
	error = WSAStartup(MAKEWORD(2, 2), &data);
	if (error != 0) {
		return DCERR_COMM_INIT;
	}

	sin.sin_port = htons(listen_port);

	error = tcp_sock_init(&sock, &sin);
	if (error < 0) {
		error = handle_sockerr(error,
			__FUNCTION__ ": initializing TCP socket");
		goto fail;
	}

	setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(int));
	
	error = bind(sock, (SockAddr *) &sin, sizeof(SockAddr));
	if (error == SOCKET_ERROR) {
		error = handle_sockerr(error,  __FUNCTION__ ": binding TCP socket");
		closesocket(sock);
		goto fail;
	}

	error = listen(sock, 1);
	if (error != SOCKET_ERROR) {
		serv->listener = sock;
		return add_server_entry(serv, listen_port);
	}
	error = handle_sockerr(error,  __FUNCTION__ ": listen to socket");
	closesocket(sock);
fail:
	WSACleanup();
	return error;
}

#elif defined (__linux__) || defined(__APPLE__) || defined(LWIP_COMPAT_SOCKETS)

static
int connection_init(Peer *p, int bufsize)
{
	int error;
	SockAddrIn sin;
	socklen_t size = (socklen_t) sizeof(int);
	int sock;
	int flag = 0;

	error = peer_init(p);
	if (error < 0) return error;
	
	// Set up socket for local source port
	sin.sin_port = htons(p->ip.sport);
	error = tcp_sock_init(&sock, &sin);
	if (error == 0) {
		setsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char *) &bufsize,
			size);
		DEB(netpp_log(DCLOG_DEBUG, "Try buffer size: %d", bufsize));
		getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char *) &bufsize,
			&size);
		DEB(netpp_log(DCLOG_DEBUG, "Receive buffer size: %d", bufsize));

		setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(int));

		// Connect to peer
		sin.sin_addr.s_addr = p->ip.ipaddr;
		sin.sin_port        = htons(p->ip.dport);

		error = connect(sock, (SockAddr *) &sin, sizeof(sin));
		p->fd = sock; // just copy
	}

	if (error < 0) {
		peer_exit(p);
		error = handle_sockerr(error, "connecting to socket");
	}
	return error;
}

static
int connection_exit(Peer *p)
{
	shutdown(p->fd, SHUT_RDWR);
	close(p->fd);
	return peer_exit(p);
}

int tcp_server_init(int listen_port, ServerContext *serv)
{
	int error;
	int flag = 1;
	SockAddrIn sin;
	int sock;

	error = servercontext_init(serv);
	if (error < 0) return error;

	// Set up socket for server port
	sin.sin_port = htons(listen_port);
	error = tcp_sock_init(&sock, &sin);
	if (error < 0) return DCERR_COMM_INIT;

	setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(int));

	error = bind(sock, (SockAddr *) &sin, sizeof(SockAddr));
	if (error < 0) return DCERR_COMM_INIT;

	error = listen(sock, 2);
	if (error < 0) return DCERR_COMM_INIT;

	serv->listener = sock;

	return add_server_entry(serv, listen_port);
}

#endif

int tcp_listen_incoming(ServerContext *serv)
{
	int error;
	SockAddrIn sin;
	fd_set fds;
	SOCKET sock;
	SOCKET listener = serv->listener;
	struct timeval tv;
	socklen_t size = sizeof(SockAddrIn);
#ifdef __WIN32__
	unsigned long flag = 0; // Blocking
#endif

	// Listen for new users coming in
	FD_ZERO(&fds);
	FD_SET(listener, &fds);
	tv.tv_sec = 0; tv.tv_usec = 5000;

	error = select(listener + 1, &fds, NULL, NULL, &tv);
	// printf("Select code %d\n", error);
	if ((error > 0) && FD_ISSET(listener, &fds)) {

		sock = accept(listener, (SockAddr *) &sin, &size);
#ifdef __WIN32__
		ioctlsocket(sock, FIONBIO, &flag);
#endif

		error = remotedevice_add(serv, sock, &sin, DC_DEVICE);
		if (error < 0) return error;
	}
	return 0;
}


int tcp_slave_mainloop(ServerContext *serv)
{
	int error = 0;
	while (1) {
		// First, we serve users
		net_serve_users(serv);
		error = tcp_listen_incoming(serv);
		if (error) break;
	}
	return error;
}

int tcp_server_exit(ServerContext *serv)
{
	int error;

	net_disconnect_users(serv);
#ifdef __WIN32__
	error = shutdown(serv->listener, SD_BOTH);
	closesocket(serv->listener);
	WSACleanup();
#else
	error = shutdown(serv->listener, SHUT_RDWR);
	close(serv->listener);
#endif

	portdesc_free(serv->hub);
	portdesc_free(serv->peerdevs);
	
	return error;
}

////////////////////////////////////////////////////////////////////////////
// TCP port methods

int tcp_openbyname(RemoteDevice *d, const char *hostname)
{
	return net_openbyname(d, hostname, &connection_init);
}

int tcp_open(RemoteDevice *d, int portindex)
{
	const char *addr;

	addr = getname(&d->peer, portindex);
	if (!addr) return DCERR_PROPERTY_UNKNOWN;

	return tcp_openbyname(d, addr);
}

int netpp_tcp_close(RemoteDevice *d)
{
	int error;

	Peer *p = &d->peer;

	error = connection_exit(p);
	return error;
}

int tcp_probe(DynPropertyDesc *hub)
{
#if !defined(CONFIG_NOPROBE) && defined(CONFIG_UDP)
	return net_probe(hub);
#else
	return 0;
#endif
}

int tcp_getremoteaddr(RemoteDevice *d, unsigned long *ip, unsigned short *port)
{
	*ip = d->peer.ip.ipaddr;
	*port = d->peer.ip.dport;
	return 0;
}

void tcp_set_disconnect_cleanup(RemoteDevice *d, int (*cleanup)(DEVICE d))
{
	d->cleanup = cleanup;
}

#ifndef C99_COMPLIANT
// Insane compilers which don't do C99

struct protocol_methods tcp_methods = {
	&tcp_probe,
	&net_create,
	&tcp_open,
	&netpp_tcp_close,
	&tcp_send_packet,
	&tcp_poll_packet
};

#else

struct protocol_methods tcp_methods = {
	.probe   = &tcp_probe,
	.create  = &net_create,
	.open    = &tcp_open,
	.close   = &netpp_tcp_close,
	.send    = &tcp_send_packet,
	.recv    = &tcp_poll_packet,
	.release = 0
};

#endif
