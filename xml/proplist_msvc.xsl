<?xml version="1.0" encoding="ISO-8859-1"?>

<!--

$Id: proplist_msvc.xsl 295 2010-05-04 15:17:45Z strubi $

Transformation style sheet DEVDESC-XML -> PropertyList C source

(c) 2005, section5 // Martin Strubel <strubel@section5.ch>

XXX root node:

 .base = <xsl:choose>
			<xsl:when test="@baseclass">g_index_<xsl:value-of select="@baseclass"/></xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose> 

-->

<xsl:stylesheet version="1.0" 
	xmlns:my="http://www.section5.ch/dclib/schema/devdesc"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

	<xsl:import href="registermap.xsl"/>

	<xsl:output method="text" encoding="ISO-8859-1"/>	

	<!-- This key is needed for register referencing -->

	<xsl:key name="regkey" match="my:register" use="@id"/>

	<!-- Bit ranges -->
	<xsl:template match="my:bitfield"><xsl:value-of select="@lsb"/>,<xsl:value-of select="@msb"/></xsl:template>

	<xsl:template match="my:register" mode="ref">
		<xsl:param name="which"/><xsl:value-of select="my:bitfield[@name=$which]/@lsb"/>, <xsl:value-of select="my:bitfield[@name=$which]/@msb"/></xsl:template>

	<xsl:template match="my:register" mode="refRegsize">
		<xsl:choose>
			<xsl:when test="@size"><xsl:value-of select="@size"/></xsl:when>
			<xsl:otherwise>1</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="my:regref">
		<xsl:choose>
			<xsl:when test="@bits">
				<xsl:apply-templates select="key('regkey', @ref)" mode="ref">
					<xsl:with-param name="which" select="@bits"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>0, LAST_BIT</xsl:otherwise>
		</xsl:choose>, <xsl:apply-templates select="key('regkey', @ref)" mode="refRegsize"/>

	</xsl:template>

	<!-- encode endianness PER register -->
	<xsl:template match="my:register" mode="endiancode">
		<xsl:if test="../@endian = 'BIG'"> | F_BIG</xsl:if>
	</xsl:template>

	<!-- Needs to be retrieved via the key mechanism -->
	<xsl:template match="my:regref" mode="endiancode">
		<xsl:apply-templates select="key('regkey', @ref)" mode="endiancode"/>
	</xsl:template>

	<!-- Enumeration of all static entities -->

	<xsl:template match="my:property|my:struct|my:array" mode="enum">
		<xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<!-- Variable handling -->

	<xsl:template match="my:variable|my:size" mode="var_enum">
		<xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<!-- Access to programming language variables -->
	<xsl:template match="my:variable" mode="var_lang">
		<xsl:choose>
			<xsl:when test="../@type = 'BUFFER'">INIT_BUF(p->access.buf, <xsl:value-of select="."/>, sizeof(<xsl:value-of select="."/>));
			</xsl:when>
			<xsl:when test="../@type = 'STRING'">INIT_BUF(p->access.buf, <xsl:value-of select="."/>, sizeof(<xsl:value-of select="."/>));
			</xsl:when>
			<xsl:when test="../@type = 'BOOL'">INIT_VARP(p->access.varp_bool, <xsl:value-of select="."/>);
			</xsl:when>
			<xsl:when test="../@type = 'INT'">INIT_VARP(p->access.varp_int, <xsl:value-of select="."/>);
			</xsl:when>
			<xsl:when test="../@type = 'MODE'">INIT_VARP(p->access.varp_mode, <xsl:value-of select="."/>);
			</xsl:when>
			<xsl:when test="../@type = 'FLOAT'">DC_VAR, { .varp_float = &amp;<xsl:value-of select="."/> },
			</xsl:when>
			<xsl:otherwise>INIT_NONE,
#error "No variable support for this datatype"
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="my:variable" mode="var_init">
		<xsl:choose>
			<xsl:when test="./my:regref">
#error "FIXME: Variables from register not supported"
			</xsl:when>
			<xsl:otherwise>DC_HANDLER, INIT_NONE</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Choice/Item handling -->

	<xsl:template match="my:item" mode="item_enum">
		<xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<xsl:template match="my:item" mode="item_init">
	{ "<xsl:value-of select="@name"/>" /* <xsl:value-of select="generate-id()"/> */, DC_INT,
		F_RO,
		DC_STATIC, INIT_NONE, 0 },
	</xsl:template>

	<!-- Limits (Min/Max) handling -->

	<xsl:template match="my:min|my:max" mode="limits_enum">
		<xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<xsl:template match="my:size/my:value">
		DC_STATIC, INIT_NONE, 0 
	</xsl:template>

	<xsl:template match="my:min/my:value|my:max/my:value">
		<xsl:choose>
			<xsl:when test="../../../@type = 'INT'">
		DC_STATIC, INIT_NONE, 0 
			</xsl:when>
			<xsl:when test="../../../@type = 'FLOAT'">
		DC_STATIC, INIT_NONE, 0 
			</xsl:when>
			<xsl:when test="../../../@type = 'STRING'">
		DC_STATIC, INIT_NONE, 0 
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- static values: -->
	<xsl:template match="my:value" mode="prop_init">
		<xsl:choose>
			<xsl:when test="../@type = 'INT'">DC_STATIC, INIT_NONE,
			</xsl:when>
			<xsl:when test="../@type = 'FLOAT'">DC_STATIC, INIT_NONE,
			</xsl:when>
			<xsl:when test="../@type = 'STRING'">DC_STATIC, INIT_NONE,
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- static values: -->
	<xsl:template match="my:value" mode="prop_init2">
		<xsl:choose>
			<xsl:when test="../@type = 'INT'">
	INIT_STATIC(p->access.s_int, <xsl:value-of select="."/>);</xsl:when>
			<xsl:when test="../../../@type = 'MODE'">
	INIT_STATIC(p->access.s_int, <xsl:value-of select="."/>);</xsl:when>
			<xsl:when test="../../../@type = 'INT'">
	INIT_STATIC(p->access.s_int, <xsl:value-of select="."/>);</xsl:when>
			<xsl:when test="../@type = 'FLOAT'">
	INIT_STATIC(p->access.s_float, <xsl:value-of select="."/>);</xsl:when>
			<xsl:when test="../@type = 'STRING'">
	INIT_STATIC(p->access.s_string, "<xsl:value-of select="."/>");</xsl:when>
			<xsl:otherwise>
#error No value support for type '<xsl:value-of select="../@type"/>'
</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="my:min" mode="limits_init">
	{ "Min" /* <xsl:value-of select="generate-id()"/>: "<xsl:value-of select="../../@name"/>" */, DC_<xsl:value-of select="../../@type"/>,
		F_RO, <xsl:apply-templates select="./my:value"/><xsl:apply-templates select="./my:variable" mode="var_init"/> },
	</xsl:template>

	<xsl:template match="my:max" mode="limits_init">
	{ "Max" /* <xsl:value-of select="generate-id()"/>: "<xsl:value-of select="../../@name"/>" */, DC_<xsl:value-of select="../../@type"/>,
		F_RO, <xsl:apply-templates select="./my:value"/><xsl:apply-templates select="./my:variable" mode="var_init"/> },
	</xsl:template>

	<xsl:template match="my:limits/my:min|my:limits/my:max|my:event|my:choice/my:item" mode="prop_children">
		<xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<xsl:template match="my:property" mode="prop_children">
		<xsl:if test="./my:limits or ./my:choice or ./my:event">
static PROPINDEX
g_children_<xsl:value-of select="generate-id()"/>[] = {
	<xsl:apply-templates select="my:limits/my:min|my:limits/my:max|my:event|my:choice/my:item" mode="prop_children"/>INVALID_INDEX
};
	</xsl:if>
	</xsl:template>


	<!-- 
		Handling of array size
		 -->

	<xsl:template match="my:size" mode="var_init">
	{ "Size" /* "<xsl:value-of select="../@name"/>" */, <!-- DC_<xsl:value-of select="../../@type"/>, -->
		DC_INT,
		F_RO, <xsl:apply-templates select="./my:value"/><xsl:apply-templates select="./my:variable" mode="var_init"/>},
	</xsl:template>

	<!-- array element size calculation -->
	<xsl:template match="my:array" mode="calcsize">
		<xsl:if test="my:property/my:regref">
			<xsl:apply-templates select="my:value"/>
		</xsl:if>
	</xsl:template>

	<!-- handler functions -->

	<xsl:template match="my:property" mode="handler">
		<xsl:choose>
			<xsl:when test="name(..) = 'struct'">
				<xsl:choose>
					<xsl:when test="not(@access) or @access = 'RW' or @access = 'RO'">get_<xsl:value-of select="./my:handler"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>, <xsl:choose>
				<xsl:when test="not(@access) or @access = 'RW' or @access = 'WO'">set_<xsl:value-of select="./my:handler"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
						
			</xsl:when>

			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="not(@access) or @access = 'RW' or @access = 'RO'">get_<xsl:value-of select="./my:handler"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>, <xsl:choose>
				<xsl:when test="not(@access) or @access = 'RW' or @access = 'WO'">set_<xsl:value-of select="./my:handler"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
	
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- handler proto -->

	<xsl:template match="my:variable/my:handler" mode="handler_proto">
DECLARE_PROTO_GETTER(get_<xsl:value-of select="."/>)
	</xsl:template>

	<xsl:template match="my:property/my:handler" mode="handler_proto">

		<xsl:if test="not(../my:regref) and not(../my:value)">
		<xsl:choose>
			<xsl:when test="name(../..) = 'struct'">
				<xsl:choose>
					<xsl:when test="not(../@access) or ../@access = 'RW' or ../@access = 'RO'">
DECLARE_PROTO_GETTER(get_<xsl:value-of select="."/>)
					</xsl:when>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="not(@access) or @access = 'RW' or @access = 'WO'">
DECLARE_PROTO_SETTER(set_<xsl:value-of select="."/>)
					</xsl:when>
					<xsl:otherwise>
// No Setter
					</xsl:otherwise>
				</xsl:choose>
						
			</xsl:when>

			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="not(../@access) or ../@access = 'RW' or ../@access = 'RO'">
DECLARE_PROTO_GETTER(get_<xsl:value-of select="."/>)
</xsl:when>
					<xsl:otherwise>
// No Getter
					</xsl:otherwise>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="not(../@access) or ../@access = 'RW' or ../@access = 'WO'">
DECLARE_PROTO_SETTER(set_<xsl:value-of select="."/>)
					</xsl:when>
					<xsl:otherwise>
// No Setter
					</xsl:otherwise>
				</xsl:choose>
	
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
	</xsl:template>


	<!-- Event descriptor initialization -->

	<xsl:template match="my:event" mode="evt_enum">
		<xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<xsl:template match="my:event" mode="evt_init">
	{ "EVT_" "<xsl:value-of select="@target"/>", /* <xsl:value-of select="generate-id()"/> */
		DC_EVENT,
		F_RO, DC_STATIC, { .event = { DC_<xsl:value-of select="@type"/>, TOKEN_<xsl:value-of select="@target"/> } }
	},
	</xsl:template>


	<!-- direct access token definitions -->
	<xsl:template match="my:property" mode="deftoken">
	<xsl:if test="@id">
#define TOKEN_<xsl:value-of select="@id"/><xsl:text> </xsl:text><xsl:value-of select="generate-id()"/>
	</xsl:if>
	</xsl:template>

	<!-- direct access via external globals -->
	<xsl:template match="my:property" mode="exttoken">
	<xsl:if test="@id">
TOKEN g_t_<xsl:value-of select="@id"/> = <xsl:value-of select="generate-id()"/>;
	</xsl:if>
	</xsl:template>

	<!-- Struct declaration:
	     This creates a sequence of member id's (statically)
		 -->

	<xsl:template match="my:struct/my:property" mode="memberlist">
		 <xsl:value-of select="generate-id()"/>,
	</xsl:template>

	<xsl:template match="my:struct" mode="struct_decl">
// Struct "<xsl:value-of select="@name"/>"
static PROPINDEX
g_struct_<xsl:value-of select="generate-id()"/>[] = {
	<xsl:apply-templates select="./my:property" mode="memberlist"/>INVALID_INDEX
};
	</xsl:template>

	<xsl:template match="my:group/my:array|my:array/my:array" mode="array_decl">
// Array "<xsl:value-of select="@name"/>"
static PROPINDEX
g_array_<xsl:value-of select="generate-id()"/>[] = {
	<xsl:value-of select="generate-id(./my:size)"/> /* Size */,
	<xsl:choose>
		<xsl:when test="./my:array"><xsl:value-of select="generate-id(./my:array)"/></xsl:when>
		<xsl:when test="./my:struct"><xsl:value-of select="generate-id(./my:struct)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="generate-id(./my:property)"/></xsl:otherwise>
	</xsl:choose>,
	INVALID_INDEX
};
	</xsl:template>

	<!-- Initialization of Property tables -->

	<xsl:template match="my:property" mode="prop_init">
	{ "<xsl:value-of select="@name"/>" /* <xsl:value-of select="generate-id()"/> */,  DC_<xsl:value-of select="@type"/>, 
		<xsl:if test="@hidden">F_PRIVATE | </xsl:if>
		<xsl:if test="./my:event">F_ACTION | </xsl:if>
		<xsl:choose>
			<xsl:when test="@access">F_<xsl:value-of select="@access"/></xsl:when>
			<xsl:otherwise>F_RW</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="./my:regref"><xsl:apply-templates select="./my:regref" mode="endiancode"/></xsl:if>,
		<xsl:choose>
			<xsl:when test="./my:regref">DC_ADDR, INIT_NONE,
			</xsl:when>
			<xsl:when test="./my:variable">DC_VAR, INIT_NONE,
			</xsl:when>
			<xsl:when test="./my:value">DC_STATIC, INIT_NONE,
			</xsl:when>
			<xsl:otherwise>DC_HANDLER, INIT_NONE,
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="./my:choice or ./my:event or ./my:limits">g_children_<xsl:value-of select="generate-id()"/></xsl:when>

			<xsl:when test="@type = 'BUFFER'">0 /* TODO: BufferDesc */</xsl:when>
			<xsl:otherwise>0 /* no children */</xsl:otherwise>
		</xsl:choose> },
	</xsl:template>

	<!-- Dynamic initialization of property tables for insane non C99
   compilers  -->
	<xsl:template match="my:property|my:item|my:min|my:max" mode="prop_init2">
	p = &amp;pds[<xsl:value-of select="generate-id()"/>];
	<xsl:choose>
		<xsl:when test="./my:regref">INIT_REG(p->access.reg, Reg_<xsl:value-of select="./my:regref/@ref"/>, <xsl:apply-templates select="./my:regref"/>);</xsl:when>
		<xsl:when test="./my:variable"><xsl:apply-templates select="./my:variable" mode="var_lang"/></xsl:when>
		<xsl:when test="./my:value"><xsl:apply-templates select="./my:value" mode="prop_init2"/></xsl:when>
		<xsl:otherwise>INIT_FUNC(p->access.func, <xsl:apply-templates select="." mode="handler"/>);</xsl:otherwise>
	</xsl:choose>
	</xsl:template>

	<xsl:template match="my:struct" mode="prop_init">
	{ "<xsl:value-of select="@name"/>" /* <xsl:value-of select="generate-id()"/> */, DC_STRUCT, 
		<xsl:if test="@hidden">F_PRIVATE | </xsl:if>
		<xsl:choose>
			<xsl:when test="@access">F_<xsl:value-of select="@access"/></xsl:when>
			<xsl:otherwise>F_RW</xsl:otherwise>
		</xsl:choose>,
		DC_HANDLER, INIT_NONE,
			g_struct_<xsl:value-of select="generate-id()"/>, },
	</xsl:template>

	<xsl:template match="my:group/my:array|my:array/my:array" mode="prop_init">
	{ "<xsl:value-of select="@name"/>" /* <xsl:value-of select="generate-id()"/> */, DC_ARRAY, 
		<xsl:if test="@hidden">F_PRIVATE | </xsl:if>
		<xsl:choose>
			<xsl:when test="@access">F_<xsl:value-of select="@access"/></xsl:when>
			<xsl:otherwise>F_RW</xsl:otherwise>
		</xsl:choose>,
		DC_HANDLER,
		<xsl:choose>
			<xsl:when test="./my:regref">{ .array = { <xsl:apply-templates select="key('regkey', ./my:regref/@ref)" mode="refRegsize"/>, Reg_<xsl:value-of select="./my:regref/@ref"/> } }, 
			</xsl:when>
			<xsl:otherwise>{ .array = { 0, 0x0000 } },</xsl:otherwise>
		</xsl:choose> g_array_<xsl:value-of select="generate-id()"/> },
		<!-- 
		<xsl:choose>
			<xsl:when test="./my:struct">g_struct_<xsl:value-of select="generate-id(./my:struct)"/></xsl:when>
			<xsl:when test="./my:array">g_array_<xsl:value-of select="generate-id(./my:array)"/></xsl:when>
			<xsl:otherwise>g_array_<xsl:value-of select="generate-id(./my:property)"/></xsl:otherwise>
		</xsl:choose> },
		-->
	</xsl:template>

	<!-- handle device types -->
	<xsl:template match="my:device" mode="dev_enum">{ g_props_<xsl:value-of select="@id"/>, <xsl:value-of select="./my:revision/my:major"/>, <xsl:value-of select="./my:revision/my:minor"/>,  MAXPROP_<xsl:value-of select="@id"/>},
	</xsl:template>

	<!-- device index enumeration -->
	<xsl:template match="my:device" mode="dev_index">g_index_<xsl:value-of select="@id"/>,
	</xsl:template>

	<xsl:template match="my:device" mode="call_init2">
	init_props_<xsl:value-of select="@id"/>();</xsl:template>

<xsl:template match="my:device" mode="dev_init">
////////////////////////////////////////////////////////////////////////////
// DEVICE: <xsl:value-of select="@name"/>, ID: <xsl:value-of select="@id"/>
////////////////////////////////////////////////////////////////////////////


// PROPERTY KEYS
enum {
	<xsl:value-of select="@id"/>, // Device Root node
	// Properties, Structs, Arrays:
	<xsl:apply-templates select=".//my:property|.//my:struct|.//my:array" mode="enum">
		<xsl:sort select="@name" order="ascending"/>
	</xsl:apply-templates>
	// Items:
	<xsl:apply-templates select=".//my:item" mode="item_enum"/>
	// Limits:
	<xsl:apply-templates select=".//my:min|.//my:max" mode="limits_enum"/>
	// Variables:
	<xsl:apply-templates select=".//my:size" mode="var_enum"/>
	<!-- TODO: Variable size array support -->
	// Events
	<xsl:apply-templates select=".//my:event" mode="evt_enum"/>
	MAXPROP_<xsl:value-of select="@id"/> // = Number of Properties
};

////////////////////////////////////////////////////////////////////////////
// Struct lists:
<xsl:apply-templates select=".//my:struct" mode="struct_decl"/>

////////////////////////////////////////////////////////////////////////////
// Array lists:
<xsl:apply-templates select=".//my:array" mode="array_decl"/>

////////////////////////////////////////////////////////////////////////////
// Children lists:
<xsl:apply-templates select=".//my:property" mode="prop_children"/>

////////////////////////////////////////////////////////////////////////////
// Register defines
<xsl:apply-templates select=".//my:registermap" mode="reg_decl"/>

////////////////////////////////////////////////////////////////////////////
// Feature tables

// ROOT LIST (Top level nodes)

static
PROPINDEX
g_root_<xsl:value-of select="@id"/>[] = {
	<xsl:apply-templates select="./my:group/my:property" mode="enum">
		<xsl:sort select="@name" order="ascending"/>
	</xsl:apply-templates>
	<xsl:apply-templates select="./my:group/my:struct" mode="enum"/>
	<xsl:apply-templates select="./my:group/my:array" mode="enum"/>INVALID_INDEX
};

static
PropertyDesc
g_props_<xsl:value-of select="@id"/>[] = {

/* ROOT NODE (always on top of the list!) */

	{ "<xsl:value-of select="@name"/>", DC_ROOT,
		F_RO <xsl:if test="@baseclass"><xsl:value-of select="@size"/> | F_LINK</xsl:if>,
		DC_STATIC, INIT_NONE, g_root_<xsl:value-of select="@id"/> },

/* Properties */
	<xsl:apply-templates select=".//my:property|.//my:struct|.//my:array" mode="prop_init">
		<xsl:sort select="@name" order="ascending"/>
	</xsl:apply-templates>
/* Items */
	<xsl:apply-templates select=".//my:item" mode="item_init"/>
/* Limits */
	<xsl:apply-templates select=".//my:min|.//my:max" mode="limits_init"/>
/* Size variables */
	<xsl:apply-templates select=".//my:size" mode="var_init"/>
/* Event descriptors */
	<xsl:apply-templates select=".//my:event" mode="evt_init"/>
	{ 0 }
};

#ifndef C99_COMPLIANT
////////////////////////////////////////////////////////////////////////////
// Second stage initialization for MSVC

static
int g_initlist_<xsl:value-of select="@id"/>[] = {
	<xsl:apply-templates select=".//my:property" mode="enum"/>
	MAXPROP_<xsl:value-of select="@id"/>
};

void init_props_<xsl:value-of select="@id"/>(void)
{
	PropertyDesc *p, *pds = g_props_<xsl:value-of select="@id"/>;
	/* Properties */
	<xsl:apply-templates select=".//my:property" mode="prop_init2"/>
	/* Items */
	<xsl:apply-templates select=".//my:item" mode="prop_init2"/>
	/* Limits */
	<xsl:apply-templates select=".//my:min|.//my:max" mode="prop_init2"/>
}

#endif

</xsl:template>

<xsl:template match="my:header">
<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="/">
////////////////////////////////////////////////////////////////////////////
//
// (c) 2005 section5, Martin Strubel
//
// THIS IS A GENERATED FILE. DO NOT MODIFY! ALL CHANGES WILL BE LOST
//
//
////////////////////////////////////////////////////////////////////////////

#include "property_types.h"
#include "proplist_init.h"

<xsl:apply-templates select=".//my:header"/>

#define DECLARE_PROTO_GETTER(func)  int func(DEVICE d, DCValue *out);
#define DECLARE_PROTO_SETTER(func)  int func(DEVICE d, DCValue *in);

////////////////////////////////////////////////////////////////////////////
// LIBRARY VERSION CONTROL

char g_vendor[]   = "<xsl:value-of select="my:devdesc/my:vendor"/>";

int g_version[2] = { <xsl:value-of select="my:devdesc/my:revision/my:major"/>, <xsl:value-of select="my:devdesc/my:revision/my:minor"/> };

char g_build[]   = "<xsl:value-of select="my:devdesc/my:revision/my:extension"/>";

////////////////////////////////////////////////////////////////////////////
// DIRECT PROPERTY ACCESS

<xsl:apply-templates select=".//my:property" mode="deftoken"/>

////////////////////////////////////////////////////////////////////////////
// PROTOTYPES

<xsl:apply-templates select=".//my:property/my:handler" mode="handler_proto"/>
<xsl:apply-templates select=".//my:variable/my:handler" mode="handler_proto"/>
////////////////////////////////////////////////////////////////////////////
// DEVICE DECLARATIONS


enum {
	<xsl:apply-templates select="//my:device" mode="dev_index"/>
	N_DEVICES
};

<xsl:apply-templates select="//my:device" mode="dev_init"/>

// Global token pointers
<xsl:apply-templates select=".//my:property" mode="exttoken"/>

////////////////////////////////////////////////////////////////////////////
// SUPPORTED DEVICES


DeviceDesc g_devices[] = {
	<xsl:apply-templates select="//my:device" mode="dev_enum"/>
};

int g_ndevices = N_DEVICES;

////////////////////////////////////////////////////////////////////////////

// Second stage initialization

#ifndef C99_COMPLIANT

void init_properties(void)
{
<xsl:apply-templates select="//my:device" mode="call_init2"/>
}

#endif
<xsl:text>
</xsl:text>
	</xsl:template>
</xsl:stylesheet>
