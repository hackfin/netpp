#include "devlib_error.h"
#include "device_error.h"
#include <avr/io.h>
#include "portmux.h"

#include "register.h"

#define PORT_RESOLVE(x) PORT_##x

#define CAPS(x) (x >> 3)

#define XPDEF(pid, pin, caps) \
 	{ PORT_RESOLVE(pid), pin, CAPS(caps) }
	
#define PDEF(pid, pin, caps) \
	(PORT_RESOLVE(pid) << PORTID_SHFT) | \
	((pin) << PININDEX_SHFT) | \
	(caps) 
	

PortMap g_portmap[] = {
	/* 00 */ PDEF(B, 3,            PORT_DIGITAL | PORT_ALT ),
	/* 01 */ PDEF(B, 2,            PORT_DIGITAL | PORT_ALT ),
	/* 02 */ PDEF(B, 1,            PORT_DIGITAL | PORT_ALT ),
	/* 03 */ PDEF(B, 0,            PORT_DIGITAL | PORT_ALT ),
	/* 04 */ PDEF(D, 2,                           PORT_ALT ),
	/* 05 */ PDEF(D, 3,                           PORT_ALT ),
	/* 06 */ PDEF(D, 1,                           PORT_ALT ),
	/* 07 */ PDEF(D, 0,                           PORT_ALT ),
	/* 08 */ PDEF(D, 4, PORT_ADC | PORT_DIGITAL | PORT_ALT ),
	/* 09 */ PDEF(C, 6,            PORT_DIGITAL | PORT_ALT ),
	/* 10 */ PDEF(D, 7, PORT_ADC | PORT_DIGITAL | PORT_ALT ),
	/* 11 */ PDEF(E, 6,            PORT_DIGITAL | PORT_ALT ),
	/* 12 */ PDEF(B, 4, PORT_ADC | PORT_DIGITAL | PORT_ALT ),
	/* 13 */ PDEF(B, 5, PORT_ADC | PORT_DIGITAL | PORT_ALT ),
	/* 14 */ PDEF(B, 6, PORT_ADC | PORT_DIGITAL | PORT_ALT ),
	/* 15 */ PDEF(B, 7,            PORT_DIGITAL | PORT_ALT ),
	/* 16 */ PDEF(D, 6, PORT_ADC | PORT_DIGITAL | PORT_ALT ),
	/* 17 */ PDEF(C, 7,            PORT_DIGITAL | PORT_ALT ),
	/* 18 */ PDEF(F, 0, PORT_ADC | PORT_DIGITAL            ),
	/* 19 */ PDEF(F, 1, PORT_ADC | PORT_DIGITAL            ),
	/* 20 */ PDEF(F, 4, PORT_ADC | PORT_DIGITAL            ),
	/* 21 */ PDEF(F, 5, PORT_ADC | PORT_DIGITAL            ),
	/* 22 */ PDEF(F, 6, PORT_ADC | PORT_DIGITAL            ),
	/* 23 */ PDEF(F, 7, PORT_ADC | PORT_DIGITAL            ),
};

// #define HAS_CAPS(e, x) (e->caps & CAPS(x))
// #define GET_PIN(e)     (1 << e->pini)
// #define GET_PORTID(e)     (1 << e->portid)

#define HAS_CAPS(e, x) (*e & x)
#define GET_PIN(e)     (1 << ((*e & PININDEX) >> PININDEX_SHFT))
#define GET_PORTID(e)     ((*e & PORTID) >> PORTID_SHFT)

int set_portfunc(uint8_t which, uint8_t func)
{
	PortMap *entry;
	int error = 0;
	if (which > sizeof(g_portmap) / sizeof(PortMap))
		return DCERR_PROPERTY_RANGE;

	entry = &g_portmap[which];

	switch (func) {
		case PINMODE_DIGITALIN:
			if (HAS_CAPS(entry, PORT_DIGITAL)) {
				uint8_t v = ~GET_PIN(entry);
				switch (GET_PORTID(entry)) {
					case PORT_B: DDRB &= v; break;
					case PORT_C: DDRC &= v; break;
					case PORT_D: DDRD &= v; break;
					case PORT_E: DDRE &= v; break;
					case PORT_F: DDRF &= v; break;
					default:
						error = DCERR_PINFUNC; // Unsupported:
				}
			} else {
				error = DCERR_PINFUNC; // Unsupported
			}
			break;
		case PINMODE_DIGITALOUT:
			if (HAS_CAPS(entry, PORT_DIGITAL)) {
				uint8_t v = GET_PIN(entry);
				switch (GET_PORTID(entry)) {
					case PORT_B: DDRB |= v; break;
					case PORT_C: DDRC |= v; break;
					case PORT_D: DDRD |= v; break;
					case PORT_E: DDRE |= v; break;
					case PORT_F: DDRF |= v; break;
					default:
						error = DCERR_PINFUNC; // Unsupported:
				}
			} else {
				error = DCERR_PINFUNC; // Unsupported
			}
			break;
		case PINMODE_ANALOGIN:
			if (HAS_CAPS(entry, PORT_ADC)) {
			} else {
				error = DCERR_PINFUNC; // Unsupported
			}
			break;
		/*
		case PINMODE_ANALOGOUT:
			if (entry->caps & PORT_DAC) {
			} else {
				error = DCERR_PINFUNC; // Unsupported
			}
			break;
		*/
		case PINMODE_ALT:
			if (HAS_CAPS(entry, PORT_ALT)) {
			} else {
				error = DCERR_PINFUNC; // Unsupported
			}
			break;
		default:
			error = DCERR_PINFUNC; // Unsupported
	}
	return error;
}


int get_port(uint8_t which)
{
	PortMap *entry;
	if (which > sizeof(g_portmap) / sizeof(PortMap)) return -1;
	uint8_t v = 0;

	entry = &g_portmap[which];

	switch (GET_PORTID(entry)) {
		case PORT_B: v = PINB; break;
		case PORT_C: v = PINC; break;
		case PORT_D: v = PIND; break;
		case PORT_E: v = PINE; break;
		case PORT_F: v = PINF; break;
			return DCERR_PINFUNC; // Unsupported:
	}
	if (v & GET_PIN(entry)) return 1;
	return 0;
}

int set_port(uint8_t which, int level)
{
	PortMap *entry;
	uint8_t v;
	volatile uint8_t *pin;

	if (which > sizeof(g_portmap) / sizeof(PortMap))
		return DCERR_PROPERTY_RANGE;

	entry = &g_portmap[which];
	if (level) {
		v = GET_PIN(entry);
	} else {
		v = 0;
	}

	uint8_t mask = ~GET_PIN(entry);

	switch (GET_PORTID(entry)) {
		case PORT_B: pin = &PORTB; break;
		case PORT_C: pin = &PORTC; break;
		case PORT_D: pin = &PORTD; break;
		case PORT_E: pin = &PORTE; break;
		case PORT_F: pin = &PORTF; break;
		default:
			return DCERR_PINFUNC; // Unsupported:
	}

	uint8_t r = (*pin & mask) | v;
	*pin = r;

	return 0;
}
