/** \file slave.c
 *
 * Slave property dispatcher
 * Property protocol back end (embedded system, etc.)
 *
 * (c) 2006 Martin Strubel <hackfin@section5.ch>
 *
 */

#ifdef DEBUG
#define DEB(x) x
#endif

#include "netpp.h"
#include "slave.h"
#include "uart.h" // XXX
// #include "example.h"
#include <stdio.h>

void devicemap_init(void);
void system_init(void);
void devices_reset(void);
void report_error(const char *msg);
void display_bootinfo(void);
void display_status(int error);
void display_status_init(void);
int led_progress(void);
void display_error(int code, int wait);

void delay_ms(int n);

/** Function retrieving the root node.
 */

TOKEN local_getroot(DEVICE d)
{
	// This is the device index of the derived class.
	// This has the value of g_index_derived.
	// FIXME: This should be taken from a generated header file.

	TOKEN index = 0;
	return DEVICE_TOKEN(index);
}

/** Device flat address register map write access.
 * For low level device access (SPI, I2C, etc.) this normally wants to
 * be implemented */


static int s_errno = 0;

struct hub_list my_hubs[] = {
	{ "DEV#1", &dev_methods },
	{ NULL }
};

static PropertyPacketHdr g_pkthdr;

void request_packet_header(Peer *peer, PropertyPacketHdr **h, unsigned int len)
{
	*h = &g_pkthdr;
}



void display_bootinfo(void)
{

}

extern int (*console_printf)(const char *fmt, ...);

int null_printf(const char *fmt, ...)
{
	return 0;
}

// EXT_PROG
int start_server(int argc, char **argv)
{
	int error;

	devicemap_init();
	error = ports_init(my_hubs);
	if (error < 0) {
		return error;
	}
	RemoteDevice dev;

	// uart1_puts("Init device server..\n");
	error = dev_server_init(&dev.peer, "avr");
	if (error < 0) {
		// uart1_puts("Failed to init device server..\n");
		while (1) display_error(-error, 9999);
	}

	// uart1_puts("Starting server..\n");

#if 0

	char buf[32];
	int a;

	while (1) {
		a = my_poll(0, 0, 100);
		if (a > 0) {
			a = my_read(0, buf, 31);
			if (a > 0) {
				my_write(0, buf, a);
			}
		} else if (a < 0) console_printf("Error: %d\n", a);
	}
#else
	console_printf = null_printf;
#endif

	while (1) {
		error = slave_handler(&dev);
		s_errno = error;
	
		switch (error) {
			case DCERR_COMM_CRC:
			case DCERR_COMM_NAK:
				display_error(error, 10);
				break;
			case DCERR_COMM_TIMEOUT:
				display_error(error, 25);
				break;
			case DCERR_PROPERTY_HANDLER:
				error = 0;
				break;
			case DCERR_COMM_OVERRUN:
			case DCERR_COMM_FRAME:
			case DCERR_COMM_GENERIC:
				// devices_reset();
				display_error(error, 75);
				break;
			default:
				if (error < 0) {
					display_error(error, 250);
					display_error(0, 125);
					display_error(error, 250);
				}
		}
	
	}

	while (1) display_error(0, 9999);

	return error;
}

////////////////////////////////////////////////////////////////////////////

#ifdef USER_INIT
void user_init(void)
{

}
#endif


int main(int argc, char **argv)
{
	int error;

	system_init();
	// display_bootinfo();

	// Note: From here we can start calling functions in external (cached)
	// memory

	// Important: Register property list
	register_proplist(g_devices, g_ndevices);
	uart1_puts("Registered properties\n");
#ifndef TINYCPU
	memset(g_globals.buffer, 0xff, BUFSIZE);
#endif

	// XXX

	error = start_server(argc, argv);
	return error;
}

