/** \file slave.c
 *
 * Slave property dispatcher
 * Property protocol back end (embedded system, etc.)
 *
 * (c) 2006 Martin Strubel <hackfin@section5.ch>
 *
 */

// For video display

#ifdef DEBUG
#define DEB(x) x
#endif

#include <stdio.h>
#include "devlib.h"

#include "display.h"

// Common slave server
#include "slave.h"
#include <SDL/SDL.h>

int device_write(RemoteDevice *d,
		uint32_t addr, const unsigned char *buf,
		unsigned long size)
{
	return 0;
}

int device_read(RemoteDevice *d,
		uint32_t addr, unsigned char *buf, unsigned long size)
{
	return 0;
}



int main(int argc, char **argv)
{
	int error;


	// Important: Register property list
	register_proplist(g_devices, g_ndevices);
	error = start_server(argc, argv);
	if (error)
		printf("Server exited with return code:\n"
			"'%s'\n", dcGetErrorString(error));
	return error;
}


