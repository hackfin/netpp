# LUT utilities for netpp display
#
# (c) 2005-2015 <hackfin@section5.ch>
#

import struct

setrgb = lambda triple, i: (triple[P[i][0]], triple[P[i][1]], triple[P[i][2]] )

P = [ (2, 0, 1), (2, 1, 0), (0, 1, 2), (1, 0, 2), (1, 2, 0), (0, 2, 1) ]


def hwb2rgb(hwb):
	"""
Ranges:
	h [0:0x600]
	w [0:255]
	b [0:255]
"""
	h, w, b = hwb

	# h %= 0x600

	v = 255 - b
	i = (h >> 8) & 0xf
	f = h & 0xff

	if i & 1:
		f = 255 - f

	u = w + ((f * (v - w)) >> 8)
	try:
		rgb = setrgb((u, v, w), i)
	except IndexError:
		rgb = (127, 127, 127)

	return rgb


def convert_default(v):
	if v < 0x100:
		hwb = (0x100 - v, 0, 255 - v / 2 )
	else:
		v -= 0x100
		v >>= 4
		if v < 0x600:
			hwb = (v, 0, 127 - v / 12)
		elif v < 0x0c00:
			v -= 0x600
			hwb = (v, v / 12, 0)
		else:
			v -= 0xc00
			hwb = (0x000, 127 + v / 8, 0)
		
	return hwb2rgb(hwb)


def convert_gamma(v, gamma):
	v = (v / 65535.0) ** gamma
	return convert_default(int(65535 * v))


def gen_lut16(convert, arg):
	buf = ""
	for i in range(0x10000):
		r, g, b = convert(i, arg)
		buf += struct.pack("BBB", r, g, b)

	return buffer(buf)

