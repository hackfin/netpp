import sys
import netpp
import pytest
import time
import os
import subprocess
import tempfile
import threading

class StdoutBuffer(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.daemon = True
		self.fdRead, self.fdWrite = os.pipe()
		self.pipeReader = os.fdopen(self.fdRead)
		self.lines = []
		self.start()

	def fileno(self):
		return self.fdWrite
	# end fileno

	def run(self):
		while True:

			messageFromPipe = self.pipeReader.readline()

			if len(messageFromPipe) == 0:
				self.pipeReader.close()
				os.close(self.fdRead)
				return
			if messageFromPipe[-1] == os.linesep:
				messageToLog = messageFromPipe[:-1]
			else:
				messageToLog = messageFromPipe
			self._write(messageToLog)
		print 'Redirection thread terminated'

	def _write(self, message):
		self.lines.append(message)

def test_localapi():
	buf = StdoutBuffer()
	test = subprocess.Popen(["devices/local/test", "LOC:0", "HandledArray[0]"],
	stdout = buf)

	test.wait()
	time.sleep(0.2) # Hack: sleep to make sure we got something in buffer
	print buf.lines
	assert buf.lines[3] == "Value: item_0"


