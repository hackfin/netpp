
#include "proxy.h"
#include <stdlib.h>

int proxytokens_init(RemoteDevice *d, struct proxy_tokens *t)
{
	int error = 0;

	error = dcProperty_ParseName(d, "Scan", &t->scan);
	if (error == 0)
		error = dcProperty_ParseName(d, "PeerID", &t->peerid);
	if (error == 0)
		error = dcProperty_ParseName(d, "Open", &t->open);
	if (error == 0)
		error = dcProperty_ParseName(d, "Close", &t->close);
	if (error == 0)
		error = dcProperty_ParseName(d, "ProxyVersion", &t->version);

	return error;
}

int proxytokens_new(RemoteDevice *d)
{
	struct proxy_tokens *ptr;
	ptr = (struct proxy_tokens *) malloc(sizeof(struct proxy_tokens));
	if (!ptr) return DCERR_MALLOC;
	proxytokens_init(d, ptr);
	d->localdata = (void *) ptr;
	return 0;
}

void proxytokens_delete(RemoteDevice *d)
{
	free(d->localdata);
}

int proxy_open(DEVICE rd, const char *prefix, char *name)
{
	int error;
	int index;
	DEVICE d;
	DCValue val;
	char buf[64];
	char *id = &buf[4];
	char ep[64];

	// Copy parent server identifier:
	memcpy(buf, rd->peer.hub->name, 3); buf[3] = ':';

	// printf("proxy_open(): full name %s\n", name);

	index = scan_into(name, '+', ep, sizeof(ep));

	// Did we specify an end point?
	if (index > 0) {
		if (index < (sizeof(buf) - 4)) {
			string_copy(id, name, index - 1); id[index-1] = '\0';
			netpp_log(DCLOG_VERBOSE, "Opening Proxy+EP: '%s+%s'",
				buf, &name[index]);
			error = dcDeviceOpen(buf, &d);
		} else error = DCERR_PROPERTY_SIZE_MATCH;
		if (error < 0) return error;

		error = proxytokens_new(d);
		if (error < 0) {
			netpp_log(DCLOG_VERBOSE,
				"Failed to initialize tokens: no proxy?\n");
			goto cleanup;
		}
		val.type = DC_UINT;
		error = dcDevice_GetProperty(d, THIS_DEVICE_TOKEN(d, version), &val);
		if (error || val.value.i != PROXY_PROTOCOL_VERSION) {
			proxytokens_delete(d);
			netpp_log(DCLOG_ERROR,
				"Proxy version mismatch (is: %d), cannot open.\n", val.value.i);
			error = DCERR_REVISION_UNSUPPORTED;
			goto cleanup;
		}

		// Pass endpoint index to "open":
		val.value.p = &name[index]; val.type = DC_STRING;
		error = dcDevice_SetProperty(d, THIS_DEVICE_TOKEN(d, open), &val);
		if (error < 0) {
			proxytokens_delete(d);
			goto cleanup;
		}
	} else {
		// Open proxy only:
		string_copy(id, name, sizeof(buf) - 4);
		// Here we open the raw device:
		memcpy(buf, prefix, 4);
		netpp_log(DCLOG_DEBUG, "<< Opening raw proxy '%s'", buf);
		error = dcDeviceOpen(buf, &d);
		if (error < 0) 
			return error;
		else 
			d->localdata = NULL;
	}

	if (error == 0) {
		// Now we have to clone the proxy device peer structures into
		// the RemoteDevice `rd` handle data with one exception:
		// We have to keep the original parent hub to be sure we
		// do all remote procedure calls in proxy mode.
		DynPropertyDesc *hub;
		hub = rd->peer.hub; // Save original hub
		memcpy(&rd->peer, &d->peer, sizeof(Peer));
		rd->peer.hub = hub; // Restore original hub
		rd->localdata = d->localdata;
		rd->flags = F_PROXY;
	}
cleanup:
	// We don't need the d struct anymore, because
	// we now own all its children:
	free(d);
	return error;
}

int proxy_close(RemoteDevice *d)
{
	int error;
	DCValue val;

	netpp_log(DCLOG_DEBUG, ">> Closing endpoint...");
	val.value.i = 1; val.type = DC_COMMAND;
	// Only when we are proxying:
	if (d->localdata) {
		error = dcDevice_SetProperty(d, THIS_DEVICE_TOKEN(d, close), &val);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Failed to close proxy hop (err %d)", error);
		}
		proxytokens_delete(d);
	}
	ports_exit(0);
	return 0;
}

int user_device_init(RemoteDevice *rdev, PortToken p)
{
	rdev->refcount = 0;
	rdev->localdata = NULL;
	rdev->cleanup = NULL;
	return 0;
}
