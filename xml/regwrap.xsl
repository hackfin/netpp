<?xml version="1.0" encoding="ISO-8859-1"?>

<!--

$Id: $

Transformation style sheet to generate a register wrapper skeleton.
Takes all registers from the first device in a device file and
turns them into directly mapped properties.

-->

<xsl:stylesheet version="1.0" 
	xmlns="http://www.section5.ch/dclib/schema/devdesc"
	xmlns:my="http://www.section5.ch/dclib/schema/devdesc"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

	<xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>	

	<!-- ID (string) of desired device -->
	<xsl:param name="selectDevice"></xsl:param>

	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>

	<xsl:template name="plainregister">
      <property type="REGISTER">
			<xsl:attribute name="name">
				<xsl:value-of select="@id"/>
			</xsl:attribute>
        <info><xsl:value-of select="my:info"/></info>

			<regref>
				<xsl:attribute name="ref">
					<xsl:value-of select="@id"/>
				</xsl:attribute>
			</regref>
      </property>
	</xsl:template>

	<xsl:template name="bitfieldregister">
      <struct>
			<xsl:attribute name="name">
				<xsl:value-of select="@id"/>
			</xsl:attribute>
        <info><xsl:value-of select="my:info"/></info>
		<xsl:apply-templates select=".//my:bitfield" mode="copy"/>
      </struct>
	</xsl:template>


	<xsl:template match="my:bitfield" mode="copy">
      <property>
			<xsl:attribute name="name">
				<xsl:value-of select="@name"/>
			</xsl:attribute>

			<xsl:choose>
				<xsl:when test="@lsb = @msb">
				<xsl:attribute name="type">BOOL</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
				<xsl:attribute name="type">INT</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>

			<info><xsl:value-of select="my:info"/></info>

			<regref>
				<xsl:attribute name="ref">
					<xsl:value-of select="../@id"/>
				</xsl:attribute>
				<xsl:attribute name="bits">
					<xsl:value-of select="@name"/>
				</xsl:attribute>
			</regref>

      </property>

	</xsl:template>

	<xsl:template match="my:register" mode="any">
		<xsl:choose>
			<xsl:when test="./my:bitfield">
				<xsl:call-template name="bitfieldregister"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="plainregister"/>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template match="my:registermap" mode="wrap">
		<group>
			<xsl:attribute name="name">
				<xsl:value-of select="@name"/>
			</xsl:attribute>
			<xsl:if test="./@id">
				<xsl:attribute name="id">
					<xsl:value-of select="concat('regprops_', @id)"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select=".//my:register" mode="any"/>
		</group>

	</xsl:template>

	<xsl:template match="my:device">
		<device>
			<xsl:attribute name="id">
				<xsl:value-of select="@id"/>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="@name"/>
			</xsl:attribute>
			<xsl:attribute name="protocol">PROPERTY</xsl:attribute>
			<xsl:apply-templates select="my:revision"/>
			<xsl:apply-templates select="my:registermap"/>
			<xsl:apply-templates select="my:registermap[not(@nodecode='true') and not(@hidden='true')]" mode="wrap"/>
			<!-- Copy original groups: -->
			<xsl:apply-templates select="./my:group"/>
		</device>
	</xsl:template>


	<xsl:template match="/my:devdesc">
		<devdesc>
			<xsl:apply-templates select="@*"/>

			<xsl:apply-templates select="my:revision"/>

		<xsl:choose>
			<xsl:when test="$selectDevice">
				<xsl:apply-templates select="my:device[@id=$selectDevice]"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="my:device"/>
			</xsl:otherwise>
		</xsl:choose>

		</devdesc>
	</xsl:template>

	<xsl:template match="/">
		<xsl:comment>This is a GENERATED file. Editing may be void.</xsl:comment>
		<xsl:comment>Contains autowrapped register-properties</xsl:comment>

		<xsl:apply-templates/>

	</xsl:template>


</xsl:stylesheet>


