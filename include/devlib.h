/** \file devlib.h
 *
 * $Id: devlib.h 335 1970-01-06 02:51:45Z strubi $
 */

#include "devlib_types.h"  // Device library data types


/*!
 * \mainpage NETPP Documentation
 * \version 0.50 (eval and commercial)
 * \author Martin Strubel
 * \date 02/2013
 *
 * \section Intro   Introduction
 * 
 * The Device Control library (DCLIB) is a library of tools and C source code
 * designed to remote control properties of devices and quickly prototype
 * access to registers on an embedded device.
 * The NETPP enhancement of this library is a network variant
 * of the DCLIB where most of the intelligence is outsourced to the
 * device controller, therefore avoiding raw register protocols between
 * the embedded devices.
 *
 * A NETPP capable device in a network can communicate its properties
 * to a client in a flexible way. A client can talk to devices of completely
 * different architecture, it is up to the client software to check the
 * device's capabilities according to whether a property exists, or according
 * to a user defined device type field.
 *
 * To keep the overhead at a minimum for embedded targets, properties
 * are defined as a static array of structures. Those structures are not
 * spelled out manually in source code, but generated from an XML description.
 * The XML description supports several data types (INT, BOOL, BUFFER, etc.)
 * as well as different access methods (global variables, handler functions,
 * etc.)
 *
 * In short, a few more properties of the netpp library:
 *
 * -  Lean and mean: Very small memory foot print, little overhead and
 *      optimized for 32 bit capable embedded systems and little CPU usage
 * -  Thread safe: Master and slaves can be run simultaneously on 
 *      POSIX style operating systems
 * -  Multi user capable: Several users can connect to the target and
 *      query the status. Concurrent accesses are sorted out entirely by
 *      user application.
 * -  Object oriented runtime data structures with scatter/gather
 *      features for least buffer transfer overhead
 *
 * Note that netpp does not cover 16 bit TOKEN size anymore.
 * Since most compilers support 32 bit values ('long'), this is
 * not a big issue. Optionally, a non-network variant can be built
 * using specific binary protocols.
 *
 * \section Programs  Client and Server programs
 *
 * To outline the functionality of NETPP: A typical application will
 * consist of a server running on the device to be controlled, and a
 * frontend on the client PC to talk to it. This is in general referred to
 * as Master (PC, User) and Slave (responding Device).
 * A Slave will, under normal circumstances, never initiate a connection.
 *
 * To summarize the terminology:
 * -  Master(typically PC): Runs the client
 * -  Slave(remote device): Runs the server and listens to clients
 *
 * NETPP provides two rudimentary frontends:
 *
 * -# A example test program, allowing to browse the property hierarchy,
 *     getting and setting of properties. See \c master/ folder.
 * -# A python module, allowing scripted remote control and easy
 *     system testing. See \c python/ folder.
 *
 *
 * The example program's source code is documented under the Examples
 * section (see \ref master.c)
 *
 * \section Api   API Details
 *
 * The API interface uses the standard C convention (CDECL) interface.
 * NETPP is portable among different operating systems such as
 * Microsoft Windows, Linux, QNX, and other Unix versions. NETPP can also
 * run on 'bare metal', i.e. without operating system using a stripped
 * down C library.
 *
 * All Master functionality is hinted with the prefix 'dc' in the
 * listed API functions.
 * The Slave functionality is not completely documented, please see
 * \ref Backend and \ref Driver for hints and examples.
 *
 * \subsection props   Property concept
 *
 * - Properties have more extended types such as structs and arrays and
 *     can contain other Property members
 * - Max and min values are encoded the same way in a parent/children
 *     hierarchy
 * - Possible Mode choices can be encoded in this tree concept as well
 *
 * \image latex properties.pdf "Property" width=15cm
 *
 * The architecture also allows to integrate user defined children types
 * to a data type via structs that allow signalling events to a specific GUI
 * element when a Property was changed, etc.
 *
 * To keep the API and the library code as compact and efficient as possible,
 * the Property access happens via tokens. The token is obtained by 
 * dcProperty_ParseName() via a given name, which is defined in the name
 * space of a certain parent node. The top root node of a Property
 * hierarchy is always the device node which is obtained by dcDevice_GetRoot().
 *
 * Some TOKEN values have special meanings, such as #TOKEN_CHECK
 * or #TOKEN_INVALID.
 *
 * Value passing is done via a runtime data type information capable structure
 * 'DCValue'. This structure contains a data union field and an associated
 * data type. The value is retrieved from the DCValue struct according
 * to the type field. The Property getter and setter functions of the library
 * perform a type check and return an error (#DCERR_PROPERTY_TYPE_MATCH) in
 * case of a mismatch.
 *
 * Strings and buffer structures are normally owned by the caller and are
 * not allocated statically by NETPP, unless they are local properties
 * like Ports or port properties. Local properties are \a only queried
 * through the pseudo device handle #DC_PORT_ROOT.
 *
 * When obtaining a buffer, the caller always must preinitialize the DCValue
 * with a pointer to the reserved buffer and the correct length value to
 * prevent buffer overflows.
 * This is done as follows:
 *
 \code
unsigned char buffer[32];
DCValue v; v.value.p = buffer; v.len = sizeof(buffer); v.type = DC_BUFFER\endcode
 *
 * Then, a dcDevice_GetProperty() or dcDevice_SetProperty() can take place.
 * For more details, see \ref String and example \ref master.c.
 *
 * When a string or buffer is allocated static (for example, a port
 * descriptor or other local property structures), \c v.value.p
 * does not need to be preinitialized. It points to the static buffer location
 * of the requested property after a successful call. So, it is in general
 * not a good idea to assign v.value.p to a malloc() return value directly,
 * as it may be modified within a GetProperty() call and you would be left
 * with a memory leak. Normally, a programmer using the API to access remote
 * devices does not have to worry about static properties. They only need to
 * be taken care of when
 *
 * - A local structure like a Hub or Port descriptor is accessed via
 *   the #DC_PORT_ROOT pseudo device
 * - A legacy variant of the DClib without network extension is used to
 *   access static string or buffer Properties.
 *
 * If the data type is not known a priori, v.type can be set
 * to DC_UNDEFINED. The handler then fills in the data type.
 *
 * If the buffer size can be dynamic on the device side, it may be desired to
 * query the buffer size beforehand. This is done via
\code
DCValue v;
retcode = dcProperty_GetProto(device, T_property, &v);\endcode
 *
 * Note: This does not work with buffer properties that are implemented
 * via a handler (non static buffers). This is a current limitation of
 * the library. In case of a handled (dynamic) buffer, v.len contains 
 * 0 for a buffer or 1 for a string. To query the buffer size of a dynamic
 * buffer, you must issue a dcDevice_GetProperty() attempt to a zero length
 * buffer. See \ref String how this is achieved.
 *
 * \subsection commonfunc Common function parameters
 *
 * Generally, the first parameter of a function whose name starts with
 * 'dcDevice_' is the current device device handle. The struct members of
 * the device object must NEVER be accessed directly, to preserve future
 * binary compatibility.
 *
 * If not documented otherwise, the function's return value is 
 * -  < 0 on failure
 * - == 0 on success
 * -  > 0 a warning
 *
 * \see ErrorHandling
 *
 * Functions which set a value may return #DCWARN_PROPERTY_MODIFIED, meaning
 * that the given parameter was not valid or not according to certain
 * constraints. All 'Event' children of this property should be queried via
 * dcProperty_Select(), because they may have changed.
 * In this case, the user should also read back the value that was passed
 * to dcDevice_SetProperty() which was possibly modified 'in place'.
 *
 * Error codes are split in library specific and device specific error codes.
 * Typically, an application controlling a backend includes "devlib_error.h"
 * (containing the generic library error codes) and "DEVICE/device_error.h"
 * (device and backend specific error codes). This file is normally found
 * in the device specific directory netpp/devices/DEVICE (where DEVICE is to
 * be replaced by the device implementation name).
 * It is recommended, to set the include paths as follows:
 * \code
INCLUDES = -I$(NETPP)/include -I$(NETPP)/devices \endcode
 *
 * The function dcGetErrorString() returns the library specific (device
 * independent) error codes. Normally, an application first checks for
 * device errors and then calls this function to get a verbose error
 * message. To create a user defined error handler, you may want to derive
 * an automatic generation from \c $NETPP/xml/errorhandler.xsl.
 *
 *
 * \section Impl   Property Implementation
 *
 * \subsection PropDef   Property definition
 *
 * The property definition happens in XML according to a schema. A property
 * always has a type specification and an access description, the
 * 'access method', which is described within a property definition.
 * To map a device side variable, flag, or buffer into a property, there are
 * several approaches.
 * - \b Register \b map: For very simple devices that use a peripheral
 *      register map, direct access to registers and bits can be implemented.
 * - \b Static variable: To access static global variables, a 'variable'
 *      specification can be used.
 * - \b Static value: A value stored directly in the property list, typically
 *      a readonly string. 'Min' and 'Max' children properties are e.g.
 *      static.
 * - \b Handler: For complicated access, a handler, i.e. Getter and Setter
 *      function is to be implemented.
 *
 * Not all access methods make sense for specific data types. The XML
 * language schema however allows some illegal combinations to occur.
 * As they might be possible in later versions or different implementations
 * (for example local protocols), the rule applies: Not all what can be done
 * in the language is implemented.
 *
 * For supported configurations, please start from the example in
 * devices/example/example.xml.
 *
 * To write the XML file, it is recommended to use an XML editor like
 * XMLMind Xml Editor, see http://www.xmlmind.com. The provided schema
 * files and style sheets are customized especially for this editor.
 * See an example XML file below. To customize XXE such that it is aware
 * of the device description XML language, please refer to the netpp HOWTO
 * found at the link in \ref Resources.
 *
 * \image html xxe.png "XMLMind XML Editor example"
 *
 * The style sheet is designed such that the item hierarchy is visible in
 * coloured blocks. The underlying Schema files guarantee that the syntax is
 * always correct and that the hierarchy is built according to the
 * schema rules.
 *
 * \subsection Proptypes      Property types
 *
 * As of now, the following property types exist:
 *
 * - BOOL: true (1) and false (0). This can refer to a single bit of a
 *     register.
 * - INT: An abstract integer. Currently, no size types are defined for
 *     netpp. The size of an integer is generally specified by the referred
 *     register, plus the valid range is specified by Min and Max children of
 *     an integer property. The maximum binary length of an integer is
 *     signed 32 bit.
 * - MODE: A mode property which is in fact an integer of a certain set or
 *     choice. The mode property has a 'choice' child which again contains
 *     item children of fixed static values.
 * - FLOAT: A IEEE 32 bit float value. No register mapping is specified
 *     for floats, a handler must be used. Min and Max children specify
 *     the valid range.
 * - STRING: A zero-terminated string. See \ref String below.
 * - BUFFER: A binary buffer with specified length. See \ref String
 *      below.
 * - COMMAND: A command definition. This normally calls a handler function.
 *     Normally, a command is write only. Commands returning a value can be
 *     queried to check the busy status. See also \ref Handlers.
 *
 * \subsection Special        Special properties
 *
 * Properties can be part of a container such as a Struct or Array node.
 * In the XML description, they display like a property, but contain other
 * properties. A Struct node is a very simple container, it is just a local
 * name space for properties. For example, you would access a property inside
 * a struct via the name "VideoStream.Enable". The members of a struct
 * may be restricted by the schema file to a certain number. This limitation
 * can be changed by the user.
 *
 * Array properties can either contain:
 * - A struct node, access example: "Coordinates[3].X"
 * - A Property, access example: "Vector[2]"
 *
 * Currently, only 1-dimensional arrays are supported for register
 * addressing mode. In some implementations, two dimensional arrays may
 * be supported via handlers.
 *
 * The size of an array can always be queried via the special child 'Size',
 * e.g. "Coordinate.Size". The second children of an array node is always
 * the contained object. Its name does not matter, since it is addressed
 * by the index automatically.
 *
 * \subsubsection HandledArray Handled properties in an array
 *
 * New with netpp 0.35 is the re-implementation of array properties
 * that use a handler.
 * The DCValue struct has an index field which is used to pass an
 * index (or offset) to a standard getter and setter.
 * See also example for the HandledArray property.
 * Note that the array index can be calculated into an offset when a
 * bank size attribute is specified. See section below.
 *
 * \subsubsection ArrayStruct Structures in Arrays
 *
 * Assume, you have a register table with values [X, Y, W, H, Mode] somewhere
 * in your chip address space. You'd want to save these values in a
 * non-volatile space on the chip for fast context switching. So you would have
 * a copy of this table somewhere else in memory. This lists in short, how
 * you wrap this with a struct/array/property hierarchy:
 *
 * -# Create an array node and a struct node as child. Inside this struct,
 *     you define the member properties according to the register table.
 * -# Add a 'array base' register to your register table. The 'size'
 *     attribute of this register defines the field size inside the array,
 *     like the address difference (not necessarily in bytes) from
 *     element[0] to element[1]. The offset address should be set to 0x0.
 *     The 'size' is also internally referenced as 'banksize'.
 * -# Refer to this base register from within the array node via a 'regref'.
 * -# For each struct member, create a register entry using the absolute
 *     address of the struct member in the <b>first</b> array element.
 * -# Refer to this register definition using a 'regref' node within the
 *     struct property member
 *
 * An example is found in the LUTvalues array property in
 * devices/example/example.xml
 *
 * If the structured register tables are organized contiguously (respectively,
 * using a fixed address spacing between the tables), they can be simply
 * addressed like "Context[index].Member".
 *
 * \subsection Translation    Translation to source code
 *
 * The XML device description is translated to source code via the
 * XML processor 'xsltproc', which must be installed separately from netpp.
 * It is part of every standard Linux distribution or Cygwin distribution on
 * Microsoft Windows.
 *
 * The generation of the source code happens according to XSLT style
 * sheets that are residing in the xml/ sub folder of the netpp distribution.
 *
 * \image html scheme.png "XML conversion scheme"
 *
 * A makefile will do the following, by running it from the build directory
 * via 'make':
 *
 * - Convert the XML device file into a C property table
 * - Compile the netpp framework
 * - Create a device control 'backend' library
 *
 * Optionally, the following type of files can be generated from the
 * XML device description:
 *
 * - Documentation (&lt;info&gt; tags)
 * - VHDL register maps
 *
 * See \c devices/ for example devices and their makefiles.
 *
 * \subsubsection Xsltproc Xsltproc and Xincludes
 *
 * Since it is possible to include elements from other XML files in the
 * devdesc dialect, a somewhat complex devdesc file can lead to a somewhat
 * deeper hierarchy of inclusions. To avoid problems with some xsltproc
 * implementations, an intermediate file 'xpointer.xml' is first created
 * from the device file. This is then actually translated into source.
 * This may change in future. Just keep this in mind when writing your own
 * Makefile rules for xslt translations. See xml/prophandler.mk for details.
 *
 * \subsection String   String and buffer handling
 *
 * On device side, an implementation of a buffer property basically consists
 * of an address and length field. The 'Property Engine' is responsible of
 * tunneling the data through a specific protocol with minimum buffer copy
 * overhead.
 *
 * The difference between string and buffers is actually none, in terms
 * of netpp handling. However the user must keep in mind, that <b>dynamic</b>
 * buffers and strings are <b>always owned by the caller</b>,
 * not by the library.
 * Thus, the DCValue proxy must always be initialized correctly when
 * transferring strings via handlers, moreover, size checks and handling
 * conditions for size mismatches <b>must</b> be implemented in the handler.
 *
 * When a (static) variable specification is used, the engine knows the address
 * immediately from the specification. The only thing the caller, i.e.
 * remote control host needs to do, is to reserve enough memory for the
 * string or buffer data, see \ref props.
 * Using a handler, i.e. when dynamic allocations and assignments are needed,
 * things can get more complicated. First, the handler needs
 * to do a check on the buffer size and return a #DCERR_PROPERTY_SIZE_MATCH,
 * if the caller's buffer is too small. Or, adapt to the situation and send
 * data in chunks.
 * The handler actually passes the buffer address to the engine <b>before</b>
 * the transfer. When the transfer is finished, the handler is called again
 * with a DCValue type field set to DC_COMMAND. This signals, that the
 * buffer was transferred and an update action can take place, for example
 * when an image is to be redrawn. See also example \ref handler.c set_buffer().
 *
 * This scheme is applying to buffer reads and writes. For a setter, the
 * handler is notified about the transfer termination (all data arrived)
 * Likewise, the getter is informed that all data was transferred and that
 * the owner may release the allocated buffer space.
 *
 * \attention The true length of a string is including the terminating
 *            zero character! Thus, always use (strlen(str)+1) when
 *            initializing ((DCValue) v).len
 *
 * There are some tricky details concerning dynamic buffer handling and
 * various ways to do it. One important detail is the support for clients
 * who do not have a notion of the buffer size a priori. A buffer data and size
 * query could be realized using a struct property, for example
 * Buf.Size and Buf.Data, but there is actually a built-in method when
 * reading a buffer property: A handler can return #DCERR_PROPERTY_SIZE_MATCH 
 * to encourage the client to adapt to the actual buffer size which is
 * stored in the len field. This is actually the method used by the python
 * wrapper.
 *
 * \code
DCValue val;
val.value.p = (char *) ""; 
val.len = 0; // Set val to zero to make this attempt fail:
ret = dcDevice_GetProperty(device, prop, &val);
// Now, val.len contains the real size of the buffer:
if (ret == DCERR_PROPERTY_SIZE_MATCH) {
	val.value.p = reserve_memory(val.len);
	// and retry:
	ret = dcDevice_GetProperty(device, prop, &val);
}
... \endcode 
 *
 * Instead of returning an error code (which could make other clients fail)
 * it is also possible to return a warning #DCWARN_PROPERTY_MODIFIED
 * to avoid this being an error. For example, if you request a number of
 * bytes from a Stream Property (which is of a BUFFER type), but only a
 * smaller number of bytes are available. In this case, it is rather desired
 * to return the available bytes, but warn the caller that his request was
 * only partially fulfilled.
 *
 * When an error occurs during the buffer request phase (before the transfer),
 * the property dispatcher fails early and does not call the handler again
 * in DC_COMMAND mode. However, if you pass a #DCWARN_PROPERTY_MODIFIED
 * during the buffer request (DC_STRING/DC_BUFFER) phase, this value may
 * be overriden by an error code in the DC_COMMAND case.
 *
 * A number of strategies and standard techniques is explained in the
 * separately distributed netpp HOWTO or user manual.
 *
 * \subsubsection StringPitfalls  String/Buffer pitfalls
 *
 * A classic problem in C and similar languages is ownership of strings
 * or buffers. For the client side, we summarize again:
 * - A string queried from a peer by a <b>master</b> is always owned by the
 *   calling routine.  So, it is safe to allocate space on the stack
 *   within the callers context.
 * - On a <b>slave</b> handler, a dynamic string must survive two calls of
 *   the same handler, thus <b>it can not live on the stack</b>. This either
 *   requires to reserve a global buffer context, or a locally allocated buffer
 *   that can be released after transfer. A global buffer can be somewhat
 *   problematic, when several device instances are used or if a handler
 *   serves several threads. In this case, it may be necessary to assign
 *   ownership of the buffer to the remote device handle. For this, a
 *   localdata pointer is available in the DCDevice structure.
 *
 * \subsection Event    Variable changes, events
 *
 * In many devices, settings of a property can have impact on another.
 * For example, changing the readout width of a optical sensor may
 * affect the timing. Or very simple, issueing a Reset command on the
 * target requires some registers being reread by the client to synchronize
 * with the user interface.
 *
 * To address this issue, an event node has to be inserted into the property.
 * When the handler returns #DCWARN_PROPERTY_MODIFIED, the event children
 * must be polled as well. An event node simply contains a 'id' reference
 * to another node (which currently must be a property).
 *
 * For more complex, dynamic rebuilds, you have to use your own mechanisms
 * on a higher level within netpp.
 *
 * Event properties are static and have an 'EVT_' prefix in their property
 * name.
 *
 * \section Ports Hubs and Port access
 *
 * A Hub represents a specific interface respectively transport layer,
 * for example USB or TCP. It is for example possible, to control the
 * same device Slave via different interfaces. Hubs are instanced as dynamic
 * property nodes, similar to the static property lists, so they can have
 * children. A Port again is always a children of a Hub and represents
 * an attached device that can be opened.
 *
 * Normally, Hubs are only relevant to the master, however, a Slave
 * uses Hubs internally for each interface it listens to. Ports do not
 * have a meaning on the Slave side, so this paragraph applies to Master
 * actions only.
 *
 * To browse this hierarchy, a special device handle #DC_PORT_ROOT exists.
 * Actions on this pseudo device, such as dcProperty_Select() operate on
 * the local port tree structure. See also example \ref master.c for details.
 * To select the first available hub, you have to call dcProperty_Select()
 * as follows:
 * \code
TOKEN parent = TOKEN_INVALID;
error = dcProperty_Select(DC_PORT_ROOT, parent, parent, &child);\endcode
 * Whenever this happens, the ports are initialized and probed.
 * Thus, an update of a device list procedure automatically rescans the
 * available hubs for newly attached devices that way.
 *
 * \section Backend Implementing a NETPP server backend on Slave
 *
 * For implementing a NETPP server, the following steps need to be taken:
 *
 * -# Create a device file in XML
 * -# Write property handlers if needed (see \ref Handlers below)
 * -# Implement device backend stubs/handlers
 * -# Register handlers/properties in your server code
 *
 * For step 1, there are a few different approaches. One scenario is, that
 * you have a set of existing global variables that you want to export to
 * NETPP. Another approach is, to start the property design first and then
 * implement the variables and handlers where necessary.
 *
 * As as starting point, you may want to look at the server example (see
 * module \ref Server)
 *
 * To start with a new project, you might also use a template:
 *
 * -# Create a directory in devices/, e.g.
 * 
 *         > mkdir devices/foo
 * 
 * -# Enter that directory and run $NETPP/bootstrap.sh, like:
 *
 *         > cd devices/foo
 *         > ../../bootstrap.sh
 * 
 * -# Run 'make' and build a first skeleton
 * 
 * -# Create slave.c:user_visit() for your own main loop handlers
 *
 *
 * \subsection VariableExport Exporting Variables to NETPP
 *
 * Assume, you want to export a variable \c x to NETPP, standing for an
 * image width, for example. If the variable is not yet global, it is
 * recommended to create a context structure that is globally instanced once
 * and make this variable a member of this structure.
 *
 * When wrapping this variable with a property, you will have to create
 * a new property with a child called 'variable'. In this variable field,
 * you enter the name of the globally accessible variable, in this example:
 * \verbatim g_imgbuf.x \endverbatim
 *
 * See \c devices/example/example.xml
 *
 * In the 'header' node of the XML file, you must enter the name of the
 * header file include statement where the global struct is defined.
 *
 * A few comments about data types: There are only a few valid data types
 * that can be wrapped. These are:
 *
 * - (unsigned) char *  : DC_BUFFER, DC_STRING
 * - char               : DC_BOOL
 * - unsigned short     : DC_MODE
 * - int                : DC_INT, DC_UINT
 * - float              : DC_FLOAT
 *
 * If you want to wrap an integer type different from the above, you have to
 * either change the variable to \c int or use the handler methods described
 * below to access the variable.
 *
 * \subsection Handlers Creating handlers for properties
 *
 * When you implement properties that must cause an action when they
 * are accessed, you would typically use a handler. This is a piece of
 * C code with a uniform Getter/Setter API.
 * When you have created a XML file with a few handlers, run the following
 * command in the devices/\<yourdevice\> directory to create a handler
 * skeleton template:
 *
 * \code make handler_skeleton.c \endcode
 *
 * Typically, this file is manually copied to handler.c and the handlers
 * are implemented.
 *
 * When dealing with buffers, make sure you have read Section \ref String
 * about buffer update/release functionality.
 *
 * \attention Make sure that your program does not spend too
 * much time inside the handler. For example, if the handler waits for a
 * buffer for a longer time than the protocol timeout, the protocol
 * breaks and resets to idle state. So make sure, that your handler
 * processes fast enough. If it does not, you must split up the request
 * for a buffer and the actual transfer into separate property commands,
 * or use a specific error code reporting that the buffer is not yet ready.
 * For example in case of a very slow camera, you would use a Trigger
 * property to start the process, poll the Trigger property again to check
 * whether the command has completed and then finally read the buffer.
 *
 * \subsection Direct Direct property access
 *
 * The DClib (non-networked portion) allows local, direct property access when
 * the TOKEN for a property is already known. Unlike in the netpp environment,
 * a local TOKEN is properly synchronized to its property, as long as the
 * code is compiled completely.
 *
 * When a property has an 'id' attribute, a global variable is created
 * in \c proplist.c  according to the given id, using a prefix 'g_t_'.
 * This variable contains the TOKEN <b>index</b> for the property.
 *
 * Setting a property then occurs by
 * \code
	DCValue value; value.val.i = 42;
	error = property_set(LOCAL_DEVICE(), g_t_my_property_id, &value) \endcode
 *
 * Likewise, use property_get() to obtain a property value.
 *
 * Note that no header is generated for these property indices,
 * therefore you have to use an 'extern' declaration in your code:
 *
 * \code
extern TOKEN g_t_my_property_id; \endcode
 *
 * \note   For the 1.0 API, the local calls will be supported via
 *         the standard netpp API, e.g. dcDevice_SetProperty()
 * 
 * \attention  This access is somewhat dirty. It might not be supported
 *             in future versions. Also, it is NOT thread safe, do not
 *             manipulate properties that can be simultaneously changed by
 *             a netpp client.
 *
 * \section Special Special features
 *
 * \subsection Derivation   Derivation of devices from base class
 *
 * When using a lot of similar devices, it makes no sense to specify
 * properties over and over again. This makes a class of devices hard to
 * handle, when a basic feature is to be changed.
 *
 * Therefore, NETPP is able to derive from a base class. This is simply
 * done by adding another "device" specification to the XML file and
 * setting the 'baseclass' attribute to the base class device's id field.
 *
 * See devices/example folder on how this is done.
 *
 * On target / backend side, you will need to implement a function called
 * local_getroot() that returns the root node of the selected device.
 * Normally, this returns TOKEN 0 when only one device is implemented.
 * It is up to this function to determine what device description instance
 * is to be used from the definition. This allows a certain device class
 * to determine its properties at runtime.
 * The user thus has the freedom to implement a device control in the following
 * ways:
 *
 * - One single software for controlling several classes of devices by using
 *     derivation
 * - Several device files that include portion from a common XML device
 *     base class file. Lean and mean - but each device needs its specific
 *     software.
 *
 * \note      In the 1.0 API, local_getroot() is implemented in server.c.
 *            See also #DEFAULT_DEVICE_INDEX define in devices/Makefile.in.
 *
 * If you wish to use a dynamic property device as default, you can
 * set

\code
DEFAULT_DEVICE_INDEX = DYNAMIC_DEVICE_INDEX\(0\)
\endcode

in the Makefile.

 *
 *
 * \subsubsection Overriding   Overriding of specifications
 *
 * Variables that are both specified in the base device and derived device's
 * name space, are overridden by the definition in the derived class.
 * So, in the above example, property 'Test2' is defined in both classes as
 * INT but with different static value. When accessing 'Test2' by name, you
 * will always see the value from the device "DerivedDevice". However, when
 * looking at the property list, the property 'Test2' will appear twice.
 * So it is possible to still access the old property by its TOKEN, however,
 * this needs to be determined by walking through the hierarchy with
 * dcProperty_Select().
 *
 * \section Driver Implementing a new NETPP driver / server
 *
 * For developing a new driver, check existing sources, such as unixdevice.c
 * Basically, you will have to implement a new struct protocol_methods for
 * the new interface. Also, you will have to define a server init function
 * which initializes a Hub structure.
 * Since the driver model tries to be as symmetrical as possible, there
 * are not separate functions for the master and the slave, unless Master
 * and slave are using a completely different operating system.
 * However, it may help to keep the differences between master and slave
 * into account:
 *
 * \subsection Slave Slave side
 *
 * To develop a new NETPP server for a device, you might want to start
 * from existing code such as in \c devices/example.
 *
 * In the main loop of your server (or your own main loop) you will have to
 * repeatedly call the slave_handler() to listen to your interface.
 *
 * \subsection Master Master side
 *
 * See protocol_methods which methods are relevant for the master only.
 * In ports_init() you will have to initialize a Hub for the new driver
 * and append it to the global PortBay using portbay_append(). See ports.c
 * for example. Note that ports_init() is called internally, the front end
 * must not call it.
 *
 * \attention ports_init() and ports_exit() are called explicitely inside
 *            dcDeviceOpen() respectively dcDeviceClose(). These functions
 *            are not thread safe. Always open/close the #DC_PORT_ROOT
 *            pseudo device in the main thread only.
 *
 * \section Advanced Advanced topics
 *
 * \subsection DynProperties Dynamic Properties
 *
 * Property lists are typically static, i.e. they do not change during
 * runtime. This is sufficient for most simple devices, however, there
 * might be cases where a device functions as a proxy. For example, you
 * might want to control a measurement bank with various sensors or
 * sub devices.
 * Depending on the connected sensors, the back end may want to list different,
 * possibly volatile properties and hierarchies for each sub-device.
 * 
 * A second use case is, when a system already has an underlying mechanism
 * of sub-device detection and enumeration.
 *
 * To address these scenarios, the \ref DynProps API allows to create and modify
 * properties on the fly. Basically, the new root node of a device is
 * defined as a dynamic node and children are inserted. The new dynamic
 * node supports derivation, so a static base node can be given.
 *
 * \attention Currently, only one level of derivation is supported with
 *            dynamic root nodes
 *
 * \note      Dynamic properties are not supported in the free netpp
 *            version.
 *
 * \section Hacking NETPP hacking and extending
 *
 * Developers who wish to modify, improve or enhance the netpp code should
 * keep the following rules in mind:
 *
 * - Change of Token encoding allowed, except reserved token defines.
 * - Don't touch devlib_types.h
 * - DO NOT change property_protocol.h
 * - The API must never be changed. Only extended.
 * - The error codes (errorcodes.xml) must never be altered. Only extended.
 *
 * If you think there is a reason for changing files in a "no go" area,
 * please consult the netpp author(s).
 * Note that changes in $NETPP/src or $NETPP/common must <b>always</b> 
 * be published according to the netpp source license.
 *
 * A few notes about the coding style:
 *
 * - No GNU coding style. We want to keep readability as it is.
 * - Staying with the Linux kernel coding style is good.
 * - Make sure that lines longer than 80 chars are wrapped.
 * - Use tabs for indenting. Use some space and alignments for defines.
 * - No DOS mode CR/LF
 *
 * \section Resources    Further resources
 *
 * The netpp home page is at:
 *
 * http://www.section5.ch/netpp
 *
 * Please check this site for further documentation resources and support.
 *
 */


// This trick is used for easy function wrapping..
#ifndef APIFUNC
#define APIFUNC(retval, name, parms)           \
	DLL_API retval APIDECL dc##name parms;
#endif


/**
 * \defgroup PortAccess Port Access, Device Initialization
 *
 * This module contains the initialization routines for the
 * device ports
 */


/**
 * \example master.c
 *
 * A simple command line program to query and set properties.
 * 
 * \b Usage
 * \verbatim
   <programname>\endverbatim
 *                       Lists options, supported interfaces and detected
 *                       devices
 * \verbatim
   <programname> <device>\endverbatim
 *                       Lists all top level properties of the device
 *
 * \verbatim
   <programname> <device> <property_name> \endverbatim
 *
 *                       Query specified Property with name 'property'.
 *                       Children are displayed as well as its value,
 *                       if applicable.
 *
 * \verbatim
   <programname> <device> <property> <value> \endverbatim
 *
 *                       Set specified Property with name 'property'
 *                       to value 'value'. Illegal values are caught.
 *
 *
 */

/**
 * \example handler.c
 *
 *  An example of a remote display implementation
 *
 */

/**
 * \example slave_test.c
 *
 *  A static test example to link against libslave
 *
 */


/**
 * \addtogroup PortAccess
 * \{
 */


/**
 * Open Device on port by name
 * 
 * \param port           The port name identifier in format
 *                       "HUB:PORT" where HUB is one of 'TCP', 'USB' or
 *                       other interface types listed in dcProperty_Select().
 *                       For example, to open a TCP connection to port 2006:
 *                       "TCP:192.168.1.2:2006"
 *
 * \param deviceP        Pointer to the DEVICE object to be initialized.
 *
 * \return               0 if successful, < 0: error.
 *                       A value > 0 means, the device was opened, but
 *                       some non blocking failure was detected (bad
 *                       device identity, etc.). In this case, some
 *                       device features may not be accessible, but
 *                       it can be normally fixed via an update.
 *
 */

APIFUNC(int, DeviceOpen, (const char *port, DEVICE *deviceP))

/**
 * Close device communication port and free Device.
 * This should be called at the end of an application,
 * when the device is no longer accessed.
 *
 * If device probes via the pseudo device #DC_PORT_ROOT were issued,
 * port structures may still be present in the application.
 *
 * To do a final cleanup prior to exit to release all allocated memory,
 * call this function with the #DC_PORT_ROOT argument.
 * 
 */

APIFUNC(int, DeviceClose, (DEVICE c))

/* \} */


////////////////////////////////////////////////////////////////////////////
// NEW API (pre 1.0)
////////////////////////////////////////////////////////////////////////////

// DLL top level stuff

#ifdef DCLIB_ONLY
APIFUNC(const char *, DeviceGetDllVersion, (DEVICE c, int *major, int *minor))
#endif

////////////////////////////////////////////////////////////////////////////
// PROPERTY API

/**
 * \defgroup PropControl Generic Device property control
 *
 * This contains the function set necessary to control arbitrary
 * device properties.
 * 
 */

/**
 * \addtogroup PropControl
 * \{
 */

/**
 * Get root namespace descriptor token of current device
 *
 * This function always returns a valid token.
 *
 * \return      The root descriptor token of the device d
 */

APIFUNC(int, Device_GetRoot, (DEVICE d, TOKEN *t))

/**
 * Select a property in the property and feature hierarchy.
 *
 * This function is used to query a property inside the property hierarchy
 * which is organized in a tree. A property can have children and has always
 * a parent, except if it is the root node.
 *
 * Quick overview: To select the first child of a node, use:
 *
 * \code
     error = dcProperty_Select(d, node, node, &child); \endcode
 *
 * To select the next children of that node:
 *
 * \code
     error = dcProperty_Select(d, node, child, &next); \endcode
 *
 * When the dummy device #DC_PORT_ROOT is specified, this function will
 * operate on the local Hub/Port structure. See Section \ref Ports.
 *
 * \param d         The DEVICE handle of the device (see special case
 *                  of #DC_PORT_ROOT)
 * \param parent    The parent node to be queried
 * \param prev      The node whose following (next) node shall be selected
 *                  If the parent node is specified, its first children is
 *                  selected.
 * \param out       Pointer to token whose value will be filled in with
 *                  the selected node token (next or children)
 */

APIFUNC(int, Property_Select, (DEVICE d, TOKEN parent, TOKEN prev, TOKEN *out))


/**
 * Get a property handle (TOKEN) by name.
 *
 * \param d          The DEVICE handle of the device
 * \param propname   Pointer to a string containing the local property name
 * \param ns         Namespace parent token
 * \param out        Pointer to the token to be filled in
 *
 * New in 0.51dev
 *
 */

APIFUNC(int, Property_Lookup, (DEVICE d, const char *propname, TOKEN ns, TOKEN *out))

/**
 * Get a property handle (TOKEN) by name in namespace of TOKEN *t
 *
 * \param d          The DEVICE handle of the device
 * \param propname   Pointer to a string containing the property name
 * \param t          Pointer to the token to be filled in
 *
 * Before v0.21, relative namespaces were not supported, always the full
 * property name had to be passed. New with this function:
 * When 'propname' begins with a '.', the name is regarded as relative
 * to a parent property. The TOKEN pointed to by the 'out' pointer must
 * be preinitialized by that parent property token.
 * Internally calls Property_Lookup()
 *
 */

APIFUNC(int, Property_ParseName, (DEVICE d, const char *propname, TOKEN *out))


/**
 * Get a property prototype by token
 * This function is needed to query a property by token.
 *
 * \param d          The DEVICE handle of the device
 * \param p          The property token whose type is to be queried
 * \param proto      Uninitialized DCValue
 *                  
 * On return without error, the property type can be read from \c proto.type,
 * and the size from \c proto.len.
 * Note: Reading the size only works on static buffers. On handled buffers,
 * the size is a priori not known. To obtain the size of a handled buffer,
 * an attempt for dcDevice_GetProperty() must be made on the buffer property.
 * When a DCERR_PROPERTY_SIZE_MATCH is returned, the size can be read
 * from \c proto.len.
 * Special case: Passing d = DC_PORT_ROOT and p = 0, the function returns
 * the protocol version. Earlier APIs than this version may not support
 * this.
 */

APIFUNC(int, Property_GetProto, (DEVICE d, TOKEN p, DCValue *proto))

/**
 * Get name of a property which is only known by token.
 *
 * \param d          The DEVICE handle of the device
 * \param p          The property token whose type is to be queried
 * \param name       DCValue which must be intialized as shown below
 *
 * In case of a remote device,
 * \c name must be preinitialized, because the string is owned by the caller,
 * like with any DCValue. This is done as follows:
 * \code
 	char propname[32];
	DCValue v; v.value.p = propname; v.len = sizeof(propname);\endcode
 *
 * On return without error, the property name is stored in \c name.value.p.
 * If the string reserved was too small to fit the property name, 
 * #DCERR_PROPERTY_SIZE_MATCH is returned.
 *
 * When specifying DC_PORT_ROOT for \c d, v.value.p does not have to be
 * preinitialized and contains a pointer to a static string on return.
 */

APIFUNC(int, Property_GetName, (DEVICE d, TOKEN p, DCValue *name))


/**
 * Get the property value of the device.
 *
 * By default, a property value is owned by the caller, i.e. in case of
 * a dynamic string property, the caller is responsible for allocating the
 * proper memory and initializing the DCValue pointer field. See
 * Section \ref props.
 *
 * \param d          The DEVICE handle of the device
 * \param p          The property token whose value is to be requested
 * \param value      Pointer to a DCValue which contains the property value
 *                   after successful return of this function. Note that
 *                   in case of a non static string property, the property
 *                   value must be initialized first.
 *
 * \return           A standard return code
 */

APIFUNC(int, Device_GetProperty, (DEVICE d, TOKEN p, DCValue *value))

/**
 * Set the value of a device property.
 *
 * \param d          The DEVICE handle of the device
 * \param p          The property token whose value is to be requested
 * \param value      Pointer to a DCValue containing the value to be set.
 *
 * \return           The standard return code
 */

APIFUNC(int, Device_SetProperty, (DEVICE d, TOKEN p,
                                        DCValue *value))
#if 0

/**
 * Set the value of a device property by string.
 *
 * This function parses a string and tries to match it to the
 * specified property. 
 *
 * \param d          The DEVICE handle of the device
 * \param p          The property token whose value is to be set
 * \param string     Pointer to a string containing the value to be set.
 *
 * \return           The standard return code
 */


APIFUNC(int, Device_SetProperty_String, (DEVICE d, TOKEN p,
                                                 const char *string))

/**
 * Get the property value of the device as string
 *
 *
 * \param d          The DEVICE handle of the device
 * \param p          The property token whose value is to be requested
 * \param outs       Pointer to a string which contains the property value
 *                   string conversion after successful return of this function.
 *                   The caller must reserve a string of the length of the
 *                   expected value string plus one, i.e. including the '\\0'
 *                   trailer.
 * \param len        Length of the string that was reserved by caller
 *
 * \note             This function only applies to simple datatypes, resp.
 *                   only displays debugging information about some
 *                   complex data types.
 *
 * \return           The standard return code
 */

APIFUNC(int, Device_GetProperty_String, (DEVICE d, TOKEN p,
                                               char *outs, int len))
#endif

/* \} */

////////////////////////////////////////////////////////////////////////////


/**
 * \defgroup Misc Auxiliary and miscellaneous functions/macros
 *
 * Auxiliary functions for handling & reporting errors.
 *
 */

/**
 * \addtogroup Misc
 * \{
 */

/**
 * Returns the Hub name of the open device.
 *
 * The hub name is merely the descriptor for the underlying transport
 * protocol (TCP, USB, etc.)
 *
 * \param d     Device handle
 * \param name  Pointer to a const char * to point to name of the associated
 *              Hub ("TCP", "USB", ...) after successfull call.
 * \return      Error code. The returned name is only valid when
 *              the error code is 0.
 */

APIFUNC(int, Device_GetHubName, (DEVICE d, const char **name))

/**
 * Converts DCValue into string representation.
 *
 * \param val   DCValue to convert
 * \param outs  Output string pointer
 * \param len   Output string length
 *
 * \return      error, if string length not sufficient
 */

APIFUNC(int, Value_ToString, (DCValue *val, char *outs, int len))

#ifdef DCLIB_ONLY
/**
 * Sets device feedback function
 *
 * The feedback function must return a value = 0, if the caller should
 * proceed. For example, a GUI based feedback routine may allow the user
 * to cancel the procedure by hitting the ESC key.
 * If the procedure was cancelled, the caller returns #DCERR_CANCEL.
 *
 * \param d     Device handle
 * \param func  Pointer to a feedback function of type FeedbackFuncP
 *              If equal 0, the default dummy routine (no feedback) is set.
 * \return      the previous feedback function pointer
 */

APIFUNC(FeedbackFuncP, SetFeedback, (DEVICE d, FeedbackFuncP func))
#endif

/** Set Log function
 *
 * Sets the local log function. The function must be of type LogFuncP.
 *
 * Returns previous log function.
 *
 */

APIFUNC(LogFuncP, SetLogFunc, (LogFuncP func))

/* \} */

/**
 * \defgroup ErrorHandling Error Handling
 *
 * Auxiliary functions for handling & reporting errors.
 *
 */

/**
 * \ingroup ErrorHandling
 *
 * Verbose error string query of an error code.
 *
 * \param error          The error code returned by a function.
 *
 * \return               Pointer to a constant string containing the error
 *                       message to the specified error code.
 */

APIFUNC(const char *, GetErrorString, (int error))

////////////////////////////////////////////////////////////////////////////


