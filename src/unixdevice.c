/** Generic /dev/xxx device driver for netpp
 *
 * (c) 11/2007 Martin Strubel <strubel@section5.ch>
 *
 */


#include "devlib.h"
#include "conffile.h"
#include "devlib_error.h"
#include "property_protocol.h"
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <termios.h> // for serial devices

#define NUM_TTY       4

// Changing this will break compatibility:
#ifndef MAX_PACKET_SIZE
#define MAX_PACKET_SIZE   32768
#endif
#define HDR_SIZE sizeof(PropertyPacketHdr)

// Default timeout to check for a request (outside protocol)
#define TIMEOUT_CHECK        1

// Default timeout to wait for a response inside protocol
#define TIMEOUT_WAIT      2000

// Some systems don't know this define:
#ifndef B500000
#define B500000         500000
#endif

// static short g_pktchunksize = MAX_PACKET_SIZE - HDR_SIZE;
static short g_pktchunksize = 128 - HDR_SIZE;

static
int handle_deverror(int error, const char *txt)
{
	netpp_log(DCLOG_ERROR,
			"Device error [%s]: %s", txt, strerror(errno));
	error = DCERR_COMM_GENERIC;
	return error;
}

void packet_dump(const char *b, int len)
{
	while (len--) {
		printf("%02x ", *b++ & 0xff);
	}
	printf("\n");
}


int dev_read(int fd, char *buf, int len)
{
	int n = 0;
	int i;
	int stat;
	struct pollfd pfd;
	int retry = 0;

	pfd.fd = fd;
	pfd.events = POLLIN | POLLPRI;

	i = read(fd, (char*) buf, len);
	if (i < 0) {
		if (errno == EAGAIN) {
			// netpp_log(DCLOG_VERBOSE, "EAGAIN");
			i = 0;
		}
		else return handle_deverror(i, "reading from device");
	}

	len -= i;

	while (len > 0) {
		n += i; buf += i;
		stat = poll(&pfd, 1, 5);

		if (stat < 0) {
			return handle_deverror(-1, "Polling RX");
		} else if (stat == 0) {
			retry++;
			if (retry > 20) {
				netpp_log(DCLOG_ERROR, "%s(): Timeout", __FUNCTION__);
				// if (n > 0) packet_dump(buf-n, n);
				return DCERR_COMM_TIMEOUT;
			}
		}

		i = read(fd, (char*) buf, len);
		if (i < 0) {
			if (errno == EAGAIN) {
				i = 0; // ignore eagain
			} else {
				netpp_log(DCLOG_ERROR, "Errno: %d", errno);
				return handle_deverror(i, "reading from device");
			}
		}
		len -= i;
	}
	n += i;
	return n;
}

int dev_write(int fd, const char *buf, int len)
{
	int n = 0;
	int stat;

	struct pollfd pfd;

	pfd.fd = fd;
	pfd.events = POLLOUT;

//	if (len > 0) {
//		printf("OUT>  ");
//		packet_dump(buf, len);
//	}

	while (len > 0) {

		if (n == 0) {
			stat = poll(&pfd, 1, 500);

			if (stat < 0) {
				return handle_deverror(n, "Polling TX");
			} else if (stat == 0) {
				netpp_log(DCLOG_ERROR, "Write timeout");
				return DCERR_COMM_TIMEOUT;
			}

		}
		n = write(fd, buf, len);
		buf += n; len -= n;
	}
	return len;
}


static
int read_packet(int fd, char *buf)
{
	int n, len;

	// Read Header from stream
	n = dev_read(fd, buf, HDR_SIZE);

	if (n < 0) return n;
	if (n != HDR_SIZE) {
		netpp_log(DCLOG_ERROR, "Did not receive full header [size %d]", n);
		return -1;
	}


	len = get_packet_length(buf);

	// If len > maximum possible payload size, prune!
	if (len > g_pktchunksize) {
		len = g_pktchunksize;
	}

	buf += n;

	// netpp_log(DCLOG_DEBUG, "Payload size %d", len);

	n += dev_read(fd, buf, len);

	return n;
}

int device_open(const char *name, const char *param)
{
	struct termios tio;
	int fd;
	speed_t baudrate;

	fd = open(name, O_RDWR | O_NONBLOCK, 0);
	if (fd < 0) {
		perror("Opening device");
		return fd;
	}

	// TODO: Detect if it's a tty kind of device
	// Terminal stuff:
	if (isatty(fd)) {
		// Make sure all flags are 0 by default:
		memset(&tio, 0, sizeof(tio));
		cfmakeraw(&tio);
		// No handshaking.
		tio.c_cflag |= CLOCAL | CREAD;
		tio.c_iflag &= ~(IXON | IXOFF);

		if      (strcmp(param, "9600") == 0)   baudrate = B9600;
		else if (strcmp(param, "19200") == 0)  baudrate = B19200;
		else if (strcmp(param, "38400") == 0)  baudrate = B38400;
		else if (strcmp(param, "57600") == 0)  baudrate = B57600;
		else if (strcmp(param, "115200") == 0) baudrate = B115200;
		else if (strcmp(param, "230400") == 0) baudrate = B230400;
		else if (strcmp(param, "500000") == 0) baudrate = B500000;
		else {
			baudrate = B9600;
			netpp_log(DCLOG_WARN, "Unsupported baud rate or not given, "
			                      "using fallback.");
			param = "9600";
		}
		netpp_log(DCLOG_VERBOSE, "Configuring for baudrate %s", param);
		
		cfsetispeed(&tio, baudrate);
		cfsetospeed(&tio, baudrate);
		tcsetattr(fd, TCSADRAIN, &tio);
		tcsetattr(fd, TCSAFLUSH, &tio);
	}

	return fd;
}

static
int peer_init(Peer *p)
{
	p->recvbuf = (char *) malloc(MAX_PACKET_SIZE);
	if (!p->recvbuf) return DCERR_MALLOC;

	p->recvtimeout_check =  TIMEOUT_CHECK;
	p->recvtimeout_wait  =  TIMEOUT_WAIT;

	return 0;
}

static
int peer_exit(Peer *p)
{
	free(p->recvbuf);
	return 0;
}

int dev_close(RemoteDevice *d)
{
	return close(d->peer.fd);
}

#ifdef CONFIG_PROXY

static
int add_group(void *c, const char *name)
{
	return 0;
}

 
static
int add_device(void *c, const char *name, const char *val)
{
	PortBay *bay = (PortBay *) c;
	PortToken p;


	if (strcmp(name, "probe") == 0) {
		printf("'%s' = '%s'\n", name, val);
		p = PortNode(bay, val, DC_PORT);
		if (!IS_VALIDTOKEN(p)) return DCERR_MALLOC;
		portbay_append(bay, p);
	}

	return 0;
}


int parse_proxyconf(const char *file, DynPropertyDesc *hub)
{
	BufStream s;
	ConfHandler h;
	int error;

	h.add_group = add_group;
	h.set_property = add_device;

	PortBay *bay = portbay_fromtoken(hub->hub.ports);

	s.file = fopen(file, "r");
	if (s.file == NULL) return -1;
	s.bufsize = 128;
	s.buf[0] = (char *) malloc(s.bufsize);
	h.user = (void *) bay;

	error = conf_parse(&s, &h);

	fclose(s.file);
	free(s.buf[0]);
	return error;
}
#endif

#ifdef CONFIG_PROXY_TTYPROBE
static
int probe_tty(PortBay *b, const char *path, const char *param)
{
	RemoteDevice d;
	int error;
	int fd;

	fd = device_open(path, param);
	if (fd < 0) {
		return DCERR_PROPERTY_ACCESS;
	}
	error = peer_init(&d.peer);
	if (error < 0 ) return error;

	d.peer.fd = fd;

	if (error == 0) {
		error = dev_close(&d);
	}
	return error;
}

#endif

#ifdef CONFIG_PROXY
int dev_probe(DynPropertyDesc *hub)
{
	return parse_proxyconf("/etc/netpp.ini", hub);
}

#else
int dev_probe(DynPropertyDesc *hub)
{
	char path[20];
	int i;
	int error;
	PortBay *bay = portbay_fromtoken(hub->hub.ports);

	char *devices[] = {
		// "/dev/pts/%d",
		"/dev/ttyS%d",
		"/dev/ttyACM%d",
		"/dev/ttyUSB%d",
		NULL
	};
	struct stat s;
	PortToken p;

	char **dev = &devices[0];
	while (*dev) {
		for (i = 0; i < NUM_TTY; i++) {
			sprintf(path, *dev, i);
			error = stat(path, &s);
			if (error == 0) {
				// netpp_log(DCLOG_VERBOSE, "Probing %s", path);
				// probe_tty(bay, path);
				// FIXME: Only show when probe was successful.
				p = PortNode(bay, path, DC_PORT);
				portbay_append(bay, p);
			}
		}
		dev++;
	}

	return 0;
}

#endif

static
int dev_create(PortToken hub, const char *name, PortToken *portid)
{
	PortToken port;
	int error;
	struct stat s;
	char device_name[64];

	error = scan_into(name, ':', device_name, sizeof(device_name));
	if (error < 0) return error;
	name += error; // Skip
	scan_into(name, ':', device_name, sizeof(device_name));
	if (error < 0) return DCERR_PROPERTY_SIZE_MATCH;

	error = stat(device_name, &s);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "'%s' not found", device_name);
		return DCERR_COMM_INIT;
	}

	port = Port(hub, name, DC_PORT);
	if (!port) return DCERR_MALLOC;
	hub_append(hub, port);
	*portid = port;
	return 0;
}

static
int dev_open(RemoteDevice *d, int portindex)
{
	int error;
	int fd;
	const char *name, *param;

	char device_name[64];

	name = getname(&d->peer, portindex);
	error = scan_into(name, ':', device_name, sizeof(device_name));
	if (error < 0) return DCERR_PROPERTY_SIZE_MATCH;
	param = &name[error]; // String after ':'
	fd = device_open(device_name, param);
	if (fd < 0) {
		return DCERR_PROPERTY_ACCESS;
	}

	error = peer_init(&d->peer);

	d->peer.fd = fd;

	return error;
}

int device_poll(int fd, int timeout)
{
	struct pollfd pfd;
	int n;

	pfd.fd = fd;
	pfd.events = POLLIN | POLLPRI;

	n = poll(&pfd, 1, timeout);
	if (n < 0) {
		return handle_deverror(n, "Polling RX");
	}
	return n;
}

static
int dev_poll_packet(Peer *p, PropertyPacketHdr **h, unsigned short timeout)
{
	int n;

	n = device_poll(p->fd, timeout);
	if (n <= 0) return n;
	n = read_packet(p->fd, p->recvbuf);
	// if (n > 0) packet_dump(p->recvbuf, n);

	*h = (PropertyPacketHdr *) p->recvbuf;
	return n;
}

static
int send_slice(Peer *peer, PropertyPacketHdr *h, const char *data,
		unsigned short len)
{
	int n;

	n = dev_write(peer->fd, (char *) h, HDR_SIZE);

	if (!n)
		n = dev_write(peer->fd, data, len);
	else
		netpp_log(DCLOG_ERROR, "Could not send header");

	fsync(peer->fd);
	return n;
}

static
int dev_send_packet(Peer *peer, PropertyPacketHdr *h,
		const char *data, size_t len)
{
	int error;
	unsigned short type = ntohs(h->type);
	// netpp_log(DCLOG_DEBUG, "Send packet dest: %08lx, dport: %d len: %ld\n",
	//		peer->ipaddr, peer->dport, len);

	// Now send packet fragments
	int i = 0;

	h->id = htons(i);

	while (len > g_pktchunksize) {
		h->type = htons(type | PP_FLAG_FRAG);
		netpp_log(DCLOG_DEBUG, "Send slice (%d left)", len);
		error = send_slice(peer, h, data, g_pktchunksize);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Failed to send slice");
			return error;
		}
		len -= g_pktchunksize;
		data += g_pktchunksize;

		// Update length field
		h->sub.set.len = htonl((unsigned long) len);

		i++;
		h->id = htons(i);
	}

	h->type = htons(type);

	// netpp_log(DCLOG_DEBUG, "Last slice (%d left)", len);
	error = send_slice(peer, h, data, (unsigned short) len);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Failed to send slice");
		return error;
	}

	return error;
}

const
struct protocol_methods dev_methods = {
	.probe   = &dev_probe,
	.create  = &dev_create,
	.open    = &dev_open,
	.close   = &dev_close,
	.send    = &dev_send_packet,
	.recv    = &dev_poll_packet,
	.release = 0
};

int dev_server_exit(Peer *p)
{
	// FIXME: Free hub
	peer_exit(p);
	return close(p->fd);
}

int dev_server_init(Peer *p, const char *device_spec)
{
	int error;
	char name[32];
	const char *param;
	PortToken hub;

	error = scan_into(device_spec, ':', name, sizeof(name));
	if (error < 0) return DCERR_PROPERTY_SIZE_MATCH;
	param = &device_spec[error]; // String after ':'

	p->fd = device_open(name, param);
	if (p->fd < 0) {
		netpp_log(DCLOG_ERROR, "Could not open device %s", name);
		return DCERR_COMM_INIT;
	}
	error = peer_init(p);
	if (error < 0) return error;

	hub = Hub(GET_PORTROOT(), "DEV", &dev_methods);
	p->hub = desc_fromtoken(hub);

	if (!p->hub) {
		dev_server_exit(p);
		error = DCERR_MALLOC;
	}

	return error;
}

