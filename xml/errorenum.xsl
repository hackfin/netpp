<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
Error enumeration module
(c) 2014, 2015 section5 // Martin Strubel <strubel@section5.ch>
-->

<xsl:stylesheet version="1.0" 
	xmlns:my="http://www.section5.ch/dclib/schema/devdesc"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

	<xsl:output method="text" encoding="ISO-8859-1"/>	

	<!-- Error list for enum (header) statement -->
	<xsl:template match="my:errorspec" mode="enum">
	/** <xsl:value-of select="my:info"/> */
	DCERR_<xsl:value-of select="@name"/><xsl:if test="@code"> = <xsl:value-of select="@code"/></xsl:if>,
	</xsl:template>
	<xsl:template match="my:warnspec" mode="enum">
	/** <xsl:value-of select="my:info"/> */
	DCWARN_<xsl:value-of select="@name"/><xsl:if test="@code"> = <xsl:value-of select="@code"/></xsl:if>,
	</xsl:template>

	<!-- Error list for case statement -->
	<xsl:template match="my:errorspec" mode ="case">
	case DCERR_<xsl:value-of select="@name"/>:
		str = "<xsl:value-of select="my:info"/>";
		break;
	</xsl:template>
	<xsl:template match="my:warnspec" mode ="case">
	case DCWARN_<xsl:value-of select="@name"/>:
		str = "<xsl:value-of select="my:info"/>";
		break;
	</xsl:template>

</xsl:stylesheet>

