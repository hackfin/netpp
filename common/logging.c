/* Logging facilities
 */

#include <stdio.h>
#include "devlib.h"
#include <stdarg.h>

#if defined(STANDALONE)

#ifdef DEBUG
#warning "No default logging enabled"
#endif

void default_log(int log_level, const char *fmt, ...)
{

}

#else


void default_log(int log_level, const char *fmt, ...)
{
	va_list args;
	FILE *fd = stderr;
	va_start(args, fmt);

#ifndef NO_COLOR
	const char *col;
	switch (log_level) {
		case DCLOG_ERROR:
			col = "\033[7;31m"; break;
		case DCLOG_DEBUG:
			col = "\033[32m"; break;
		case DCLOG_WARN:
			col = "\033[35m"; break;
		case DCLOG_NOTICE:
			col = "\033[34m"; break;
		default:
			col = "\033[33m"; break;
	}
#endif

	switch (log_level) {
		case DCLOG_NOTICE:
		case DCLOG_WARN:
		case DCLOG_VERBOSE:
			fd = stdout;
			break;
	}
#ifndef NO_COLOR
	fputs(col, fd);
#endif
#ifdef TINYCPU
	printf(fmt, args);
#else
	vfprintf(fd, fmt, args);
#endif
#ifndef NO_COLOR
	fputs("\033[0m", fd);
#endif
#ifdef TINYCPU
	printf("\n");
#else
	fputs("\n", fd);
#endif
	va_end(args);
}


void (*netpp_log)(int log_level, const char *fmt, ...) = default_log;

LogFuncP dcSetLogFunc(LogFuncP logfunc)
{
	LogFuncP tmp;
	tmp = netpp_log;

	netpp_log = logfunc;

	return tmp;
}


#endif // STANDALONE
