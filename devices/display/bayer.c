/** Linear stupid bayer pattern to RGB */

/*  G B G B G B G B
 *  R G R G R G R G
 *  G B G B G B G B
 *
 */

#define DIV1 (BAYER_BITDEPTH - 8)
#define DIV2 (BAYER_BITDEPTH - 8 + 1)
#define DIV4 (BAYER_BITDEPTH - 8 + 2)

void bayer_linear(uint32_t *rgb, BAYER_DATA *in,
	int x, int y)
{
	BAYER_DATA *up, *dn;
	BAYER_DATA *end;
	unsigned long r, g, b;

	up = in;
	in += x;
	dn = &in[x];
	y -= 2;

	end = &in[x-1];

	while (y) {

// -- line 1
		*rgb = 0; // beginning
		in++; rgb++; up++; dn++;

		while (in < end) {
#ifdef FLIPPED
			r = PIXEL(up[0]) + PIXEL(dn[0]);
			g = PIXEL(in[0]);
			b = PIXEL(in[-1]) + PIXEL(in[1]);

			*rgb = ( (((r >> DIV2) << 16) & 0x00ff0000) |
					 (((g >> DIV1) <<  8) & 0x0000ff00) |
					 (((b >> DIV2)      ) & 0x000000ff) );
#else
			g = PIXEL(up[0]) + PIXEL(dn[0]);
			r = PIXEL(in[0]);
			b = PIXEL(in[-1]) + PIXEL(in[1]);

			*rgb = ( (((r >> DIV1) << 16) & 0x00ff0000) |
					 (((g >> DIV2) <<  8) & 0x0000ff00) |
					 (((b >> DIV2)      ) & 0x000000ff) );
#endif

			in++; rgb++; up++; dn++;

#ifdef FLIPPED
			r = PIXEL(up[-1]) + PIXEL(up[1]) + PIXEL(dn[-1]) + PIXEL(dn[1]);
			g = PIXEL(in[-1]) + PIXEL(in[1]) + PIXEL(up[0]) + PIXEL(dn[0]);
			b = PIXEL(in[0]);

			*rgb = ( (((r >> DIV4) << 16) & 0x00ff0000) |
					 (((g >> DIV4) <<  8) & 0x0000ff00) |
					 (((b >> DIV1)      ) & 0x000000ff) );
#else
			r = PIXEL(in[-1]) + PIXEL(in[1]);
			b = PIXEL(up[0]) + PIXEL(dn[0]);
			g = PIXEL(in[0]);

			*rgb = ( (((r >> DIV2) << 16) & 0x00ff0000) |
					 (((g >> DIV1) <<  8) & 0x0000ff00) |
					 (((b >> DIV2)      ) & 0x000000ff) );
#endif

			in++; rgb++; up++; dn++;

		}

		*rgb = 0; // end
		in++; rgb++; up++; dn++;

		y--;
		end += x;
// -- line 2

		*rgb = 0; // beginning
		in++; rgb++; up++; dn++;

		while (in < end) {
#ifdef FLIPPED
			r = PIXEL(in[0]);
			g = PIXEL(in[-1]) + PIXEL(in[1]) + PIXEL(up[0]) + PIXEL(dn[0]);
			b = PIXEL(up[-1]) + PIXEL(up[1]) + PIXEL(dn[-1]) + PIXEL(dn[1]);

			*rgb = ( (((r >> DIV1) << 16)  & 0x00ff0000) |
					 (((g >> DIV4) <<  8)  & 0x0000ff00) |
					 (((b >> DIV4)      )  & 0x000000ff) );

#else
			b = PIXEL(in[-1]) + PIXEL(in[1]);
			r = PIXEL(up[0]) + PIXEL(dn[0]);
			g = PIXEL(in[0]);

			*rgb = ( (((r >> DIV2) << 16)  & 0x00ff0000) |
					 (((g >> DIV1) <<  8)  & 0x0000ff00) |
					 (((b >> DIV2)      )  & 0x000000ff) );
#endif

			in++; rgb++; up++; dn++;

#ifdef FLIPPED
			r = (PIXEL(in[-1]) + PIXEL(in[1]));
			g = PIXEL(in[0]);
			b = (PIXEL(up[0]) + PIXEL(dn[0]));

			*rgb = ( (((r >> DIV2) << 16) & 0x00ff0000) |
					 (((g >> DIV1) <<  8) & 0x0000ff00) |
					 (((b >> DIV2)      ) & 0x000000ff) );
#else
			r = PIXEL(up[-1]) + PIXEL(up[1]) + PIXEL(dn[-1]) + PIXEL(dn[1]);
			g = PIXEL(in[-1]) + PIXEL(in[1]) + PIXEL(up[0]) + PIXEL(dn[0]);
			b = PIXEL(in[0]);

			*rgb = ( (((r >> DIV4) << 16) & 0x00ff0000) |
					 (((g >> DIV4) <<  8) & 0x0000ff00) |
					 (((b >> DIV1)      ) & 0x000000ff) );

#endif

			in++; rgb++; up++; dn++;

		}


		*rgb = 0; // end
		in++; rgb++; up++; dn++;

		y--;
		end += x;
	}
}
