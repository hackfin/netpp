/* 
 * (c) 01/2007 <hackfin@section5.ch>
 *
 */

/** Ports implementation
 *
 * Ports are associated with a 'PortBay', which is a virtual port bay.
 *
 * Before a device instance is accessed, the library may need to probe
 * ports such as USB, COM, or Ethernet.
 *
 * The 'PortBay' node is a preinitialized root node which has children of
 * type HUB.
 *
 * A Hub has the following properties:
 *
 * - If a PROBE command is sent, it looks for supported ports, initializes
 *   them and inserts them as its children.
 * - The port children again can have specific properties.
 *
 * Port properties:
 * -----------------
 *
 * A set_property() on a port property calls appropriate handlers as
 * defined in the descriptor tree in hub.props.
 *
 * On function level:
 *
 * - Hub has protocol handler methods and send/receive methods
 *
 * This allows for example UDP, TCP, USB and other packet based protocols
 * to coexist with a stupid command based protocol over a COM port.
 *
 * Operation on these nodes happens like with an open device, except that
 * the device handle is DC_PORT_ROOT (static predefined value)
 *
 * Port/Hub token encoding:
 *
 * HHPPPPpp   H: Hub, P: Port, p: Property
 *
 * Port descriptors are allocated inside so called Pool structures.
 * This is a list base associated with a memory block that is allocated
 * in chunks to avoid fragmentation. A Hub index points to a Pool, a
 * Port index points to an List entry inside the pool.
 *
 * The global 'pools' array contains the pointer to the available pools.
 * The Hub index thus refers to the index of the pool in this array.
 * Pool 0 can have a special function: To keep track of the dynamically
 * allocated pools in the array.
 *
 */

// #define DEBUG

#ifndef STANDALONE
#include <assert.h>
#endif
#include "devlib.h" // netpp_log
#include "devlib_error.h"
#include "devlib_types.h"
#include "property_protocol.h"
#include <stdlib.h>

// Workaround to disable LWIP macros
#if defined(LWIP_COMPAT_SOCKETS)
#undef close
#endif

/** The default pool chunk that is allocated for a new pool entry */
#ifndef DEFAULT_POOL_CHUNK
#define DEFAULT_POOL_CHUNK 8
#endif

// Temp:
#define GET_PTR(p, x) \
	(&(p)->base[PORTINDEX_FROMTOKEN(x)])

#define PTR(x) \
	(&g_port.pools[HUBINDEX_FROMTOKEN(x)].base[PORTINDEX_FROMTOKEN(x)])

#define GET_POOL(x) \
	(&g_port.pools[HUBINDEX_FROMTOKEN(x)])

#define GET_HUB(x) \
	(&g_port.pools[PORTINDEX_FROMTOKEN(x) + 1])

#define GET_ROOTPOOL() \
	(&g_port.pools[0])


struct port_globals {
	short refcount;
	DlistPool pools[MAX_NUM_HUBS];
} g_port = {
	0,
};


#ifdef CONFIG_LOCALHUB
extern struct protocol_methods loc_methods;
#endif

void portdesc_free(PortToken d)
{
	PortBay *b;
	b = GET_POOL(d);
	if (PTR(d)->type == DC_HUB) {
		pool_free(GET_POOL(PTR(d)->hub.ports));
		DEB(netpp_log(DCLOG_VERBOSE,
			"Freeing pool index %ld", HUBINDEX_FROMTOKEN(d)));
	}

	DEB(netpp_log(DCLOG_NOTICE,
		"Freeing item [%d] '%s' from pool index %d",
		PORTINDEX_FROMTOKEN(d), PTR(d)->name, GET_POOL(d)->id));

	dlist_remove(b, PORTINDEX_FROMTOKEN(d));
	dlist_free(b, PORTINDEX_FROMTOKEN(d));
}

#if 0
int portbay_remove(PortToken d)
{
	PortBay *b;
	b = GET_POOL(d);
	DEB(netpp_log(DCLOG_VERBOSE,
		"Freeing element %d from portbay id %d",
		PORTINDEX_FROMTOKEN(d), b->id));
	dlist_remove(b, PORTINDEX_FROMTOKEN(d));
	dlist_free(b, PORTINDEX_FROMTOKEN(d));
	return 0;
}
#endif

static
PortToken portdesc_new(PortBay *b, const char *name)
{
	int n = (int) strlen(name) + 1;
	INDEX i;
	DynPropertyDesc *pd;

	if (n > NAMELENGTH) return 0;

	i = dlist_new(b, &pd);

	if (i == INDEX_NIL) {
		return TOKEN_INVALID;
	}

	string_copy(pd->name, name, n);
	pd->next = pd->prev = DYN_NIL;

	return MAKE_PORTID(b->id, i);
}

int portdesc_getname(PortToken p, const char **name)
{
	*name = PTR(p)->name;
	return 0;
}

static
void hub_init(DynPropertyDesc *hub, INDEX i,
	const struct protocol_methods *methods)
{
	hub->type = DC_HUB;
	hub->hub.methods = methods;
	// Insert link to root node:
	hub->hub.ports = MAKE_PORTID(PORTINDEX_FROMTOKEN(i), 0);
}

DynPropertyDesc *find_portnode(PortBay *bay, const char *portid, TOKEN *node)
{
	PortToken walk;
	DynPropertyDesc *pd = 0;

	walk = ITERATOR_FIRST(bay);

	DEB(netpp_log(DCLOG_VERBOSE,
		"Scan for: %s", portid));
	while (ITERATOR_VALID(walk)) {
		pd = desc_fromtoken(walk);
		DEB(netpp_log(DCLOG_VERBOSE,
			"Scan node[%x]: %s", walk, pd->name));

		if (strcmp(pd->name, portid) == 0) {
			*node = walk;
			return pd;
		}
		walk = ITERATOR_NEXT(bay, pd);
	}
	return NULL;
}

DynPropertyDesc *find_hub(const char *hubid, TOKEN *hub)
{
	return find_portnode(GET_ROOTPOOL(), hubid, hub);
}

TOKEN hub_reuse(const char *hubid, const struct protocol_methods *methods)
{
	DynPropertyDesc *pd;
	TOKEN hub;

	pd = find_hub(hubid, &hub);

	if (!pd) {
		hub = Hub(GET_PORTROOT(), hubid, methods);
	} else {
		DEB(netpp_log(DCLOG_VERBOSE,
			"Found existing hub '%s'", hubid));
	}
	return hub;
}

PortToken Hub(PortBay *b, const char *name,
	const struct protocol_methods *methods)
{
	int i, n;
	const char *num;

//	i = scan_into(name, '#', hubname, 8);
//	if (i > 0 && name[i]) {
//		n = atoi(&name[i]);

	num = skip_delimiter(name, '#');
	if (num && *num) {
		n = atoi(num);
		netpp_log(DCLOG_DEBUG, "Reserve Hub '%.8s' with %d ports", name, n);
	} else {
		n = DEFAULT_POOL_CHUNK;
	}

	PortToken p = PortNode(b, name, DC_HUB);

	if (!IS_VALIDTOKEN(p)) return p;

	// Use hub index in pools[0] to create another port bay
	// for the ports children starting from pools[1]:

	i = PORTINDEX_FROMTOKEN(p) + 1;

	if (i >= MAX_NUM_HUBS) {
		DEB(netpp_log(DCLOG_VERBOSE,
			"Out of Hubs, index %d (MAX_NUM_HUBS-1 = %d)!", i, MAX_NUM_HUBS-1));
		return TOKEN_INVALID;
	}
	DEB(netpp_log(DCLOG_VERBOSE,
		"Create new Hub '%s', index %d", name, i));
	if (pool_new(&g_port.pools[i], n) < 0) {
		DEB(netpp_log(DCLOG_VERBOSE,
			"pool_new n=%d Out of memory", n));
		return TOKEN_INVALID;
	}

	g_port.pools[i].id = i;

	hub_init(PTR(p), i, methods);
	return p;
}

PortToken PortNode(PortBay *b, const char *name, int type)
{
	DynPropertyDesc *pd;
	PortToken p = portdesc_new(b, name);
	if (!IS_VALIDTOKEN(p)) return p;

	pd = PTR(p);
	pd->type = type;
#ifdef CONFIG_PROXY
	if (type == DC_PORT) pd->port.proxy = TOKEN_INVALID;
#endif
	return p;
}

PortToken Port(PortToken hub, const char *name, int type)
{
	return PortNode(GET_HUB(hub), name, type);
}

int portbay_append(PortBay *b, PortToken d)
{
	if (b->id != HUBINDEX_FROMTOKEN(d)) {
		netpp_log(DCLOG_ERROR, "portbay_append(): Not a member!");
		return -1;
	}
	return dlist_append(b, PORTINDEX_FROMTOKEN(d));
}

void hub_append(PortToken hub, PortToken d)
{
	DynPropertyDesc *pd = PTR(hub);

	if (pd->type != DC_HUB) {
		netpp_log(DCLOG_ERROR, "hub_append(): Not a hub!");
		return;
	}
	portbay_append(GET_POOL(pd->hub.ports), d);
}

/* Specific proxy code. A proxy is not allowed to clear its port bays
 * while a connection might be active
 */

void portbay_purge(PortToken port)
{
	PortToken walk, next;
	DynPropertyDesc *pd;
	PortBay *b = GET_POOL(port);
	walk = b->first;

	while (walk != DYN_NIL) {
		pd = GET_PTR(b, walk);
		next = pd->next;
		if (pd->type == DC_PORT) {
			DEB(netpp_log(DCLOG_NOTICE, "Remove %08x:[%d] '%s'",
				port, walk, pd->name));
			dlist_remove(b, walk);
			dlist_free(b, walk);
		}
		walk = next;
	}
}


PortBay *portbay_fromtoken(PortToken b)
{
	return GET_POOL(b);
}

DynPropertyDesc *desc_fromtoken(PortToken b)
{
	return &GET_POOL(b)->base[PORTINDEX_FROMTOKEN(b)];
}

#ifdef TEST_STRUCT
void dumpfunc(DLIST_ITEM *e)
{
	switch (e->type) {
		case DC_DEVICE:
			printf("\t (D)"); break;
		case DC_PORT:
			printf("\t (P)"); break;
		case DC_HUB:
			printf("\t (H)"); break;
		default:
			break;
	}
	printf("\n");
}

#endif

#ifdef DEBUG
void hub_portlist(PortToken d)
{
	int error;
	PortToken n = PORTTOKEN_NIL;
	error = port_select(d, n, &n);
#ifdef TEST_STRUCT
	dlist_dump(&g_port.pools[HUBINDEX_FROMTOKEN(d)], dumpfunc);
#endif

	while (error == 0) {
		netpp_log(DCLOG_VERBOSE, "Port [%08x]: %s", n, PTR(n)->name);
		error = port_select(d, n, &n);
	}
}
#endif

int ports_probe(void)
{
	int error;
	PortToken d;
	DynPropertyDesc *pd;
	PortBay *b;

	b = GET_ROOTPOOL();
	d = ITERATOR_FIRST(b);

	while (ITERATOR_VALID(d)) {
		pd = desc_fromtoken(d);
		d = ITERATOR_NEXT(b, pd);
		// We only purge/clear if we have probe capabilities:
		if (pd->hub.methods && pd->hub.methods->probe && pd->name[0] != '_') {
			portbay_purge(pd->hub.ports);

			// Warning: This function can realloc, thus 'pd' may be invalid
			// after this call
			error = pd->hub.methods->probe(pd);
			if (error < 0) return error;
		}
	}
	return 0;
}

/* Port/Hub node selection:
 *
 * \param parent  Parent node, either PROPERTY_NIL or hub.
 *                PROPERTY_NIL specifies the root node.
 * \param prev    Previous (current) node
 * \param next    Node next to prev node.
 *
 * To be called by dcProperty_Select().
 *
 * \todo TOKEN -> PortDesc pointer encoding (POOL.ITEM)
 * \return 0 if 'next' valid, 1 if PROPERTY_NIL, negative when error
 *
 */

int port_select(PortToken parent, PortToken prev, PortToken *next)
{
	PortBay *b = GET_POOL(prev);
	DynPropertyDesc *pd;
	int ret = 0;

	char is_root = (prev == parent);

	pd = PTR(parent);

	if (parent == TOKEN_INVALID || parent == 0 /* legacy */) {
		if (is_root) {
			if (g_port.refcount == 0) ports_init(NULL);
			ports_probe();
			b = GET_ROOTPOOL();
		}
	} else if (pd->type == DC_HUB) {
		b = GET_POOL(pd->hub.ports);
#ifdef CONFIG_PROXY
	} else if (pd->type == DC_PORT && pd->port.proxy != TOKEN_INVALID) {
		b = GET_POOL(pd->port.proxy);
#endif
	} else {
		*next = TOKEN_INVALID;
		return DCWARN_PROPERTY_NIL;
	}

	if (prev == PORTTOKEN_NIL || is_root) {
		if (b->first == DYN_NIL) {
			*next = TOKEN_INVALID;
			ret = DCWARN_PROPERTY_NIL;
		} else
			*next = MAKE_PORTID(b->id, b->first);
	} else {
		if (PTR(prev)->next == DYN_NIL) {
			*next = TOKEN_INVALID;
			ret = DCWARN_PROPERTY_NIL;
		} else 
			*next = MAKE_PORTID(b->id, PTR(prev)->next);
	}
	return ret;
}

#ifdef DEBUG
void portbay_list(PortBay *b)
{
	DynPropertyDesc *pd;
	PortToken d = ITERATOR_FIRST(b);

	while (ITERATOR_VALID(d)) {
		pd = desc_fromtoken(d);
		netpp_log(DCLOG_VERBOSE, "Node: %s", pd->name);
		hub_portlist(d);
		d = ITERATOR_NEXT(b, pd);
	}
}
#endif

/* Hub list. Important: As of now, hub names for client interfaces have a
 * three letter name ONLY (see strncasecmp() in this file)
 *
 */

#ifndef DEFINE_OWN_HUB_LIST
static const struct hub_list s_hubs[] = {
#ifdef CONFIG_LOCALHUB
	{ "LOC", &loc_methods },
#endif
#ifdef CONFIG_TCP
	{ "TCP", &tcp_methods },
#endif
#ifdef CONFIG_UDP
	{ "UDP", &udp_methods },
#endif
#ifdef CONFIG_PROXY
	{ "PRX", &prx_methods },
#endif
#ifdef CONFIG_USB
	{ "USB", &usb_methods },
#endif
#ifdef CONFIG_DEVICE
	{ "DEV", &dev_methods },
#endif
#ifdef CONFIG_DEVICEMUX
	{ "MUX", &mux_methods },
#endif
	{ NULL }
};
#endif

/** Master specific code: */

int ports_init(const struct hub_list *inithubs)
{
	PortToken hub;
	int error = 0;
	PortBay *b;
	b = GET_ROOTPOOL();
	int i;
	DynPropertyDesc *pd;


#ifdef DEFINE_OWN_HUB_LIST
	if (!inithubs) {
		netpp_log(DCLOG_ERROR, "Invalid 'inithubs' in %s", __FUNCTION__);
		return DCERR_BADPTR;
	}
#else
	if (!inithubs) {
		inithubs = s_hubs;
	}
#endif

	if (g_port.refcount == 0) {
		// Initialize with maximum number of hubs:
		error = pool_new(b, MAX_NUM_HUBS);
		if (error < 0) return error;

		i = 0;
		while (inithubs->name) {
			DEB(netpp_log(DCLOG_VERBOSE,
				"Scanning hub [%d], '%s'",
			i, inithubs->name));
			pd = find_hub(inithubs->name, &hub);
			if (!pd) {

				hub = Hub(b, inithubs->name, inithubs->methods);

				if (hub != TOKEN_INVALID) {
					portbay_append(b, hub);
				} else {
					error = DCERR_MALLOC;
				}
			}
			i++; inithubs++;
		}

	}

	g_port.refcount++;

	return error;
}


void ports_exit(int final_cleanup)
{
	g_port.refcount--;
#ifndef NO_MALLOC
	if (g_port.refcount == 0 || (g_port.refcount >= 0 && final_cleanup)) {
		PortBay *b;
		PortToken hub;
		DynPropertyDesc *pd;
		b = GET_ROOTPOOL();
		hub = ITERATOR_FIRST(b);

		// Free all hubs:
		while (ITERATOR_VALID(hub)) {
			pd = desc_fromtoken(hub);
			hub = ITERATOR_NEXT(b, pd);
			pool_free(GET_POOL(pd->hub.ports));
		}
		// Free root pool:
		pool_free(GET_ROOTPOOL());
	}
#endif
}

int port_open(RemoteDevice *d, TOKEN portid)
{
	int hub_i, port_i;
	DynPropertyDesc *hubd;
	int error;
	PortBay *b;

	// -1, because we want the parenting hub:
	hub_i = HUBINDEX_FROMTOKEN(portid) - 1;
	port_i = PORTINDEX_FROMTOKEN(portid);

	b = GET_ROOTPOOL();

	hubd = &b->base[hub_i];

	d->peer.hub = hubd;
	d->portid = portid;

	error = hubd->hub.methods->open(d, port_i);
	if (error < 0) d->peer.hub = 0;
#ifdef CONFIG_PROXY
	else {
		d->refcount++;
	}
#endif
	return error;
}

int port_close(RemoteDevice *d)
{
	DynPropertyDesc *pd;
	int error = DCERR_PROPERTY_HANDLER;
#ifdef CONFIG_PROXY
	// Save state:
	int state;
	state = d->peer.proxy.state;
#endif
	if (d->peer.hub) error = d->peer.hub->hub.methods->close(d);
	// We don't free the port id this time, but turn it into a PORT
	// These port entries are freed on the next probe()
	pd = desc_fromtoken(d->portid);
	pd->type = DC_PORT;
#ifdef CONFIG_PROXY
	pd->port.state = state; // Restore state
#endif
	return error;
}

int portid_fromname(const char *id, PORTID *pid)
{
	DynPropertyDesc *hubd = 0;
	DynPropertyDesc *pd = 0;
	PortToken h;
	int error = 0;

	PortBay *b = GET_ROOTPOOL();

	h = b->first;

	while (h != DYN_NIL) {
		hubd = GET_PTR(b, h);
		if (strncasecmp(hubd->name, id, 3) == 0) break;
		h = hubd->next;
		hubd = NULL; // Mark invalid by default, in case we fail
	}
	
	if (!hubd) {
		return DCERR_PROPERTY_UNKNOWN;
	}

	// Create Hub token:

	pd = find_portnode(GET_POOL(hubd->hub.ports), &id[4], pid);

	if (!pd) {
		// We did not exist, thus we create ourselves:
		if (hubd->hub.methods->create) {
			h = MAKE_PORTID(b->id, h);
			error = hubd->hub.methods->create(h, id, pid);
			if (error < 0) {
				netpp_log(DCLOG_ERROR, "Failed to create connection");
				return error;
			}
			// We just created a port, return it:
		} else {
			netpp_log(DCLOG_ERROR, "Can not create dynamic device node");
			return DCERR_PROPERTY_HANDLER;
		}
		
		if (error) return error;
	}
	return error;
}

#ifdef HAVE_MULTIUSER
int rdev_new(PortToken p, RemoteDevice **rdevp)
{
	RemoteDevice *rdev;
	DynPropertyDesc *pd;

	pd = desc_fromtoken(p);

	rdev = rdev_alloc(1);
	if (!rdev) {
		return DCERR_MALLOC;
	}

	rdev->cleanup = NULL;

	// Default: abuse user field for enumeration value:
	// This can be used by a device handler to determine a device specific
	// context (like a video buffer)
	rdev->peer.user.node = p;

#if defined(CONFIG_PROXY) || defined(CONFIG_USER_DEVICE_INIT)
	int ret;
	ret = user_device_init(rdev, p);
	if (ret < 0) {
		rdev_dealloc(rdev);
		return DCERR_MALLOC;
	}
#endif

#ifdef CONFIG_PROXY
	// inherit state
	rdev->peer.proxy.state = pd->port.state;
#endif
	pd->device = rdev;
	pd->type = DC_DEVICE;
	*rdevp = rdev;
	return 0;
}
#endif // HAVE_MULTIUSER

char *getname(Peer *p, int portindex)
{
	PortToken t;
	PortBay *b = GET_POOL(p->hub->hub.ports);
	DynPropertyDesc *pd;

	t = MAKE_PORTID(b->id, portindex);
	pd = desc_fromtoken(t);

	if (pd) return pd->name;
	else return NULL;
}


/* Other auxiliaries that should actually move to another file */



/* This function copies from the string s to dest until the delimiter
 * delim occurs. The length of the dest string (including trailing 0) must
 * be specified in n.
 *
 * \return < 0 : error, 0: last occurence of delimiter, > 0: position of the
 *         char after next delimiter occurence.
 */


int scan_into(const char *s, char delim, char *dest, int n)
{
	int i = 0;
	n--;
	while (*s != delim && *s) {
		*dest++ = *s++;
		i++;
		if (i == n) return -1;
	}
	if (!*s) i = 0;  else i++;
	*dest = '\0';
	return i;
}

const char *skip_delimiter(const char *name, const char delim)
{
	while (*name != delim && *name) {
		name++;
	}
	if (*name) { name++; return name; }
	return NULL;
}


////////////////////////////////////////////////////////////////////////////
// Names

#ifndef TINYCPU

const char g_muxserv_name[]   = "_MUXserv";
const char g_muxpeers_name[]  = "_MUXpeers";
const char g_prxserv_name[]   = "_PRXserv";
const char g_prxpeers_name[]  = "_PRXpeers";
const char g_tcpserv_name[]   = "_TCPserv";
const char g_tcppeers_name[]  = "_TCPpeers";
const char g_udpserv_name[]   = "_UDPserv";
const char g_udppeers_name[]  = "_UDPpeers";
const char g_udp_probe_name[] = "_UDPprobe";

#endif
