/* Decode JPEG and display */

#include "jpeg.h"
#include <stdio.h>
#include <stdlib.h>



static
void my_init_source(jpeg_decompress_struct *cinfo)
{

}

static
void my_term_source(j_decompress_ptr cinfo)
{

}

static
int my_fill_inbuf(j_decompress_ptr cinfo)
{
	struct jpeg_source_mgr *mgr = cinfo->src;
	static JOCTET FakeEOI[] = { 0xFF, JPEG_EOI };

	/* Generate warning */
	WARNMS(cinfo, JWRN_JPEG_EOF);
  
	/* Insert a fake EOI marker */
	mgr->next_input_byte = FakeEOI;
	mgr->bytes_in_buffer = 2;

	return 1;
}

static
void skip_input_data (j_decompress_ptr cinfo, long num_bytes)
{
	struct jpeg_source_mgr *mgr = cinfo->src;

	if(num_bytes >= (long) mgr->bytes_in_buffer) {
		fill_input_buffer(cinfo);
		return;
	}

	mgr->bytes_in_buffer -= num_bytes;
	mgr->next_input_byte += num_bytes;
}

////////////////////////////////////////////////////////////////////////////

void *jpeg_decoder_init(void)
{
	jpeg_decompress_struct *cinfo;
	struct jpeg_source_mgr *mgr;
	struct jpeg_error_mgr jerr;

	cinfo = (jpeg_decompress_struct *) malloc(sizeof(jpeg_decompress_struct));
	if (!cinfo) return (void *) cinfo;

	mgr = (struct jpeg_source_mgr *)
		(*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_PERMANENT,
                                            sizeof(struct jpeg_source_mgr));
	cinfo->src = mgr;

	mgr->init_source = my_init_source;
	mgr->fill_input_buffer = my_fill_inbuf;
	mgr->skip_input_data = my_skip_indata;
	mgr->resync_to_restart = jpeg_resync_to_restart; // default
	mgr->term_source = my_term_source;

	jpeg_create_decompress( &cinfo );

	return (void *) cinfo;
}

int jpeg_readhdr(void *handle)
{
	jpeg_decompress_struct *cinfo = (jpeg_decompress_struct*) handle ;
	return jpeg_read_header(cinfo, TRUE);
}

int jpeg_decode_frame(void *handle,
	unsigned char *dest,
	unsigned char *stream, unsigned int len)
{
	struct jpeg_source_mgr *mgr;
	jpeg_decompress_struct *cinfo = (jpeg_decompress_struct*) handle ;
	JSAMPROW row_ptr[1];

	n = cinfo->num_components;

	cinfo->err = jpeg_std_error( &jerr );

	mgr->bytes_in_buffer = len;
	mgr->next_input_byte = src;

	jpeg_start_decompress(cinfo);

	row_ptr[0] = dest;
	for (i = 0; i < cinfo->image_height; i++) {
		row_ptr[0] += n * cinfo->image_width;
		jpeg_read_scanlines(cinfo, row_ptr, 1);
	}

	jpeg_finish_decompress(cinfo);

	return 0;
}

void jpeg_decoder_exit(void *handle)
{
	jpeg_decompress_struct *cinfo = (jpeg_decompress_struct*) handle ;
	
	jpeg_destroy_decompress(cinfo);
	free(cinfo);
}

