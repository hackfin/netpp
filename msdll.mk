#
# Common makefile to create a msvc compatible DLL
#
# $Id: msdll.mk 317 2012-10-26 12:07:22Z strubi $
#

include $(NETPP)/devices/platform.mk

OPTIMIZATIONS = -finline-functions #-O2
# Set this if you want to use __stdcall calling convention
#USE_STDCALL = -DUSE_STDCALL   # for old compatible lib

USE_STDCALL =

CXXFLAGS = $(INCLUDES) $(OPTIMIZATIONS) $(USE_STDCALL)
CXXFLAGS += $(DLLFLAGS) 
CXXFLAGS += -Wall -MD

CFLAGS += $(INCLUDES) $(OPTIMIZATIONS) $(USE_STDCALL)
CFLAGS += $(DLLFLAGS) 
CFLAGS += -Wall -MD

ifdef RELEASE
	CONFIG = $(ARCH)Release
else
	CONFIG = $(ARCH)Debug
endif

ifndef RELEASE
	CFLAGS += $(DEBUGOPTS)
endif

OBJDIR = $(CONFIG)

DLLCOMMONOBJS = $(DLLCOMMONSRCS:%.c=$(OBJDIR)/%.o)
DLLCPPOBJS = $(DLLCPPSRCS:%.cpp=$(OBJDIR)/%.o)
DLLOBJS = $(DLLCSRCS:%.c=$(OBJDIR)/%.o) $(DLLCOMMONOBJS)
DLLOBJS += $(DLLCPPOBJS)

ifndef DLLINSTALLDIR
	DLLINSTALLDIR = $(HOME)/src/netpp/$(CONFIG)
endif

DEFFILE = $(DLLTYPE).def

ifndef DLLTOOL
	DLLTOOL=$(ARCH)dlltool
	DLLWRAP=$(ARCH)dllwrap
endif

# Please don't change this: 
ifndef DLLTYPE
	DLLTYPE = $(LIBNAME)
endif

DLL = $(OBJDIR)/$(LIBNAME).dll
LIB = $(OBJDIR)/$(DLLTYPE).lib     # The import library
DLLEXT = .dll

STATICLIB = $(OBJDIR)/lib$(LIBNAME).a

ifndef INSTALLBASE
	INSTALLNAME = $(DLLTYPE)
else
	INSTALLNAME = $(LIBNAME)
endif

############################################################################
# RULES
#

$(OBJDIR)/%.o : %.cpp
	$(CXX) -o $@  -c $< $(CXXFLAGS) 

$(OBJDIR)/%.o : %.c
	$(CC) -o $@  -c $< $(CFLAGS) 

$(OBJDIR)/%.o: $(PREFIX)/%.c
	$(CC) -o $@ -c $< $(CFLAGS) 

$(OBJDIR)/%.o: $(COMMONSRCDIR)/%.c
	$(CC) -o $@ -c $< $(CFLAGS) 

dll: $(DLL)
	
# Make mingw/cygwin compatible DLL for internal use.
$(DLL): dirs $(DLLOBJS) $(DEFFILE)
	@echo ">> creating $(DLL)..."
	$(DLLWRAP) -k -o $(DLL) --target=i386-mingw32  \
	    --dllname $(DLLTYPE).dll --def $(DEFFILE)\
		$(DLLOBJS) \
		$(DLLDEPS) 
	@echo ">> creating MinGW import library $(LIB)"
	@$(DLLTOOL) --dllname $(DLLTYPE).dll -a --def $(DEFFILE) --output-lib $(LIB) 
# Make DLL directory, if it does not exist
$(OBJDIR):
	[ -e $(OBJDIR) ] || mkdir $(OBJDIR)

dirs: $(OBJDIR)

$(STATICLIB): dirs $(DLLOBJS)
	ar ruv $(STATICLIB) $(DLLOBJS)

lib: $(STATICLIB)
	
$(DEFFILE): $(STATICLIB)
	$(DLLTOOL) $(STATICLIB) --output-def $(DEFFILE) -k

libclean:
	rm -f $(DLL) $(LIB) $(STATICLIB)

installdll:: $(DLL)
	[ -e $(DLLINSTALLDIR) ] || mkdir $(DLLINSTALLDIR)
	cp $(DLL) $(DLLINSTALLDIR)/$(INSTALLNAME).dll

#install import library (careful!)
implibinstall:
	cp $(LIB) $(DLLINSTALLDIR)/$(DLLTYPE).lib

clean::
	rm -fr Debug/ Release/
