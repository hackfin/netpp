/** \file
 *
 * Dynamic Properties
 *
 * \brief Dynamic properties are an option to create property hierarchies ad hoc
 * and dynamically.
 *
 */

/**
 * \defgroup DynProps Dynamic Properties
 */

/**
 * \addtogroup DynProps
 * \{
 *
 * Dynamic property usage: First, a property pool must be initialized using
 * dynprop_init(), giving a maximum number of properties. Then, a PropertyDesc
 * structure must be allocated and initialized, then a new dynamic node can
 * be created using new_dynprop() by passing the PropertyDesc pointer.
 *
 * The first node to be created is the dynamic root node of the device.
 * This node can use derival mechanisms by setting the access.base property
 * of the associated PropertyDesc.
 *
 * Then, newly created nodes can be inserted using dynprop_append() into
 * the root node respectively container nodes like structs.
 *
 * Destruction of nodes occurs using dynprop_unlink() or the bulky way,
 * typically at the end of the deinitialization section using dyntree_free()
 *
 * \section memory Allocation/deallocation
 *
 * The allocation method of the dynamic property descriptors is left to
 * the user, typically by implementing the following external function:
 * property_desc_new()
 *
 * For example using malloc():
\code 
PropertyDesc *property_desc_new(const PropertyDesc *template)
{
	PropertyDesc *p;
	p = (PropertyDesc*) malloc(sizeof(PropertyDesc));
	if (p) {
		memcpy(p, template, sizeof(PropertyDesc));
	}
	return p;
}
\endcode
  * Release of a property descriptor is done inside dyntree_free() using
  * the propfree callback. According to the example above, propfree is
  * set to the standard library's free() function by the user, for example
  * by calling
\code
	dynprop_destroy(root, p, free);
\endcode
 *
 */

typedef void (*prop_release_func)(void *p);

/** Initialize global pool of dynamic properties with given size
 * \param maxprops      Number of maximum properties in pool
 */
int dynprop_init(int maxprops);

/** Release global pool of dynamic propertiues */
void dynprop_exit(void);

/** Retrieve property descriptor of dynamic property
 *
 * \param t       The TOKEN of the dynamic property
 * \return        Pointer to the property descriptor
 *
 * Normally, you should use getProperty_ByToken()
 */
PropertyDesc *dynprop_get(DynPropToken t);

/** Append a DynPropToken node into a parent
 *
 *  \param parent   The parent node
 *  \param elem     The dynamic property to be inserted into the parent
 *                  hierarchy
 */
int dynprop_append(DynPropToken parent, DynPropToken elem);

/** Walk through the dynamic property hierarchy. Works like
 * property_select().
 * 
 *  \param parent   The parent node
 *  \param prev     The previous node: When parent, descend hiearchy.
 *                  When child, select next child
 *
 *  \return         Next node in hiearchy or TOKEN_INVALID when NIL.
 */
TOKEN dynprop_select(DynPropToken parent, DynPropToken prev);

/** Create a new dynamical property from a PropertyDesc template.
 * \param name      The name of the property
 * \param prop      Pointer to Property description
 *
 * \return          A dynamic property TOKEN.
 *
 * \attention       The Property descriptor is owned and allocated by the
 *                  user! So make sure it does not live on the stack.
 */
DynPropToken new_dynprop(const char *name, PropertyDesc *prop);

/** Remove dynamic property from parent
 * \param parent    Parent node containing 'elem'
 * \param elem      Node to remove from parent
 *
 * \return          0 if ok, error else.
 */
int dynprop_unlink(DynPropToken parent, DynPropToken elem);

/** Recursively free property tree
 * \param root     The root node of the device property tree to free
 * \param propfree Function pointer to release function (typically free()).
 * \return         0 if elements were freed, 1 if node had no child.
 *
 */
int dyntree_free(DEVICE d, DynPropToken root, prop_release_func propfree);

/** Externally defined property creation function. To be defined by
 * the user.
 */

extern
PropertyDesc *property_desc_new(const PropertyDesc *templ);


/** Create dynamic property descriptor from template
 *
 * This function creates a new property inside a dynamic container (or root
 * node) by using a static template property defined somewhere in the property
 * list. A copy of this template is created and associated with the
 * entity data. An entity is typically a C struct, allocated on the heap.
 *
 * \param parent   Container to create new property in
 * \param entity   Pointer to a C structure or element associated with
 *                 this property
 * \param templ    Token of static property template
 * \param name     Name for the new property
 */
TOKEN dynprop_from_entity(TOKEN parent, void *entity, 
	TOKEN templ, const char *name);

/** Auxiliary to remove dynamic property. The freeing of the associated
 * entity structure has to be done by the user.
 */
TOKEN dynprop_destroy(TOKEN parent, TOKEN prop, prop_release_func propfree);

/* \} */


/** Calculate CRC of dynproperty hierarchy. Internal function
 */
int dynproperty_crc(DCValue *val);
