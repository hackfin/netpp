LUT_PRECISION = 16
UV_OFFSET = -128
DELTA = 0.0

normfac = 1.0

if LUT_PRECISION == 8:
	offset = 256
	format = "\t0x%02x, "
else:
	offset = 65536
	format = "0x%04x, "
	# offset = 0
	# format = "%d, "

def table(file, f):
	c = 0
	for i in range(256):
		val = f(i)
		if (val < 0):
			val = offset + val
		file.write(format % val)
		c += 1
		if c == 8:
			c = 0
			file.write("\n")


def emit(file, x):
	file.write(x)
	file.write("\n")


f = open("tables.c", "w")


emit(f, "#include \"yuv2rgb.h\"")
emit(f, "\n");

def write_tables(f):

	# Luminance:

	emit(f, "PIXEL8 tab_y[256] = {")
	table(f, lambda y: (FAC_Y * (y + Y_OFFSET) + DELTA))
	emit(f, "};\n")

	# RED:

	emit(f, "PIXEL8 tab_rv[256] = {")
	table(f, lambda v: (FAC_RV * (v + UV_OFFSET) + DELTA))
	emit(f, "};\n")

	# GREEN:

	emit(f, "PIXEL8 tab_gv[256] = {")
	table(f, lambda v: (FAC_GV * (v + UV_OFFSET) + DELTA))
	emit(f, "};\n")

	emit(f, "PIXEL8 tab_gu[256] = {")
	table(f, lambda u: (FAC_GU * (u + UV_OFFSET) + DELTA))
	emit(f, "};\n")

	# BLUE:

	emit(f, "PIXEL8 tab_bu[256] = {")
	table(f, lambda u: (FAC_BU * (u + UV_OFFSET) + DELTA))
	emit(f, "};\n")

############################################################################
# ITU-R BT.601 conversion

emit(f, "#ifdef ITU_R_BT_601\n")
emit(f, "\n#warning \"Using ITU BT601 model\"")

FAC_Y = 1.164
FAC_RV = 1.596
FAC_GV = -0.813
FAC_GU = -0.391
FAC_BU = 2.018
Y_OFFSET = -16

write_tables(f)

emit(f, "\n#else")
############################################################################
# sRGB

# emit(f, "\n#warning \"Using sRGB model\"")

# FAC_Y = 1.0
# FAC_RV = 1.5748
# FAC_GV = -0.4681
# FAC_GU = -0.1873
# FAC_BU = 1.855
# Y_OFFSET = 0

FAC_Y = 1.0
FAC_RV = 1.402
FAC_GV = -0.34414
FAC_GU = -0.71414
FAC_BU = 1.772
Y_OFFSET = 0


write_tables(f)

emit(f, "\n#endif")

f.close()
