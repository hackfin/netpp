/* Stubs to make libslave a library that actually can be linked without
 * external dependencies.
 * These are weak symbols, so clients can override them.
 */

#include "slave.h"


#ifndef __MINGW32__
#define __WEAK __attribute__((weak))
#else
#define __WEAK
#endif

__WEAK
int device_read(RemoteDevice *d,
	uint32_t addr, unsigned char *buf, unsigned long size)
{
	return 0;
}

__WEAK
int device_write(RemoteDevice *d,
	uint32_t addr, const unsigned char *buf,
	unsigned long size)
{
	return 0;
}

#ifdef __MINGW32__
TOKEN local_getroot(RemoteDevice *d)
{
	return DEVICE_TOKEN(0);
}
#endif
