/*
 * Header for timing auxiliaries.
 *
 */

#if defined(__WIN32) || defined(__CYGWIN__)
#	include <windows.h>
#	define TIMEVAL unsigned long
#else
#	include <sys/time.h> // time measurement
#	include <time.h> // time measurement
#	define TIMEVAL struct timeval
#endif

int get_time(TIMEVAL *t);

float took_time(TIMEVAL *t0, TIMEVAL *t1);

