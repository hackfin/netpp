/* Sensor error codes
 *
 * 08/2004 Martin Strubel <hackfin@section5.ch>
 *
 * $Id: sensor_error.h 53 2007-04-18 18:39:12Z strubi $
 *
 */

enum {
	ERR_COMM_RX = -512,
	ERR_COMM_NAK,
	ERR_COMM_TIMEOUT,
	ERR_COMMAND
};

