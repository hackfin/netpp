#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include "i2c-dev.h"
#include "devlib.h"
#include "devlib_error.h"

int g_fd;

int i2c_open(const char *i2cdev)
{
	int fd;
	
	fd = open(i2cdev, O_RDWR);

	if (fd < 0) {
		perror("Opening i2cdevice");
		return DCERR_COMM_ONBOARD;
	}
	printf("i2c initalized\n");
	g_fd = fd;

	int t = 200;
	ioctl(fd, I2C_TIMEOUT, &t);

	return 0;
}

int i2c_close(int fd)
{
	close(fd);
	return 0;
}

int i2c_write(unsigned short addr, unsigned short value)
{
	int error;

	ioctl(g_fd, I2C_SLAVE, (addr >> 8));
	
	printf("Write i2c dev: $%02x [$%02x]: %d\n", addr >> 8, addr & 0xff, value);
	error = i2c_smbus_write_byte_data(g_fd, addr & 0xff, value);
	if (error < 0) return DCERR_COMM_ONBOARD;
	return 0;
}

int i2c_read(unsigned short addr, unsigned short *value)
{
	int error;

	ioctl(g_fd, I2C_SLAVE, (addr >> 8));
	error = i2c_smbus_read_byte_data(g_fd, addr & 0xff);
	if (error < 0) return DCERR_COMM_ONBOARD;
	*value = error & 0xff;
	printf("Read i2c dev: $%02x [$%02x]: %d\n", addr >> 8, addr & 0xff, *value);
	return 0;
}


#ifdef TEST

int main()
{
	int fd;

	fd = i2c_open("/dev/i2c-0");

	i2c_write(0x7008, 0xff);

	i2c_close(fd);
	return 0;
}

#endif
