
/**************************************************************************
 *
 * (c) 2005-2009 section5 // Martin Strubel
 *
 * THIS IS A GENERATED FILE. DO NOT MODIFY! ALL CHANGES WILL BE LOST
 *
 *
 **************************************************************************/

/** \ingroup ErrorHandling */

enum {
	/* Errors: */

	/** Generic communication failure */
	DCERR_COMM_GENERIC = -256,
	
	/** Communication port initialization failed */
	DCERR_COMM_INIT,
	
	/** Command not acknowledged. Possible communication failure. */
	DCERR_COMM_NAK,
	
	/** Bad return code. Possible communication failure. */
	DCERR_COMM_RET,
	
	/** CRC error */
	DCERR_COMM_CRC,
	
	/** Communication busy */
	DCERR_COMM_BUSY,
	
	/** Communication timeout */
	DCERR_COMM_TIMEOUT,
	
	/** Communication frame error (Parity, word length, ...) */
	DCERR_COMM_FRAME,
	
	/** Communication buffer overrun */
	DCERR_COMM_OVERRUN,
	
	/** Onboard communication failure */
	DCERR_COMM_ONBOARD,
	
	/** Peer has disconnected */
	DCERR_COMM_DISCONNECT,
	
	/** Module loader: could not load module (DLL) */
	DCERR_MODULE_LOAD = -128,
	
	/** Module loader: could not find symbol in module (DLL) */
	DCERR_MODULE_SYMBOL,
	
	/** Could not reserve memory */
	DCERR_MALLOC,
	
	/** Bad device handle or pointer value (Null pointer?) */
	DCERR_BADPTR,
	
	/** Operation was cancelled by user */
	DCERR_CANCEL,
	
	/** No more elements available in Pool */
	DCERR_EXCEED_POOL,
	
	/** Fatal mutex/threading error */
	DCERR_MUTEX,
	
	/** Unknown device property */
	DCERR_PROPERTY_UNKNOWN = -64,
	
	/** Property access error, Property is read- or write only */
	DCERR_PROPERTY_ACCESS,
	
	/** Property type mismatch */
	DCERR_PROPERTY_TYPE_MATCH,
	
	/** Property size mismatch (buffer or register size) */
	DCERR_PROPERTY_SIZE_MATCH,
	
	/** Unknown property type */
	DCERR_PROPERTY_TYPE_UNKNOWN,
	
	/** Property Handler missing */
	DCERR_PROPERTY_HANDLER,
	
	/** Invalid value range */
	DCERR_PROPERTY_RANGE,
	
	/** This operation is not supported on the specified Property */
	DCERR_PROPERTY_OPERATION,
	
	/** This device model is unsupported or can not be autoprobed. Please use the software that came with the device. */
	DCERR_MODEL_UNSUPPORTED = -16,
	
	/** This revision is not supported, please update your software */
	DCERR_REVISION_UNSUPPORTED,
	
	/* Warnings: */

	/** Code signalling that a slave event was handled */
	DCWARN_SLAVE_HANDLED = 1,
	
	/** The device config header is corrupted */
	DCWARN_HEADER_INVALID = 4,
	
	/** The firmware revision is newer than the one supported by this driver. Please update your software. */
	DCWARN_REVISION_NEWER,
	
	/** Setting of this property affected another property, or the buffer size may have changed. */
	DCWARN_PROPERTY_MODIFIED,
	
	/** Property is inactive, value setting causes no effect */
	DCWARN_PROPERTY_INACTIVE,
	
	/** Property event occured. Poll event list. */
	DCWARN_PROPERTY_EVENT = 32,
	
	/** Property was deleted/deleted itself */
	DCWARN_PROPERTY_DESTROYED,
	
	/** Unsupported command */
	DCWARN_CMD_UNSUPPORTED = 64,
	
	/** Last property in list reached */
	DCWARN_PROPERTY_NIL = 128,
	
};


