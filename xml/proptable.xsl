<?xml version="1.0" encoding="ISO-8859-1"?>

<!--

$Id: proptable.xsl 335 1970-01-06 02:51:45Z strubi $

Transformation style sheet to generate documentation from XML
device description


-->

<xsl:stylesheet version="1.0" 
	xmlns:my="http://www.section5.ch/dclib/schema/devdesc"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

<xsl:import href="hexutils.xsl"/>	

<xsl:param name="addrlen">8</xsl:param>
<xsl:param name="mmr_base">0x00000000</xsl:param>
<xsl:param name="emit_hardcoded">0</xsl:param>
<xsl:param name="tbl_floatstyle">Hb</xsl:param>

	<xsl:output method="xml" encoding="ISO-8859-1" indent="yes" doctype-system="/usr/share/xml/docbook/schema/dtd/4.5/docbookx.dtd" doctype-public="-//OASIS//DTD DocBook XML V4.5//EN"/>	

	<xsl:template name="getparentid">
		<xsl:param name="context" select="."/>
		<xsl:param name="delim">_</xsl:param>

		<xsl:choose>
			<xsl:when test="$context/parent::my:array"><xsl:value-of select="$context/../@name"/><xsl:value-of select="$delim"/></xsl:when>
			<xsl:when test="$context/parent::my:struct"><xsl:value-of select="$context/../@name"/><xsl:value-of select="$delim"/></xsl:when>
			<xsl:when test="$context/parent::my:property"><xsl:value-of select="$context/../@name"/><xsl:value-of select="$delim"/></xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$context/@name"/>
	</xsl:template>

	<!-- Register maps -->

	<xsl:template match="my:register" mode="overview">
		<row>
			<entry><systemitem><xsl:value-of select="@addr"/>
				<xsl:if test="@size"> [<xsl:value-of select="@size"/>]</xsl:if>
			</systemitem></entry>
			<entry>
<!-- Create link, if we have detailed table -->
				<xsl:choose>
					<xsl:when test="my:bitfield">
                        <link><xsl:attribute name="linkend">tbl_reg_<xsl:value-of select="@id"/></xsl:attribute><property><xsl:value-of select="@id"/></property></link>
					</xsl:when>
					<xsl:otherwise>
						<property><xsl:value-of select="@id"/></property>
					</xsl:otherwise>
				</xsl:choose>
			</entry>
		
			<entry>
				<xsl:choose>
					<xsl:when test="@access"><xsl:value-of select="@access"/></xsl:when>
					<xsl:when test="../@access"><xsl:value-of select="../@access"/></xsl:when>
					<xsl:otherwise>RW</xsl:otherwise>
				</xsl:choose>
			</entry>
			<entry><xsl:value-of select="my:info"/></entry>
		</row>
	</xsl:template>

	<!-- Register details -->

	<xsl:template match="my:bitfield">
		<row>
			<entry>
			<xsl:choose>
			<xsl:when test="@msb = @lsb">
			<xsl:value-of select="@lsb"/>
			</xsl:when>
			<xsl:otherwise>
			<xsl:value-of select="@msb"/>:<xsl:value-of select="@lsb"/>
			</xsl:otherwise>
			</xsl:choose>
			</entry>
			<entry><systemitem><xsl:value-of select="@name"/></systemitem></entry>
			<entry><xsl:value-of select="my:info"/></entry>
		</row>
	</xsl:template>

	<xsl:template match="my:register" mode="list">
		<varlistentry>
			<term>
				<xsl:choose>
					<xsl:when test="my:bitfield">
                        <link><xsl:attribute name="linkend">tbl_reg_<xsl:value-of select="@id"/></xsl:attribute><property><xsl:value-of select="@id"/></property></link>
					</xsl:when>
					<xsl:otherwise>
						<property><xsl:value-of select="@id"/></property>
					</xsl:otherwise>
				</xsl:choose>
			</term>
			<listitem>
				<xsl:choose>
				<xsl:when test="my:info">
				<para><xsl:value-of select="my:info"/></para>
				</xsl:when>
				<xsl:otherwise>
				<para>Undocumented</para>
				</xsl:otherwise>
				</xsl:choose>
			</listitem>
		</varlistentry>
	</xsl:template>

	<xsl:template match="my:register" mode="detail">

		<xsl:if test="my:bitfield">
			<table pgwide="1">
				<xsl:attribute name="floatstyle"><xsl:value-of select="$tbl_floatstyle"/></xsl:attribute>
				<xsl:attribute name="id">tbl_reg_<xsl:value-of select="@id"/></xsl:attribute>
				<title>
				<xsl:value-of select="@id"/>
				<xsl:text> register </xsl:text>
				<xsl:if test="@access">
					(<xsl:value-of select="@access"/>)
				</xsl:if>
				<xsl:choose>
				<xsl:when test="../@nodecode='true'">
				<xsl:value-of select="my:info"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
					<xsl:when test="../@offset">
					<xsl:text> Address: 0x</xsl:text>
					<xsl:call-template name="calcaddr">
						<xsl:with-param name="base" select="../@offset"/>
						<xsl:with-param name="offs" select="@addr"/>
						<xsl:with-param name="padded-length" select="$addrlen"/>
					</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> Offset: </xsl:text>
						<xsl:value-of select="@addr"/>
					</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
				</xsl:choose>
				</title>
			<tgroup cols="3">
			<colspec align="left" colnum="1" colwidth="1*"></colspec>
			<colspec align="left" colnum="2" colwidth="3*"></colspec>
			<colspec align="justify" colnum="3" colwidth="5*"></colspec>
			<thead>
				<row>
					<entry>Bit(s)</entry>
					<entry>Name</entry>
					<entry>Description</entry>
				</row>
			</thead>

			<tbody>
				<xsl:apply-templates select="./my:bitfield">
					<xsl:sort select="@lsb" data-type="number" order="descending"/>
				</xsl:apply-templates>
			</tbody>
			</tgroup>
		</table>
		</xsl:if>
	</xsl:template>

	<xsl:template match="my:registermap" mode="overview">
	<section><title><xsl:value-of select="@name"/></title>
	<para><xsl:value-of select="my:info"/></para>
	<para>
		<xref><xsl:attribute name="linkend">tbl_regmap_<xsl:value-of select="@id"/>
		</xsl:attribute></xref> shows the detailed map of the <xsl:value-of select="@name"/> address space.
	</para>
	<table floatstyle="htp">
		<xsl:choose>
			<xsl:when test="@id">
				<xsl:attribute name="id">tbl_regmap_<xsl:value-of select="@id"/></xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="id">tbl_regmap_<xsl:value-of select="generate-id()"/></xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>

		<title>Address map <xsl:value-of select="@name"/> 
		<xsl:if test="@offset">
			<xsl:text> starting at MMR base 0x</xsl:text>
			<xsl:call-template name="calcaddr">
				<xsl:with-param name="base" select="@offset"/>
				<xsl:with-param name="offs" select="0"/>
				<xsl:with-param name="padded-length" select="$addrlen"/>
			</xsl:call-template>
		</xsl:if>
		</title>

		<tgroup cols="4">
			<colspec align="left" colnum="1" colwidth="2*"></colspec>
			<colspec align="left" colnum="2" colwidth="3*"></colspec>
			<colspec align="left" colnum="3" colwidth="1*"></colspec>
			<colspec align="justify" colnum="4" colwidth="6*"></colspec>
			<thead>
				<row>
					<entry>Offset [Span]</entry>
					<entry>Name(Id)</entry>
					<entry>Access</entry>
					<entry>Description</entry>
				</row>
			</thead>
			<tbody>
				<xsl:apply-templates select="./my:register" mode="overview">
					<xsl:sort select="@addr" data-type="text"/>
				</xsl:apply-templates>
			</tbody>
		</tgroup>
	</table>

	</section>

	</xsl:template>
	<xsl:template match="my:registermap" mode="detail">
		<xsl:if test="my:register/my:bitfield">
		<section><title>'<xsl:value-of select="@name"/>' core registers</title>
			<variablelist>
			<xsl:apply-templates select="./my:register" mode = "list"/>
			</variablelist>
			<xsl:apply-templates select="./my:register" mode = "detail"/>
		</section>
		</xsl:if>
		<xsl:processing-instruction name="latex">
\vfill
\newpage
</xsl:processing-instruction>

	</xsl:template>
	
	<!-- Properties -->
	<xsl:template match="my:property" mode="tab">
		<xsl:variable name="parentid">
			<xsl:call-template name="getparentid"/>
		</xsl:variable>

		<row>
			<entry>
				<xsl:choose>
					<xsl:when test="@type='MODE'">
<link><xsl:attribute name="linkend">tbl_choice_<xsl:value-of select="$parentid"/></xsl:attribute><property><xsl:value-of select="@name"/></property></link>
					</xsl:when>
					<xsl:otherwise>
					<property><xsl:value-of select="@name"/></property>
					</xsl:otherwise>
				</xsl:choose>
			</entry>
			<entry>
				<xsl:value-of select="@type"/>
			</entry>
			<entry>
				<xsl:choose>
					<xsl:when test="@access"><xsl:value-of select="@access"/></xsl:when>
					<xsl:otherwise>RW</xsl:otherwise>
				</xsl:choose>
			</entry>
			<entry><xsl:value-of select="my:info"/></entry>
		</row>
	</xsl:template>

	<xsl:template match="my:struct" mode="tab">
	<xsl:variable name="parentid">
		<xsl:call-template name="getparentid"/>
	</xsl:variable>
		<row>
			<entry>
					<link><xsl:attribute name="linkend">tbl_<xsl:value-of select="$parentid"/></xsl:attribute><property><xsl:value-of select="@name"/></property></link>
			</entry>

			<entry>STRUCT</entry>
			<entry>
				<xsl:choose>
					<xsl:when test="@access"><xsl:value-of select="@access"/></xsl:when>
					<xsl:otherwise>RW</xsl:otherwise>
				</xsl:choose>
			</entry>
			<entry><xsl:value-of select="my:info"/></entry>
		</row>
	</xsl:template>

	<xsl:template match="my:array" mode="tab">
		<xsl:variable name="parentid">
			<xsl:call-template name="getparentid"/>
		</xsl:variable>
		<row>
			<entry>
				<xsl:choose>
				<xsl:when test="./my:struct">
					<link>
						<xsl:attribute name="linkend">tbl_<xsl:value-of select="$parentid"/></xsl:attribute>
						<property><xsl:value-of select="@name"/></property>

					</link>
				</xsl:when>
				<xsl:otherwise>
					<property><xsl:value-of select="@name"/></property>
				</xsl:otherwise>
			</xsl:choose>
			</entry>
			<entry>ARRAY</entry>
			<entry>
				<xsl:choose>
					<xsl:when test="@access"><xsl:value-of select="@access"/></xsl:when>
					<xsl:otherwise>RW</xsl:otherwise>
				</xsl:choose>


			</entry>
			<entry><xsl:value-of select="my:info"/></entry>
		</row>
	</xsl:template>

	<!-- Modes -->

	<xsl:template match="my:item" mode="itemize">
		<xsl:variable name="parentid">
			<xsl:call-template name="getparentid">
				<xsl:with-param name="context" select=".."/>
			</xsl:call-template>
		</xsl:variable>

		<row>
			<entry><xsl:value-of select="./my:value"/></entry>
			<entry><xsl:value-of select="@name"/></entry>
			<entry><xsl:value-of select="./my:info"/></entry>
		</row>
	</xsl:template>

	<xsl:template match="my:choice" mode="itemize">
		<xsl:variable name="parentid">
			<xsl:call-template name="getparentid">
				<xsl:with-param name="context" select=".."/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:variable name="parentname">
			<xsl:call-template name="getparentid">
				<xsl:with-param name="delim">.</xsl:with-param>
				<xsl:with-param name="context" select=".."/>
			</xsl:call-template>
		</xsl:variable>

		<table>
			<xsl:attribute name="id">tbl_choice_<xsl:value-of select="$parentid"/></xsl:attribute>
			<title>Mode <property><xsl:value-of select="$parentname"/></property> -- possible values</title>
			<tgroup cols="3">
				<colspec align="left" colnum="1" colwidth="1*"></colspec>
				<colspec align="left" colnum="2" colwidth="2*"></colspec>
				<colspec align="justify" colnum="3" colwidth="5*"></colspec>
				<thead>
					<row>
						<entry>Value</entry>
						<entry>Mode name</entry>
						<entry>Description</entry>
					</row>
				</thead>
				<tbody>
					<xsl:apply-templates select="./my:item" mode="itemize"/>
				</tbody>
			</tgroup>
		</table>

	</xsl:template>

	<xsl:template match="my:struct" mode="proplist">
		<xsl:variable name="parentid">
			<xsl:call-template name="getparentid"/>
		</xsl:variable>


		<table pgwide="1">
		<xsl:choose>
		<xsl:when test="../../my:array">
			<xsl:attribute name="id">tbl_<xsl:value-of select="../@name"/></xsl:attribute>
			<title>Array item <property><xsl:value-of select="../@name"/>[i]</property></title>
		</xsl:when>
		<xsl:otherwise>
			<xsl:attribute name="id">tbl_<xsl:value-of select="$parentid"/></xsl:attribute>
			<title>Struct <property><xsl:value-of select="@name"/></property></title>
		</xsl:otherwise>
		</xsl:choose>
			<tgroup cols="4">
				<colspec align="left" colnum="1" colwidth="2*"></colspec>
				<colspec align="left" colnum="2" colwidth="2*"></colspec>
				<colspec align="left" colnum="3" colwidth="1*"></colspec>
				<colspec align="justify" colnum="4" colwidth="5*"></colspec>
				<thead>
					<row>
						<entry>Property</entry>
						<entry>Type</entry>
						<entry>Flags</entry>
						<entry>Description</entry>
					</row>
				</thead>
				<tbody>
					<xsl:apply-templates select="./my:property[not(@hidden='true')]" mode="tab"/>
					<xsl:apply-templates select="./my:struct[not(@hidden='true')]" mode="tab"/>
					<xsl:apply-templates select="./my:array[not(@hidden='true')]" mode="tab"/>
				</tbody>
			</tgroup>
		</table>
	</xsl:template>

	<xsl:template match="my:group">

		<section>
			<xsl:attribute name="id">

			<xsl:choose>
				<xsl:when test="@id">sec_<xsl:value-of select="@id"/></xsl:when>
				<xsl:otherwise>sec_<xsl:value-of select="generate-id()"/></xsl:otherwise>
			</xsl:choose>
			</xsl:attribute>

		<title><xsl:value-of select="@name"/></title>
		<table>
			<xsl:attribute name="id">
			<xsl:choose>
				<xsl:when test="@id">tbl_<xsl:value-of select="@id"/></xsl:when>
				<xsl:otherwise>tbl_<xsl:value-of select="generate-id()"/></xsl:otherwise>
			</xsl:choose>
			</xsl:attribute>

			<title>Property group '<xsl:value-of select="@name"/>'</title>
			<tgroup cols="4">
				<colspec align="left" colnum="1" colwidth="2*"></colspec>
				<colspec align="left" colnum="2" colwidth="2*"></colspec>
				<colspec align="left" colnum="3" colwidth="1*"></colspec>
				<colspec align="justify" colnum="4" colwidth="5*"></colspec>
				<thead>
					<row>
						<entry>Property</entry>
						<entry>Type</entry>
						<entry>Flags</entry>
						<entry>Description</entry>
					</row>
				</thead>
				<tbody>
					<xsl:apply-templates select="./my:property[not(@hidden='true')]" mode="tab"/>
					<xsl:apply-templates select="./my:struct[not(@hidden='true')]" mode="tab"/>
					<xsl:apply-templates select="./my:array[not(@hidden='true')]" mode="tab"/>
				</tbody>
			</tgroup>
		</table>
		<xsl:if test="my:info">
		<para><xsl:value-of select="my:info"/></para>
		</xsl:if>
		<para>See <xref><xsl:attribute name="linkend">
			<xsl:choose>
				<xsl:when test="@id">tbl_<xsl:value-of select="@id"/></xsl:when>
				<xsl:otherwise>
					<xsl:text>tbl_</xsl:text>
					<xsl:value-of select="generate-id()"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute></xref></para>

		<xsl:if test="./my:property/@type='MODE' or ./my:struct or ./my:array">
			<xsl:apply-templates select=".//my:property[@type='MODE' and not(@hidden='true')]/my:choice" mode="itemize"/>
			<xsl:apply-templates select=".//my:struct[not(@hidden='true')]" mode="proplist"/>
		</xsl:if>
		<xsl:processing-instruction name="latex">
\vfill
\newpage
</xsl:processing-instruction>

		</section>
	</xsl:template>

	<xsl:template match="my:devdesc/my:device">

		<!-- Device section -->
		<section>
			<xsl:attribute name="id">
				<!-- if device has an ID, use it, otherwise generate -->
				<xsl:choose>
					<xsl:when test="@id">sec_<xsl:value-of select="@id"/></xsl:when>
					<xsl:otherwise>
						sec_<xsl:value-of select="generate-id()"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<title><xsl:value-of select="@name"/></title>

			<remark>
				<xsl:attribute name="id">rev_tag_<xsl:value-of select="@id"/></xsl:attribute>
				<xsl:text>Device Revision: </xsl:text>
				<xsl:value-of select="my:revision/my:major"/>.<xsl:value-of select="my:revision/my:minor"/><xsl:value-of select="my:revision/my:extension"/>
</remark>
			<para><xsl:value-of select="my:info"/></para>

			<!-- Property section -->

			<xsl:if test="my:group">
			<section>
				<xsl:attribute name="id">
					<xsl:choose>
						<xsl:when test="@id">sec_prop_<xsl:value-of select="@id"/></xsl:when>
						<xsl:otherwise>
							sec_prop_<xsl:value-of select="generate-id()"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<title>Properties</title>
				<xsl:apply-templates select="./my:group[not(@hidden='true')]"/>
			</section>
			</xsl:if>
			<!-- Registermap section, if existing -->
			<xsl:if test="./my:registermap">
				<section>
					<xsl:attribute name="id">sec_mmr_<xsl:value-of select="@id"/></xsl:attribute>
					<title>Low level address map</title>
					<xsl:apply-templates select="./my:registermap[not(@nodecode='true') and not(@hidden='true')]" mode="overview"/>
				</section>
				<section>
					<xsl:attribute name="id">sec_reg_<xsl:value-of select="@id"/></xsl:attribute>
					<title><xsl:value-of select="@name"/> registers</title>
					<xsl:apply-templates select="./my:registermap[not(@nodecode='true') and not(@hidden='true')]" mode="detail"/>
				</section>
			</xsl:if>

		<xsl:if test="./my:registermap[not(@hidden='true') and (@nodecode='true')]">
		<section>
			<title>Pseudo register map</title>
			<xsl:apply-templates select="./my:registermap[not(@hidden='true') and (@nodecode='true')]" mode="overview"/>

			</section>
		</xsl:if>
		</section>

	</xsl:template>

	<xsl:template match="/">
		<chapter><title>Device hardware properties</title>

		<xsl:apply-templates select=".//my:device"/>
		</chapter>

	</xsl:template>

</xsl:stylesheet>
