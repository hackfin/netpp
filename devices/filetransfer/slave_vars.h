/*
 * These are all external definitions of slave variables that are
 * visible/accessible through netpp.
 *
 */

#define MAX_FILENAME_LEN  0x100

extern int s_get_error;
extern char g_filename[MAX_FILENAME_LEN];

#define BUFFER_SIZE      0x20000

extern unsigned char g_buffer[BUFFER_SIZE];
