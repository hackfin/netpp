/** Example implementation of direct variable access via the netpp
 * property mechanisms.
 */

/** Define this, when you wish to use the derived class
 *  with experimental code. This is an example, not actually experimental
 *  code from netpp! */

#define BUFSIZE    0x2000
#define STRINGSIZE 64

/** An image buffer structure definition */
typedef struct my_imagebuffer {
	void           *data;
	int             x;
	int             y;
	unsigned int    size;
	uint16_t        bpp;
	char            indexed;
} ImgBuffer;

/** An external image buffer descriptor */
ImgBuffer g_imgbuf;

extern
int32_t g_array[16];

struct my_globals {
	/** A float value */
	float           zoom;
	/** A global buffer */
#ifndef TINYCPU
	unsigned char buffer[BUFSIZE];
	int bufsize;
#endif
	/** A string */
	char string[STRINGSIZE];
	/** Global integer */
	int mode;
	/** Global event counter */
	int count;
	/** Global boolean flag */
	uint8_t flag;
	/** Value for demonstration */
	int demo;
};

/** Here we expose the above global structure to all source including
 * this header */
extern struct my_globals g_globals;

#define NAME_SPACE 80

struct Object {
	int count;
	char name[NAME_SPACE];
};
