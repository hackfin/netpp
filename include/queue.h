#include "queue_config.h"

#ifdef __MSP430__

#include "intrinsics.h"

#define MAYBE(x)
#define ENTER_CRITICAL_PUT(x)
#define LEAVE_CRITICAL_PUT(x)

#define ENTER_CRITICAL(p) __disable_interrupt()
#define LEAVE_CRITICAL(p) __enable_interrupt()

#else

#ifndef MAYBE
#define MAYBE(x) x
#endif

#ifndef QUEUE_MUTEX
#define QUEUE_MUTEX NETPP_MUTEX
#endif

#endif

/** Queue state
 * Each queue has an associated protocol state.
 */

typedef enum {
	Q_IDLE,           ///< Queue is idle
	Q_COMMAND,        ///< Queue is in command sequence
	Q_QUERY,          ///< Queue is in query sequence
	Q_TIMEOUT,        ///< Queue timed out
	Q_FULL,           ///< Queue is full (overflow)
} QueueState;

/** Packet queue structure */
typedef struct pktqueue {
	PACKET          q[Q_SIZE];   ///< Queue buffers
	int          size[Q_SIZE];   ///< size array (for each buffer)
	uint8_t      head;           ///< Queue head
	uint8_t      tail;           ///< Queue tail
	MAYBE(QUEUE_MUTEX  mutex);   ///< Mutex (OS dependent)
	QueueState   state;          ///< Queue state
} PacketQueue;

/** Allocate and return a new queue structure. Not on standalone OS. */
PacketQueue *queue_new(void);

/** Initialize queue */
void queue_init(PacketQueue *pq);

/** Free queue */
void queue_exit(PacketQueue *pq);

/** Queue packet (from inside ISR or filler thread)
 * \param pq     Packet queue pointer
 * \param p      Pointer to packet
 * \param len    Length of packet
 */
int queue_put(PacketQueue *pq, void *p, int len);

/** Retrieve packet pointer from queue
 *
 * \param pq     Packet queue pointer
 * \param p      Pointer to data pointer to be returned
 * \param len    Raw packet length to be returned
 * 
 */
int queue_get(PacketQueue *pq, void **p, int *len);

/** Set queue head pointer
 *
 */
int queue_set(PacketQueue *pq, void *p, int len);


/** Advance tail pointer after data from queue_get() has been processed.
 */
int queue_pop(PacketQueue *pq);

/** Retrieve head pointer to packet to be filled
 *
 * Does NOT lock.
 *
 * \param p    Pointer to data pointer variable to be returned from this call
 * \return 0: Pointer valid, -1: Overrun
 *
 */
int queue_ptr(PacketQueue *pq, void **p);

/** Commit 'len' bytes to queue head */
int queue_commit(PacketQueue *pq, int len);

