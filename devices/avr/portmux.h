/* Port multiplexer functionality */


enum {
	// PORT_A,
	PORT_B,
	PORT_C,
	PORT_D,
	PORT_E,
	PORT_F
};

typedef
struct port_map {
	uint16_t portid:3;
	uint16_t pini:3;
	uint16_t caps:4;
	uint16_t current:3;
} XPortMap;

typedef uint16_t PortMap;

extern PortMap g_portmap[];

int set_portfunc(uint8_t which, uint8_t func);
int set_port(uint8_t which, int level);
int get_port(uint8_t which);


