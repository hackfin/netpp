<?xml version="1.0" encoding="ISO-8859-1"?>


<!--

$Id: proplist.xsl 334 2015-02-28 10:20:53Z strubi $

Transformation style sheet DEVDESC-XML -> PropertyList C source

(c) 2005, section5 // Martin Strubel <strubel@section5.ch>

-->

<xsl:stylesheet version="1.0" 
	xmlns:my="http://www.section5.ch/dclib/schema/devdesc"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

	<xsl:output method="text" indent="yes" encoding="ISO-8859-1"/>	

<!-- Index of desired device -->
<xsl:param name="selectDevice">1</xsl:param>

<xsl:template match="my:property|my:struct|my:array" mode="enum">
<xsl:text>	</xsl:text>
	<xsl:value-of select="generate-id()"/>, // <xsl:value-of select="@name"/>
	<xsl:if test="@volatile='true'"> (volatile)</xsl:if>
	<xsl:text>
</xsl:text>
</xsl:template>

<xsl:template match="/">
<xsl:variable name="index" select="number($selectDevice)"></xsl:variable>

<xsl:text>// Generated class
// (c) 2016 &lt;hackfin@section5.ch&gt;
//
// NOTE: Changes to this file will be lost.

import VisualLogic.*;
import java.awt.*;
import java.awt.event.*;
import tools.*;
import VisualLogic.variables.*;

</xsl:text>
	<xsl:choose>
	<xsl:when test="string($index) != 'NaN'">
		<xsl:apply-templates select=".//my:device[$index]"/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:apply-templates select=".//my:device[@name=$selectDevice]"/>
	</xsl:otherwise>
	</xsl:choose>

</xsl:template>

<xsl:template match="my:property" mode="declare">
<xsl:text>	</xsl:text>
		<xsl:choose>
			<xsl:when test="@type='BOOL'">VSBoolean</xsl:when>
			<xsl:when test="@type='REGISTER'">VSInteger</xsl:when>
			<xsl:when test="@type='MODE'">VSInteger</xsl:when>
			<xsl:when test="@type='COMMAND'">VSInteger</xsl:when>
			<xsl:when test="@type='INT'">VSInteger</xsl:when>
			<xsl:when test="@type='FLOAT'">VSDouble</xsl:when>
			<xsl:otherwise>// Property </xsl:otherwise>
		</xsl:choose>
		<xsl:text>	</xsl:text>
		<xsl:value-of select="@name"/><xsl:text>;
</xsl:text>
</xsl:template>

<xsl:template match="my:property" mode="decltoken">
	<xsl:choose>
		<xsl:when test="@type='STRING'">// String <xsl:value-of select="@name"/>
		<xsl:text>	 not supported
</xsl:text>
		</xsl:when>
		<xsl:when test="@type='BUFFER'">// Buffer <xsl:value-of select="@name"/>
		<xsl:text>	 not supported
</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>	Property p_</xsl:text>
				<xsl:value-of select="@name"/>
			<xsl:text>;
</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="my:property" mode="inittoken">
	<xsl:choose>
		<xsl:when test="@type='STRING'">// String <xsl:value-of select="@name"/>
		<xsl:text>	 not supported
</xsl:text>
		</xsl:when>
		<xsl:when test="@type='BUFFER'">// Buffer <xsl:value-of select="@name"/>
		<xsl:text>	 not supported
</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>			p_</xsl:text>
			<xsl:value-of select="@name"/> = dev.getProperty("<xsl:value-of select="@name"/>
			<xsl:text>");
</xsl:text>
			if (p_<xsl:value-of select="@name"/> == null) {
				System.out.println("Property not found: <xsl:value-of select="@name"/>");
					
			<xsl:text>}
</xsl:text>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>

<xsl:template match="my:property" mode="initinputpin">
	<xsl:choose>
		<xsl:when test="@type='STRING'">// String <xsl:value-of select="@name"/>
		<xsl:text>	 not supported
</xsl:text>
		</xsl:when>
		<xsl:when test="@type='BUFFER'">// Buffer <xsl:value-of select="@name"/>
		<xsl:text>	 not supported
</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>		</xsl:text>
			<xsl:value-of select="@name"/>
				<xsl:text> = (</xsl:text>
				<xsl:choose>
					<xsl:when test="@type='BOOL'">VSBoolean</xsl:when>
					<xsl:when test="@type='REGISTER'">VSInteger</xsl:when>
					<xsl:when test="@type='MODE'">VSInteger</xsl:when>
					<xsl:when test="@type='COMMAND'">VSInteger</xsl:when>
					<xsl:when test="@type='INT'">VSInteger</xsl:when>
					<xsl:when test="@type='FLOAT'">VSDouble</xsl:when>
					<xsl:otherwise></xsl:otherwise>
				</xsl:choose>
				<xsl:text>) 
				element.getPinInputReference(PropEnum.</xsl:text>
				<xsl:value-of select="generate-id()"/>
				<xsl:text>.ordinal());
</xsl:text>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>

<xsl:template match="my:property" mode="initoutputpin">
	<xsl:choose>
		<xsl:when test="@type='STRING'">// String <xsl:value-of select="@name"/>
		<xsl:text>	 not supported</xsl:text>
		</xsl:when>
		<xsl:when test="@type='BUFFER'">// Buffer <xsl:value-of select="@name"/>
		<xsl:text>	 not supported</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>		</xsl:text>
			<xsl:value-of select="@name"/>
			<xsl:text> = new </xsl:text>
				<xsl:choose>
					<xsl:when test="@type='BOOL'">VSBoolean</xsl:when>
					<xsl:when test="@type='REGISTER'">VSInteger</xsl:when>
					<xsl:when test="@type='MODE'">VSInteger</xsl:when>
					<xsl:when test="@type='COMMAND'">VSInteger</xsl:when>
					<xsl:when test="@type='INT'">VSInteger</xsl:when>
					<xsl:when test="@type='FLOAT'">VSDouble</xsl:when>
					<xsl:otherwise></xsl:otherwise>
				</xsl:choose>
			<xsl:text>();
</xsl:text>

			<xsl:text>		</xsl:text>
			<xsl:value-of select="@name"/>
				<xsl:text>.setPin(PropEnum.</xsl:text>
<xsl:value-of select="generate-id()"/>
				<xsl:text>.ordinal());
		element.setPinOutputReference(PropEnum.</xsl:text>
				<xsl:value-of select="generate-id()"/>
				<xsl:text>.ordinal(), </xsl:text>
			<xsl:value-of select="@name"/>
				<xsl:text>);</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:text>
</xsl:text>

</xsl:template>


<xsl:template match="my:property" mode="writeaction">
	<xsl:choose>
		<xsl:when test="@type='STRING'">// String <xsl:value-of select="@name"/>
		<xsl:text>	 not supported
</xsl:text>
		</xsl:when>
		<xsl:when test="@type='BUFFER'">// Buffer <xsl:value-of select="@name"/>
		<xsl:text>	 not supported
</xsl:text>
		</xsl:when>

		<xsl:otherwise>
		if (idx == PropEnum.<xsl:value-of select="generate-id()"/>.ordinal()) {
			System.out.println("Set <xsl:value-of select="@name"/>");
			p_<xsl:value-of select="@name"/>
				<xsl:text>.setValue((</xsl:text>
				<xsl:choose>
					<xsl:when test="@type='BOOL'">boolean</xsl:when>
					<xsl:when test="@type='REGISTER'">int</xsl:when>
					<xsl:when test="@type='MODE'">short</xsl:when>
					<xsl:when test="@type='COMMAND'">byte</xsl:when>
					<xsl:when test="@type='INT'">int</xsl:when>
					<xsl:when test="@type='FLOAT'">float</xsl:when>
					<xsl:otherwise></xsl:otherwise>
				</xsl:choose>
		<xsl:text>) </xsl:text>
		 <xsl:value-of select="@name"/>
		<xsl:text>.getValue());
		} else
</xsl:text>

		</xsl:otherwise>
	</xsl:choose>

</xsl:template>

<xsl:template match="my:property" mode="initpin">
	<xsl:variable name="vltype">
		<xsl:choose>
			<xsl:when test="@type='BOOL'">BOOLEAN</xsl:when>
			<xsl:when test="@type='REGISTER'">INTEGER</xsl:when>
			<xsl:when test="@type='MODE'">INTEGER</xsl:when>
			<xsl:when test="@type='COMMAND'">INTEGER</xsl:when>
			<xsl:when test="@type='INT'">INTEGER</xsl:when>
			<xsl:when test="@type='FLOAT'">DOUBLE</xsl:when>
			<xsl:otherwise>// Property </xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:choose>
		<xsl:when test="@type='STRING'">// String <xsl:value-of select="@name"/>
		<xsl:text>	 not supported
</xsl:text>
		</xsl:when>
		<xsl:when test="@type='BUFFER'">// Buffer <xsl:value-of select="@name"/>
		<xsl:text>	 not supported
</xsl:text>
		</xsl:when>
		<xsl:otherwise>
	<xsl:text>		setPin(PropEnum.</xsl:text>
    	<xsl:value-of select="generate-id()"/>.ordinal(),
			ExternalIF.C_<xsl:value-of select="$vltype"/>
	<xsl:text>, ExternalIF.</xsl:text>
	<xsl:choose>
		<xsl:when test="@access='RO'">PIN_OUTPUT</xsl:when>
		<xsl:otherwise>PIN_INPUT</xsl:otherwise>
	</xsl:choose>
	<xsl:text>);
</xsl:text>
	<xsl:text>		element.jSetPinDescription(PropEnum.</xsl:text>
    	<xsl:value-of select="generate-id()"/>.ordinal(),
			"<xsl:value-of select="@name"/>
		<xsl:text>");
</xsl:text>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>

<xsl:template match="my:property" mode="readprop">
			if (p_<xsl:value-of select="@name"/> != null) {
				error = p_<xsl:value-of select="@name"/>
		<xsl:text>.getValue(v);
				if (error >= 0) { </xsl:text>
				<xsl:value-of select="@name"/>
		<xsl:text>.setValue(v.as</xsl:text>
		<xsl:choose>
			<xsl:when test="@type='BOOL'">Bool</xsl:when>
			<xsl:when test="@type='REGISTER'">Int </xsl:when>
			<xsl:when test="@type='MODE'">Int</xsl:when>
			<xsl:when test="@type='COMMAND'">Int</xsl:when>
			<xsl:when test="@type='INT'">Int</xsl:when>
			<xsl:when test="@type='FLOAT'">Float</xsl:when>
			<xsl:otherwise>Int</xsl:otherwise>
		</xsl:choose>
		<xsl:text>());
					element.notifyPin(PropEnum.</xsl:text>
				<xsl:value-of select="generate-id()"/>.ordinal());
				}
			} else {
				System.out.println("NULL property <xsl:value-of select="@name"/>");
			}
</xsl:template>



<xsl:template match="my:device">

enum PropEnum {
	out_connected_index,
	out_error_index,
	// Readonly:
<xsl:apply-templates select="my:group/my:property[not(@hidden='true') and @access='RO']" mode="enum"/>
	// Refresh pin:
	in_refresh_index,
	// Writeable properties:
<xsl:apply-templates select="my:group/my:property[not(@hidden='true') and not(@access='RO')]" mode="enum"/>
	NUM_PINS
};

public class <xsl:value-of select="@name"/> extends JVSMain
{

	private Image image;

	VSString target = new VSString();
	VSBoolean disable = new VSBoolean();

	Netpp netpp = Netpp.init();
	Device dev = null;
	boolean connected = false;

	// Variable declaration:
	private VSInteger out_error = new VSInteger(0);
	private VSBoolean out_connected = new VSBoolean(false);
	private VSBoolean in_refresh = new VSBoolean(false);

<xsl:apply-templates select="my:group/my:property[not(@hidden='true')]" mode="declare"/>
	// Property tokens:
<xsl:apply-templates select="my:group/my:property[not(@hidden='true')]" mode="decltoken"/>

	<xsl:variable name="outports"
              select= "my:group/my:property[not(@hidden='true') and @access='RO']"/>
	<xsl:variable name="inports"
              select= "my:group/my:property[not(@hidden='true') and not(@access='RO')]"/>
	// Methods:
	public void init()
	{
		initPins(2, <xsl:value-of select="count($outports)"/>, 1, <xsl:value-of select="count($inports)"/>);
		initPinVisibility(true, true, true, true);
		setSize(64+0, 64+32);
		image = element.jLoadImage(element.jGetSourcePath() + "image.gif");

		setPin(PropEnum.out_connected_index.ordinal(),
			ExternalIF.C_BOOLEAN, ExternalIF.PIN_OUTPUT);
		element.jSetPinDescription(PropEnum.out_connected_index.ordinal(),
			"Connected");
		setPin(PropEnum.out_error_index.ordinal(),
			ExternalIF.C_INTEGER, ExternalIF.PIN_OUTPUT);
		element.jSetPinDescription(PropEnum.out_error_index.ordinal(),
			"Errorcode");
		setPin(PropEnum.in_refresh_index.ordinal(),
			ExternalIF.C_BOOLEAN, ExternalIF.PIN_INPUT);
		element.jSetPinDescription(PropEnum.in_refresh_index.ordinal(),
			"Refresh");
		//
		// Input pins (generated):
	<xsl:apply-templates select="my:group/my:property[not(@hidden='true')]" mode="initpin"/>

		element.jSetResizable(true);
		element.jSetResizeSynchron(false);

	}

	public void initInputPins()
	{
		in_refresh = (VSBoolean) element.getPinInputReference(PropEnum.in_refresh_index.ordinal());

<xsl:apply-templates select="my:group/my:property[not(@hidden='true') and not(@access='RO')]" mode="initinputpin"/>
	}

	public void initOutputPins()
	{
		element.setPinOutputReference(PropEnum.out_connected_index.ordinal(), out_connected);
		out_connected.setPin(PropEnum.out_connected_index.ordinal());
		element.setPinOutputReference(PropEnum.out_error_index.ordinal(), out_error);
		out_error.setPin(PropEnum.out_error_index.ordinal());
		// Generated outputs:
<xsl:apply-templates select="my:group/my:property[not(@hidden='true') and @access='RO']" mode="initoutputpin"/>
	}

	public void start()
	{
		if (disable.getValue()) return;

		System.out.println("Open device at " + target.getValue());

		dev = netpp.connect(target.getValue());
		int ret = -1;
		if (dev != null) ret = 0;

		if (ret >= 0) {
			System.out.println("Success");
			out_error.setValue(0);
			out_connected.setValue(true);

			// Init tokens:
<xsl:apply-templates select="my:group/my:property[not(@hidden='true')]" mode="inittoken"/>

			connected = true;

		} else {
			System.out.println("Failure to open device");
			out_error.setValue(-1);
			out_connected.setValue(false);
		}
		element.notifyPin(PropEnum.out_connected_index.ordinal());
		element.notifyPin(PropEnum.out_error_index.ordinal());

	}

	public void stop()
	{
		if (disable.getValue()) return;

		out_connected.setValue(false);
		if (dev != null) {
			dev.close();
			connected = false;
			System.out.println("Close device");
		}
	}

	public void process()
	{
		if (!connected) return;

		if ((in_refresh != null) &amp;&amp; in_refresh.isChanged()) {
			System.out.println("REFRESH");
		}


	}

	public void elementActionPerformed(ElementActionEvent evt)
	{
		int idx = evt.getSourcePinIndex();
		int error = 0;
<!-- xsl:apply-templates select="my:group/my:property" mode="action_reset"/-->
		// Fixme: This is inefficient:
		if (idx == PropEnum.in_refresh_index.ordinal()) {
			Value v = new Value();
<xsl:apply-templates select="my:group/my:property[not(@hidden='true') and @volatile='true']" mode="readprop"/>

		} else 
<xsl:apply-templates select="my:group/my:property[not(@hidden='true') and not(@access='RO')]" mode="writeaction"/>
		{ }
		out_error.setValue(error);
		element.notifyPin(PropEnum.out_error_index.ordinal());
	}

	public void loadFromStream(java.io.FileInputStream fis)
	{
		target.loadFromStream(fis);
		disable.loadFromStream(fis);
	}

	public void saveToStream(java.io.FileOutputStream fos)
	{
		target.saveToStream(fos);
		disable.saveToStream(fos);
	}


	// Default methods:
	public void onDispose()
	{
		if (image!=null) image.flush();
		image=null;
	}

	public void paint(java.awt.Graphics g)
	{
		if (image!=null) drawImageCentred(g,image);
	}

	public void setPropertyEditor()
	{
		element.jAddPEItem("Target", target, 0, 0);
		element.jAddPEItem("Disable", disable, 0, 0);

		localize();
	}

	private void localize()
	{
		String language;
		int d=6;
		language="en_US";
		element.jSetPEItemLocale(d+0, language,"Target");

		language="es_ES";
		element.jSetPEItemLocale(d+0, language,"Target");
	}

	public void xOnInit()
	{
	}



}
</xsl:template>

</xsl:stylesheet>

