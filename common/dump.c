#include <stdio.h>
#include "devlib.h"
#include "devlib_error.h"

int dump_children(FILE *f, DEVICE d, TOKEN t, char *fullname, char *pos,
	int left, int mode);

int convert_string(DCValue *val, char *str, int len)
{
	int n;
	const char *p = (const char *) val->value.p;
	char *dst = str;
	char b;

	n = len;
	// Big hacky, but simple:
	while (*p && n-- > 2) {
		b = *p++;
		if (b == '"') {
			*dst++ = '\\'; *dst++ = b;
			n--;
		} else {
			*dst++ = b;
		}
		if (n < 0) return DCERR_PROPERTY_SIZE_MATCH;
	}
	*dst = '\0';
	return 0;
}

void output_property(FILE *f, char *fullname, DCValue *val)
{
	static char longname[80];
	int error;

	if (val->flags & F_RO) {
		fprintf(f, "# (READONLY) ");
	}
	switch (val->type) {

		case DC_COMMAND:
			fprintf(f, "# Command %s omitted\n", fullname);
			break;
		case DC_BUFFER:
			fprintf(f, "# Buffer %s omitted\n", fullname);
			break;
		case DC_MODE:
		case DC_INT:
		case DC_BOOL:
			fprintf(f, "%s = %d\n", fullname, val->value.i);
			break;
		case DC_UINT:
			fprintf(f, "%s = %d # 0x%x\n", fullname,
				val->value.u, val->value.u);
			break;
		case DC_STRING:
			error = convert_string(val, longname, sizeof(longname));
			if (error == 0)
				fprintf(f, "%s = \"%s\"\n", fullname, longname);
			break;
		case DC_ROOT:
			break;
		default:
			error = dcValue_ToString(val, longname, sizeof(longname));
			if (error == 0) {
				fprintf(f, "%s = %s\n", fullname, longname);
			} else {
				fprintf(f, "# %s was not converted\n", fullname);
			}

			break;
	}
}

int dump_array(FILE *f, DEVICE d, TOKEN t, char *fullname, char *pos, int left)
{
	int error;
	TOKEN size;
	TOKEN item;
	int i, k;
	DCValue val;
	char *indexstr;
	char tmpname[32];

	indexstr = pos;

	error = dcProperty_GetProto(d, t, &val);
	if (error < 0) return error;

	size = t; // Query inside 't's namespace:
	error = dcProperty_ParseName(d, ".Size", &size);
	if (error < 0) return error;

	val.type = DC_UINT;
	error = dcDevice_GetProperty(d, size, &val);
	if (error < 0) return error;

	fprintf(f, "# Array '%s' size %d\n", fullname, val.value.i);
	int n = val.value.i;

	// Following property is array item:
	error = dcProperty_Select(d, t, size, &item);
	
	// Select next:
	if (error >= 0) {
		for (i = 0; i < n; i++) {
			val.value.p = tmpname; val.len = sizeof(tmpname);

			item = t; // Parse inside t namespace
			sprintf(tmpname, ".[%d]", i);
			error = dcProperty_ParseName(d, tmpname, &item);
			if (error < 0) break;
			error = dcDevice_GetProperty(d, item, &val);
			if (val.type == DC_STRUCT) {
				k = sprintf(indexstr, "[%d].", i);
				dump_children(f, d, item, fullname, indexstr + k, left, 0);
				// Ignore property handler error:
				if (error == DCERR_PROPERTY_HANDLER) error = 0;
			} else
			if (error < 0) {
				fprintf(f, "# %s: Error: %s\n", fullname,
				dcGetErrorString(error));
			} else {
				k = sprintf(indexstr, "[%d]", i);
				output_property(f, fullname, &val);
			}
		}
	}
	return error;
}


int dump_children(FILE *f, DEVICE d, TOKEN t, char *fullname, char *pos,
	int left, int mode)
{
	int error;
	static char buf[80];
	DCValue val;
	TOKEN child;
	int i;

	error = dcProperty_Select(d, t, t, &child);
	while (error == 0) {
		val.value.p = pos; val.len = left;
		error = dcProperty_GetName(d, child, &val);
		if (error < 0) {
			printf("Couldnt get name\n");
			return error;
		}
		i = strlen(pos);

		if (mode == 1) {
			fprintf(f, "%32s = 0x%08x\n", fullname, child);

			if (left >= 0) {
				pos[i] = '.'; i++;
				dump_children(f, d, child, fullname, pos + i, left - i, mode);
			}
		} else {
		
			// Use val.value.p rather than propname in next line, because
			// callee may have changed it (e.g. for static port descriptors)
			// fprintf(f, "[%08x] '%s'\n", child, propname);

			val.value.p = buf; val.len = sizeof(buf); val.type = DC_STRING;
			error = dcDevice_GetProperty(d, child, &val);
			i = strlen(pos);
			if ((error >= 0 || error == DCERR_PROPERTY_HANDLER) && left >= 0) {
				switch (val.type) {
					case DC_STRUCT:
						pos[i] = '.'; i++;
						dump_children(f, d, child, fullname,
							pos + i, left - i, mode);
						break;
					case DC_ARRAY:
						error = dump_array(f,
							d, child, fullname, pos + i, left - i);
						if (error < 0) {
							fprintf(f, "# %s: Array error: %s\n", fullname,
							dcGetErrorString(error));
						}
						break;
					default:
						if (error < 0) {
							fprintf(f, "# %s: Error: %s\n", fullname,
							dcGetErrorString(error));
						} else {
							output_property(f, fullname, &val);
						}
				}
			} else {
				// handleError(error);
				const char *msg = 0;
				switch (error) {
					case DCERR_PROPERTY_ACCESS:
						if (val.flags & F_WO) {
							msg = "# %s is writeonly (WO)\n";
						} else {
							msg = "# Access to %s not permitted\n";
						}
						break;
					case DCERR_PROPERTY_SIZE_MATCH:
						msg = "# %s is too large for inline storage\n"; break;
				}
				if (msg) fprintf(f, msg, fullname);
				else {
					fprintf(f, "# %s: Error %d: %s\n", fullname, error,
					dcGetErrorString(error));
				}
			}
		}

		error = dcProperty_Select(d, t, child, &child); // iterate to next
	}
	return error;
}


int dcDumpProperties(FILE *f, DEVICE d, TOKEN root)
{
	int error;
	DCValue val;
	char propname[128];
	val.value.p = propname; val.len = sizeof(propname);
	dcProperty_GetName(d, root, &val);

	fprintf(f, "[%s]\n", (char *) val.value.p);

	error = dcDevice_GetProperty(d, TOKEN_CHECK, &val);
	fprintf(f, "# CRC16 = %04x\n", val.value.u & 0xffff);
	error = dcDevice_GetProperty(d, TOKEN_NPROPS, &val);
	fprintf(f, "# NumProperties = %d\n", val.value.i);

	error = dump_children(f, d, root, propname, propname, sizeof(propname), 0);
	if (error > DCWARN_PROPERTY_NIL) error = 0;
	return error;
}

