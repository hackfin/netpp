/* Common IP code
 *
 * (c) 2005-2013 <hackfin@section5.ch>
 *
 * TODO: Split this code into IP-agnostic and IP/OS-specific source files.
 *
 */

#include <stdio.h> // snprintf
#include "devlib.h"
#include "devlib_error.h"
#include "property_protocol.h"
#include <errno.h>

#include <stdlib.h>

#if defined(__linux__) || defined(__APPLE__)
#include <netdb.h>
#endif

#if LWIP_COMPAT_SOCKETS
#include <lwip/netdb.h>
#endif

#define HDR_SIZE sizeof(PropertyPacketHdr)
#define MAX_NUM_PROBE_RETRIES 10

#define DEB1(x)

int str_fromip(char name[], uint32_t ip, unsigned short port)
{
	port = ntohs(port);
#if defined(MSC_2005_SAFE_STRING)
	sprintf_s(name, 32,
#elif defined(PROPRIETARY_CTYPES)
	// Unsafe sprintf:
	sprintf(name,
#else
	snprintf(name, 31,
#endif

		"%d.%d.%d.%d:%d", 
		((ip) & 0xff),
		((ip >> 8) & 0xff),
		((ip >> 16) & 0xff),
		((ip >> 24) & 0xff),
		port
// To keep editor happy:
#ifdef MSC_2005_SAFE_STRING
	);
#elif defined(PROPRIETARY_CTYPES)
	);
#else
	);
#endif

	return 0;
}

int handle_sockerr(int error, const char *txt)
{
#if defined (__WIN32__)
	netpp_log(DCLOG_ERROR, "%s: ", txt);
	error = WSAGetLastError();
	switch (error) {
		case WSAECONNREFUSED:
			netpp_log(DCLOG_ERROR, "Connection refused");
			error = DCERR_COMM_INIT;
			break;
		case WSAENOTSOCK:
			netpp_log(DCLOG_ERROR, "not a socket");
			error = DCERR_COMM_INIT;
			break;
		case WSAEADDRINUSE:
			netpp_log(DCLOG_ERROR, "Address in use");
			error = DCERR_COMM_BUSY;
			break;
		default:
			netpp_log(DCLOG_ERROR, "Winsock error #%d", error);
			error = DCERR_COMM_GENERIC;
	}
#elif defined (__linux__) || defined(__APPLE__)
	switch (errno) {
		case ECONNRESET:
#ifndef __APPLE__
		case ECOMM:
#endif
		case EPIPE:
			error = DCERR_COMM_DISCONNECT;
			break;
		default:
			error = DCERR_COMM_GENERIC;
	}
	netpp_log(DCLOG_ERROR,
			"Socket error (%s): %s", txt, strerror(errno));
#endif
	return error;
}

int net_send_slice(Peer *peer, PropertyPacketHdr *h, const char *data,
		unsigned short len)
{
	int error;
	char *p = peer->sendbuf;

	// This is inefficient, because we copy buffers around.
	// TODO: Scatter-gather mechanisms
	memcpy(p, h, HDR_SIZE);
	p += HDR_SIZE;
	if (len)
		memcpy(p, data, len);

//	printf(">");
//	packet_dump(peer->sendbuf, len + 14);

	DEB1(netpp_log(DCLOG_VERBOSE, "Send slice %d bytes", len));
	error = send(peer->fd, (char *) peer->sendbuf, len + HDR_SIZE, 0);
	if (error < 0) {
		return handle_sockerr(error, "socket write()");
	}
	return 0;
}

#if 0
int net_send_packet(Peer *peer, PropertyPacketHdr *h,
		const char *data, size_t len)
{
	int error;
	unsigned int n;
	int i = 0;
	unsigned short type = ntohs(h->type);
	netpp_log(DCLOG_DEBUG, "Send packet dest: %08lx, dport: %d len: %ld",
		peer->ip.ipaddr, peer->ip.dport, len);

	// Now send packet fragments

	h->id = htons(i);
	n = peer->packetsize;

	while (len > n) {
		h->type = htons(type | PP_FLAG_FRAG);
		DEB(netpp_log(DCLOG_DEBUG, "Send slice (%d left)", len));
		error = net_send_slice(peer, h, data, n);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Failed to send slice");
			return error;
		}
		len -= n;
		data += n;

		// Update length field
		h->sub.set.len = htonl((unsigned long) len);

		i++;
		h->id = htons(i);
	}

	h->type = htons(type);

	error = net_send_slice(peer, h, data, (unsigned short) len);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Failed to send slice");
	}

	return error;
}
#endif


static
int resolve_name(const char *hostname, in_addr_t *ip)
{
	struct hostent *h;
#if !LWIP_COMPAT_SOCKETS
	struct in_addr addr;
#endif

#ifdef __WIN32__
	int error;
	WSADATA data;
	error = WSAStartup(MAKEWORD(2, 2), &data);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Could not initialize Winsock'");
		return DCERR_COMM_INIT;
	}
#endif

	h = gethostbyname(hostname);
	if (!h) {
#if LWIP_COMPAT_SOCKETS
		netpp_log(DCLOG_ERROR, "Cannot resolve host name '%s'", hostname);
		return DCERR_COMM_INIT;
#else
		addr.s_addr = inet_addr(hostname);
		h = gethostbyaddr((char *) &addr, 4, AF_INET);
		if (!h) {
			netpp_log(DCLOG_ERROR, "Cannot resolve host name '%s'", hostname);
			return DCERR_COMM_INIT;
		}
#endif
	}
	if (h->h_addrtype != AF_INET || h->h_length != 4) {
		netpp_log(DCLOG_ERROR, "Other than IPv4 addresses not supported");
		return DCERR_COMM_INIT;
	}

	memcpy(ip, h->h_addr_list[0], h->h_length);

#ifdef _MSC_VER 
	WSACleanup();
#endif

	return 0;
}

int net_openbyname(RemoteDevice *d, const char *name,
	int (*con_init)(Peer *p, int bufsize))
{
	int error;
	int retries = 0;
	char hostname[64], port_str[16];
	in_addr_t ipaddr;
	int port = DEFAULT_LISTEN_PORT;

	error = scan_into(name, ':', hostname, 64);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Invalid hostname");
		return DCERR_COMM_INIT;
	}

	if (error) {
		error = scan_into(&name[error], ':', port_str, 16);
		port = atoi(port_str);
		if (error < 0 || port == 0) {
			netpp_log(DCLOG_ERROR, "Invalid Port specification");
			return DCERR_COMM_INIT;
		}
	}

	error = resolve_name(hostname, &ipaddr);
	if (error < 0) return error;

	d->peer.ip.dport = port;
	d->peer.ip.ipaddr = ipaddr;

	do {
		d->peer.ip.sport = 1024 + (rand() & 0x3fff);
		error = con_init(&d->peer, 0x80000);
		retries++;
	} while (error == DCERR_COMM_BUSY && retries < 200);

	return error;
}


////////////////////////////////////////////////////////////////////////////
//

int net_peer_exit(Peer *p, int kill)
{
	int error = 0;

	if (kill) {
#ifdef __WIN32__
		printf("KILL WIN SOCK\n");
		shutdown(p->fd, SD_RECEIVE);
		closesocket(p->fd);
		WSACleanup();
#else
		shutdown(p->fd, SHUT_RDWR);
		close(p->fd);
#endif
	} else {
#ifdef __WIN32__
		printf("SHUTDOWN WIN SOCK\n");
		closesocket(p->fd);
		// WSACleanup();

		SockAddr saddr;
		SOCKET s = p->fd;
		saddr.sa_family = AF_UNSPEC;

		printf("DISCONNECT WIN SOCK\n");
		error = connect(s, &saddr, sizeof(saddr));
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Couldn't disconnect UDP socket");
		}

#else
		SockAddr saddr;
		SOCKET s = p->fd;
		saddr.sa_family = AF_UNSPEC;

		error = connect(s, &saddr, sizeof(saddr));
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Couldn't disconnect UDP socket");
		}

		close(p->fd);
#endif
	}
	if (p->sendbuf) free(p->sendbuf);
	return error;
}

#ifdef __WIN32__
#define SOCK_VALID(x) (x != (SOCKET) -1)
#else
#define SOCK_VALID(x) (x > 0)
#endif

int remotedevice_add(ServerContext *serv, SOCKET sock, SockAddrIn *sin,
	int nodetype)
{
	PortToken p;
	int error;
	RemoteDevice *rdev;

	char name[32];
	uint32_t ip = sin->sin_addr.s_addr;

	str_fromip(name, ip, sin->sin_port);
	if (SOCK_VALID(sock)) {
		DEB(netpp_log(DCLOG_DEBUG,
			"Add remote client %s, socket %d", name, sock));
	}

	p = PortNode(serv->devices, name, nodetype);
	if (!IS_VALIDTOKEN(p)) {
		netpp_log(DCLOG_ERROR,
			"Failed to alloc Device node");
		return DCERR_MALLOC;
	}

	// We only allocate device descriptors when a device peer connection
	// is requested. For probed (DC_PORT) entries, we don't do that, because
	// they're a) not needed, b) not removed (freed).
	if (nodetype == DC_DEVICE) {
		error = rdev_new(p, &rdev);
		if (error < 0) {
			portdesc_free(p);
			netpp_log(DCLOG_DEBUG,
				"Failed to alloc remote device");
			return error;

		}

		// If no cleanup function was defined, use server default's:
		if (!rdev->cleanup) rdev->cleanup = serv->cleanup;

		// If we are a PROXY server, flag users accordingly
		if (serv->flags & F_SERV_PROXY) rdev->flags |= F_PROXY;

		error = serv->peer_init(&rdev->peer);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "peer init failed");
			rdev_dealloc(rdev); portdesc_free(p);
			return DCERR_MALLOC;
		}

		rdev->peer.hub = desc_fromtoken(serv->hub);
		rdev->peer.fd = sock;
#ifdef __WIN32__
		rdev->peer.ip.ipaddr = sin->sin_addr.S_un.S_addr;
#else
		rdev->peer.ip.ipaddr = sin->sin_addr.s_addr;
#endif
		rdev->peer.ip.dport = sin->sin_port;

		serv->usercount++;
	}

	portbay_append(serv->devices, p);
	return 0;
}

/* Remove remove device. ONLY call for DC_DEVICE Nodes! */
void remotedevice_remove(ServerContext *serv, PortToken d)
{
	DynPropertyDesc *pd;
	int error;
	pd = desc_fromtoken(d);
	if (!pd) {
		netpp_log(DCLOG_ERROR, "NULL token %08x", d);
		return;
	}
#ifdef PARANOIA_MODE
	assert(pd->type == DC_DEVICE);
#endif
	DEB(netpp_log(DCLOG_DEBUG, "Disconnect %s", pd->name));
	if (pd->device->cleanup) {
		error = pd->device->cleanup(pd->device);
		if (error) netpp_log(DCLOG_ERROR, "Failed to cleanup (error %d)",
		error);
	}

	if (serv->flags & F_SERV_TCP) 
		net_peer_exit(&pd->device->peer, 1);
	else
		net_peer_exit(&pd->device->peer, 0);

	rdev_dealloc(pd->device);
	portdesc_free(d);
	serv->usercount--;
}

int net_disconnect_users(ServerContext *serv)
{
	PortToken walk, tmp;
	DynPropertyDesc *pd;

	walk = ITERATOR_FIRST(serv->devices);
	while (ITERATOR_VALID(walk)) {
		pd = desc_fromtoken(walk);
		tmp = ITERATOR_NEXT(serv->devices, pd);
		remotedevice_remove(serv, walk);
		walk = tmp;
	}
	return 0;
}

#if !defined(CONFIG_NOPROBE) && defined(CONFIG_UDP)

#if (defined(__linux__) || defined(__APPLE__)) && !defined(__ADSPBLACKFIN__)

// This isn't very portable...
// but hopefully /proc/net/route will not change much

#include <net/route.h>

static
int route_init(FILE **file)
{
	int ret;
	FILE *f;
	f = fopen("/proc/net/route", "r");
	// Skip first line:
	ret = fscanf(f, "%*[^\n]\n");
	*file = f;
	return ret;
}

static
int get_route(FILE *f, int ptr, uint32_t *bcaddr)
{
	char dev[32];
	unsigned long d, g, m;
	int fl, ref, use, mtr, mtu, win, ir;
	int ret;

	do {
		ret = fscanf(f, "%31s%lx%lx%X%d%d%d%lx%d%d%d\n",
			dev, &d, &g, &fl, &ref, &use, &mtr, &m, &mtu, &win, &ir
		);
		if (ret < 11) return -1;
		// Keep reading, when it's a GW or not up:
	} while ((fl & RTF_GATEWAY) || !(fl & RTF_UP));
	
	// Calc broadcast address from network mask:
	*bcaddr = d | ~m;
	// netpp_log(DCLOG_VERBOSE, "broadcast addr %08x", *bcaddr);

	ptr++;
	return ptr;
}

static
void route_exit(FILE *f)
{
	fclose(f);
}

#endif

static int code_from_id(const char *name)
{
	if (strcmp(name, "UDP") == 0) {
		return PROBE_QUERY_UDP;
	} else
	if (strcmp(name, "TCP") == 0) {
		return PROBE_QUERY_TCP;
	} else
	if (strcmp(name, "PRX") == 0) {
		return PROBE_QUERY_PRX;
	}
	return 0;
}


/** Probe network for listening netpp servers. Client side. */
int net_probe(DynPropertyDesc *hub)
{
	int error;
	SOCKET out;
	SockAddrIn sout;
	ServerContext serv;
	int i;
	int code = 0;
	PropertyPacketHdr *h;
#ifdef TINYCPU
#error "Unsupported NULL peer"
#else
	REQUEST_PACKET_HDR(NULL, h, 0);
#endif

	int option = 1;

	char desc[16];

	snprintf(desc, sizeof(desc), "PR_%s", hub->name);

	code = code_from_id(hub->name);

	// Use broadcast address:
	sout.sin_family      = AF_INET;
	// TODO: Add Hub properties, such that one can choose specific
	// broadcast addresses
	sout.sin_port        = htons(DISCOVERY_PORT);
			
	// We reject packets not having the same server code,
	// as we may have a network race condition.
	// We simply do this by using different port numbers. If the
	// temporary server was shut down, the packet simply bounces.

	error = udp_server_init(22220 + code, &serv, desc);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Failed to alloc discovery listener");
		return error;
	}
	serv.flags |= F_SERV_DISC; // We're a discovery server

#if defined(__WIN32__) || LWIP_COMPAT_SOCKETS
	out = serv.listener;
#else
	out = dup(serv.listener);
#endif

	// Allow broadcasting on socket:
	setsockopt(out, SOL_SOCKET, SO_BROADCAST, (char *) &option, sizeof(option));

	h->type = htons(PP_REQUEST);
	h->sub.status.token = htonl(REQUEST_PROBE);

	// Send out query codes for the Hubs we support:

	h->sub.status.code = htons(code);

	PortBay *save = serv.devices; // Save current peer devices portbay

	// Answering devices are stored in the .devices PortBay.
	// But since we want them in the hub.ports member of the
	// hub structure, we redirect the devices list pointer:
	serv.devices = portbay_fromtoken(hub->hub.ports);


#if defined(__WIN32__) || LWIP_COMPAT_SOCKETS || defined(__ADSPBLACKFIN__)

	// Cheap broadcasting, do not check different routes
	// FIXME: Windows may show redundant entries with the cheap probe.
	sout.sin_addr.s_addr = htonl(INADDR_BROADCAST);
	error = sendto(out, (char *) h, HDR_SIZE, 0,
		(SockAddr *) &sout, sizeof(sout));
#else
	uint32_t bcast = 0xffffffff;
	int walk = 0;
	int ret;
	FILE *route;
	ret = route_init(&route);
	if (ret < 0) return ret;
	do {
		walk = get_route(route, walk, &bcast);
		sout.sin_addr.s_addr = bcast;

		if (walk >= 0) {
			error = sendto(out, (char *) h, HDR_SIZE, 0,
				(SockAddr *) &sout, sizeof(sout));
		}

		if (error < 0) {
			error = handle_sockerr(error, "socket sendto()");
		}
	} while (walk >= 0);

	route_exit(route);
#endif
	DEB(netpp_log(DCLOG_NOTICE, "Send PROBE packet (Hub %s), code %d",
		hub->name, code));

	i = 0;
	while (error >= 0 && i < MAX_NUM_PROBE_RETRIES) {
		error = udp_listen_incoming(&serv, 20);
		if (error != DCWARN_SLAVE_HANDLED) i++;
	}
	// And we now can exit our temporary server.
	serv.devices = save; // Restore peer devices
	close(out);
	// Note: All peer info gets destroyed on server exit
	udp_server_exit(&serv);

	return error;
}

#endif

int net_serve_users(ServerContext *serv)
{
	int error;
	int n;
	PortToken walk, tmp;
	RemoteDevice *d;
	DynPropertyDesc *pd;

	walk = ITERATOR_FIRST(serv->devices);
	n = 0;
	while (ITERATOR_VALID(walk)) {
		pd = desc_fromtoken(walk);
#ifdef DEBUG
		// netpp_log(DCLOG_DEBUG, "Serve peer device '%s'", pd->name);
#endif
		d = pd->device;
		error = serv->slave_handler(d);
		if (error < 0) {
			switch (error) {
				case DCERR_COMM_DISCONNECT:
					DEB(netpp_log(DCLOG_VERBOSE, "Peer has disconnected"));
					break;
				default:
					netpp_log(DCLOG_ERROR, "Retcode %d in Slave handler: '%s'",
							error, dcGetErrorString(error));
			}
			tmp = ITERATOR_NEXT(serv->devices, pd);
			remotedevice_remove(serv, walk);
			walk = tmp;
		} else {
			walk = ITERATOR_NEXT(serv->devices, pd);
			n++;
		}
	}
	return n;
}


int net_create(PortToken hub, const char *name, PortToken *out)
{
	PortToken p;

	name = skip_delimiter(name, ':');
	if (!name) return DCERR_COMM_INIT;

	// Remain a port until we actually create a device
	p = Port(hub, name, DC_PORT);
	if (!IS_VALIDTOKEN(p)) {
		netpp_log(DCLOG_ERROR, "Out of ports in pool!");
		return DCERR_MALLOC;
	}
	hub_append(hub, p);
	*out = p;
	return 0;
}

int add_server_entry(ServerContext *serv, int port)
{
	char name[32];
	PortToken p;
#ifdef MSC_2005_SAFE_STRING
	sprintf_s(name, 32, ":%d", port);
#else
	snprintf(name, 31, ":%d", port);
#endif
	p = Port(serv->hub, name, DC_PORT);
	if (!IS_VALIDTOKEN(p)) return DCERR_MALLOC;
	hub_append(serv->hub, p);
	portbay_append(GET_PORTROOT(), serv->hub);
	return 0;
}

