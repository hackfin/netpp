/** Proxy buffer */
#include "property_protocol.h"

typedef struct {
	TOKEN    curbuf;    ///< Hack for buffer proxying
	int      len;       ///< Maximum length of buffer
	DCValue val;        ///< DCValue cache for access
} BufDesc;
