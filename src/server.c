/** \file server.c
 *
 * Slave property default dispatcher
 * Property protocol back end (embedded system, etc.)
 *
 * (c) 2006-2011 Martin Strubel <strubel@section5.ch>
 *
 * This is a simple netpp main loop implementation where all client requests
 * are processed sequentially.
 * This is ok for most systems, however, if you need fast and asynchronous
 * simultaneous master/slave transfers, you'd be better off with threads,
 * if your OS has them.
 * In this case, it is recommended to implement all threading in your own
 * main routine and use this file only as an example on how to implement
 * the netpp main loop.
 *
 * TODO on Posix OS: Catch SIGPIPE signals (we leave this up to the user)
 *
 */

#ifdef DEBUG
#define DEB(x) x
#endif

#if defined(__linux__) || defined(__APPLE__)
#include <signal.h>
#endif

#include "slave.h"
#include "devlib_error.h"

#if !defined(STANDALONE) && !defined(TINYCPU)
#	define HAVE_GETOPT
#endif

#include <stdlib.h>
#ifdef HAVE_GETOPT
#include <getopt.h>
#endif

#include <stdio.h>

// Macro to allow 'nice' top level encoding of default device
#define DYNAMIC_DEVICE_INDEX(x) \
	DYNAMIC_PROPERTY | DEVICE_TOKEN((-(x) + 1))

#ifndef DEFAULT_DEVICE_INDEX
#define DEFAULT_DEVICE_INDEX 0
#endif

/** Configuration options */
static struct server_config {
	int port;           // UDP and TCP listening port
	int discoveryport;  // Discovery server port (only change if
	                    // necessary.
	int devindex;
	char *devname;      // Device name
	int hide;           // Do not start discovery server, if set
} g_config = {
	.port          = DEFAULT_LISTEN_PORT,
	.discoveryport = DISCOVERY_PORT,
	.devindex      = DEFAULT_DEVICE_INDEX,
	.devname       = 0,
	.hide          = 0,
};

#ifdef HAVE_GETOPT

enum {
	OPT_DEVICE,
	OPT_HELP,
	OPT_PORT,
	OPT_DEVINDEX,
	OPT_DISCOVERY,
};

static struct option long_options[] = {
	{ "dev",           1,              0, OPT_DEVICE },
	{ "port",          1,              0, OPT_PORT },
	{ "help",          0,              0, OPT_HELP },
	{ "hide",          0, &g_config.hide, 1 },
	{ "deviceindex",   1,              0, OPT_DEVINDEX  },
	{ "discoveryport", 1,              0, OPT_DISCOVERY },
	{ NULL } // Terminator
};

#endif

__attribute__((weak))
void user_visit(void) 
{
}

/** Function retrieving the root node.
 *
 * Only provided for non-windows environments (due to back-references)
 */

#ifndef __MINGW32__
TOKEN local_getroot(DEVICE d)
{
	TOKEN root;
	// We return the dynamic class node (see s_rootprop below)
#ifdef ALLOW_DYNAMIC_PROPERTIES
	if (g_config.devindex < 0) 
		root = DYNAMIC_DEVICE_INDEX(g_config.devindex);
	else
		root = DEVICE_TOKEN(g_config.devindex);
#else
	root = DEVICE_TOKEN(g_config.devindex);
#endif

	return root;
}
#endif



int start_server(int argc, char **argv)
{
#ifdef CONFIG_UDP
	ServerContext udpserv;
#endif
#ifdef CONFIG_USBFIFO
	RemoteDevice usb;
#endif
#ifdef CONFIG_DEVICE
	RemoteDevice dev;
#endif
#ifdef CONFIG_TCP
	ServerContext tcpserv;
#endif
	int error;
#ifdef CONFIG_UDP
	ServerContext discoveryserv;
#endif
#ifdef PROXY
	ServerContext proxyserv;
#endif

// Ignore sigpipe by default:
#if defined(__linux__) || defined(__APPLE__)
	signal(SIGPIPE, SIG_IGN);
#endif


#ifdef HAVE_GETOPT

	while (1) {
		int c;
		int option_index;
		c = getopt_long(argc, argv, "+", long_options, &option_index);

		if (c == EOF) break;
		switch (c) {
			case OPT_HELP:
				fprintf(stderr, "\nOptions:\n"
				"--help                    This text\n"
				"--dev=<device>            Local device to listen on (default: none)\n"
				"--port=<number>           Port number to listen on (default: %d)\n"
				"--deviceindex=[-1, 0, .., %d]   Device class index (default: %d)\n"
				"--discoveryport=<number>  Port of discovery server (default: %d)\n"
				"--hide                    Hide from discovery\n\n",
				g_config.port, g_ndevices - 1,
				g_config.devindex, g_config.discoveryport);
				return 0;
			case OPT_DEVICE:
				g_config.devname = optarg;
				break;
			case OPT_PORT:
				g_config.port = atoi(optarg);
				break;
			case OPT_DEVINDEX:
				g_config.devindex = atoi(optarg);
				if (g_config.devindex >= g_ndevices) {
					fprintf(stderr, "Bad device index %d\n",
						g_config.devindex);
					return DCERR_PROPERTY_RANGE;
				}
				break;
			case OPT_DISCOVERY:
				g_config.discoveryport = atoi(optarg);
				break;
		}
	}

#endif
	DEB(netpp_log(DCLOG_DEBUG, "Init ports..."));
	ports_init(NULL);

#ifdef CONFIG_DEVICE
	if (g_config.devname) {
		error = dev_server_init(&dev.peer, g_config.devname);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Error initializing DEV server");
			return DCERR_COMM_INIT;
		}
		netpp_log(DCLOG_VERBOSE, "Using device %s", g_config.devname);
	}
#endif

#ifdef CONFIG_USBFIFO
	error = usb_server_init(&usb.peer);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Error initializing USB");
		return DCERR_COMM_INIT;
	}
	netpp_log(DCLOG_VERBOSE, "Listening to USBFIFO");
#endif

#ifdef CONFIG_UDP
	if (!g_config.hide) {
		error = udp_server_init(g_config.discoveryport, &discoveryserv,
			UDP_PROBE_NAME);

		if (error < 0) {
			netpp_log(DCLOG_ERROR,
		"Error initializing discovery protocol UDP server. Still continuing.");
			g_config.hide = 1;
		} else {
			netpp_log(DCLOG_VERBOSE, "ProbeServer listening on UDP Port %d...",
				g_config.discoveryport);
		}
	}
		
#endif

#ifdef PROXY
	error = prx_server_init(2014, &proxyserv, "PRXserv");
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Error initializing proxy server on port 2014");
		return DCERR_COMM_INIT;
	}
	netpp_log(DCLOG_VERBOSE, "Proxy running on Port 2014");

#endif

#ifdef CONFIG_UDP
	error = udp_server_init(g_config.port, &udpserv, UDP_SERVER_NAME);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Error initializing socket");
		return DCERR_COMM_INIT;
	}
	netpp_log(DCLOG_VERBOSE, "Listening on UDP Port %d...", g_config.port);
#endif

#ifdef CONFIG_TCP
	error = tcp_server_init(g_config.port, &tcpserv);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Error initializing socket");
		return DCERR_COMM_INIT;
	}
	netpp_log(DCLOG_VERBOSE, "Listening on TCP Port %d...", g_config.port);
#endif

#ifdef DEBUG
	netpp_log(DCLOG_VERBOSE, "Servers running:");
	portbay_list(GET_PORTROOT());
	netpp_log(DCLOG_VERBOSE, "------------------------");
#endif

	while (1) {
		// Serve USB:
#ifdef CONFIG_USBFIFO
		slave_handler(&usb);
#endif
#ifdef CONFIG_DEVICE
		if (g_config.devname)
			slave_handler(&dev);
#endif

#ifdef CONFIG_UDP
		if (!g_config.hide) {
			error = udp_listen_incoming(&discoveryserv, 1);
			if (error < 0) {
				netpp_log(DCLOG_ERROR, "UDP discovery server error");
				error = DCERR_COMM_DISCONNECT;
				break;
			}
		}
		error = udp_serve(&udpserv);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "UDP server error");
			error = DCERR_COMM_DISCONNECT;
			break;
		}
#endif
		// First, we serve existing (logged in) TCP users
#ifdef CONFIG_TCP
		error = net_serve_users(&tcpserv);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "TCP server error");
			error = DCERR_COMM_DISCONNECT;
			break;
		}
		// Here, we listen to new incoming connections
		error = tcp_listen_incoming(&tcpserv);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "TCP listen error");
			// We don't quit:
			// break;
		}
#endif

#ifdef PROXY
		error = net_serve_users(&proxyserv);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Proxy server error");
			error = DCERR_COMM_DISCONNECT;
			break;
		}
		// Here, we listen to new incoming connections
		error = tcp_listen_incoming(&proxyserv);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Proxy listen error");
			break;
		}
#endif
		user_visit();
	}

	// We clean up:
#ifdef CONFIG_TCP
	tcp_server_exit(&tcpserv);
#endif

#ifdef CONFIG_DEVICE
	if (g_config.devname) {
		error = dev_server_exit(&dev.peer);
		}
#endif

#ifdef CONFIG_UDP
	udp_server_exit(&discoveryserv);
	udp_server_exit(&udpserv);
#endif
	ports_exit(1);

	return error;
}

