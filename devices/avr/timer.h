void systimer_init(void);
void set_blink_frequency(unsigned short f);
void timer0_expire(uint32_t t);
char timer0_expired(void);
unsigned long timer0_get(void);

