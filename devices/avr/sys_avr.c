/* atmega32u4 I/O routines
 *
 * This implements the system driver for the rawio netpp device wrapper
 * to allow the avr to speak netpp.
 * my_open(), my_read(), my_write(), my_poll(), my_close()
 *
 * */

#define CPU_SYSCLK  8000000UL

#include <stdlib.h>
#include "property_protocol.h"
#include "property_types.h"
#include <avr/interrupt.h>
#include "rawio.h"
#include "adc.h"
#include "uart.h"
#include "timer.h"

struct fifo g_rxfifo = {
	0, 0,
};

#ifndef MAX_NUM_POOL_ELEMENTS
#define MAX_NUM_POOL_ELEMENTS 16
#endif


// FIXME: Implement proper allocation scheme for dynamic pool alloc/dealloc


struct MemoryPool {
	// uint16_t   free;
	uint8_t   index;
	DLIST_ITEM pool[MAX_NUM_POOL_ELEMENTS];
} g_mempool = {
	// .free = 0,
	.index = 0
};


static
RemoteDevice s_devicepool[MAX_NUM_DEVICES];

static char g_buffer[MAX_PACKET_SIZE];

char *buf_alloc(int n)
{
	if (n > sizeof(g_buffer)) return 0;
	return g_buffer;
}

void buf_free(char *buf)
{

}


////////////////////////////////////////////////////////////////////////////

void delay_ms(int n)
{
	timer0_expire(n);
	while (!timer0_expired());
}

static char s_devmap[MAX_NUM_DEVICES];

void devicemap_init(void)
{
	int i;
	for (i = 0; i < sizeof(s_devmap); i++) {
		s_devmap[i] = (char) -1;
	}
}

RemoteDevice *rdev_alloc(int n)
{
	int i;
	char stat;
	RemoteDevice *dev = NULL;
	if (n > 1) return NULL;
	for (i = 0; i < MAX_NUM_DEVICES; i++) {
		stat = s_devmap[i];
		if (stat == (char) -1) {
			dev = &s_devicepool[i];
			s_devmap[i] = i; // mark used
			break;
		}
	}
	return dev;
}

void rdev_dealloc(RemoteDevice *rdev)
{
	int index;
	index = rdev - &s_devicepool[0];
	s_devmap[index] = -1;
}

DLIST_ITEM *standalone_alloc_pool(int n)
{
	DLIST_ITEM *new;
	uint8_t i = g_mempool.index + n;
	if (i >= MAX_NUM_POOL_ELEMENTS) {
		return (DLIST_ITEM *) NULL;
	}
	new = &g_mempool.pool[g_mempool.index];
	netpp_log(DCLOG_VERBOSE, "Alloc pool: n=%d", n);
	g_mempool.index = i;
	return new;
}

void standalone_dealloc_pool(DlistPool *pool)
{

}

void display_error(int code, int wait)
{
	unsigned long ta, tb;
	set_blink_frequency(wait);
	ta = timer0_get();
	tb = ta + 1000;
	while (timer0_get() < tb);
	set_blink_frequency(500);
}

void system_init(void)
{
	DDRC |= (1 << DDC7);

	// PORTC |= (1 << PORTC7);
	PORTC = 0;
	systimer_init();
	adc_init(1);
	uart1_init((CPU_SYSCLK / 8 / BAUDRATE - 1) >> 1);
	sei();

	set_blink_frequency(25);
	delay_ms(500);
	set_blink_frequency(500);
}

char *string_copy(char *dest, const char *src, unsigned short n)
{
	char *end = &dest[n-1];
	char *walk = dest;
	char c = 0;

	while ((c = *src++) !='\000' ) {
		if (walk == end) { c = '\000'; break; }
		*walk++ = c;
	}
	*walk = c;
	return dest;
}


unsigned short fifo_read(struct fifo *f, char *data, int n)
{
	unsigned short i = 0;
	unsigned char tail = f->tail;
	while (tail != f->head && n--) {
		tail++; tail &= FIFO_BUFFER_MASK;
		*data++ = f->buf[tail];
		i++;
	}
	f->tail = tail;
	return i;
}

char fifo_fill(struct fifo *f)
{
	if (f->err) return f->err;
	return (f->head - f->tail) & FIFO_BUFFER_MASK;
}

int my_read(int fd, char *buf, int len)
{
	unsigned short l;
	timer0_expire(30);
	int n = 0;
	while (len > 0) {
		l = fifo_read(&g_rxfifo, buf, len);
		if (l == 0) {
			if (timer0_expired()) return -1;
		} else {
			timer0_expire(50);
			len -= l; n += l; buf += l;
		}
	}
	return n;
}

int my_write(int fd, const char *buf, int len)
{
	while (len--) {
		uart1_putc(*buf++);
	}
	return 0;
}

int my_poll(int fd, int expect, unsigned short timeout)
{
	int ret;
	timer0_expire(timeout);
	do {
		ret = fifo_fill(&g_rxfifo);
		if (ret > 0) {
			set_blink_frequency(100);
			return 1;
		} else 
		if (ret < 0) {
			g_rxfifo.err = 0;
			return ret;
		}
	} while (!timer0_expired());
	set_blink_frequency(500);

	return 0;
}

int my_open(const char *name)
{
	return 1;
}

int my_close(int fd)
{
	return 0;
}

////////////////////////////////////////////////////////////////////////////

void store_unaligned_float(float f, void *data)
{
	#warning "Unaligned store float not implemented"
}

