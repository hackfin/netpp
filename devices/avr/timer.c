/* Timer functionality */

#include <avr/interrupt.h>
#include <avr/io.h>
#include "timer.h"

static volatile unsigned long g_timer = 0;
static unsigned long g_expire;
static char g_expired = 0;
static unsigned short g_blink = 500;

ISR(TIMER0_COMPA_vect)
{
	g_timer++;
	if (!(g_timer % g_blink)) PORTC ^= (1 << PORTC7);
	if (g_timer == g_expire) {
		g_expired = 1;
	}
}

void timer0_expire(uint32_t t)
{
	if (t > 0) {
		cli();
		g_expire = g_timer + t;
		g_expired = 0;
		sei();
	} else {
		g_expired = 1;
	}
}

char timer0_expired(void)
{
	return g_expired;
}

unsigned long timer0_get(void)
{
	return g_timer;
}

void set_blink_frequency(unsigned short f)
{
	g_blink = f;
}

void systimer_init(void)
{

	TCCR0A = 0;
	// Configure timer for 1 ms, reset on compare match A
	TCCR0B = _BV(CS01) | _BV(CS00) | (2 << WGM02); // DIV64
	OCR0A = 125;

	TIMSK0 = _BV(OCIE0A); // Enable IRQ on compare match A

}
