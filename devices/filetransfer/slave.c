/** \file slave.c
 *
 * Slave property dispatcher
 * Property protocol back end (embedded system, etc.)
 *
 * (c) 2006 Martin Strubel <hackfin@section5.ch>
 *
 */

#ifdef DEBUG
#define DEB(x) x
#endif

#include "slave_vars.h"
#include <stdio.h>

#include "devlib.h"

// Generic slave server
#include "slave.h"

/** Device flat address register map write access.
 * For low level device access (SPI, I2C, etc.) this normally wants to
 * be implemented */

int device_write(RemoteDevice *d,
		uint32_t addr, const unsigned char *buf,
		unsigned long size)
{
	return 0;
}

/** Device flat address register map read access.
 * For low level device access (SPI, I2C, etc.) this normally wants to
 * be implemented */

int device_read(RemoteDevice *d,
		uint32_t addr, unsigned char *buf, unsigned long size)
{
	return 0;
}

////////////////////////////////////////////////////////////////////////////
//

int main(int argc, char **argv)
{
	int error;

	// Important: Register property list
	register_proplist(g_devices, g_ndevices);

	error = start_server(argc, argv);
	if (error)
		printf("Server exited with error: '%s'\n", dcGetErrorString(error));
	return error;
}

