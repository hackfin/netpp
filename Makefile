# Makefile for netpp Linux/Cygwin/Mingw
#
# (c) 2006-2012 <hackfin@section5.ch>
#
#

ifndef PLATFORM
PLATFORM = $(shell uname)
endif

# SVNVERSION = $(shell svnversion)
PYVERSION = $(shell pyversions -d)

.PHONY: all install clean doc

.EXPORT_ALL_VARIABLES:

ifdef CUSTOM
VER = $(CUSTOM)
else
VER = 0.50
endif

ifneq (,$(SVNVERSION))
	VERSION = $(VER)-svn$(SVNVERSION)
else
	VERSION = $(VER)
endif

CURDIR = $(shell pwd)

# Installation target dir for install. Must be an ABSOLUTE path!
DIST = $(CURDIR)/install

ifndef DESTDIR
	ifndef INSTALLDIR
		INSTALLDIR = $(DIST)/netpp-$(VERSION)
	endif
	HEADERDIR= $(INSTALLDIR)/include
	XMLDIR = $(INSTALLDIR)/xml
	DEVICESDIR= $(INSTALLDIR)/devices
	PYTHONDIR= $(INSTALLDIR)/python
	DOCDIR= $(INSTALLDIR)/doc
else
	INSTALLDIR = $(DESTDIR)
	HEADERDIR= $(INSTALLDIR)/share/netpp/include/
	XMLDIR = $(INSTALLDIR)/share/netpp/xml
	DEVICESDIR= $(INSTALLDIR)/share/netpp/devices
	PYTHONDIR= $(INSTALLDIR)/lib/$(PYVERSION)/dist-packages
	DOCDIR= $(INSTALLDIR)/share/doc/netpp
endif

DIR_PREFIX = $(notdir $(INSTALLDIR))

ifeq ($(PLATFORM),CYGWIN_NT-5.1)
	DISTDEPS = headers msvc_dist
else
	DISTDEPS = headers linux_dist
endif

DISTDEVICES = master streamtest libslave filetransfer

all:
	make -C devices all
	make -C master all
	make -C python install
	make -C wrapper install

all_release:
	make -C devices all RELEASE=1
	make -C master all RELEASE=1
	make -C python RELEASE=1 install
	make -C wrapper install

test:
	py.test

clean:
	make -C devices clean
	make -C master clean
	make -C python clean
	make -C wrapper clean

doc:
	doxygen

$(INSTALLDIR):
	[ -e $(DIST) ] || mkdir $(DIST)
	install -d $(INSTALLDIR)

include/devlib_error.h:
	$(MAKE) -f xml/prophandler.mk $(NETPP)/$@

headers: include/devlib_error.h
	install -d $(HEADERDIR)
	install -m644 include/netpp.h $(HEADERDIR)
	install -m644 include/devlib.h $(HEADERDIR)
	install -m644 include/devlib_types.h $(HEADERDIR)
	install -m644 include/devlib_error.h $(HEADERDIR)
	install -m644 include/property_types.h $(HEADERDIR)
	install -m644 include/property_protocol.h $(HEADERDIR)
	install -m644 include/property_api.h $(HEADERDIR)
	install -m644 include/dynprops.h $(HEADERDIR)
	install -m644 include/dlistpool.h $(HEADERDIR)
	install -m644 include/ports.h $(HEADERDIR)
	install -m644 include/netpp_platform.h $(HEADERDIR)
	install -m644 include/api.h $(HEADERDIR)
	install -m644 include/platform.h $(HEADERDIR)
	install -m644 include/slave.h $(HEADERDIR)

doc_install: doc
	install -d $(DOCDIR)/html
	install -d $(DOCDIR)/images
	install -m644 doc/html/* $(DOCDIR)/html
	install -m644 doc/images/* $(DOCDIR)/images
	install -m644 README $(DOCDIR)

xml_dist:
	install -d $(XMLDIR)
	# install -m644 xml/templ.xml $(XMLDIR)
	# install -m644 xml/example.xml $(XMLDIR)
	install -m644 xml/prophandler.mk $(XMLDIR)
	install -m644 xml/xmldata.css $(XMLDIR)
	install -m644 xml/devdesc.css $(XMLDIR)
	install -m644 xml/devdesc.xsd $(XMLDIR)
	install -m644 xml/devdesc.xxe $(XMLDIR)
	install -m644 xml/devdesc_config.addon $(XMLDIR)
	install -m644 xml/memmap.xsd $(XMLDIR)
	install -m644 xml/interfaces.xsd $(XMLDIR)
	install -m644 xml/proplist.xsl $(XMLDIR)
	install -m644 xml/proptable.xsl $(XMLDIR)
	install -m644 xml/errorlist.xsl $(XMLDIR)
	install -m644 xml/errorhandler.xsl $(XMLDIR)
	install -m644 xml/reg8051.xsl $(XMLDIR)
	install -m644 xml/errorenum.xsl $(XMLDIR)
	install -m644 xml/registerdefs.xsl $(XMLDIR)
	install -m644 xml/registermap.xsl $(XMLDIR)
	install -m644 xml/xpointer.xsl $(XMLDIR)
	install -d $(INSTALLDIR)/share/netpp/common
	install -m644 common/errorcodes.xml $(INSTALLDIR)/share/netpp/common

dev_dist: xml_dist headers
	install -d $(DEVICESDIR)
	for i in $(DISTDEVICES); do \
		make -C devices/$$i install RELEASE=1 DESTDIR=$(DEVICESDIR); \
	done
	install -m644 devices/libs.mk $(DEVICESDIR)
	install -m644 devices/platform.mk $(DEVICESDIR)
	# Explicitely link a few utils:
	install -d $(INSTALLDIR)/bin
	[ -e devices/display/slave ] && \
	install -m755 devices/display/slave $(INSTALLDIR)/bin/netpp-display
	install -m755 devices/example/slave $(INSTALLDIR)/bin/netpp-example
	install -d $(INSTALLDIR)/include/netpp
	ln -s /$(DIR_PREFIX)/share/include/netpp $(INSTALLDIR)/include/netpp 

linux_dist: all_release dev_dist doc_install
	install -d $(INSTALLDIR)/lib
	install -d $(INSTALLDIR)/bin
	install -m755 master/master $(INSTALLDIR)/bin/netpp
	install -m755 master/netpp-cli $(INSTALLDIR)/bin/
	install -m755 master/netpp-dump $(INSTALLDIR)/bin/
	install -m755 master/netpp-config $(INSTALLDIR)/bin/
	install -d $(PYTHONDIR)
	install -m755 Release/device.so $(PYTHONDIR)
	install -m644 python/netpp.py $(PYTHONDIR)
	install -m755 devices/libslave/libslave.so $(INSTALLDIR)/lib

msvc_dist:
	install -d $(INSTALLDIR)/lib/debug
	install -d $(INSTALLDIR)/bin/debug
	install -d $(INSTALLDIR)/common
	install -d $(INSTALLDIR)/doc/html
	
	install -d $(INSTALLDIR)/master/msvc/master
	install -m644 src/msvc/debug/netpp.lib $(INSTALLDIR)/lib/debug
	install -m755 src/msvc/debug/netpp.dll $(INSTALLDIR)/bin/debug
	install -m755 src/msvc/debug/master.exe $(INSTALLDIR)/bin
	install -m644 src/msvc/release/netpp.lib $(INSTALLDIR)/lib/release
	install -m755 src/msvc/debug/netpp.dll $(INSTALLDIR)/bin/
	# install MSVC example
	# install -m644 common/auxiliaries.c $(INSTALLDIR)/common
	# install -m644 src/msvc/master/master.vcproj \
	   	# $(INSTALLDIR)/master/msvc/master
	# install -m644 master/main.c $(INSTALLDIR)/master
	install -m755 doc/html/* $(INSTALLDIR)/doc/html
	install -m755 doc/images/* $(INSTALLDIR)/doc/images

doczip: doc
	zip -r netpp-doc.zip doc -x *.svn/*

install: doc $(INSTALLDIR) $(DISTDEPS)
	@echo building for $(PLATFORM)
	@echo Including modules: $(DISTDEPS)

mrproper:
	rm -fr $(DIST)

zipdist: install
	cd $(DIST); \
	zip -r $(CURDIR)/../netpp-$(VERSION).zip netpp-$(VERSION)

tardist: install
	cd $(DIST); \
	tar cfz $(CURDIR)/../netpp-$(VERSION).tgz netpp-$(VERSION)

sign: LICENSE.asc

LICENSE.asc: LICENSE
	gpg --clearsign LICENSE

srcdist: doc sign
	$(MAKE) -f dist.mk VERSION=$(VERSION)

dist: tardist zipdist

deb:
	fakeroot debian/rules clean binary
	
cleandist: mrproper dist

testdist: cleandist
	tar xfz ../netpp-$(VERSION).tgz -C /tmp
