/** Handler example code.
 *
 * All handlers (getters and setters) start with get_ respectively with
 * set_.
 *
 * Note that for readonly/writeonly properties, only the relevant handler
 * functions need to be specified.
 *
 */

#include "devlib.h"
#include "devlib_error.h"
#include "example.h"
#include "property_protocol.h"
#ifndef __ZPU__
#include <stdio.h>           // For debugging only

#include <stdlib.h>           // For debugging only
#endif

// Global variables exposed to property access:

struct my_globals g_globals = {
	.zoom = 0.5,
#ifndef TINYCPU
	.buffer = { 0x0de, 0xad, 0xbe, 0xef, 0x00,  },
	.bufsize = BUFSIZE,
#endif
	.string = "Default",
	.mode = 0,
	.count = 0,
	.flag = 0,
	.demo = 42
};

unsigned short g_mode = 0;

/* Handler for property 'Temperature' */
	
int get_temperature(DEVICE d, DCValue *out)
{
	netpp_log(DCLOG_VERBOSE, "Get 'temperature'");
	out->value.f = 23.3;
	return 0;
}

