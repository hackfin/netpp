import sys
sys.path.append("../python")

import netpp

target = "127.0.0.1"

# d0 = netpp.connect("UDP:" + target)
# r = raw_input("Hit return")
# d1 = netpp.connect("UDP:" + target)
# r = raw_input("Hit return")

t = netpp.connect("TCP:" + target)

# This can take a while
t.sync()

t.root.prettyprint()

r = t.root

l = r.LUTarray

a = t.device.getToken("LUTarray[12]")

a.set(144)
print a.get()

size = l.Size.get()
print size

print l[12].get()
