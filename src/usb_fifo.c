/** Generic /dev/xxx device driver for netpp
 *
 * (c) 4/2007 Martin Strubel <strubel@section5.ch>
 *
 */


#include "devlib.h"
#include "devlib_error.h"
#include "property_protocol.h"
#include <errno.h>
#include <stdlib.h>
#include <sys/ioctl.h>

#define MAX_PACKET_SIZE   63488
#define HDR_SIZE sizeof(PropertyPacketHdr)
#define PKT_CHUNK_SIZE (MAX_PACKET_SIZE - HDR_SIZE)

#define IOCTL_FLUSH 1  // This also resides in a include/linux header.

// Default timeout to check for a request (outside protocol)
#define TIMEOUT_CHECK        10

// Default timeout to wait for a response inside protocol
#define TIMEOUT_WAIT       1000

int handle_deverror(int error, const char *txt)
{
	netpp_log(DCLOG_ERROR,
			"Device error: %s", strerror(errno));
	error = DCERR_COMM_GENERIC;
	return error;
}

int my_read(int fd, char *buf, int len)
{
	int n = 0;
	int i;
	int stat;
	struct pollfd pfd;

	pfd.fd = fd;
	pfd.events = POLLIN | POLLPRI;

	while (len > 0) {
		stat = poll(&pfd, 1, 1000);

		if (stat < 0) {
			return handle_deverror(n, "Polling RX");
		} else if (stat == 0) {
			netpp_log(DCLOG_ERROR, "Read timeout");
			return DCERR_COMM_TIMEOUT;
		}

		i = read(fd, (char*) buf, len);
		DEB(printf("got %d/%d bytes\n", i, len));
		if (i < 0 && errno != EAGAIN) {
			netpp_log(DCLOG_ERROR, "Errno: %d", errno);
			handle_deverror(i, "reading from device");
			break;
		}
		buf += i; n += i; len -= i;

	}
	return n;
}


int my_write(int fd, const char *buf, int len)
{
	int i;
	int n = 0;
	int stat;

	struct pollfd pfd;

	pfd.fd = fd;
	pfd.events = POLLOUT;

	if (len == 0) {
		// Flush FIFO (Send Immediate)
		ioctl(fd, IOCTL_FLUSH, 1);
		return len;
	}

	while (len > 0) {

		if (n == 0) {
			stat = poll(&pfd, 1, 4000);
			if (stat < 0) {
				return handle_deverror(n, "Polling TX");
			} else if (stat == 0) {
				netpp_log(DCLOG_ERROR, "Write timeout");
				return DCERR_COMM_TIMEOUT;
			}

		}
		i = len;
		n = write(fd, buf, i);
		// usleep(10);
		DEB(printf("Wrote %d bytes of %d\n", n, i));
		buf += n; len -= n;
	}
	return len;
}

static
int read_packet(int fd, char *buf)
{
	int n, len;

	// Read Header from stream
	n = my_read(fd, (char *) buf, HDR_SIZE);

	if (n < 0) return handle_deverror(n, "reading packet");
	if (n != HDR_SIZE) {
		netpp_log(DCLOG_ERROR, "Did not receive full header [size %d]", n);
		return -1;
	}

	len = get_packet_length(buf);

	// If len > maximum possible payload size, prune!
	if (len > PKT_CHUNK_SIZE) {
		len = PKT_CHUNK_SIZE;
	}

	buf += n;

	n += my_read(fd, buf, len);

	return n;
}

static
int usb_poll_packet(Peer *p, PropertyPacketHdr **h, unsigned short timeout)
{
	int n;

	struct pollfd pfd;

	pfd.fd = p->fd;
	pfd.events = POLLIN | POLLPRI;

	n = poll(&pfd, 1, timeout);
	if (n < 0) {
		return handle_deverror(n, "Polling RX");
	} else if (n == 0) return n;

	n = read_packet(p->fd, p->recvbuf);
	*h = (PropertyPacketHdr *) p->recvbuf;
	return n;
}

static
int send_slice(Peer *peer, PropertyPacketHdr *h, const char *data,
		unsigned short len)
{
	int n;

	n = my_write(peer->fd, (char *) h, HDR_SIZE);
	// printf("Send packet len %d\n", len);

	if (!n)
		n = my_write(peer->fd, data, len);
	else
		netpp_log(DCLOG_ERROR, "Could not send header");
	return n;
}

static
int usb_send_packet(Peer *peer, PropertyPacketHdr *h,
		const char *data, size_t len)
{
	int error;
	unsigned short type = ntohs(h->type);
	// netpp_log(DCLOG_DEBUG, "Send packet dest: %08lx, dport: %d len: %ld",
	//		peer->ipaddr, peer->dport, len);

	// Now send packet fragments
	int i = 0;

	h->id = htons(i);

	while (len > PKT_CHUNK_SIZE) {
		h->type = htons(type | PP_FLAG_FRAG);
		// netpp_log(DCLOG_DEBUG, "Send slice (%d left)", len);
		error = send_slice(peer, h, data, PKT_CHUNK_SIZE);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Failed to send slice");
			return error;
		}
		len -= PKT_CHUNK_SIZE;
		data += PKT_CHUNK_SIZE;

		// Update length field
		h->sub.set.len = htonl((unsigned long) len);

		i++;
		h->id = htons(i);
	}

	h->type = htons(type);

	error = send_slice(peer, h, data, (unsigned short) len);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Failed to send slice");
		return error;
	}

	return error;
}

static
int peer_init(Peer *p)
{
	p->recvbuf = (char *) malloc(MAX_PACKET_SIZE);
	if (!p->recvbuf) return DCERR_MALLOC;

	p->recvtimeout_check =  TIMEOUT_CHECK;
	p->recvtimeout_wait  =  TIMEOUT_WAIT;

	// Flush FIFO
	while(read(p->fd, (char *) p->recvbuf, MAX_PACKET_SIZE));

	return 0;
}

static
int peer_exit(Peer *p)
{
	free(p->recvbuf);
	return 0;
}

struct protocol_methods usb_methods = {
	.send    = &usb_send_packet,
	.recv    = &usb_poll_packet,
	.release = 0
};

int usb_server_init(Peer *p)
{
	int error;
	PortToken hub;

	p->fd = open("/dev/usbfifo", O_RDWR, 0);
	if (p->fd < 0) return DCERR_COMM_INIT;
	error = peer_init(p);
	if (error < 0) return error;

	hub = Hub(GET_PORTROOT(), "USB", &usb_methods);
	p->hub = desc_fromtoken(hub);
	if (!p->hub) {
		usb_server_exit(p);
		error = DCERR_MALLOC;
	}

	return error;
}

int usb_server_exit(Peer *p)
{
	// FIXME: Free hub
	peer_exit(p);
	return close(p->fd);
}


