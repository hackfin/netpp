// Linux input
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include "shell.h"



int input_poll(int fd, char *buf, int n)
{
	struct pollfd pfd;
	int stat;
	int i;

	pfd.fd = fd;
	pfd.events = POLLIN | POLLPRI;
	stat = poll(&pfd, 1, 5);

	if (stat < 0) {
		return -1;
	} else if (stat == 0) {
		return 0;
	}

	i = read(fd, buf, n);
	// EOF emulation:
	if (i == 0) {
		*buf = C_EOF; return 1;
	}
	return i;
}


int input_init(int ch)
{
	int fd = STDIN_FILENO;
	struct termios dcb;
	setvbuf(stdout,NULL,_IONBF,0);
	tcgetattr(fd, &dcb); 
	// dcb.c_cflag |= ICANON;
	dcb.c_cflag &= ~ICANON;
	tcsetattr(fd, TCSANOW, &dcb); 
	return fd;
}

int input_exit(int fd)
{
	struct termios dcb;
	tcgetattr(fd, &dcb); 
	dcb.c_cflag |= ICANON;
	tcsetattr(fd, TCSANOW, &dcb); 
	return 0;
}
