/** \file ports.h
 *
 * Hub/Port structure implementation
 *
 * Hubs and Ports are dynamic properties. Hubs are assigned on initialization
 * and represent a hardware interface (TCP, USB, ...). Ports are created
 * as Hub children within a specific hub probe procedure.
 *
 * Do not use functions of this header outside netpp. They may change.
 *
 * Hubs, Ports and Devices are dynamic property structures that are
 * allocated dynamically in memory. See property_types.h, DynPropertyDesc.
 *
 * They are instanced using the functions Hub(), PortNode() and
 * freed using portdesc_free().
 *
 * To understand the meaning of these nodes, we follow the analogy of a
 * typical PC having several interfaces with several devices attached.
 *
 * Each interface type (TCP, Serial, USB) can possibly serve several
 * attached devices, thus counts as a Hub with several Ports.
 * The difference between a Port and a Device is, that a Port MAY host
 * an attached Device, which may or may not be open (accessed by a process)
 * and may even refuse to open, if another process is already occupying it.
 * The Device instance finally is the successfully opened remote device.
 *
 * Each Hub maintains a children node list, called PortBay.
 *
 * \verbatim
 Top node:                  [Root]
                            /   \
                           /     \
 Hub layer:             TCP      USB
                         /\        \
                        /  \        \
 Port layer:       Host0   Host1   Device0
 \endverbatim
 *
 * Since the peers in a netpp interaction are non-symmetrical, one
 * being the Master (user) and one the Slave (the controlled device),
 * the above structure applies to the Master only.
 *
 * The Slave again has Hubs as well, but no notion of Ports. A Hub
 * on a Slave represents a server (in fact, it is a children of a server
 * context). The server again has users (which can be several Masters
 * connected to one Slave over TCP).
 *
 * Note that a Port does not know whether its associated Device was opened.
 * Depending on the driver and Hub implementation, a device can be opened
 * several times.
 *
 * Due to its multi user nature, exclusive device control sessions need
 * to be implemented either on Hub level (by introducing device locking
 * methods, unless the kernel provides them) or explicitely supported
 * within the device specific property protocol.
 *
 * For example, you may want to allow several users to view the status
 * of the device and show them who is currently in charge of controlling it.
 * But you might not want concurrent accesses to the actual device control.
 *
 * Sorting out the access priorities is entirely up to the netpp backend.
 *
 */


struct port_methods;
struct dyn_prop_t;

struct _propertydesc;

struct DCDevice;

typedef uint32_t PortToken;

#define PORTID TOKEN

struct peer;
struct property_packet_hdr;

/** The Hub's protocol methods. As a Hub represents a driver for a physical
 * communication protocol (abbreviated as 'Phy' in this document), it
 * has to implement a few methods to access its (possibly virtual) ports.
 *
 * Some methods are relevant for the Master only and are marked with a [M].
 *
 * See tcp.c and ipcommon.c for details.
 */

struct protocol_methods {
	/** [M] Probe ports on a Hub
	 * The probe function inserts all attached devices found into the 'ports'
	 * member of the Hub structure */
	int (*probe)(struct dyn_prop_t *hub);
	/** [M] Create a connection to a port */
	int (*create)(TOKEN hub, const char *name, PortToken *out);
	/** Open device on Hub
	 * \param portindex   The index of the port within the Hubs port list */
	int (*open)(DEVICE d, int portindex);
	/** Close the device */
	int (*close)(DEVICE d);
	/** Method to send packet data to the device */
	int (*send)(struct peer *p, struct property_packet_hdr *h,
			const char *data, size_t len);
	/** Method to receive data from the device */
	int (*recv)(struct peer *p, struct property_packet_hdr **h,
			unsigned short timeout);
	/** Release last receive buffer (only for special drivers) */
	int (*release)(struct peer *p);
};

struct hub_list {
	const char *name;
	const struct protocol_methods *methods;
};

// Protocol handler per hub:

// Compatibility typedef
// typedef DynPropertyDesc PortDesc;

typedef DlistPool PortBay;

#define DYN_NIL (INDEX) -1
#define PORTTOKEN_NIL   0xffffffff

// Property list Null pointer
#define PROPERTY_NIL    0


#define M_IDX                   0x0f

#define MAKE_PORTID(h, p)       ((( (PORTID) (h) & M_IDX) << 16) | \
                                    ((p) & 0xffff) | \
	                                DYNAMIC_PROPERTY)

#define HUBINDEX_FROMTOKEN(x)   (((x) >> 16) & M_IDX)
#define PORTINDEX_FROMTOKEN(x)   (INDEX) ((x) & 0xffff)

#define GET_PORTROOT() portbay_fromtoken(0)

// In future, we might want to change it all to a nicer PortIterator
// struct:
#if 0
typedef struct {
	PortToken parent;    ///< PortBay token (parent Hub)
	PortToken walk;      ///< Iterator token
} PortIterator;
#endif

#define ITERATOR_FIRST(pool) MAKE_PORTID(pool->id, pool->first)
#define ITERATOR_NEXT(pool, pd)  MAKE_PORTID(pool->id, pd->next)
#define ITERATOR_VALID(it)   (PORTINDEX_FROMTOKEN(it) != INDEX_NIL)

// These enums determine how many pools in g_port.pools we need:

enum {
#ifdef CONFIG_LOCALHUB
	HUB_LOCAL,
#endif
#ifdef CONFIG_TCP
	#ifdef CONFIG_SLAVE
	HUB_TCP_SERV_ENUM,
	HUB_TCP_SERV_PEER_ENUM,
	#endif
	HUB_TCP_MASTER_ENUM,
	HUB_TCP_PEER_ENUM,
#endif
#ifdef CONFIG_PROXY
	#ifdef CONFIG_SLAVE
	HUB_PRX_SERV_ENUM,
	HUB_PRX_SERV_PEER_ENUM,
	#endif
	HUB_PRX_MASTER_ENUM,
	HUB_PRX_PEER_ENUM,
#endif
#ifdef CONFIG_UDP
	#ifdef CONFIG_SLAVE
	HUB_UDP_SERV_ENUM,
	HUB_UDP_SERV_PEER_ENUM,
	HUB_UDP_DISCOVERY_PROBE_ENUM,
	HUB_UDP_DISCOVERY_RESPONSE_ENUM,
	#endif
	HUB_UDP_MASTER_ENUM,
	HUB_UDP_PEER_ENUM,
#endif
#ifdef CONFIG_USBFIFO
	HUB_USBFIFO_SERV_ENUM,
	HUB_USBFIFO_PEER_ENUM,
#endif
#ifdef CONFIG_DEVICE
	HUB_DEVICE_SERV_ENUM,
	HUB_DEVICE_PEER_ENUM,
#endif
#ifdef CONFIG_DEVICEMUX
	HUB_DEVMUX_SERV_ENUM,
	HUB_DEVMUX_PEER_ENUM,
#endif
	LAST_HUB
};

// Server codes:
#define PROBE_QUERY_UDP 0
#define PROBE_QUERY_TCP 1
#define PROBE_QUERY_PRX 2

// Each Server has 2 hubs (one for available ports, one for attached
// clients. Plus, pool '0' is reserved. We're on the safe side with
// this formula.

#ifndef NUM_EXTRA_SERVERS
#define NUM_EXTRA_SERVERS 0
#endif

#ifndef MAX_NUM_HUBS
#define MAX_NUM_HUBS (LAST_HUB + NUM_EXTRA_SERVERS + 1)
#endif

void hub_append(PortToken hub, PortToken d);

DynPropertyDesc *find_portnode(DlistPool *pool,
	const char *portid, TOKEN *node);

DynPropertyDesc *find_hub(const char *hubid, TOKEN *hub);

TOKEN hub_reuse(const char *hubid, const struct protocol_methods *methods);

/** Append port descriptor to port bay */
int portbay_append(PortBay *bay, PortToken d);

#define  portbay_remove portdesc_free

/** Only for debugging: Lists portbay members */
void portbay_list(PortBay *b);

/** Proxy variant for portbay purging */
void portbay_purge(PortToken port);
 
/** Free a port descriptor [Hub|Port|Device] */
void portdesc_free(PortToken d);

/** Get Portbay pointer from PortToken */
PortBay *portbay_fromtoken(PortToken b);

/** Get Port descriptor pointer from PortToken */
DynPropertyDesc *desc_fromtoken(PortToken b);

/** Get Name of port descriptor */
int portdesc_getname(PortToken d, const char **name);

/** Server port init function. Call before running a server. */
int serverports_init(void);

/** Free server pool */
// Removed. 
// void serverports_exit(void);

/** Port bay init function. Called internally.
 *
 * A service will register the required port hubs using this function.
 * See source code examples.
 */
int ports_init(const struct hub_list *inithubs);

/** Ports deinitialization
 * \param final_cleanup    0: normal cleanup 1: final cleanup/exit
 *
 * Each service will call ports_init() to register its hubs and ports.
 * Upon exit of the service, it is mandatory to call ports_exit(0).
 * Internal reference counting asserts that no resources used by another
 * service are freed.
 *
 * Upon final exit of the server, ports_exit(1) is called to free all
 * resources.
 */
void ports_exit(int final_cleanup);

/** Probe ports */
int ports_probe(void);

/** Probe devices on a logical network hub */
int net_probe(DynPropertyDesc *hub);

/** Create a port connection descriptor */
int net_create(PortToken hub, const char *name, PortToken *out);

/** Create and return a port structure
 * \param hub    Parent hub
 * \param name   A unique name for the port
 *               port. Must be owned by caller.
 * \param type   [#DC_DEVICE, #DC_PORT]
 */
PortToken Port(PortToken hub, const char *name, int type);

/** Create and return a Hub structure
 * \param bay       The owning port bay, normally global.
 * \param name      A unique name for the Hub (like 'TCP', 'UART', etc.)
 * \param methods   Pointer to protocol method structure
 */
PortToken Hub(PortBay *parent,
	const char *name, const struct protocol_methods *methods);

/** Create and return a PortNode structure (Device or Port)
 * \param parent The parenting Hub's pool
 * \param name   A unique name for the node
 * \param type   [#DC_DEVICE, #DC_PORT]
 */
PortToken PortNode(PortBay *parent, const char *name, int type);

int portid_fromname(const char *id, PORTID *p);

/** Opens port to device d with the port id 'portid' */
int port_open(DEVICE d, TOKEN portid);

/** Close port */
int port_close(DEVICE d);

int port_select(PortToken parent, PortToken prev, PortToken *next);

// GLOBALS

extern struct protocol_methods tcp_methods;
extern struct protocol_methods prx_methods;
extern struct protocol_methods udp_methods;
extern struct protocol_methods usb_methods;
extern const struct protocol_methods dev_methods;
extern const struct protocol_methods mux_methods;

// Auxiliaries
int scan_into(const char *s, char delim, char *dest, int n);
const char *skip_delimiter(const char *name, const char delim);

#if defined(LWIP) && !LWIP_COMPAT_SOCKETS
struct tcp_pcb;
#endif

/** A server context for multi user environments
 *
 * A server context keeps track of connections, independent of the
 * underlying transport protocol.
 * For this, it uses dynamic properties stored in two port bay members:
 *
 * - 'hub'
 * - 'peerdevs'
 *
 * The 'hub' port bay keeps track of possible connections that can be
 * made to this server, for example, TCP ports. If TCP servers are running
 * on port 2010 and 2014, the hub port bay will contain two DC_PORT
 * type entries with the name ":2010" and :2014".
 *
 * When a client makes a connection to the server, it will register itself
 * in the 'peerdevs' bay as a DC_DEVICE with an identifier that depends on
 * the transport protocol. For example, a TCP connection will create an
 * ID of the sort "192.168.2.1:46612". When the connection is terminated,
 * the entry will be removed.
 *
 * Master connections, i.e. protocol requests initiated by the same
 * program the server is running in, are stored in a different and global
 * port bay structure named with the protocol identifier, like "TCP".
 * They are of the type DC_PORT, even when the remote device is opened.
 *
 * All available HUBs are listed in the global port bay.
 * You can use the gdb script 'dump_pool <index>' to display the
 * hub structures inside gdb when including \c netpp/pool.gdb.
 *
 * The global port bay has index 0. Members of the HUBs are displayed
 * by incrementing the index, for example, if the TCP HUB is the fourth
 * element in the array, use index 5 to display the connections made
 * from this hub as argument to dump_pool.
 *
 * Multiple connections to the same netpp address and port will only
 * create one entry in the HUB.
 *
 */
typedef struct server_context_t {
	unsigned short   flags;       ///< Server flags [#F_SERV_TCP, ...]
	PortToken        hub;         ///< Pointer to associated Hub node
#if defined(LWIP) && !LWIP_COMPAT_SOCKETS
	struct tcp_pcb  *pcb;
#elif defined(HAVE_SOCKET)
	SOCKET           listener;    ///< Listening socket, if networking server
	unsigned short   port;
#endif // LWIP
	PortToken        peerdevs;    ///< Descriptor for peer user list
	PortBay         *devices;     ///< List of peer users
	int              usercount;   ///< Number of peer users
	/** Function to init a peer structure */
	int            (*peer_init)(struct peer *p);
	/** Slave handler function pointer */
	int            (*slave_handler)(struct DCDevice *d);
	/** Disconnect cleanup hook */
	int            (*cleanup)(struct DCDevice *d);
} ServerContext;

#define F_SERV_TCP     0x0001  ///< Server is a TCP server
#define F_SERV_DISC    0x0002  ///< ..is a temp. discovery server on client
#define F_SERV_PROXY   0x0004  ///< ..is a Proxy server (RESERVED)

#ifdef HAVE_SOCKET
/* Socket aux types */
typedef struct sockaddr     SockAddr;
typedef struct sockaddr_in  SockAddrIn;
#endif

/** Initializes a remote device structure inside the corresponding Port token.
 * The PortNode must be of type DC_DEVICE
 * \param p      Port token that the device structure is allocated in
 * \param rdevp  Pointer to RemoteDevice pointer
 */
int rdev_new(PortToken p, struct DCDevice **rdevp);


/** Remove a remote device from port hub/Server context
 * Only for session based networking protocols.
 *
 * \param d    Pointer to Device to be removed. The Device instance is
 *             destroyed.
 */
void remotedevice_remove(ServerContext *serv, PortToken d);

#ifdef HAVE_SOCKET
/** Add a remote device to port hub/Server context
 * Used by session based networking protocols to add a Device to
 * the server's user list.
 * */
int remotedevice_add(ServerContext *serv, SOCKET sock, SockAddrIn *sin,
	int nodetype);

#endif

/** Add a server entry to the active server list
 *
 * \param port     The network server's listening port
 */
int add_server_entry(ServerContext *serv, int port);

////////////////////////////////////////////////////////////////////////////
// PROTOCOL SPECIFIC

/** Serve connected users
 *
 * \param devices   A pointer to PortBay structure that must be initialized to 
 *                  zero in the function head.
 */
int net_serve_users(ServerContext *serv);

// Compatibility hack
#define tcp_serve_users net_serve_users

/** Disconnect all network users
 *
 * \param serv        Pointer to ServerContext as initialized in
 *                    tcp_server_init()
 */
int net_disconnect_users(ServerContext *serv);


/** Initializes a netpp TCP server.
 * 
 * \param listen_port  Port number of port to listen
 * \param serv         Pointer to ServerContext instance to be initialized
 *
 */
int tcp_server_init(int listen_port, ServerContext *serv);

/** Exits a TCP server
 *
 * You should disconnect all users before exiting the server.
 */
int tcp_server_exit(ServerContext *serv);

/** Listen to incoming TCP connections
 * \param serv   Pointer to initialized ServerContext structure
 */
int tcp_listen_incoming(ServerContext *serv);

/** High level function calling tcp_serve_users() and tcp_listen_incoming()
 *
 * Normally, this function is only called for a TCP only server.
 */
int tcp_slave_mainloop(ServerContext *serv);

/* Exported TCP close function */
int netpp_tcp_close(struct DCDevice *d); // Workaround for LWIP API

////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_SOCKET
/** UDP socket init function */
int udp_sock_init(SOCKET *sock, SockAddrIn *sin);
#endif

/** UDP server init function */
int udp_server_init(int listen_port, ServerContext *serv, const char *name);

/** UDP user serve function */
int udp_serve(ServerContext *serv);

/** UDP listener */
int udp_listen_incoming(ServerContext *serv, unsigned int timeout_ms);

/** Exit UDP server */
int udp_server_exit(ServerContext *serv);

////////////////////////////////////////////////////////////////////////////

// Proxy server:
int prx_server_init(int listen_port, ServerContext *serv);
int prx_server_exit(ServerContext *serv);
#define prx_listen_incoming tcp_listen_incoming

////////////////////////////////////////////////////////////////////////////
// Device server:

/** DEV server init function
 *
 * \param p     Pointer to uninitialized Peer structure
 * \param param Device parameters, depending on device. For a tty, specify
 *              baud rate, like "38400". Default Nbits/Parity/Stop is 8N1.
 */

int dev_server_init(struct peer *p, const char *param);

/** DEV server deinitialization
 */
int dev_server_exit(struct peer *p);

/** USB server init function
 *
 * \param p     Pointer to uninitialized Peer structure
 */
int usb_server_init(struct peer *p);

/** USB server deinitialization
 */
int usb_server_exit(struct peer *p);

/** DEV server init function
 *
 * \param p     Pointer to uninitialized Peer structure
 * \param param Device parameters, depending on device. For a tty, specify
 *              baud rate, like "38400". Default Nbits/Parity/Stop is 8N1.
 */


////////////////////////////////////////////////////////////////////////////
DynPropertyDesc *server_getdesc(PortToken serv);


////////////////////////////////////////////////////////////////////////////
// Hub name definitions
//

extern const char g_muxserv_name[];
extern const char g_muxpeers_name[];
extern const char g_prxserv_name[];
extern const char g_prxpeers_name[];
extern const char g_tcpserv_name[];
extern const char g_tcppeers_name[];
extern const char g_udpserv_name[];
extern const char g_udppeers_name[];
extern const char g_udp_probe_name[];
#define MUX_SERVER_NAME g_muxserv_name
#define MUX_PEERS_NAME  g_muxpeers_name
#define PRX_SERVER_NAME g_prxserv_name
#define PRX_PEERS_NAME  g_prxpeers_name
#define TCP_SERVER_NAME g_tcpserv_name
#define TCP_PEERS_NAME  g_tcppeers_name
#define UDP_SERVER_NAME g_udpserv_name
#define UDP_PEERS_NAME  g_udppeers_name
#define UDP_PROBE_NAME  g_udp_probe_name
