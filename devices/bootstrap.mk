
ifeq ($(PROJ),)
PROJNAME = "mydevice"
else
PROJNAME = $(PROJ)
endif

DUTIES = Makefile local_config.mk .gitignore
DUTIES += slave.c example.h handler.c $(PROJNAME).xml

all: $(DUTIES)

slave.c: $(NETPP_DIR)/devices/template/slave.c
	cp $< $@

handler.c: $(NETPP_DIR)/devices/template/handler.c
	cp $< $@

example.h: $(NETPP_DIR)/devices/template/example.h
	cp $< $@

$(PROJNAME).xml: $(NETPP_DIR)/devices/template/example.xml
	cp $< $@

Makefile: $(NETPP_DIR)/devices/Makefile.in
	m4 \
		-DLICENSE_INFORMATION="# netpp OpenSource license" \
		-DCONFIG_LIBNAME=$(PROJNAME) \
		<$< >$@	

local_config.mk:
	@echo "NETPP = $(NETPP_DIR)" > $@

.gitignore:
	@echo "local_config.mk" >$@
