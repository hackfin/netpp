/* Slave back end handler
 *
 * This function set does all data handling between the property protocol
 * and the raw data
 *
 */

#include "netpp.h"
#include "device_error.h"
#include "property_protocol.h"

#include "fileops.h"
#include "slave_vars.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>

#define MAX_FILE_SIZE     0x200000
#define FBUFFER_SIZE      0x8000

static char s_filebuf[FBUFFER_SIZE];
static FILE *s_filehandle = NULL;

char g_filename[MAX_FILENAME_LEN];

unsigned char g_buffer[BUFFER_SIZE];

////////////////////////////////////////////////////////////////////////////
// AUX

int handle_ferror(int error)
{
	switch (error) {
		case EACCES:
			printf("Could not access file\n");
			return DCERR_FILE_ACCESS;
		case ENOENT:
			printf("File does not exist\n");
			return DCERR_FILE_NOT_EXISTENT;
		default:
			printf("Generic File error %d\n", error);
			return DCERR_FILE_GENERIC;
	}
}

////////////////////////////////////////////////////////////////////////////

int get_filesize(DEVICE d, DCValue *out)
{
	int error;
	struct stat st;


	error = stat(g_filename, &st);
	if (error < 0) return handle_ferror(errno);

	out->value.i = st.st_size;
	printf("get_filesize(): %ld\n", st.st_size);
	return 0;
}


int get_filedata(DEVICE d, DCValue *out)
{
	int count, warn = 0;
	char *buf;
	int error;

	switch (out->type) {
		case DC_BUFFER:
			buf = s_filebuf;

			netpp_log(DCLOG_VERBOSE, "read request %d bytes", out->len);

			if (out->len > FBUFFER_SIZE) {
				out->len = FBUFFER_SIZE;
				netpp_log(DCLOG_WARN, "modified length: %d", out->len);
				warn = DCWARN_PROPERTY_MODIFIED;
			} else
			// Catch len 0 also to implement a little python hook.
			// The python wrapper always sets buffer size 0 to query the
			// actual buffer size.
			if (out->len == 0) {
				error = DCERR_PROPERTY_SIZE_MATCH;
				// attempt to read to tell effective number of bytes:
				count = fread(buf, 1, FBUFFER_SIZE, s_filehandle);
				if (count == 0) {
					// Must set the buffer, even if it's len 0
					out->value.p = s_filebuf;
					error = 0;
				} else {
					fseek(s_filehandle, -count, SEEK_CUR);
				}
				out->len = count;
				return error;
			}

			if (s_filehandle == NULL) return DCERR_FILE_GENERIC;

			count = fread(buf, 1, out->len, s_filehandle);
			if (count < out->len) {
				out->len = count;
				netpp_log(DCLOG_WARN, "modified length: %d", out->len);
				warn = DCWARN_PROPERTY_MODIFIED;
			}
			out->value.p = s_filebuf;
			break;
		case DC_COMMAND:
			break;
		default:
			printf("type: %d\n", out->type);
			warn = DCERR_PROPERTY_TYPE_MATCH;
			break;
	}

	return warn;
}

int set_filedata(DEVICE d, DCValue *in)
{
	static char *buf = NULL;
	int error = 0;

	switch (in->type) {
		case DC_BUFFER:
			/* This function doesn't work right when the filedata block length
			 * is bigger than the protocol block size.
			 * We have to deal with any sequence of the sort:
			   'BBBBCBBBBC' where B: BUFFER request
			                      C: COMMAND request
			 FIXME: support B request with ANY block size.

			 REMOVE ALL MALLOC STUFF FROM HERE.
			 See streamtest example on how to do it right.
			 *
			 */

			if (buf) { free(buf); buf = 0; }
			if (in->len == 0) return 0;

			// We don't allow too big file chunk sizes
			if (in->len > MAX_FILE_SIZE) {
				in->len = MAX_FILE_SIZE;
				netpp_log(DCLOG_WARN, "modified length: %d", in->len);
				error = DCWARN_PROPERTY_MODIFIED;
			}

			buf = (char *) malloc(in->len);
			if (!buf) return DCERR_MALLOC;
			netpp_log(DCLOG_VERBOSE, "reserve buffer (%lu)", in->len);

			in->value.p = buf;
			break;
		case DC_COMMAND:
			fwrite(buf, 1, in->len, s_filehandle);
			netpp_log(DCLOG_VERBOSE, "fwrite (%lu)", in->len);
			break;
		default:
			printf("type: %d\n", in->type);
			error = DCERR_PROPERTY_TYPE_MATCH;
			break;
	}
			
	return error;
}

int set_file_open(DEVICE d, DCValue *in)
{
	int error;
	char *mode;
	struct stat st;
	printf("file_open('%s')\n", g_filename);
	
	// Check if file is existent in current dir
	if (in->value.i != FILE_OPEN_WRITE) {
		error = stat(g_filename, &st);
		if (error < 0) return handle_ferror(errno);
	}

	switch (in->value.i) {
		case FILE_OPEN_READ:
			mode = "rb"; break;
		case FILE_OPEN_WRITE:
			mode = "wb"; break;
		default:
			mode = "rb"; break;
	}

	s_filehandle = fopen(g_filename, mode);
	if (s_filehandle == 0) {
		printf("Could not open file\n");
		error = DCERR_FILE_OPEN;
		return error;
	}
	return 0;
}

int set_file_close(DEVICE d, DCValue *in)
{
	int error;

	printf("file_close()\n");
	error = fclose(s_filehandle);
	if (error < 0) {
		error = DCERR_FILE_GENERIC;
		return error;
	}
	return 0;
}

