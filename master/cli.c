#include "master_util.h"
#include "shell.h"
#include <signal.h>
#include "conffile.h"
#include "cache.h"

#define MAX_NUM_ARGS 16

char code_fromtype(DCValue *v);

int input_readline(const char *prompt);

void write_string(int fd, const char *s)
{
	write(fd, s, strlen(s));
}

typedef struct {
	DEVICE dev;
	Cache cache;
} Context;

int cache_populate(Cache *cache, DEVICE d)
{
	int error;
	TOKEN child;
	char buf[256];
	DCValue val;
	char code;

	TOKEN t = cache->root;

	error = dcProperty_Select(d, t, t, &child);
	while (error == 0) {
		error = dcProperty_GetProto(d, child, &val);
		code = code_fromtype(&val);

		val.value.p = buf; val.len = sizeof(buf);
		error = dcProperty_GetName(d, child, &val);
		printf("Child: (%c) [%08x] '%s'\n", code, child, (char *) val.value.p);
		cache_add(cache, (const char *) val.value.p, child);
		error = dcProperty_Select(d, t, child, &child); // Iterate
	}

	return error;
}

#ifdef TEST_STRUCT
static
void dump(DLIST_ITEM *e)
{
	printf("%s: rank: %d\n", e->name, e->cache.rank);
}
#endif

int get_token(Context *c, const char *name, TOKEN *token)
{
	int ret;
	TOKEN t;
	DCValue val;
	DEVICE d = c->dev;
	t = cache_find(&c->cache, name);
	if (t == TOKEN_INVALID) {
		printf("'%s' not in cache, querying...\n", name);
		ret = dcProperty_ParseName(d, name, &t);
		if (ret < 0) {
			printf("Property not found or bad index\n");
			return ret;
		}
		cache_add(&c->cache, name, t);
		val.value.p = NULL; // do not query name
		ret = dcProperty_GetProto(d, t, &val);
		if (!ret) ret = print_type(&val);
		if (ret < 0) return ret;
	}
	*token = t;
	return 0;
}


const char s_help[] = "Interactive netpp shell v0.2\n\n"
	       "COMMANDS:\n"
	       " '?'                 : show top level properties\n"
	       " '!'                 : clear cache\n"
	       " <Property>          : Query property\n"
	       " <Property> <Value>  : Set property to value\n\n";

int default_exec_cmd(Context *c, int argc, char **argv)
{
	int ret = 0;
	int i;
	TOKEN t = TOKEN_INVALID;
	unsigned int v;
	DCValue val;
	DEVICE d = c->dev;
	char buf[64];
	val.value.p = buf; val.len = sizeof(buf);
	switch (argc) {
		case 0:
			printf("Last\n");
			break;
		case 1:
			switch (argv[0][0]) {
#ifdef TEST_STRUCT
				case '*':
					dlist_dump(&c->cache.pool, dump);
					break;
#endif
				case '!':
					cache_clear(&c->cache);
					printf("Cleared cache.\n");
					break;
				case '?':
					printf(s_help);
					cache_clear(&c->cache);
					ret = dcProperty_GetName(c->dev, c->cache.root, &val);
					if (ret < 0) return ret;
					printf("Connected to '%s' device class\n",
						(char *) val.value.p);
					cache_populate(&c->cache, d);
					ret = S_IDLE;
					break;
				default:
					if (argv[0][0]) {
						ret = get_token(c, argv[0], &t);
						if (ret < 0) break;
						ret = query_prop(d, t, &val);
					}
					break;
			}
			break;
		case 2:
			ret = get_token(c, argv[0], &t);
			if (ret < 0) break;
			ret = value_from_string(d, t, argv[1]);
			break;
		default:
		{
			char *p = buf;
			ret = get_token(c, argv[0], &t);
			if (ret < 0) break;
			for (i = 1; i < argc; i++) {
				ret = sscanf(argv[i], "%02x", &v);
				if (ret == 0) ret = DCERR_PROPERTY_TYPE_UNKNOWN;
				*p++ = v;
			}
			val.type = DC_BUFFER;
			val.len = argc - 1;
			ret = dcDevice_SetProperty(d, t, &val);
		}
	}
	if (ret < 0) return ret;
	return S_IDLE;
}

volatile int g_break = 0;
const char s_prompt[] = "netpp> ";

	       
int run_shell(DEVICE d, TOKEN root)
{
	MainState state = S_IDLE;
	int argc = 0;
	char *argv[MAX_NUM_ARGS];
	Token t;
	char c;
	char word[32];
	static char buf[256];
	int ret;
	int i = 0, j;

	FILEDES fd = input_init(0);
	int out = STDOUT_FILENO;

	Context context;
	cache_init(&context.cache, 200);
	context.dev = d;
	context.cache.root = root;

	printf(s_help);

	while (!g_break) {
		// Here we can check for other input events:
		// mainloop_handler(state);
		switch (state) {
			case S_IDLE:
#ifdef USE_RAW_INPUT
				write_string(out, s_prompt);
#else
				ret = input_readline(s_prompt);
				if (ret < 0) return ret;
#endif
				state = S_INPUT;
			case S_INPUT:
				ret = gettoken(fd, &t, word, sizeof(word));
				if (ret < 0) { state = S_ERROR; break; }
				switch (t) {
					case T_NONE: break;
					case T_EOF:
						printf("EOF, quitting.\n");
						g_break = 1;
					case T_NL: state = S_IDLE; break;
						break;
					case T_CHAR:
						state = S_CMD;
						break;
					default:
						state = S_ERROR;
				}
				break;
			case S_CMD:
				ret = gettoken(fd, &t, word, sizeof(word));
				if (ret < 0) { state = S_ERROR; break; }
				switch (t) {
					case T_WORD_LAST:
					case T_WORD:
						j = 0;
						if (argc >= MAX_NUM_ARGS) {
							fprintf(stderr, "Number of arguments exceeded\n");
							state = S_ERROR;
							ret = DCERR_PROPERTY_RANGE;
							break;
						}
						argv[argc++] = &buf[i];
						while ( (c = word[j++]) ) { buf[i++] = c; }
						buf[i++] = '\0';
						if (t == T_WORD) break;
					case T_NL:
						ret = default_exec_cmd(&context, argc, argv);
						argc = 0; i = 0;
						if (ret < 0) state = S_ERROR;
						else         state = ret;
						break;
					default:
						break;
				}
				break;
			case S_INTERACTIVE:
				ret = gettoken(fd, &t, word, sizeof(word));
				if (ret < 0) { state = S_ERROR; break; }
				switch (t) {
					case T_ESC:
						state = S_IDLE;
						break;
					default:
						break;
				}
				break;
			case S_ERROR:
				// write_string(errtostring(ret));
				handleError(ret);
				write_string(out, "\n");
				state = S_IDLE;
				break;
			default:
				write_string(out, "Illegal state.\n");
				g_break = 1;
		}
	}
	input_exit(fd);
	return ret;
}

void sigpipe_handler(int n)
{
	printf("\nReceived disconnect(broken pipe)...\n");
	g_break = 1;
}

void break_handler(int n)
{
	printf("\nTerminating...\n");
	g_break = 1;
}


int main_cli(int argc, char **argv)
{
	int error;
	DEVICE d;
	TOKEN root;

#ifndef __WIN32__
	signal(SIGINT, break_handler);
	signal(SIGPIPE, sigpipe_handler);
#endif
	if (argc < 2) {
		fprintf(stderr, "\nUsage: %s <target>\n",
			argv[0]);

		printf("\nAvailable Hubs/Targets:\n");
		error = list_children(DC_PORT_ROOT, INVALID_TOKEN, 1);
		// Final cleanup:
		dcDeviceClose(DC_PORT_ROOT);

		return -1;
	}

	error = dcDeviceOpen(argv[1], &d);
	if (error < 0) {
		handleError(error);
		return -1;
	}

	error = dcDevice_GetRoot(d, &root);
	if (error < 0) {
		printf("Failed to get device root node\n");
		handleError(error);
		return error;
	}

	error = run_shell(d, root);

	dcDeviceClose(d);
	return error;
}
