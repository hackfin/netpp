# Test CPP wrapper and property cache
#
#
import netpp
import pytest
import difflib
import filecmp
import subprocess
import time

SLAVE = "devices/example/slave"
slave = None

@pytest.yield_fixture(autouse=True, scope='session')
def test_init_and_cleanup():
	global slave, dev, root
	slave = subprocess.Popen([SLAVE, "--port=2018"])
	time.sleep(0.5)
	yield
	slave.terminate()


def test_dump():
	log = open("testbench/out_plusplus.txt", "w")
	master = subprocess.Popen(["wrapper/testplusplus", "TCP:localhost:2018"], stdout=log)
	master.wait()
	log.close()

	assert filecmp.cmp("testbench/out_plusplus.txt.ref", "testbench/out_plusplus.txt")

# This test fails in netpp 0.x, as it allows undefined
# fields in protocol versions up to 0x2.
# Disabled in this release.
def DISABLED_test_valgrind():
	log = open("testbench/valgrind.txt", "w")
	master = subprocess.Popen(["valgrind", "--error-exitcode=127",
		"wrapper/testplusplus", "TCP:localhost:2018"], stderr=log)
	ret = master.wait()
	log.close()

	assert ret == 0



