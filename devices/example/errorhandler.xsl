<?xml version="1.0" encoding="ISO-8859-1"?>

<!--

$Id: errorhandler.xsl 143 2007-05-26 07:06:34Z strubi $

Transformation style sheet DEVDESC-XML -> Error handler C source

(c) 2005-2007 section5 // Martin Strubel <strubel@section5.ch>

-->

<xsl:stylesheet version="1.0" 
	xmlns:my="http://www.section5.ch/dclib/schema/devdesc"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

	<xsl:output method="text" encoding="ISO-8859-1"/>

	<xsl:template match="my:errorspec">
	case DCERR_<xsl:value-of select="@name"/>:
		str = "<xsl:value-of select="my:info"/>"; break;
	</xsl:template>
	<xsl:template match="my:warnspec">
	case DCWARN_<xsl:value-of select="@name"/>:
		str = "<xsl:value-of select="my:info"/>"; break;
	</xsl:template>

	<xsl:template match="my:header">
	<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="/">
////////////////////////////////////////////////////////////////////////////
//
// (c) 2004-2007 section5 // Martin Strubel
//
// THIS IS A GENERATED FILE. DO NOT MODIFY! ALL CHANGES WILL BE LOST
//
//
////////////////////////////////////////////////////////////////////////////

#include &lt;stdio.h&gt;
#include "devlib.h"
#include "device_error.h"
<xsl:apply-templates select=".//my:header"/>

const char *
device_strerr(int error)
{
	const char *str;
	switch (error) {
	/* Errors: */
<xsl:apply-templates select=".//my:errorspec"/>
	/* Warnings: */
<xsl:apply-templates select=".//my:warnspec"/>
	default:
		str = NULL;
	}
	return str;
};

int my_errorhandler(int error) {
	const char *s;

	s = device_strerr(error);

	if (!s) {
		s = dcGetErrorString(error);
	}
	printf("Return code: %d: '%s'\n", error, s);
	return error;
}

<xsl:text>
</xsl:text>
	</xsl:template>
</xsl:stylesheet>

