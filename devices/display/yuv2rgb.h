
#define PIXEL8 short
// #define ITU_R_BT_601
#define UV_OFFSET (-128)

extern PIXEL8 tab_y[256];
extern PIXEL8 tab_rv[256];
extern PIXEL8 tab_gv[256];
extern PIXEL8 tab_gu[256];
extern PIXEL8 tab_bu[256];

