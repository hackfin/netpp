/** Standalone (non OS) device emulation
 *
 * This is so board specific that this implements just a few stubs.
 * User is responsible of supplying the missing board I/O and
 * polling routines.
 *
 * (c) 11/2007 Martin Strubel <strubel@section5.ch>
 *
 */


#include "rawio.h"
#include "devlib.h"
#include "devlib_error.h"
#include "property_protocol.h"

#ifndef MAX_PACKET_SIZE
#define MAX_PACKET_SIZE   128
#endif
#define HDR_SIZE sizeof(PropertyPacketHdr)
#define PKT_CHUNK_SIZE (MAX_PACKET_SIZE - HDR_SIZE)

// Default timeout to check for a request (outside protocol)
#ifndef TIMEOUT_CHECK
#define TIMEOUT_CHECK       1
#endif

// Default timeout to wait for a response inside protocol
#ifndef TIMEOUT_WAIT
#define TIMEOUT_WAIT     2000
#endif

static
int handle_deverror(int error, const char *txt)
{
	error = DCERR_COMM_TIMEOUT;
	return error;
}

static
int read_packet(int fd, char *buf)
{
	int n, len;
	int ret;

	// Read Header from stream
	n = my_read(fd, (char *) buf, HDR_SIZE);

	if (n < 0) return handle_deverror(n, "reading packet");
	if (n != HDR_SIZE) {
		netpp_log(DCLOG_ERROR, "Did not receive full header [size %d]", n);
		return DCERR_COMM_FRAME;
	}

	len = get_packet_length(buf);

	// If len > maximum possible payload size, prune!
	if (len == 0) return n;
	else if (len > PKT_CHUNK_SIZE) {
		len = PKT_CHUNK_SIZE;
	}

	buf += n;

	ret = my_read(fd, buf, len);
	if (ret < 0) return ret;
	n += ret;

	return n;
}

int rawdevice_open(const char *name)
{
	return my_open(name);
}

static
int peer_init(Peer *p)
{
	p->recvbuf = buf_alloc(MAX_PACKET_SIZE);
	if (!p->recvbuf) return DCERR_MALLOC;

	p->recvtimeout_check =  TIMEOUT_CHECK;
	p->recvtimeout_wait  =  TIMEOUT_WAIT;

	p->packetsize = MAX_PACKET_SIZE;

	// Flush FIFO
#if 0
	while(read(p->fd, (char *) p->recvbuf, MAX_PACKET_SIZE));
#endif

	return 0;
}

static
int peer_exit(Peer *p)
{
	buf_free(p->recvbuf);
	return 0;
}

static
int dev_probe(DynPropertyDesc *hub)
{
	PortBay *b;
	PortToken port;

	// Just support one device:
	b = portbay_fromtoken(hub->hub.ports);
	port = PortNode(b, "0", DC_PORT);
	portbay_append(b, port);
	return 0;
}

static
int dev_create(PortToken hub, const char *name, PortToken *portid)
{
	PortToken port;

	while (*name != ':' && *name) {
		name++;
	}
	if (*name) name++;
	else return DCERR_COMM_INIT;

	port = Port(hub, name, DC_PORT);
	if (!port) return DCERR_MALLOC;
	hub_append(hub, port);
	*portid = port;
	return 0;
}

static
int dev_open(RemoteDevice *d, int portindex)
{
	int error;
	int fd;
	const char *name;

	name = getname(&d->peer, portindex);
	fd = rawdevice_open(name);
	if (fd < 0) {
		return DCERR_PROPERTY_UNKNOWN;
	}

	error = peer_init(&d->peer);

	d->peer.fd = fd;

	return error;
}

static
int dev_close(RemoteDevice *d)
{
	return my_close(d->peer.fd);
}

static
int dev_poll_packet(Peer *p, PropertyPacketHdr **h, unsigned short timeout)
{
	int n;

	n = my_poll(p->fd, sizeof(PropertyPacketHdr), timeout);
	if (n <= 0) return n;

	n = read_packet(p->fd, p->recvbuf);
	*h = (PropertyPacketHdr *) p->recvbuf;
	return n;
}

void toggle_led(char which); // XXX

static
int send_slice(Peer *peer, PropertyPacketHdr *h, const char *data,
		unsigned short len)
{
	int n;

	n = my_write(peer->fd, (char *) h, HDR_SIZE);

	if (!n) {
		n = my_write(peer->fd, data, len);
	} else
		netpp_log(DCLOG_ERROR, "Could not send header");

	return n;
}

static
int dev_send_packet(Peer *peer, PropertyPacketHdr *h,
		const char *data, size_t len)
{
	int error;
	unsigned short type = ntohs(h->type);
	// netpp_log(DCLOG_DEBUG, "Send packet dest: %08lx, dport: %d len: %ld\n",
	//		peer->ipaddr, peer->dport, len);

	// Now send packet fragments
	int i = 0;

	h->id = htons(i);

	while (len > PKT_CHUNK_SIZE) {
		h->type = htons(type | PP_FLAG_FRAG);
		netpp_log(DCLOG_DEBUG, "Send slice (%d left)\n", len);
		error = send_slice(peer, h, data, PKT_CHUNK_SIZE);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Failed to send slice");
			return error;
		}
		len -= PKT_CHUNK_SIZE;
		data += PKT_CHUNK_SIZE;

		// Update length field
		h->sub.set.len = htonl((unsigned long) len);

		i++;
		h->id = htons(i);
	}

	h->type = htons(type);
	// h->id = htons(0xbeef); // XXX

	error = send_slice(peer, h, data, (unsigned short) len);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Failed to send slice");
		return error;
	}

	return error;
}

const
struct protocol_methods dev_methods = {
	.probe   = &dev_probe,
	.create  = &dev_create,
	.open    = &dev_open,
	.close   = &dev_close,
	.send    = &dev_send_packet,
	.recv    = &dev_poll_packet
};

int dev_server_exit(Peer *p)
{
	peer_exit(p);
	return my_close(p->fd);
}

int dev_server_init(Peer *p, const char *device_spec)
{
	int error;
	PortToken hub;
	char name[32];

	error = scan_into(device_spec, ':', name, sizeof(name));
	if (error < 0) return DCERR_PROPERTY_SIZE_MATCH;
	// param = &device_spec[error]; // String after ':'

	p->fd = rawdevice_open(name);
	if (p->fd < 0) {
		netpp_log(DCLOG_ERROR, "Could not open device");
		return DCERR_COMM_INIT;
	}
	error = peer_init(p);
	if (error < 0) return error;

	hub = hub_reuse("DEV#1", &dev_methods);

	if (!IS_VALIDTOKEN(hub)) return DCERR_MALLOC;
	p->hub = desc_fromtoken(hub);

	if (!p->hub) {
		dev_server_exit(p);
		error = DCERR_MALLOC;
	}

	return error;
}



