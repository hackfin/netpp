/** \file slave_test.c
 *
 * (c) 9/2009 Martin Strubel <hackfin@section5.ch>
 *
 */

#include <stdio.h>

#include "slave.h"
#include "dynprops.h"

/* This propery list is statically encoded into the main program,
 * we don't generate it here from an XML description. */

enum {
	device_dummy,
	id2251236,
	MAXPROP_example // = Number of Properties
};

static
PROPINDEX
g_root_example[] = {
	id2251236,
	INVALID_INDEX
};

static
int g_integer = 42;

static
PropertyDesc
g_props_example[] = {

/* ROOT NODE (always on top of the list!) */
	{ "DummyDevice", .flags = F_RO , .type = DC_ROOT,
		DC_STATIC, { .base = 0 } , g_root_example },
	{ "StaticString" /* [1] */, .flags = F_RW,  .type = DC_STRING, 
		DC_STATIC, { .s_string = "This is a minimal slave server sample" },
			0 /* no children */ },
	{ "Integer" /* [2] */, .flags = F_RO, .type = DC_INT, 
		DC_STATIC, { .s_int = 14 },
			0 /* no children */ },
	{ "RWInt" /* [3] */, .flags = F_RW, .type = DC_INT, 
		DC_VAR, { .varp_int = &g_integer },

			0 /* no children */ },
	{ 0 }
};

/* Properties */

enum {
	g_index_example,

	N_DEVICES
};
	

DeviceDesc g_devices[] = {
	{ g_props_example, 0, 0,  MAXPROP_example },
};
	
int g_ndevices = N_DEVICES;

/* We don't need to define device_read() and device_write() functions.
 * They exist in libslave as weak symbols, calling a dummy function.
 */
	
int main(int argc, char **argv)
{
	// Important: Register property list
	TOKEN t;
	TOKEN template;
#ifdef ALLOW_DYNAMIC_PROPERTIES
	dynprop_init(40);

	// Create a root node:
	TOKEN root = new_dynprop("Test", &g_props_example[0]);

	t = new_dynprop("Fitze", &g_props_example[1]);
	dynprop_append(root, t);
	t = new_dynprop("Butze", &g_props_example[2]);
	dynprop_append(root, t);
	t = new_dynprop("Fnarf", &g_props_example[3]);
	dynprop_append(root, t);

	template = t; // Save this as a template to reproduce

	// Only support entity = NULL for DC_VAR type templates:
	t = dynprop_from_entity(root, NULL, template, "Knarf");
	if (t == TOKEN_INVALID) {
		netpp_log(DCLOG_ERROR, "Unable to create dynamic property");
	}

#endif

	register_proplist(g_devices, g_ndevices);

	return start_server(argc, argv);
}

