import ch.s5.netpp.Device;

class NetppDevice
{
	Device device;

	NetppDevice()
	{
		device = null;
	}

	int open(String destination)
	{
		device = new Device();
		return device.open(destination);
	}

	NetppProperty getProperty(String prop)
	{
		return new NetppProperty(device.getProperty(prop));
	}

}
