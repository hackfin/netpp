/** \file netppplus.cpp
 *
 * Rudimentary C++ wrapper.
 *
 * Since there are so many flavours of C++ coding, this class misses some
 * fancy functionality.
 *
 * Some people do not like exceptions. Sorry, we used them.
 *
 */

#include "netppplus.h"
// private header:
#include "property_types.h"
#include <stdio.h>

extern "C"
int dcString_ToValue(DEVICE d, TOKEN p, DCValue *val, const char *outs);

// Non exported, private functions

////////////////////////////////////////////////////////////////////////////
// Value class implementation

int
Value::toString(std::string &str)
{
	int error;
	char buf[64];

	error = dcValue_ToString(this, buf, 64);
	str = buf;
	return error;
}

////////////////////////////////////////////////////////////////////////////
// Property class implementation

Property::Property(Device *d, TOKEN t)
{
	device = d->getDEVICE();
	token = t;
}

int
Property::getName(std::string &s)
{
	int error;
	DCValue v;
	char buf[64];

	v.value.p = buf; v.len = 64;
	v.type = DC_STRING;

	error = dcProperty_GetName(device, token, &v);
	if (error < 0) return error;

	s = (char *) v.value.p;
	
	return error;
}

int
Property::getValue(Value &val)
{
	return dcDevice_GetProperty(device, token, &val);
}

int
Property::getProto(Value &val)
{
	return dcProperty_GetProto(device, token, &val);
}

Property
Property::getTarget(Value &val)
{
	if (val.type != DC_EVENT) return Property();
	return Property(device, val.asUInt());
}

int
Property::findChild(const char *name, Property &child)
{
	int error;
	TOKEN t;

	error = dcProperty_Lookup(device, name, token, &t);
	if (error == 0) 
		child = Property(device, t);
	return error;
}

int
Property::setValue(Value &val)
{
	return dcDevice_SetProperty(device, token, &val);
}

int Property::parseValue(Value &val, const char *s)
{
	return dcString_ToValue(device, token, &val, s);
}

Property
Property::select(Property *prev)
{
	TOKEN t = INVALID_TOKEN;
	int error;

	error = dcProperty_Select(device, token, prev->token, &t);
	if (error < 0) { throw error; }

	if (t != INVALID_TOKEN && error == 0) {
		return Property(device, t);
	} else {
		return Property((DEVICE) 0, TOKEN_INVALID); // invalid property
	}
}

////////////////////////////////////////////////////////////////////////////
// DEVICE code

Device::Device(DEVICE d)
{
	device = d;
}

Device::~Device()
{
	if (device && device != DC_PORT_ROOT) {
		close();
	}
}

int
Device::open(const char *port_name)
{
	return dcDeviceOpen(port_name, &device);
}

int
Device::close()
{
	int error;
	error = dcDeviceClose(device);
	device = 0;
	return error;
}

int Device::getChecksum()
{
    Value v(0);
    int error;
    Property p(this, TOKEN_CHECK);
    error = p.getValue(v);
	if (error < 0) { throw error; }
    return v.value.u;
}

Property
Device::getRoot(void)
{
	TOKEN root;
	int error;

	error = dcDevice_GetRoot(device, &root);
	if (error < 0) { throw error; }
	return Property(device, root);
}

Property
Device::getProperty(const char *name)
{
	TOKEN t;
	int error;

	error = dcProperty_ParseName(device, name, &t);
	// Do not throw exceptions on unknown property.
	// if (error < 0) { throw error; }
	if (t != INVALID_TOKEN) {
		return Property(device, t);
	} else {
		return Property((DEVICE) 0, INVALID_TOKEN); // invalid property
	}
}


int
Device::getName(std::string &s)
{
	int error;
	DCValue v;
	char buf[64];
	TOKEN root;

	v.value.p = buf; v.len = 64;

	error = dcDevice_GetRoot(device, &root);
	if (error < 0) return error;

	error = dcProperty_GetName(device, root, &v);
	if (error < 0) return error;

	s = buf;
	return error;
}

#ifdef TEST_MAIN

int handleError(int error)
{
	fprintf(stderr, "Error: %d : %s\n", error, dcGetErrorString(error));
	return error;
}

#define BUFLEN 0x8000

const char *getTypeString(int type)
{
	switch (type) {
		case DC_INVALID: return "<invalid>";
		case DC_EVENT: return "<event>";
		case DC_BOOL: return "bool";
		case DC_UINT: return "uint";
		case DC_INT: return "int";
		case DC_FLOAT: return "float";
		case DC_STRING: return "string";
		case DC_BUFFER: return "buffer";
		case DC_MODE: return "mode";
		case DC_COMMAND: return "cmd";
		case DC_ARRAY: return "array";
		case DC_STRUCT: return "struct";
		default: return "<unknown>";
	}
}

int list_children(Property &p, int recursive)
{
	int error;
	std::string name;
	std::string value;
	unsigned char buf[BUFLEN];
	const char *typestring;
	Property child;
	Value val;
	int i;

	child = p.getChild();

	while (child.isValid()) {
		child.getName(name);

		i = recursive;
		for (i = 0; i < recursive; i++) {
			printf("   ");
		}

		// Find out what type we expect:
		error = child.getProto(val);
		if (error < 0) return error;

		// If we expect a buffer, reserve some memory:
		if (val.type == DC_BUFFER || val.type == DC_STRING) {
			val = Value(buf, BUFLEN);
		}
		typestring = getTypeString(val.type);

		error = child.getValue(val);
		if (error == DCERR_PROPERTY_HANDLER) {
			value = "[No value]";
			error = 0; // We ignore this error
		} else if (error < 0) {
			handleError(error);
			value = "[err]";
		} else {
			val.toString(value);
		}
		printf("Child: [%08lx] '%s' (%s): %s\n",
			child.getToken(), name.c_str(), typestring,
			value.c_str());

		if (recursive) {
			list_children(child, recursive + 1);
			if (recursive > 8) {
				printf("Too deeply nested. Something wrong.\n");
				return -1;
			}
		}
		child = p.getNext(child); // iterate to next
	}
	return error;
}

void setProperty(Property &p, int value)
{
	int error;
	Value v(value);
	std::string name;
	std::string valuestring;

	error = p.setValue(v);
	if (error < 0) handleError(error);
	error = p.getValue(v);
	if (error < 0) handleError(error);

	p.getName(name);
	v.toString(valuestring);

	printf("Current value of %s is: %s\n", name.c_str(), valuestring.c_str());
}

int main(int argc, char **argv)
{
	int error;
	class Device d;

	const char *url = "TCP:localhost:2008";

	Property root;
	Property hubs(DC_PORT_ROOT, 0);

	if (argc > 1) {
		url = argv[1];
	}


#ifdef TEST_BROADCAST
	printf("----------------- HUBS ------------------\n");

	try {
		list_children(hubs, 1);
	}

	catch (int error) {
		handleError(error);
		return -1;
	}
#endif

	printf("Opening %s...\n", url);
	error = d.open(url);
	if (error < 0) return handleError(error);

	printf("----------------- PROPS -----------------\n");

	root = d.getRoot();

	try {
		error = list_children(root, 1);
		if (error < 0) handleError(error);

		Property x, y, failing;

		x = d.getProperty("Stream.X");
		y = d.getProperty("Stream.Y");

		if (x.isValid() and y.isValid()) {
			setProperty(x, 640);
			setProperty(y, 480);
		} else {
			fprintf(stderr, "Properties not found\n");
		}
	}

	catch (int error) {
		handleError(error);
	}

	d.close();

	return 0;
}


#endif
