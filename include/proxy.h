/** \file
 *
 * Proxy generic functions
 *
 * Used by device drivers that allow proxying.
 *
 */

#include "devlib.h"
#include "devlib_error.h"
#include "property_protocol.h"

#define THIS_DEVICE_TOKEN(d, m) ((struct proxy_tokens *) d->localdata)->m

struct proxy_tokens {
	TOKEN scan;
	TOKEN open;
	TOKEN close;
	TOKEN peerid;
	TOKEN version;
};

int proxytokens_new(DEVICE d);
void proxytokens_delete(DEVICE d);
int proxytokens_init(DEVICE d, struct proxy_tokens *t);
int parse_endpoint(const char *name, char *endpoint, int len);
/** Generic proxy open functionality.
 * \param rd       Remote device handle
 * \param prefix   Prefix used by underlying logical interface driver
 * \param name     Proxy device specification (device node name)
 */
int proxy_open(DEVICE rd, const char *prefix, char *name);
/** Close proxy */
int proxy_close(DEVICE d);
