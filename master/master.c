#include <stdlib.h>
#include <libgen.h>

#include "conffile.h"
#include "master_util.h"


// PROTOS

extern const char version_string[];


////////////////////////////////////////////////////////////////////////////


int main_cli(int argc, char **argv);

int handleError(int error)
{
	const char *s;

	s = dcGetErrorString(error);
	if (error < 0) {
		fprintf(stderr, "Error %d: %s\n", error, s);
	} else {
		fprintf(stderr, "Warning %d: %s\n", error, s);
	}
	return error;
}


char code_fromtype(DCValue *v)
{
	char code;
	switch (v->type) {
		case DC_INVALID: code = v->value.i; break;
		case DC_ARRAY:   code = 'A'; break;
		case DC_STRUCT:  code = 'S'; break;
		case DC_MODE:    code = 'm'; break;
		case DC_BOOL:    code = 'b'; break;
		case DC_COMMAND: code = 'c'; break;
		case DC_STRING:  code = 's'; break;
		case DC_BUFFER:  code = 'B'; break;
		case DC_UINT:    code = 'u'; break;
		case DC_INT:     code = 'i'; break;
		case DC_FLOAT:   code = 'f'; break;
		default: code = 'p';
	}
	return code;
}

int list_children(DEVICE d, TOKEN t, int recursive)
{
	int error;
	char propname[64];
	DCValue val;
	TOKEN child;
	int i;
	char code;

	error = dcProperty_Select(d, t, t, &child);
	while (error == 0) {
		val.type = DC_BUFFER; val.len = 0;
		error = dcProperty_GetProto(d, child, &val);
		if (error < 0) {
			if (error == DCERR_PROPERTY_SIZE_MATCH) {
				DEB(printf("Buffer size adjustment: %d\n", val.len));
			} else {
				fprintf(stderr, "failed to get Property prototype\n");
				break;
			}
		}
		code = code_fromtype(&val);
		val.value.p = (char *) propname; val.len = sizeof(propname);
		error = dcProperty_GetName(d, child, &val);
		if (error < 0) return error;
		// Use val.value.p rather than propname in next line, because
		// callee may have changed it (e.g. for static port descriptors)
		i = recursive;
		for (i = 0; i < recursive; i++) {
			printf("   ");
		}
		printf("Child: (%c) [%08x] '%s'\n", code, child, (char *) val.value.p);

		if (recursive) {
			error = list_children(d, child, recursive + 1);
		}
		error = dcProperty_Select(d, t, child, &child); // iterate to next
	}
	return error;
}

static
char *g_types[] = {
	"Invalid value",
	"ROOT NODE",
	"Integer",
	"Float",
	"Boolean",
	"Mode",
	"Register",
	"String",
	"Buffer",
	"{Struct}",
	"{Array}",
	"[Command]",
	"[Event]",
	"<unknown>"
};

int print_type(DCValue *val)
{

	unsigned short tp;
	int n = sizeof(g_types) / (sizeof(char *));

	tp = val->type;
	printf("Type : ");
	if (tp == DC_STRING || tp == DC_BUFFER) {
		printf("%s[%lu] [%c%c%c]\n", g_types[tp], val->len,
		(val->flags & F_WO) ? '.' : 'R',
		(val->flags & F_RO) ? '.' : 'W',
		(val->flags & F_VOLATILE) ? 'V' : '.');
	} else {
		if (tp & DC_UNSIGNED) {
			printf("Unsigned ");
		}
		tp &= ~DC_UNSIGNED;
		if (tp >= n) tp = n-1;
		printf("%s [%c%c%c]\n", g_types[tp & 0xf],
			(val->flags & F_WO) ? '.' : 'R',
			(val->flags & F_RO) ? '.' : 'W',
			(val->flags & F_VOLATILE) ? 'V' : '.');
	}
	return 0;
}

void hexdump(unsigned char *buf, unsigned long n)
{
	int i = 0;
	int c = 0;

	while (i < n) {
		printf("%02x ", buf[i]);
		c++;
		if (c == 16) { c = 0; printf("\n"); }
		i++;
	}
	if (c)
		printf("\n");
}

int dump_value(DEVICE d, TOKEN p, DCValue *val)
{
	int error;
	int warn = 0;
	char *buf;
	char valstr[80];

	int len = 1024; // Assume 1 kB is enough
		
	buf = (char *) malloc(len);
	if (!buf) return DCERR_MALLOC;

	val->type = DC_BUFFER; // assume buffer by default
	val->value.p = buf; val->len = len;

	error = dcDevice_GetProperty(d, p, val);
	if (val->type == DC_ROOT) {
		error = dcProperty_GetName(d, p, val);
	}


	// This is a complicated one:
	// - Handlers tolerating greater client side buffers just
	// send a DCWARN_PROPERTY_MODIFIED.
	// - Insufficient buffer size will cause a realloc and retry
	switch (error) {
		case DCERR_PROPERTY_SIZE_MATCH:
			if (( val->type == DC_STRING || val->type == DC_BUFFER)
				&& (len < val->len)) {
					printf("Received new size, reallocate: %ld\n", val->len);
					buf = (char *) realloc(buf, val->len);
					val->value.p = buf;
					if (!buf) return DCERR_MALLOC;
				// And retry:
				error = dcDevice_GetProperty(d, p, val);
			}
		default:
			if (error < 0) break;
		case DCWARN_PROPERTY_MODIFIED:
		case 0:
			warn = dcValue_ToString(val, valstr, sizeof(valstr));
			if (val->type == DC_STRING && valstr[val->len-1] != '\0') {
				fprintf(stderr,
					"Warning: target didn't send 0-terminated string\n");
				if (val->len < sizeof(valstr)) {
					valstr[val->len] = '\0';
				} else {
					valstr[val->len-1] = '\0';
					printf("(truncated)");
				}

				printf("Value: %s\n", valstr);
			} else 
			printf("Value: %s\n", valstr);
	}

	if (error >= 0 && val->type == DC_BUFFER) {
		if (val->len > 0x20000) {
			fprintf(stderr, "Buffer bigger than 128k, not dumping\n");
		} else
		if (val->len == 0) {
			fprintf(stderr, "Zero buffer size returned\n");
		} else {
			hexdump(val->value.p, val->len);
		}
	}

	free(buf);

	if (error < 0) return error;
	return warn;
}

int handle_file_protocol(DEVICE d, TOKEN p, const char *str)
{
	char *buf;
	int size = 65536;
	FILE *f;
	TOKEN name, open, data, close;

	int ret;

	DCValue val;
	int error;
	name = p; // Search in this parent
	error = dcProperty_ParseName(d, ".Name", &name);
	if (error < 0) return error;
	open = p; // Search in this parent
	error = dcProperty_ParseName(d, ".Open", &open);
	if (error < 0) return error;
	data = p; // Search in this parent
	error = dcProperty_ParseName(d, ".Data", &data);
	if (error < 0) return error;
	close = p; // Search in this parent
	error = dcProperty_ParseName(d, ".Close", &close);
	if (error < 0) return error;

	printf("Open remote file...\n");
	val.value.p = (void *) str;
	val.len = strlen(str) + 1;
	val.type = DC_STRING;
	error = dcDevice_SetProperty(d, name, &val); // RPC "name"
	if (error < 0) {
		return error;
	} 

	val.type = DC_INT; val.value.i = 0; // read mode
	error = dcDevice_SetProperty(d, open, &val); // RPC "open()"
	if (error < 0) {
		fprintf(stderr, "Failed to open file %s on target\n", str);
		return error;
	} 

	val.type = DC_STRING;

	buf = (char *) malloc(size);
	if (!buf) return DCERR_MALLOC;

	// Retrieve data:
	val.value.p = buf;
	val.len = size;

	f = fopen(str, "w");

	// First attempt to adjust the buffer size:
	error = dcDevice_GetProperty(d, data, &val);
	if (error == DCWARN_PROPERTY_MODIFIED) error = 0;

	ret = fwrite(buf, 1, val.len, f);
	if (ret < 0) {
		perror("Local file write error");
		error = -1;
	}

	while (error == 0) {
		error = dcDevice_GetProperty(d, data, &val);
		ret = fwrite(buf, 1, val.len, f);
		if (ret < 0) {
			perror("Local file write error");
			error = -1;
		}
	}

	val.type = DC_INT; val.value.i = 1;
	error = dcDevice_SetProperty(d, close, &val); // RPC "close"

	fclose(f);
	free(buf);
	return error;
}

int handle_meta_protocol(DEVICE d, TOKEN p, const char *str)
{
	int error;
	DCValue val;
	char buf[128];
	val.value.p = (void *) buf;
	val.len = sizeof(buf);

	error = dcProperty_GetName(d, p, &val);
	if (error < 0) return error;
	error = DCERR_PROPERTY_HANDLER;
	// See if it's a file, then try simple file protocol:
	if (strcmp(buf, "File") == 0) {
		error = handle_file_protocol(d, p, str);
	}
	return error;
}

int value_from_string(DEVICE d, TOKEN p, const char *str)
{
	int error;
	DCValue val;
	char buf[32];
	
	error = dcProperty_GetProto(d, p, &val);
	if (error < 0) return error;

	error = DCERR_PROPERTY_HANDLER;

	switch (val.type) {
		case DC_BOOL:
		case DC_UINT:
		case DC_INT:
		case DC_COMMAND:
			if ( sscanf(str, "0x%x", &val.value.i) == 1 ||
			     sscanf(str, "%d", &val.value.i) == 1 )
			{
				error = 0;
			}
			break;
		case DC_MODE:
			if (sscanf(str, "%d", &val.value.i) == 1 ) {
				error = 0;
			}
			break;
		case DC_FLOAT:
			if (sscanf(str, "%f", &val.value.f)) {
				error = 0;
			}
			break;
		case DC_STRING:
			val.value.p = (void *) str;
			val.len = strlen(str) + 1;
			error = 0;
			break;
		case DC_BUFFER:
			// Create one byte buffer from hex value
			val.len = 1;
			if (sscanf(str, "0x%x", &val.value.i)) {
				error = 0;
				buf[0] = val.value.i;
				val.value.p = (void *) buf;
			} else { // interpret as string
				val.value.p = (void *) str;
				val.len = strlen(str) + 1;
				error = 0;
			}
			break;
		case DC_STRUCT:
			// Special meta protocol handlers:
			return handle_meta_protocol(d, p, str);
		default:
			printf("(YET) Unsupported data type\n");
	}

	if (error == 0) {
		error = dcDevice_SetProperty(d, p, &val);
	}

	return error;
}

int proplist_crc(DEVICE d)
{
	int error;
	DCValue val;
	val.len = 0; val.type = DC_INVALID;
	error = dcDevice_GetProperty(d, TOKEN_CHECK, &val);
	if (error == DCERR_PROPERTY_UNKNOWN) {
		printf("Peer does not have checksum feature\n");
	} else {
		printf("Checksum: %04x\n", (unsigned short) val.value.i);
	}
	return error;
}

int proplist_nprops(DEVICE d)
{
	DCValue val;
	int error;

	val.len = 0; val.type = DC_INVALID;
	error = dcDevice_GetProperty(d, TOKEN_NPROPS, &val);

	if (error == DCERR_PROPERTY_UNKNOWN) {
		printf("Peer does not have property count feature\n");
		error = 0;
	} else {
		printf("Number of properties: %0d\n", val.value.i);
	}
	return error;
}

int query_prop(DEVICE d, TOKEN t, DCValue *val)
{
	int error;
	error = list_children(d, t, 0);
	if (error >= 0) {
		error = dump_value(d, t, val);
	}
	if (error >= 0) {
	} else if (error == DCERR_PROPERTY_HANDLER) {
		error = 0; // Ignore this one.
	}
	return error;
}

int main_master(int argc, char **argv)
{
	int error;
	TOKEN t, root;
	DCValue val; val.type = DC_INVALID;
	DEVICE d;
	char rootname[32];

	if (argc < 2) {
		fprintf(stderr,
			"\nnetpp version %s\n", version_string);
		fprintf(stderr, "\nUsage: %s [<target>] [<property>] [<value>]\n",
			argv[0]);
		fprintf(stderr,
			"       <target>   : <hub>:<port> e.g. TCP:127.0.0.1:2008\n");
		fprintf(stderr,
			"       <property> : a property name\n");
		fprintf(stderr,
			"       <value>    : a value. The format of the value must\n"
			"                    match its type, see below.\n\n");          
		fprintf(stderr,
			"Depending on the number of arguments (shown in [ ]), this netpp master can:\n"
			"[0] Show usage and list available hubs/ports\n"
			"[1] Show property list of specified target\n"
			"[2] Get value of specified property\n"
			"[3] Set property to specified value\n\n");
		fprintf(stderr,
			"Meta protocol support (struct properties):\n"
			"  File <filename>     - retrieve file and store locally, e.g.:\n"
			"                        %s <target> File test.log\n", argv[0]);
#ifdef HAVE_TIME_PROTOCOL
		fprintf(stderr,
			"  Time 'now'          - sync time on target with master\n");

#endif
		printf("\nAvailable Hubs/Targets:\n");
		error = list_children(DC_PORT_ROOT, INVALID_TOKEN, 1);
		// Final cleanup:
		dcDeviceClose(DC_PORT_ROOT);
		if (error < 0) return -1;
		return 0;
	}

	// open Device
	error = dcDeviceOpen(argv[1], &d);
	if (error < 0) {
		handleError(error);
		return -1;
	}

	error = dcDevice_GetRoot(d, &root);
	if (error < 0) {
		printf("Failed to get device root node\n");
		handleError(error);
		goto abort;
	}

	if (argc >= 3) {
		// Preinitialize, we don't know what we get in argv:
		t = INVALID_TOKEN;
		error = dcProperty_ParseName(d, argv[2], &t);
		if (error) {
			printf("Property not found or bad index\n");
			goto abort;
		} else {
			val.value.p = NULL; // do not query name
			error = dcProperty_GetProto(d, t, &val);
			if (error) handleError(error);
			else {
				error = print_type(&val);
				if (error) handleError(error);
			}
		}

		if (argc == 3) {
			error = query_prop(d, t, &val);
			if (error) { handleError(error); }
		} else
		if (argc == 4) {
			error = value_from_string(d, t, argv[3]);
			if (error) {
				handleError(error);
			}
		} else {
			fprintf(stderr, "Wrong number of arguments\n");
		}
		goto abort;
	} else {
		// Show the CRC:
		error = proplist_crc(d);
		if (error < 0) {
			handleError(error);
			goto abort;
		}

		error = proplist_nprops(d);
		if (error < 0) {
			handleError(error);
			goto abort;
		}

		const char *name;
		error = dcDevice_GetHubName(d, &name);
		if (error < 0) {
			handleError(error);
			goto abort;
		}
		val.value.p = rootname;
		val.len = sizeof(rootname);
		error = dcProperty_GetName(d, root, &val);
		if (error == 0) {
			printf("Properties of Device '%s' associated with Hub '%s':\n",
				(char *) val.value.p, name);
		}
		error = list_children(d, root, 0);
		if (error < 0) {
			handleError(error);
			goto abort;
		}
	}

abort:
	dcDeviceClose(d);
	dcDeviceClose(DC_PORT_ROOT);
	return error;
}

int my_add_group(void *c, const char *name)
{
	printf("ADD GROUP: %s\n", name);
	return 0;
}

static DEVICE g_curdev;

int my_set_property(void *c, const char *name, const char *val)
{
	int error;
	TOKEN t;
	printf("Set property %s to %s\n", name, val);
	error = dcProperty_ParseName(g_curdev, name, &t);
	if (error < 0) {
		handleError(error);
		return error;
	}
	return value_from_string(g_curdev, t, val);
}

int main_config(int argc, char **argv)
{
	BufStream s;
	ConfHandler h;
	int error;
	DEVICE d;

	h.add_group = my_add_group;
	h.set_property = my_set_property;

	if (argc < 3) {
		fprintf(stderr, "\nUsage: %s <target> <inifile>\n",
			argv[0]);
		return -1;
	}

	error = dcDeviceOpen(argv[1], &d);
	if (error < 0) {
		handleError(error);
		return -1;
	}
	
	g_curdev = d;

	s.file = fopen(argv[2], "r");
	if (s.file) {
		s.bufsize = 128;
		s.buf[0] = (char *) malloc(s.bufsize);

		error = conf_parse(&s, &h);
		if (error <= 0) {
			printf("Failed to parse conf file\n");
		}

		fclose(s.file);
		free(s.buf[0]);
	} else {
		printf("Could not open %s\n", argv[2]);
	}
	dcDeviceClose(d);
	return 0;
}

int main_dump(int argc, char **argv)
{
	DEVICE d;
	int error;
	TOKEN root;
	FILE *f = stdout;
	char propname[256];

	if (argc < 2) {
		fprintf(stderr, "\nUsage: %s <target> [<inifile>]\n",
			argv[0]);
		fprintf(stderr,
			"       <target>   : <hub>:<port> e.g. TCP:127.0.0.1:2008\n");
		fprintf(stderr,
			"       <inifile>  : e.g. defaults.ini\n\n");          
		return 0;
	} else
	if (argc == 2) {
		f = stdout;
	} else {
		f = fopen(argv[2], "w");
		if (!f) {
			printf("Unable to open file, using stdout\n");
			f = stdout;
		}
	}

	error = dcDeviceOpen(argv[1], &d);
	if (error < 0) {
		handleError(error);
		return -1;
	}

	error = dcDevice_GetRoot(d, &root);
	if (error < 0) {
		printf("Failed to get device root node\n");
		handleError(error);
		goto abort;
	}

	DCValue val;

	val.value.p = propname; val.len = sizeof(propname);
	dcProperty_GetName(d, root, &val);

	fprintf(f, "[%s]\n", (char *) val.value.p);

	error = dcDevice_GetProperty(d, TOKEN_CHECK, &val);
	fprintf(f, "# CRC16 = %04x\n", val.value.i);
	error = dcDevice_GetProperty(d, TOKEN_NPROPS, &val);
	fprintf(f, "# NumProperties = %d\n", val.value.i);

	// fprintf(f, "[TokenCache]\n");
	// error = dump_children(f, d, root, propname, propname, sizeof(propname), 1);

	fprintf(f, "[Properties]\n");
	error = dump_children(f, d, root, propname, propname, sizeof(propname), 0);

abort:
	dcDeviceClose(d);

	if (f != stdout) fclose(f);

	return error;
}

#ifdef __WIN32__
#define EXE ".exe"
#else
#define EXE
#endif

int main(int argc, char **argv)
{
	const char *pname;
	char path[256];

	strncpy(path, argv[0], sizeof(path)-1);
	path[sizeof(path)-1] = '\0';

	pname = basename(path);

	if (strcmp(pname, "netpp-dump" EXE) == 0) {
		return main_dump(argc, argv);
	} else
	if (strcmp(pname, "netpp-config" EXE) == 0) {
		return main_config(argc, argv);
	} else

#ifdef CONFIG_INTERACTIVE
	if (strcmp(pname, "netpp-cli" EXE) == 0) {
		return main_cli(argc, argv);
	} else
	if (strcmp(pname, "local-cli" EXE) == 0) {
		return main_cli(argc, argv);
	} else
#endif
		return main_master(argc, argv);
}

