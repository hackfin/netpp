/* Drawing routines for display server
 *
 * (c) 2005-2011, Martin Strubel <strubel@section5.ch>
 *
 */

#include "display.h"
#include "yuv2rgb.h"
#include <math.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

// These are needed for user index access:
#include "property_protocol.h"
#include "device_error.h"

#define PIXELFORMAT const struct SDL_PixelFormat * const 

// Use a dirty little hack to generate the strings:
char *g_videomodes[] = 
#define GENERATE_MODE_STRINGS
#include "vkit/videomodes.h"

////////////////////////////////////////////////////////////////////////////
//

int draw_frame(struct virtfb_t *vfb)
{
	int n;
	Uint32 *fbuf;
	uint8_t *data = (uint8_t *) vfb->rawdata;
	PIXELFORMAT format = vfb->screen->format;

	fbuf = (Uint32 *) vfb->screen->pixels;

	n = vfb->npixels;
	while (n--) {
		*fbuf++ = SDL_MapRGB(format, *data, *data, *data);
		data++;
	}
	
	return 0;
}

int draw_frame_rgb(struct virtfb_t *vfb, uint8_t *data)
{
	int n;
	Uint32 *fbuf;

	SDL_Surface *screen = vfb->screen;

	fbuf = (Uint32 *) screen->pixels;

	n = vfb->npixels;
	while (n--) {
		*fbuf++ = SDL_MapRGB(screen->format, data[0], data[1], data[2]);
		data += 3;
	}

	return 0;
}


// Dirty hack to create 8 and 16 bit version of the bayer code
//

#define PIXEL(x) x
#define bayer_linear bayer_linear8
#define BAYER_DATA uint8_t
#define BAYER_BITDEPTH 8

#include "bayer.c"

#undef PIXEL
#undef bayer_linear
#undef BAYER_DATA
#undef BAYER_BITDEPTH

#define PIXEL(x) ((x) & 0x3ff)
#define bayer_linear bayer_linear16
#define BAYER_DATA uint16_t
#define BAYER_BITDEPTH 16

#include "bayer.c"


int draw_frame_bayer8(struct virtfb_t *vfb)
{
	bayer_linear8((Uint32 *) vfb->screen->pixels,
		(uint8_t *) vfb->rawdata, vfb->width, vfb->height);
	return 0;
}

int draw_frame_bayer16(struct virtfb_t *vfb)
{
	bayer_linear16((Uint32 *) vfb->screen->pixels,
		(uint16_t *) vfb->rawdata, vfb->width, vfb->height);
	return 0;
}


#define CLAMP(col) \
	col = (col < 0 ? 0: col); col = (col > 0xff ? 0xff : col);


int draw_frame_uyvy(struct virtfb_t *vfb)
{
	Uint32 *fbuf;
	uint8_t *data = (uint8_t *) vfb->rawdata;
	short r, g, b;
	unsigned short x, y;

	fbuf = (Uint32 *) vfb->screen->pixels;
	PIXELFORMAT format = vfb->screen->format;

	y = vfb->height;
	while (y--) {
		x = vfb->width;
		while (x) {
			register int y;
			y = tab_y[data[1]];
			r = y + tab_rv[data[2]];
			g = tab_gu[data[0]] + y + tab_gv[data[2]];
			b = tab_bu[data[0]] + y;
			CLAMP(r); CLAMP(g); CLAMP(b);
			// r >>= 4; g >>= 4; b >>= 4;
			*fbuf++ = SDL_MapRGB(format, r, g, b);
			y = tab_y[data[3]];
			r = y + tab_rv[data[2]];
			g = tab_gu[data[0]] + y + tab_gv[data[2]];
			b = tab_bu[data[0]] + y;
			CLAMP(r); CLAMP(g); CLAMP(b);
			// r >>= 4; g >>= 4; b >>= 4;
			*fbuf++ = SDL_MapRGB(format, r, g, b);

			data += 4; x -= 2;
		}
	}
 	return 0; 
}

int draw_frame_yuyv(struct virtfb_t *vfb)
{
	Uint32 *fbuf;
	uint8_t *data = (uint8_t *) vfb->rawdata;
	short r, g, b;
	unsigned short x, y;

	fbuf = (Uint32 *) vfb->screen->pixels;
	PIXELFORMAT format = vfb->screen->format;

	y = vfb->height;
	while (y--) {
		x = vfb->width;
		while (x) {
			register int y;
			y = tab_y[data[0]];
			r = y + tab_rv[data[3]];
			g = tab_gu[data[1]] + y + tab_gv[data[3]];
			b = tab_bu[data[1]] + y;
			CLAMP(r); CLAMP(g); CLAMP(b);
			// r >>= 4; g >>= 4; b >>= 4;
			*fbuf++ = SDL_MapRGB(format, r, g, b);
			y = tab_y[data[2]];
			r = y + tab_rv[data[3]];
			g = tab_gu[data[1]] + y + tab_gv[data[3]];
			b = tab_bu[data[1]] + y;
			CLAMP(r); CLAMP(g); CLAMP(b);
			// r >>= 4; g >>= 4; b >>= 4;
			*fbuf++ = SDL_MapRGB(format, r, g, b);

			data += 4; x -= 2;
		}
	}
 	return 0; 
}


int draw_frame_jpeg(struct virtfb_t *vfb)
{
	// This page is intentionally left blank (I'm afraid)

	return 0;
}

int draw_frame_indexed16(struct virtfb_t *vfb)
{
	int n;
	Uint32 *fbuf;
	uint8_t *rgb;
	PIXELFORMAT format = vfb->screen->format;
	uint16_t *data = (uint16_t *) vfb->rawdata;

	fbuf = (Uint32 *) vfb->screen->pixels;

	n = vfb->npixels;
	while (n--) {
		rgb = &vfb->lut[(*data++) * 3];
		*fbuf++ = SDL_MapRGB(format, rgb[0], rgb[1], rgb[2]);
	}
  
	return 0;
}

void draw_histo(struct virtfb_t *vfb, uint32_t *bins)
{
	int i, j;
	uint32_t max = 0;
	Uint32 *fbuf, *walk;
	PIXELFORMAT format = vfb->screen->format;

	for (i = 0; i < 256; i++) {
		if (bins[i] > max) max = bins[i];
	}

	SDL_Surface *screen = vfb->screen;

	if (SDL_MUSTLOCK(screen)) SDL_LockSurface(screen);

	fbuf = (Uint32 *) screen->pixels;
	
	fbuf += vfb->width * (vfb->height - 1);

	for (i = 0; i < 256; i++) {
		j = 0;
		walk = fbuf;
		while ((float) j < 256.0 * bins[i] / (float) max) {
			*walk = SDL_MapRGB(format, 255, 0, 0);
			walk -= vfb->width;
			j++;
		}
			
		fbuf++;
	}


	if(SDL_MUSTLOCK(screen)) SDL_UnlockSurface(screen);
}

static
void draw_markers(struct virtfb_t *vfb)
{
	SDL_Surface *marker = vfb->marker_bmp;
	SDL_Rect dstr;
	int i;

	if (!marker) return;

	dstr.w = marker->w;
	dstr.h = marker->h;

	uint16_t *markers = vfb->marker;

	for (i = 0; i < vfb->nmarkers; i++) {
		dstr.x = *markers++ - dstr.w / 2;
		dstr.y = *markers++ - dstr.h / 2;
		SDL_BlitSurface(marker, NULL, vfb->screen, &dstr);
	}
}

////////////////////////////////////////////////////////////////////////////
// Auxiliaries

// Special color codes for indexed display
#define CODE_RED	0xfffe
#define CODE_BLUE	0xfffd
#define CODE_YELLOW 0xfffc
#define CODE_GREEN	0xfffb
#define CODE_GRAY	0xfffa
#define CODE_WHITE	0xfff0

static uint8_t icols[16][3] = {
	{ 220, 145, 145 },
	{ 151, 221, 146 },
	{ 142, 216, 185 },
	{ 143, 196, 211 },
	{ 144, 154, 206 },
	{ 175, 148, 204 },
	{ 206, 150, 177 },
	{ 207, 164, 153 },
	{ 228, 229, 146 },
	{ 249, 165, 96 },
	{ 100, 255, 99 },
	{ 101, 255, 204 },
	{ 102, 196, 255 },
	{ 103, 107, 255 },
	{ 174, 101, 255 },
	{ 255, 101, 244 }
};


#define SET_RGB(rgb, u, v, w) \
	rgb[0] = u; rgb[1] = v; rgb[2] = w;

void hwb2rgb(uint16_t hwb[3], uint8_t rgb[3])
{
	short h, w, b;
	short u, v;
	short i, f;

	h = hwb[0]; w = hwb[1]; b = hwb[2];

	v = 255 - b;
	i = (h & 0x0f00) >> 8;
	f = h & 0xff;

	if (i & 1)
		f = 255 - f;
	u = w + ((f * (v - w)) >> 8);

	switch (i) {
		case 0:  SET_RGB(rgb, w, u, v); break; // blue to cyan
		case 1:  SET_RGB(rgb, w, v, u); break; // green to cyan
		case 2:  SET_RGB(rgb, u, v, w); break; // green to yellow
		case 3:  SET_RGB(rgb, v, u, w); break; // red to yellow
		case 4:  SET_RGB(rgb, v, w, u); break; // red to magenta
		case 5:  SET_RGB(rgb, u, w, v); break; // blue to magenta
		default:  SET_RGB(rgb, 127, 127, 127);
	}
}

uint16_t gamma16(uint16_t v)
{
	float f = (float) v / 65535.0;
	f = 65535.0 * powf(f, g_config.gamma);
	return (uint16_t) f;
}

void convert_pseudo16(uint16_t v, uint8_t rgb[3])
{
	uint16_t hwb[3];

	if (v < 256) {
		hwb[0] = 255 - v;
		hwb[1] = 0;
		hwb[2] = 255 - v / 2;
	} else {
		v -= 256;
		v >>= 4;
		if (v < 0x600) {
			hwb[0] = v;
			hwb[1] = 0;
			hwb[2] = 127 - v / 12;
		} else
		if (v < 0xc00) {
			v -= 0x600;
			hwb[0] = v;
			hwb[1] = v / 12;
			hwb[2] = 0;
		} else {
			v -= 0xc00;
			hwb[0] = 0;
			hwb[1] = 127 + v / 8;
			hwb[2] = 0;
		}
	}
	hwb2rgb(hwb, rgb);
}

void convert_pseudo(uint16_t v, uint8_t rgb[3])
{
	return convert_pseudo16(gamma16(v), rgb);
}

void convert_pseudo_15bit(uint16_t v, uint8_t rgb[3])
{
	if (v > 0x7fff) {
		rgb[0] = 127; rgb[1] = 127; rgb[2] = 127;
	} else {
		convert_pseudo16(gamma16(v << 1), rgb);
	}
}

void indexed_convert(uint16_t val, uint8_t rgb[3])
{
	unsigned char *icol;

	if (val > 0 && val < 0xf000) {
		icol = icols[val & 0xf];
		rgb[0] = icol[0]; rgb[1] = icol[1]; rgb[2] = icol[2];
	} else {
		switch (val) {
			case CODE_BLUE:
				rgb[0] = 0; rgb[1] = 0; rgb[2] = 255; break;
			case CODE_RED:
				rgb[0] = 255; rgb[1] = 0; rgb[2] = 0; break;
			case CODE_YELLOW:
				rgb[0] = 255; rgb[1] = 255; rgb[2] = 0; break;
			case CODE_GREEN:
				rgb[0] = 0; rgb[1] = 255; rgb[2] = 0; break;
			case CODE_GRAY:
				rgb[0] = 127; rgb[1] = 127; rgb[2] = 127; break;
			case 0:
				rgb[0] = 0; rgb[1] = 0; rgb[2] = 0; break;
			default:
				rgb[0] = 255; rgb[1] = 255; rgb[2] = 255;
		}
	}
}

void lut_update(VirtFB *vfb)
{
	unsigned int i;
	unsigned int j;
	void (*convert)(uint16_t val, uint8_t rgb[3]);

	if (!vfb->lut) {
		printf("LUT Update command not effective when stream stopped\n");
		return;
	}
	
	switch (vfb->preset) {
		case LUT_CUSTOM:
			return;
		case LUT_REGION:
			convert = indexed_convert;
			break;
		case LUT_PSEUDO:
			convert = convert_pseudo;
			break;
		case LUT_PSEUDO_15BIT:
			convert = convert_pseudo_15bit;
			break;
		default:
			convert = convert_pseudo_15bit;
	}
	j = 0;
	for (i = 0; i < 0x10000; i++) {
		convert(i, &vfb->lut[j]);
		j += 3;
	}

}

int lut_init(struct virtfb_t *vfb)
{
	if (vfb->lut) return 1;

	vfb->lut = (unsigned char *) malloc(LUT16_SIZE);
	if (!vfb->lut) return -1;

	lut_update(vfb);

	return 0;
}

int init_drawcallback(VirtFB *vfb)
{
	vfb->bpp = g_config.bpp;
	vfb->mode = g_config.mode;

	int (*update)(VirtFB *vfb);

	switch (vfb->bpp) {
	case 8:
		switch (vfb->mode) {
			case MODE_BAYER:
				update = &draw_frame_bayer8;
				break;
			case MODE_JPEG_RAW:
			case MODE_JPEG:
				update = &draw_frame_jpeg;
				break;
			case MODE_RAW:
			case MODE_GRAY:
				update = &draw_frame;
				break;
			default:
				printf("Unknown/Unsupported video mode in 8 bit: %d\n",
					vfb->mode);
				return -1;
		}
		break;
	default:
		if (vfb->bpp > 7 && vfb->bpp <= 16) {
			switch (vfb->mode ) {
				case MODE_YUYV:
					update = &draw_frame_yuyv;
					break;
				case MODE_UYVY:
					update = &draw_frame_uyvy;
					break;
				case MODE_BAYER:
					update = &draw_frame_bayer16;
					break;
				default:
					update = &draw_frame_indexed16;
			}
		} else {
			printf("%d bits per pixel not supported in mode %d\n",
				g_config.bpp, vfb->mode);
			return -1;
		}
	}
	vfb->update = update;
	return 0;
}
	
int sdl_update(struct virtfb_t *vfb)
{
	SDL_Surface *screen = vfb->screen;

	// LOCK
	if (SDL_MUSTLOCK(screen) && SDL_LockSurface(screen) < 0) return -1;

	if (vfb->update) {
		vfb->update(vfb);
	}

	draw_markers(vfb);

	if(SDL_MUSTLOCK(screen)) SDL_UnlockSurface(screen);

	if (!(vfb->flags & (F_SURFACE | F_OVERLAY)))
		SDL_Flip(vfb->screen); 

	return 0;
}

// Currently, we support only one (this is a SDL restriction. May not appl
// to other front ends

VirtFB g_vfb[1] = {
	{ .lut = NULL, .update = NULL, },
};

// Virtual framebuffers:
VirtFB *g_vfbs[MAX_VFBS] = {
	&g_vfb[0],
};

// Funky. Get device index from user field and return its attached
// Framebuffer, if legal.
VirtFB *getVirtualFB(DEVICE d)
{
	// SDL hack: SDL 1.2 has only one window, therefore:
	return g_vfbs[0];
	
	// int i = INDEX_FROMTOKEN(d->peer.user.node);
	// if (i < MAX_VFBS) return g_vfbs[i];
	// fprintf(stderr, "Supports only %d VFBs\n", MAX_VFBS);
	// return NULL;
}

////////////////////////////////////////////////////////////////////////////
// Display init/exit

int display_init(struct virtfb_t *vfb)
{
	SDL_Surface *screen;

	vfb->width = g_config.x;
	vfb->height = g_config.y;
	vfb->npixels =	g_config.x * g_config.y;
	// vfb->update = NULL;

	if (SDL_Init(SDL_INIT_VIDEO) < 0 ) return DCERR_STREAM_START;

	lut_init(vfb);

	vfb->flags = g_config.flags;

	vfb->marker_bmp = IMG_Load("marker.png");
	if (!vfb->marker_bmp) {
		printf("ERROR: Unable to load Marker\n");
	}

	if (!(g_config.flags & F_SURFACE)) {
		screen = SDL_SetVideoMode(vfb->width, vfb->height, 32,
			SDL_SWSURFACE);
	} else {
		screen = SDL_CreateRGBSurface(SDL_SWSURFACE,
			vfb->width, vfb->height, 32, 0, 0, 0, 0);
	}

	if (!screen) {
		printf("Could not open SDL screen\n");
		return 1;
	}

	vfb->screen = screen;


	return 0;
}

int display_exit(struct virtfb_t *vfb)
{

	if (vfb->lut) {
		free(vfb->lut);
		vfb->lut= 0;
	}

	if (vfb->screen) {
		if (vfb->flags & F_SURFACE) SDL_FreeSurface(vfb->screen);
		vfb->screen = 0;
		SDL_Quit();
	}

	return 0;
}

int display_set_current(unsigned short which, struct virtfb_t *vfb)
{
	if (which > MAX_VFBS) return -1;
	g_vfbs[which] = vfb;
	return 0;
}

int display_set_callback(unsigned short which, int (*cb)(VirtFB *v), void *c)
{
	if (which > MAX_VFBS) return -1;
	g_vfbs[which]->update = cb;
	g_vfbs[which]->context = c;
	return 0;
}

