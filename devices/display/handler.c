
////////////////////////////////////////////////////////////////////////////
//
// (c) 2005, 2006 section5 // Martin Strubel <hackfin@section5.ch>
//
// Handler for storing a byte stream on the client ("slave")
//
////////////////////////////////////////////////////////////////////////////

#include "devlib_types.h"
#include "devlib_error.h"


#include "device_error.h"
#include <stdio.h> // XXX just for debugging

#include "display.h"
#include <stdlib.h>

#include "property_protocol.h"

// Prototype:
struct virtfb_t *getVirtualFB(DEVICE d);

Config g_config = {
	.x = 640,
	.y = 480,
	.bpp = 8,
	.mode = VIDEOMODE_8BIT,
	.flags = 0,
	.gamma = 0.7,
};

uint32_t g_histo[256];

/** Save a JPEG frame.
 *
 * \param have_header        If 0, data does not have a header, take it
 *                           from preinitialized header data.
 */

int save_frame(VirtFB *vfb, int have_header)
{
	FILE *f;
	char fname[32];
	unsigned char eoi[] = { 0xff, 0xd9 };

	sprintf(fname, "disp%04d.jpg", vfb->frameno);
	vfb->frameno++;

	f = fopen(fname, "wb");
	if (!have_header) {
		fwrite(vfb->header, 1, vfb->headerlen, f);
	}
	fwrite(vfb->rawdata, 1, vfb->len, f);
	fwrite(eoi, 1, sizeof(eoi), f);
	printf("Saved '%s'\n", fname);
	fclose(f);
	return 0;
}

int get_stream_blocksize(DEVICE d, DCValue *out)
{
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) return DCERR_BADPTR;

	out->value.i = vfb->size;
	return 0;
}

#if 0
int set_stream_blocksize(DEVICE d, DCValue *in)
{
	return 0;
}
#endif

int set_stream_start(DEVICE d, DCValue *in)
{
	int n = g_config.x * g_config.y;
	int bytes =  (g_config.bpp + 7) / 8;
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) return DCERR_BADPTR;

	vfb->size = n * bytes;
	printf("Start stream with %dx%d, bufsize %d\n", g_config.x, g_config.y,
			vfb->size);
	if (g_config.mode < VIDEOMODE_JPEG_RAW) {
		printf("Bit depth: %d, mode #%s\n", g_config.bpp,
			g_videomodes[g_config.mode]);
	}
	vfb->frameno = 0;

	if (vfb->rawdata) {
		free(vfb->rawdata);
	}

	if (vfb->header) {
		free(vfb->header);
	}

	vfb->rawdata = (unsigned short *) malloc(vfb->size);
	if (!vfb->rawdata) {
		return DCERR_MALLOC;
	}

	if (g_config.mode == MODE_JPEG_RAW) {
		vfb->header = (unsigned char *) malloc(HEADER_SIZE);
		if (!vfb->header) {
			return DCERR_MALLOC;
		}
		// XXX g_config.user = jpeg_decoder_init(void);
	} else {
		if (display_init(vfb) < 0) {
			return DCERR_STREAM_START;
		}
	}

	return init_drawcallback(vfb);
}

int set_stream_stop(DEVICE d, DCValue *in)
{
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) return DCERR_BADPTR;
	// XXX jpeg_decoder_exit(g_config.user)
	vfb->update = NULL;

	if (vfb->rawdata) {
		free(vfb->rawdata);
		vfb->rawdata = NULL;
	}
	if (vfb->header) {
		free(vfb->header);
		vfb->header = NULL;
	}
	if (g_config.mode != MODE_JPEG && g_config.mode != MODE_JPEG_RAW) {
		display_exit(vfb);
	}
	return 0;
}

int set_stream_data(DEVICE d, DCValue *in)
{
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) {
		netpp_log(DCLOG_ERROR, "Error: Framebuffer not initialized\n");
		return DCERR_BADPTR;
	}

	if (in->type == DC_COMMAND) {
		sdl_update(vfb);
	} else if (in->type == DC_BUFFER) {
		if (in->len > vfb->size || vfb->rawdata == NULL) {
			fprintf(stderr, "Image size (%ld) exceeds reserved buffer (%d)\n",
				in->len, vfb->size);
			return DCERR_PROPERTY_SIZE_MATCH;
		}
		in->value.p = vfb->rawdata;
		vfb->len = in->len;
		// printf("Got %d bytes of data\n", vfb->len);
	} else
		return DCERR_PROPERTY_TYPE_MATCH;

	return 0;
}

int set_header_data(DEVICE d, DCValue *in)
{
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) return DCERR_BADPTR;

	if (in->type == DC_COMMAND) {
		printf("Got JPEG header\n");
	} else if (in->type == DC_BUFFER) {
		if (in->len > HEADER_SIZE) return DCERR_PROPERTY_SIZE_MATCH;
		in->value.p = (void *) vfb->header;
		vfb->headerlen = in->len;
	} else
		return DCERR_PROPERTY_TYPE_MATCH;

	return 0;
}

int get_histo(DEVICE d, DCValue *out)
{
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) return DCERR_BADPTR;
	switch (out->type) {
		case DC_COMMAND:
			break;
		case DC_BUFFER:
			if (out->len != sizeof(g_histo)) {
				printf("Bad histogram buffer size, got %ld\n", out->len);
				out->len = sizeof(g_histo);
				return DCERR_PROPERTY_SIZE_MATCH;
			}
			out->value.p = g_histo;
			break;
		default:
			return DCERR_PROPERTY_TYPE_MATCH;
	}
	return 0;
}

int set_histo(DEVICE d, DCValue *in)
{
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) return DCERR_BADPTR;
	switch (in->type) {
		case DC_COMMAND:
			draw_histo(vfb, g_histo);
			break;
		case DC_BUFFER:
			if (in->len != sizeof(g_histo)) {
				printf("Bad histogram buffer size, got %ld\n", in->len);
				in->len = sizeof(g_histo);
				return DCERR_PROPERTY_SIZE_MATCH;
			}
			in->value.p = g_histo;
			break;
		default:
			return DCERR_PROPERTY_TYPE_MATCH;
	}
	return 0;
}


int set_marker(DEVICE d, DCValue *in)
{
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) return DCERR_BADPTR;

	switch (in->type) {
		case DC_COMMAND:
			break;
		case DC_BUFFER:
			if (in->len > sizeof(vfb->marker)) {
				printf("Marker buffer size excess, got %ld\n", in->len);
				in->len = sizeof(vfb->marker);
				return DCERR_PROPERTY_SIZE_MATCH;
			}
			in->value.p = vfb->marker;
			vfb->nmarkers = in->len / sizeof(*vfb->marker) / 2;
			break;
		default:
			return DCERR_PROPERTY_TYPE_MATCH;
	}
	return 0;
}

#if 0 // unused for now
int set_resx(DEVICE d, DCValue *in)
{
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) return DCERR_BADPTR;

	vfb->width = in->value.i;
	return 0;
}

int set_resy(DEVICE d, DCValue *in)
{
	vfb->height = in->value.i;
	return 0;
}

#endif

int set_preset(DEVICE d, DCValue *in)
{
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) return DCERR_BADPTR;

	vfb->preset = in->value.i;
	return 0;
}

int get_preset(DEVICE d, DCValue *out)
{
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) return DCERR_BADPTR;

	out->value.i = vfb->preset;
	return 0;
}

int set_lut16(DEVICE d, DCValue *in)
{
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) return DCERR_BADPTR;

	switch (in->type) {
		case DC_COMMAND:
			break;
		case DC_BUFFER:
			if (!vfb->lut) {
				vfb->lut = (unsigned char *) malloc(LUT16_SIZE);
				if (!vfb->lut) return DCERR_MALLOC;
			}
			in->value.p = vfb->lut; // Tell netpp about data destination
			if (in->len != LUT16_SIZE) {
				in->len = LUT16_SIZE;
				return DCERR_PROPERTY_SIZE_MATCH;
			}
			break;
		default:
			return DCERR_PROPERTY_TYPE_MATCH;
	}
	return 0;
}

int get_lut16(DEVICE d, DCValue *out)
{
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) return DCERR_BADPTR;

	switch (out->type) {
		case DC_COMMAND:
			break;
		case DC_BUFFER:
			out->value.p = vfb->lut; // Tell netpp about data source
			if (out->len != LUT16_SIZE) {
				out->len = LUT16_SIZE;
				return DCERR_PROPERTY_SIZE_MATCH;
			}
			break;
		default:
			return DCERR_PROPERTY_TYPE_MATCH;
	}
	return 0;
}

int set_lut_update(DEVICE d, DCValue *in)
{
	VirtFB *vfb = getVirtualFB(d);
	if (!vfb) return DCERR_BADPTR;
	lut_update(vfb);
	return 0;
}

int get_marker_size(DEVICE d, DCValue *out)
{
	out->value.i = 1;
	out->type = DC_INT;
	netpp_log(DCLOG_VERBOSE, "Get 'marker_size'");
	return 0;
}

int set_marker_size(DEVICE d, DCValue *in)
{
	netpp_log(DCLOG_VERBOSE, "Set 'marker_size'");
	return 0;
}
		
/* Handler for property 'X' */
	
int get_marker_x(DEVICE d, DCValue *out)
{
	netpp_log(DCLOG_VERBOSE, "Get 'marker_x'");
	return 0;
}

int set_marker_x(DEVICE d, DCValue *in)
{
	netpp_log(DCLOG_VERBOSE, "Set 'marker_x'");
	return 0;
}
		
/* Handler for property 'Y' */
	
int get_marker_y(DEVICE d, DCValue *out)
{
	netpp_log(DCLOG_VERBOSE, "Get 'marker_y'");
	return 0;
}

int set_marker_y(DEVICE d, DCValue *in)
{
	netpp_log(DCLOG_VERBOSE, "Set 'marker_x'");
	return 0;
}
