#include <stdio.h>
#include <fcntl.h>
#include "timing.h"

#define CHUNKSIZE  63488

int do_write(int fd, unsigned char *buf, int count)
{
	int n, l;
	int i;

	float t;
	TIMEVAL t0, t1;

	l = count;

	while (l > 0) {

		// if (l > 32) n = 32;
		// else        n = l;
		n = l;

		get_time(&t0);
		n = write(fd, buf, n);
		get_time(&t1);
		t = took_time(&t0, &t1);
		// if (n > 64) 
		// printf("Write rate: %f kB/s (%d)\n", (float) n / 1000.0 / t, n);

		// printf("Wrote %d\n", n);
		// usleep(1);
		if (n < 0) return n;
		l -= n;
		buf += n;
	}

	return count;
}

void fill_buf(unsigned char *buf, int n)
{
	unsigned short *c = (unsigned short *) buf;
	int i;
	unsigned short s;

	n /= 2;
	// for (i = 0; i < n; i++) {
		// *c++ = i;
	// }
	s = 1;
	for (i = 0; i < n; i++) {
		*c++ = s;
		s = (s << 1) | ((s >> 15) & 0x1);
	}
}

int main(int argc, char **argv)
{
	FILE *f;
	int dev;

	int count;
	int tot;
	TIMEVAL t0, t1;
	float t;
	int n;

	static unsigned char buf[CHUNKSIZE];

	f = fopen(argv[1], "rb");
	if (!f) {
		printf("File not found\n");
		return -1;
	}

	dev = open("/dev/usbfifo", O_RDWR, 0);
	if (dev < 0) {
		perror("opening device");
		return -1;
	}

	count = 0x100000;

	get_time(&t0);

	// fill_buf(buf, 0x2000);

	tot = 0;
	do {
		n = fread(buf, 1, CHUNKSIZE, f);

		printf("Read %d bytes from file\n", n);
		printf("Write %d bytes\n", n);

		// n = 0x2000; count -= n;
		do_write(dev, buf, n);
		tot += n;
		count -= n;

	} while (count > 0);

	get_time(&t1);

	t = took_time(&t0, &t1);
	printf("--------------------------------------------------\n");
	printf("Total Write rate: %f kB/s\n", (float) tot / 1000.0 / t);

	close(dev);
	fclose(f);
	return 0;
}
