DISTFILES =  \
wrapper/netppplus.cpp \
wrapper/Makefile \
python/py_device.c \
python/Makefile \
python/netpp.py \
msdll.mk \
devices/platform.mk \
devices/filetransfer/main.c \
devices/filetransfer/test.py \
devices/filetransfer/fileops.h \
devices/filetransfer/timing.c \
devices/filetransfer/template.xml \
devices/filetransfer/slave.c \
devices/filetransfer/slave_vars.h \
devices/filetransfer/Makefile \
devices/filetransfer/timing.h \
devices/filetransfer/handler.c \
devices/example/example.h \
devices/example/test.py \
devices/example/threadtest.c \
devices/example/example.xml \
devices/example/slave.c \
devices/example/errorhandler.xsl \
devices/example/Makefile \
devices/example/handler.c \
devices/streamtest/test.py \
devices/streamtest/streamtest.xml \
devices/streamtest/streamtest.h \
devices/streamtest/slave.c \
devices/streamtest/Makefile \
devices/streamtest/handler.c \
devices/libs.mk \
devices/libmaster/Makefile \
devices/libslave/slave.c \
devices/libslave/slave_test.c \
devices/libslave/Makefile \
devices/display/extern.h \
devices/display/display.h \
devices/i2c-linux/i2c-dev.h \
devices/Makefile \
include/devlib_types.h \
include/netppplus.h \
include/netpp_platform.h \
include/devlib_ports.h \
include/proplist_init.h \
include/devlib.h \
include/dlistpool.h \
include/dynprops.h \
include/property_protocol.h \
include/proxy.h \
include/ports.h \
include/property_types.h \
include/property_api.h \
include/api.h \
include/slave.h \
include/devlib_error.h \
include/arm-unaligned.h \
include/bfin-unaligned.h \
include/platform.h \
include/conffile.h \
include/netpp.h \
Doxyfile \
LICENSE.asc \
unixdll.mk \
src/dlistpool.c \
src/unixdevice.c \
src/propdummies.c \
src/server.c \
src/ports.c \
src/muxer.c \
src/packet.c \
src/propapi.c \
src/wrapper.c \
src/rawio.c \
src/ipcommon.c \
src/conffile.c \
src/rawio.h \
src/tcp.c \
src/proxy_generic.c \
src/proxy_tcp.c \
src/udp.c \
src/usb_fifo.c \
common/local_api.c \
common/prophandler.c \
common/auxiliaries.c \
common/errorcodes.xml \
common/version.c \
common/logging.c \
common/errorhandler.c \
common/dynprops.c \
common/crc16.c \
master/cache.h \
master/driver.h \
master/input.c \
master/wininput.c \
master/shell.h \
master/shell.c \
master/cli.c \
master/cache.c \
master/master_util.h \
master/master.c \
master/Makefile \
xml/interfaces.xsd \
xml/proplist.xsl \
xml/userhandler.xsl \
xml/xmldata.css \
xml/prophandler.mk \
xml/registermap.xsl \
xml/errorlist.xsl \
xml/registerdefs.xsl \
xml/errorenum.xsl \
xml/errorhandler.xsl \
xml/proptable.xsl \
xml/devdesc.css \
xml/devdesc.xsd \
xml/xpointer.xsl \
doc/html \
doc/images/ \
README \
LIESMICH.txt \
Makefile

ifeq ($(CUSTOM),vkit)
	DISTFILES += devices/display/Makefile
	DISTFILES += devices/display/display.xml
	DISTFILES += devices/display/bayer.c
	DISTFILES += devices/display/slave.c
	DISTFILES += devices/display/handler.c
	DISTFILES += devices/display/tables.py
	DISTFILES += devices/display/tables.c
	DISTFILES += devices/display/dummy.c
	DISTFILES += devices/display/drawing.c
	DISTFILES += devices/display/device_error.h
	DISTFILES += devices/display/display.h
	DISTFILES += devices/display/yuv2rgb.h
	DISTFILES += devices/display/marker.png
	DISTFILES += devices/display/test.py
endif

FILES=$(DISTFILES:%=netpp/%)

dist:
	tar cfz ../netpp_src-$(VERSION).tgz -C .. \
	--exclude=*/.svn \
	$(FILES)
