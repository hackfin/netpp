/* DList types */

#ifdef TINYCPU
#define INDEX     uint8_t
#else
#define INDEX     int16_t
#endif

#define INDEX_NIL (INDEX) -1

#define DLIST_ITEM struct dyn_prop_t 

typedef struct dlist_pool_t {
	INDEX id;
	DLIST_ITEM *base;
	INDEX first, last;
	INDEX free;
	unsigned short n;
} DlistPool;

#ifdef NO_MALLOC
DLIST_ITEM *standalone_alloc_pool(int n);
void standalone_dealloc_pool(DlistPool *pool);
#endif

int   pool_new(DlistPool *pool, int initsize);
int   pool_init(DlistPool *pool);
void  pool_free(DlistPool *pool);

INDEX dlist_new(DlistPool *pool, DLIST_ITEM **pd);
int   dlist_free(DlistPool *pool, INDEX i);

void  dlist_link(DlistPool *pool, INDEX to, INDEX i);
void  dlist_unlink(DlistPool *pool, INDEX i);
int   dlist_append(DlistPool *pool, INDEX i);
void dlist_insert(DlistPool *pool, INDEX to, INDEX i);
int   dlist_remove(DlistPool *pool, INDEX i);
void  dlist_clear(DlistPool *pool);

DLIST_ITEM *dlentry_as_pointer(DlistPool *pool, INDEX i);

// Debugging
typedef void (*dispfunc)(DLIST_ITEM *p);
void  dlist_dump(DlistPool *pool, dispfunc f);

