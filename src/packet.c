/**
 * Packetizer
 *
 * Symmetrical to both peers
 *
 * Protocol v2 changes (+)/feature requests (*):
 * (+) Early errors tolerated during packet sequence. This can occur on
 *     small embedded targets that can not take all data (packet buffer
 *     overruns).
 * (+) Early errors can also occur with UDP. We should probably support
 *     reverse packet order (status first, data later)
 * (*) Master: Implement polling for early status returns during
 *     packet sequence. This is disputable, contra arguments are:
 *     1) Packet buffer size should be queried upfront
 *     2) Extra wait time introduced by packet polling during burst
 *     3) Buffer specific stuff should rather be buried in the driver
 *        backend.
 *
 */

#include "devlib.h"
#include "devlib_error.h"
#include "property_protocol.h"

// Extra hack for standalone non uClinux UDP stack
#if !defined(STANDALONE_UDPSTACK)
#define free_cur_rx_buf(x)
#endif

#define DECODE_TYPE(x)  (x & PP_MASK_TYPE)

// AUX
#define GET_STATUSCODE(h) (short) ntohs(h->sub.status.code)
#define GET_TOKEN(h)      ntohl(h->sub.status.token)
#define GET_VALUETYPE(h)  (ntohs(h->sub.set.type) & ~FLAG_MASK_PUBLIC)
#define GET_VALUEFLAGS(h) (ntohs(h->sub.set.type) & FLAG_MASK_PUBLIC)


#ifdef DEBUG_PACKETS
void packet_dump(PropertyPacketHdr *h)
{
	const unsigned char *b = (const unsigned char *) h;
	int i = sizeof(PropertyPacketHdr);

	while (i--) {
		printf("%02x ", *b++);
	}
	printf("\n");
}
#endif

////////////////////////////////////////////////////////////////////////////

// Tricky one: Keep alignment in mind

static size_t append_value(void *p, DCValue *val)
{
	union payload_u {
		float f;
		int32_t i;
		int16_t s;
		char buf[4];
	} *vp = (union payload_u *) p;

	size_t len = 0;
	memset(&vp->buf, 0, sizeof(vp->buf));

	switch (val->type) {
		case DC_FLOAT:
			vp->f = htonf(val->value.f);
			len = 4;
			break;
		case DC_UINT:
		case DC_EVENT:
		case DC_INT:
			len = 4;
			vp->i = htonl(val->value.i);
			break;
		case DC_MODE:
		case DC_COMMAND:
		case DC_BOOL:
			vp->s = htons((uint16_t) val->value.i);
			len = 2;
			break;
		case DC_STRING:
		case DC_BUFFER:
			len = val->len;
			break;
		default:
			netpp_log(DCLOG_ERROR, "%s: unhandled type %d", __FUNCTION__,
			val->type);
			return DCERR_PROPERTY_HANDLER;
	}
	return len;
}

int send_property_packet(Peer *peer,
		TOKEN p, DCValue *val, int mode)
{
	int error;
	size_t len;
	PropertyPacketHdr *h;
	char *payload;

	REQUEST_PACKET(peer, h, payload, 4);

	len = append_value(payload, val);
	if (len < 0) return len;

	h->type = htons(mode);
#ifdef CONFIG_NONCE
	h->id = get_nonce();
#else
	h->id = 0;
#endif
	h->sub.set.token = htonl(p);
	h->sub.set.type = htons(val->type | (val->flags & FLAG_MASK_PUBLIC));


	h->sub.set.len = htonl( (unsigned long) len);

	if (IS_BUFFER(val->type)) {
		if (!val->value.p) {
			len = 0; h->sub.set.len = htonl( (unsigned long) len);
			netpp_log(DCLOG_DEBUG, "Handler returned NULL buffer");
		}
		error = send_packet(peer, h, (const char*) val->value.p, len);
	} else {
		error = send_packet(peer, h, payload, len);
	}
	return error;
}

/** Sends a descriptor packet.
 *
 * This contains type, and length in the DCValue struct.
 */

int send_desc_packet(Peer *peer,
		TOKEN p, DCValue *val)
{
	PropertyPacketHdr *h;
	REQUEST_PACKET_HDR(peer, h, 0);

	h->type = htons(PP_SET | PP_FLAG_PROTO);
	h->sub.set.token = htonl(p);
	h->sub.set.type = htons(val->type | (val->flags & FLAG_MASK_PUBLIC));
	h->sub.set.len = htonl((unsigned long) val->len); // Property size!

	return send_packet(peer, h, NULL, 0);
}

static
int send_status_packet(Peer *peer, TOKEN t, int16_t code)
{
	PropertyPacketHdr *h;
	REQUEST_PACKET_HDR(peer, h, 0);
	DEB(netpp_log(DCLOG_DEBUG, "SEND STATUS (%d) token %lx", code, t));

#ifdef CONFIG_NONCE
	h->id = id;
#endif
	h->type = htons(PP_STATUS);
	h->sub.status.token = htonl(t);
	h->sub.status.code = htons(code);
	h->sub.status.len = peer->packetsize;
	return send_packet(peer, h, NULL, 0);
}

#if 0
// No longer used:
void flush_packets(Peer *peer)
{
	PropertyPacketHdr *h;
	int n;

	do {
		n = poll_packet(peer, &h, POLL_WAIT);
		DEB(netpp_log(DCLOG_DEBUG, "Got packet type %x", ntohs(h->type)));
		DEB(netpp_log(DCLOG_DEBUG, "           id   %d", ntohs(h->id)));
		DEB(netpp_log(DCLOG_DEBUG, "     payloadsz  %d", ntohl(h->sub.set.len)));
		if (n) release_packet(peer);
	} while (n != 0);
}
#endif

int get_status_packet(Peer *peer, TOKEN *p)
{
	short error;
	PropertyPacketHdr *h;
	int n;
	
	n = poll_packet(peer, &h, POLL_WAIT);
	if (n < 0) {
		netpp_log(DCLOG_ERROR, "Polling error");
		return DCERR_COMM_TIMEOUT;
	} else
	if (n == 0) {
		netpp_log(DCLOG_ERROR, "Timed out waiting for status packet");
		return DCERR_COMM_TIMEOUT;
	}

	if (ntohs(h->type) == PP_STATUS) {
		*p = ntohl(h->sub.status.token);
		error = ntohs(h->sub.status.code);
		release_packet(peer);  // And release.
		return error;
	} else {
		netpp_log(DCLOG_DEBUG,
				"Did not get a status packet! [%d]", ntohs(h->type));
		release_packet(peer);
		// No longer do that.
		// flush_packets(peer);
		return DCERR_COMM_RET;
	}
}

/* Parse property packet.
 *
 * This function is rather complex.
 * If the property is of a buffer type, buffers are guarded, i.e.
 * Data is only copied when buffer sizes given by the backend handler
 * match. Otherwise, the function returns DCERR_PROPERTY_SIZE_MATCH.
 *
 */

static
int parse_property_packet(PropertyPacketHdr *h, DCValue *val,
		size_t n, uint16_t *packetid)
{
	int code = 0;
	unsigned short type;
	uint16_t i;
	size_t l;
	int tot;
	void *data;
	unsigned char *out;

	type = ntohs(h->type);
	n -= sizeof(PropertyPacketHdr); // payload length

	if (DECODE_TYPE(type) != PP_SET) {
		netpp_log(DCLOG_DEBUG, "Bad packet type %d", type);
		return DCERR_COMM_RET;
	}

	val->type = (PropertyType) GET_VALUETYPE(h);
	val->flags = GET_VALUEFLAGS(h);

	data = (void *) &h[1]; // Skip header

	switch (val->type) {
		case DC_FLOAT:
			store_unaligned_float(&val->value.f, data);
			break;
		// In case of a register, we parse the transmitted register length
		case DC_EVENT:
		case DC_UINT:
			val->value.u = ntohl(get_unaligned((uint32_t *) data));
			break;
		case DC_INT:
		// For portability reasons: extend sign right:
			val->value.i = (int32_t) ntohl(get_unaligned((int32_t *) data));
			break;
		case DC_MODE:
		case DC_COMMAND:
		case DC_BOOL:
			val->value.i = ntohs(get_unaligned((uint16_t *) data));
			break;
		case DC_STRING:
		case DC_BUFFER:
			tot = ntohl(h->sub.set.len);
			// netpp_log(DCLOG_DEBUG, "bytes remaining: %d", tot);
			l = val->len;
			val->len = tot;
	
			// Here we store our data
			out = (unsigned char *) val->value.p;

			if (out == NULL) {
				netpp_log(DCLOG_DEBUG,
					"Uninitialized pointer proxy passed");
				return DCERR_BADPTR;
			}

			i = ntohs(h->id);
			if (*packetid != i) {
				netpp_log(DCLOG_DEBUG, "ID mismatch: %d - %d", *packetid, i);
				netpp_log(DCLOG_DEBUG, "Possibly lost packet %d", *packetid);

#ifdef CONFIG_UDP

				// should: bytes_left = tot - count;
				// is:     l
				// fixup counter:
				// We missed a packet, so we adjust the counter
				// to the current packet received
				l = tot - ntohl(h->sub.set.len) + n;
				*packetid = i;
				// Adjust output:
				out += l;
#else
				return DCERR_COMM_FRAME;
#endif
			}

			data = (unsigned char *) &h[1];
			if (l >= n) 
				memcpy(out, data, n);
			else
				return DCERR_PROPERTY_SIZE_MATCH;

			// netpp_log(DCLOG_DEBUG, "Copying to %08lx", out);
	
			// increment target pointer
			out += n;
			val->value.p = out;
			(*packetid)++;
			code = 0;
			break;
		default:
			DEB(netpp_log(DCLOG_ERROR,
						"%s: Unknown property packet", __FUNCTION__));
			code = DCERR_PROPERTY_HANDLER;
	}
	return code;
}

static
int pktparse_localname(RemoteDevice *d, PropertyPacketHdr *h)
{
	TOKEN t;
	TOKEN parent;
	int code = 0;
	char *data = (char *) &h[1];

	// Get namespace:
	parent = ntohl(h->sub.name.token);

	if (parent == TOKEN_INVALID) {
		parent = local_getroot(d);
	}

	t = property_parsename(d, data, parent);
	if (t == TOKEN_INVALID) {
		DEB(netpp_log(DCLOG_ERROR, "Property '%s' unknown", data));
		code = DCERR_PROPERTY_UNKNOWN;
	}

	DEB(netpp_log(DCLOG_DEBUG, "Query local Name: '%s' [%lx] in ns [%lx]",
		data, t, parent));
	return send_status_packet(&d->peer, t, code);
}

static
int pktparse_select(RemoteDevice *d, PropertyPacketHdr *h, TOKEN *child)
{
	TOKEN parent, prev, t;
	
	parent = ntohl(h->sub.select.parent);
	prev = ntohl(h->sub.select.prev);

	if (parent == TOKEN_INVALID && prev == TOKEN_INVALID) {
		*child = local_getroot(d);
		return 0;
	}

	DEB(netpp_log(DCLOG_DEBUG, "Select parent: %lx  prev: %lx", parent, prev));
	t = property_select(d, parent, prev);
	*child = t;
	if (t == TOKEN_INVALID) return DCWARN_PROPERTY_NIL;
	return 0;
}

static
int pktparse_getname(RemoteDevice *d, PropertyPacketHdr *h)
{
	int error;
	char name[NAMELENGTH];
	DCValue tmp;
	TOKEN t;

	name[NAMELENGTH-1] = '\000';

	// It is important that WE own the string
	tmp.value.p = name;
	tmp.len = sizeof(name)-1;
	tmp.type = DC_STRING;
	tmp.index = 0;
	tmp.flags = 0;

	t = GET_TOKEN(h);

	error = property_getname(d, t, &tmp);
	if (error < 0) {
		send_property_packet(&d->peer, t, &tmp, PP_SET);
	} else {
		error = send_property_packet(&d->peer, t, &tmp, PP_SET);
	}
	return send_status_packet(&d->peer, t, error);
}

static
int handle_request_pkt(RemoteDevice *d, TOKEN t, short code)
{
	int error;

	switch (t) {
		case REQUEST_VERSION:
			// Return protocol version
			netpp_log(DCLOG_VERBOSE, "Remote version: %d", code);
			d->proto_version = code;
			t = NETPP_PROTOCOL_VERSION;
			error = send_status_packet(&d->peer, t, 0);
			break;
		case REQUEST_SESSION_END:
			// Return error code to signal end of session
			t = 0;
			DEB(netpp_log(DCLOG_VERBOSE, "Got session end request"));
			send_status_packet(&d->peer, t, 0);
			error = DCERR_COMM_DISCONNECT;		
			break;

		default:
			netpp_log(DCLOG_VERBOSE, "Request not acknowledged");
			error = send_status_packet(&d->peer, t, DCERR_COMM_NAK);

	}
	return error;
}


enum pp_state {
	S_INVALID,
	S_READY,     // Ready/Idle
	S_REQUEST,   // We have sent a request
	S_RECEIVE,
	S_BULK,      // Bulk packet sequence (reserved for 1.0)
	S_SEND,
	S_WAIT_ACK,
	S_CANCEL,    // Cancel large block transaction
	S_FLUSH,     // Flush packet queue until timeout
};


/** Protocol handler
 *
 * Bloody wicked and hardly readable function. Don't touch the state
 * machine!
 *
 */

int proto_handler(RemoteDevice *d, int master, int entrystate,
		TOKEN t, DCValue *val)
{
	enum pp_state state = entrystate;
	int type = 0, flags = 0;
	int len;
	int code, error = 0;
	int reversed = 0;
	PropertyPacketHdr *h;
	DCValue tmp;
	TOKEN rt;
	uint16_t packetid = 0;

	// A very long protocol state loop.
	// It is left when the state is S_READY or an error occurs.
	do {
		len = 0;
		if (master) {
			switch (state) {
				// Here, we poll for packets, because we expect them
				case S_WAIT_ACK:
					DEB(netpp_log(DCLOG_DEBUG, "Waiting for status packet"));
				case S_REQUEST:
					tmp = *val;
				case S_FLUSH:
				case S_RECEIVE:
					len = poll_packet(&d->peer, &h, POLL_WAIT);
					if (!len) {
						netpp_log(DCLOG_DEBUG, "Protocol Timeout");
						if (!error) error = DCERR_COMM_TIMEOUT;
						goto leave;
					}
					break;
				case S_SEND:
					tmp = *val;
					break;
				default:
					break;
			}
		} else { // We're slave:
			switch (state) {
				case S_SEND:
					break;
				case S_CANCEL:
				case S_READY:
					len = poll_packet(&d->peer, &h, POLL_CHECK);
					if (!len) {
						error = 0;
						goto leave;
					}
					break;
				default:
					len = poll_packet(&d->peer, &h, POLL_WAIT);
					if (!len) continue;
			}
		}

		// Here begins the common master and slave code:

		if (len < 0) { // Error
			error = len;		
			// On the TINY cpus, we can not take big buffers.
			// If we happen to have an overrun, don't abort with
			// timeout but notify client with an early error.
			if (error == DCERR_COMM_OVERRUN) {
				send_status_packet(&d->peer, t, error);
				// error = 0; // Ignore local error
			}
			goto leave;
		} else if (len) {
			flags = ntohs(h->type);
			type = flags & PP_MASK_TYPE;
		}

		switch (state) {
			case S_READY:
				error = 0;
				switch (type) {
					case PP_SELECT:
						error = pktparse_select(d, h, &t);
						send_status_packet(&d->peer, t, error);
						if (error < 0)
							netpp_log(DCLOG_ERROR, "Error %d", error);
						break;
					case PP_NAME:
						if (flags & PP_FLAG_PROTO) {
							error = pktparse_getname(d, h);
						} else {
							error = pktparse_localname(d, h);
						}
						break;
					case PP_SET:
						t = GET_TOKEN(h);
						DEB(netpp_log(DCLOG_DEBUG, "PKT: Set(%ld) len: %d", t, len));
						tmp.type = (PropertyType) GET_VALUETYPE(h);
						if (IS_BUFFER(tmp.type)) {
							// Initialize buffer descriptor
							tmp.len = ntohl(h->sub.set.len);
							error = property_set(d, t, &tmp);
						}

						packetid = 0;

						if (error < 0) {
							if (flags & PP_FLAG_FRAG) {
								state = S_CANCEL;
							} else {
								state = S_READY;
							}
							error = send_status_packet(&d->peer, t, error);
						} else {
							// Everything fine so far, we can now
							// start parsing the packet.
							error = parse_property_packet(h, &tmp, len,
									&packetid);
							// More packets to come?
							if (flags & PP_FLAG_FRAG) {
								state = S_RECEIVE;
							} else {
								state = S_READY;
								// If we have transferred a buffer, add
								// UPDATE command
								if (IS_BUFFER(tmp.type)) {
									tmp.type = DC_COMMAND;
									error = property_set(d, t, &tmp);
								} else {
									error = property_set(d, t, &tmp);
								}
								error = send_status_packet(&d->peer, t, error);
							} 
						}

						break;
					case PP_GET:
						t = GET_TOKEN(h);
						tmp.type = DC_UNDEFINED;
						tmp.len = ntohl(h->sub.get.len);
						// Query property prototype only?
						if (flags & PP_FLAG_PROTO) {
							DEB(netpp_log(DCLOG_DEBUG, "PKT: Proto(%08x)", t));
							error = property_getdesc(d, t, &tmp);
							state = S_READY;
							send_desc_packet(&d->peer, t, &tmp);
							error = send_status_packet(&d->peer, t, error);
						} else {
							DEB(netpp_log(DCLOG_DEBUG, "PKT: Get(%08x)", t));
							error = property_get(d, t, &tmp);
							state = S_SEND;
						}
#ifdef DEBUG
						if (error == DCERR_PROPERTY_SIZE_MATCH) {
							netpp_log(DCLOG_DEBUG,
									"Possibly string size does not fit");
						}
#endif
						break;
					case PP_REQUEST:
						t = GET_TOKEN(h);
						error = GET_STATUSCODE(h);
						error = handle_request_pkt(d, t, error);
						break;
					default:
						netpp_log(DCLOG_ERROR,
							"Bad netpp packet header type %04x", type);
						state = S_INVALID;
				}
				break;
// Only a master can be in this state
			case S_REQUEST: // we have sent a GET
				switch (type) {
					case PP_SET:
						rt = GET_TOKEN(h);
						if (t != rt) {
							netpp_log(DCLOG_ERROR,
								"Token mismatch, wanted: %08x, got: %08x",
								t, rt);
							error = DCERR_COMM_GENERIC;
							state = S_FLUSH;
							break;
						}

						// We don't need to handle Buffer types
						// especially
						packetid = 0;

						if (flags & PP_FLAG_PROTO) {
							// Was it a proto?
							// If yes, simply copy relevant data
							val->type =
							tmp.type = GET_VALUETYPE(h);
							tmp.flags = GET_VALUEFLAGS(h);
							tmp.len = ntohl(h->sub.set.len);
						} else {
							error = parse_property_packet(h, &tmp, len,
								&packetid);
						}

						if (flags & PP_FLAG_FRAG) {
							state = S_RECEIVE;
						} else
						if (reversed) {
							netpp_log(DCLOG_DEBUG, "Reversed pkt order");
							state = S_READY;
						} else {
							state = S_WAIT_ACK;
						} 

						// We must only copy the entire struct when
						// not a buffer:
						// Note: requested buffer length may have been
						// changed.
						if (IS_BUFFER(tmp.type)) {
							val->len = tmp.len;
							val->flags = tmp.flags;
							val->type = tmp.type;
						}
						else
							*val = tmp;

						break;
					case PP_STATUS:
						// Each transaction is ended with a status msg
						error = GET_STATUSCODE(h);
						// Handle early error. With UDP, packets may
						// come in reversed order.

						if (error >= 0) {
							d->errcount++;
							if (d->errcount > ERRCOUNT_LIMIT) {
								d->errcount = 0;
								state = S_FLUSH;
								break;
							} else {
								reversed = 1;
								break; // Stay in READY state
							}
						}

						netpp_log(DCLOG_DEBUG, "Early error %d", error);

						// Do not tolerate early errors, unless we
						// got a size mismatch
						if (error == DCERR_PROPERTY_SIZE_MATCH) error = 0;

						state = S_READY;

						break;
					default:
						netpp_log(DCLOG_ERROR,
							"Unexpected request header type %04x", type);
						state = S_INVALID;
				}
				break;
			case S_RECEIVE:
				// When expecting more than 1 packet, we stay in this state
				DEB(netpp_log(DCLOG_DEBUG, "more packets to come..."));
				switch (type) {
					case PP_SET:
						if (error >= 0) {
							error = parse_property_packet(h,
									&tmp, len, &packetid);
						}
						// Last packet? Then finish.
						if (!(flags & PP_FLAG_FRAG)) {
							if (master) {
								state = S_WAIT_ACK;
							}  else {
								state = S_READY;
								if (!IS_BUFFER(tmp.type)) {
									error = property_set(d, t, &tmp);
								}
								DEB(netpp_log(DCLOG_DEBUG,
										"Got last packet from trail"));
								error = send_status_packet(&d->peer, t, error);
								// Send slave a notice
								// that all data was transferred.
								// This is done by sending a dummy command
								if (IS_BUFFER(tmp.type)) {
									tmp.type = DC_COMMAND;
									error = property_set(d, t, &tmp);
									if (error < 0) 
										error = DCERR_PROPERTY_HANDLER;
								}
							}
						} 
						break;
					case PP_STATUS:
						// Handle possible late error
						state = S_READY;
						error = GET_STATUSCODE(h);
						netpp_log(DCLOG_ERROR,
							"Got status packet in RECEIVE, code: %d",
							error);
						break;
					default:
						netpp_log(DCLOG_ERROR,
							"Unexpected type during streaming: %04x", type);
						state = S_INVALID;
				}

				break;
			case S_SEND:
				if (error >= 0) {
					// This function is responsible for splitting up
					// the data
					code = send_property_packet(&d->peer, t, &tmp, PP_SET);
					DEB(netpp_log(DCLOG_DEBUG,
						"Sent property packet: ret: %d", code));
					// Break on error
					if (code < 0) {
						error = code;
						goto leave;
					}

					state = S_READY;

					if (!master && IS_BUFFER(tmp.type)) {
						// Send RELEASE dummy command.
						// It is up to the handler to correctly handle
						// a DC_COMMAND
						tmp.type = DC_COMMAND;
						code = property_get(d, t, &tmp);
						// override previous ret code only
						// when not null
						if (code) error = code;
					}
				} else {
					// If we failed, we send a dummy packet.
					// This is a Property prototype packed with
					// length = 0. After that, we send the error code.
					DEB(netpp_log(DCLOG_DEBUG, "Sending description packet"));
					send_desc_packet(&d->peer, t, &tmp);
					state = S_READY;
				}

				if (master)
					state = S_WAIT_ACK;
				else {
					error = send_status_packet(&d->peer, t, error);
				}
				break;
			case S_WAIT_ACK:
				// Bail out on earlier errors:
				if (error) { state = S_READY; break; }
				switch (type) {
					case PP_STATUS:
						state = S_READY;
						error = GET_STATUSCODE(h);
						break;
					default:
						netpp_log(DCLOG_DEBUG,
							"Illegal ACK packet type %04x", type);
						state = S_INVALID;
				}
				break;
			case S_FLUSH:
				break;
			case S_CANCEL:
				if (!(flags & PP_FLAG_FRAG)) {
					state = S_READY;
				}
				break;
			default:
				netpp_log(DCLOG_DEBUG, "Illegal State %d", state);
				state = S_INVALID;
		}
		// Here, we have to release a packet that was just received.

		if (len > 0)
			release_packet(&d->peer);
		if (state == S_INVALID) {
			netpp_log(DCLOG_DEBUG, "Invalid state");
			error = DCERR_COMM_NAK;
			break;
		}
	} while (state != S_READY);


leave:
	return error;
}

#define MASTER 1
#define SLAVE  0

int slave_handler(RemoteDevice *d)
{
	DCValue tmp;
	return proto_handler(d, SLAVE, S_READY, 0, &tmp);
}

int handle_request(RemoteDevice *d, TOKEN t, DCValue *val)
{
	return proto_handler(d, MASTER, S_REQUEST, t, val);
}

int handle_setproperty(RemoteDevice *d, TOKEN t, DCValue *val)
{
	return proto_handler(d, MASTER, S_SEND, t, val);
}


////////////////////////////////////////////////////////////////////////////
// AUXILIARIES
//

int get_packet_length(const char *buf)
{
	int len, type;
	PropertyPacketHdr *h;

	h = (PropertyPacketHdr *) buf;

	type = ntohs(get_unaligned(&h->type));

	switch (type & PP_MASK_TYPE) {
		case PP_SET:
			len = ntohl(get_unaligned(&h->sub.set.len));
			break;
		case PP_NAME:
			len = ntohs(get_unaligned(&h->sub.name.len));
			break;
		default:
			len = 0;
			break;
	}

	// Ignore length, and clear, if we are a prototype packet
	if (type & PP_FLAG_PROTO) {
		len = 0;
	}

	return len;
}
