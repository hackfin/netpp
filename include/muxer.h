#define MAX_PACKET_SIZE 128

int muxdev_init(RemoteDevice *devices, const char *dev);
int muxdev_handle(RemoteDevice *devices);
int muxdev_exit(RemoteDevice *devices);
