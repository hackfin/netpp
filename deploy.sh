#!/bin/sh

install -m 755 -d /tmp/bin
make -C master clean all
make -C devices/factory clean all install_binary BIN_DESTDIR=/tmp/bin
install -m 755 master/master /tmp/bin/netpp
install -m 755 master/netpp-cli /tmp/bin
install -m 755 master/netpp-config /tmp/bin
install -m 755 master/netpp-dump /tmp/bin
echo
echo Installed in /tmp/bin
