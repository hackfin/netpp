/* UDP (re-)implementation
 *
 * (c) 08/2008 <hackfin@section5.ch>
 *
 *
 */

// Maximum possible
// #define MAX_PACKET_SIZE    1472

// We choose it a bit smaller for now:
#ifndef TINYCPU
#define MAX_PACKET_SIZE    1464
#endif

// Default timeout to check for a request (outside protocol) in ms
#define TIMEOUT_CHECK        10

// Default timeout to wait for a response inside protocol in ms
#define TIMEOUT_WAIT        250

#define HDR_SIZE sizeof(PropertyPacketHdr)

#include <stdio.h> // Order matters. Can mess up fd_set definition on LWIP
#include "devlib.h"
#include "devlib_error.h"
#include "property_protocol.h"
#ifndef LWIP_COMPAT_SOCKETS
#include <errno.h>
#endif

int net_peer_exit(Peer *p, int kill);

int udp_sock_init(SOCKET *sock, SockAddrIn *sin)
{
	int error = 0;
	SOCKET s;


	s = (SOCKET) socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP); 
	if (s == INVALID_SOCKET) { 
		netpp_log(DCLOG_ERROR, "Error initializing UDP socket: %s",
#ifdef LWIP_COMPAT_SOCKETS
			"FIXME"
#else
			strerror(errno)
#endif
		);
		return DCERR_COMM_INIT;
	}

	sin->sin_addr.s_addr    = INADDR_ANY;
	sin->sin_family         = AF_INET;
#ifdef LWIP_COMPAT_SOCKETS
	sin->sin_len            = sizeof(sin);
#endif

	*sock = s;

	return error;
}


static
int peer_init(Peer *p)
{
	p->sendbuf = buf_alloc(MAX_PACKET_SIZE * 2);
	if (!p->sendbuf) return DCERR_MALLOC;
	p->recvbuf = (char *) &p->sendbuf[MAX_PACKET_SIZE];

	p->recvtimeout_check =  TIMEOUT_CHECK;
	p->recvtimeout_wait  =  TIMEOUT_WAIT;

	p->packetsize = MAX_PACKET_SIZE - HDR_SIZE;
	return 0;
}


int udp_connection_init(Peer *p, int bufsize)
{
	int error;
	SockAddrIn sin;
	socklen_t size = (socklen_t) sizeof(int);
#ifdef __WIN32__
	WSADATA data;
#endif
	SOCKET sock;


#ifdef __WIN32__
	error = WSAStartup(MAKEWORD(2, 2), &data);
	if (error != 0) {
		return DCERR_COMM_INIT;
	}
#endif
	
	// Set up socket for local source port
	sin.sin_port = htons(p->ip.sport);
	error = udp_sock_init(&sock, &sin);
	if (error >= 0) {
		setsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char *) &bufsize,
			size);
		DEB(netpp_log(DCLOG_DEBUG, "Try buffer size: %d", bufsize));
		getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char *) &bufsize,
			&size);
		DEB(netpp_log(DCLOG_DEBUG, "Receive buffer size: %d", bufsize));

		// Connect to peer
		sin.sin_addr.s_addr = p->ip.ipaddr;
		sin.sin_port        = htons(p->ip.dport);

		error = connect(sock, (SockAddr *) &sin, sizeof(sin));
		p->fd = sock; // just copy
	}

	if (error < 0) {
		error = handle_sockerr(error, "connecting to socket");
		return error;
	}

	error = peer_init(p);

	return error;
}

int udp_connection_exit(Peer *p)
{
	int error = 0;

	// End session
#ifdef __WIN32__
	error = shutdown(p->fd, SD_BOTH);
#else
	error = shutdown(p->fd, SHUT_RDWR);
#endif
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Failed to shutdown, error %d\n", error);
		error = DCERR_COMM_INIT;
	}

	close(p->fd);
	return 0;
}

static
int read_packet(Peer *p)
{
	int n;
	socklen_t len = sizeof(SockAddrIn);

	char *buf = p->recvbuf;

	// Read Header from stream

#ifdef __WIN32__
	n = recv(p->fd, buf, MAX_PACKET_SIZE, 0);
#else
	n = recv(p->fd, buf, MAX_PACKET_SIZE, MSG_DONTWAIT | MSG_TRUNC);
#endif

	if (n < 0) return handle_sockerr(n, "reading packet");
	// If n = 0, connection ended. If we miss the header, fail too.
	if (n < HDR_SIZE) {
		netpp_log(DCLOG_ERROR, "Packet size mismatch: %d < 14\n", n);
		return DCERR_COMM_GENERIC;
	}

//	printf("<");
//	packet_dump(buf, n);

	len = get_packet_length(buf);
	
	if (len + HDR_SIZE < n) {
		netpp_log(DCLOG_ERROR,
			"Packet size mismatch: payload %d < %lu", len, n - HDR_SIZE);
		return DCERR_COMM_GENERIC;
	}

	return n;
}

int udp_send_packet(Peer *peer, PropertyPacketHdr *h,
		const char *data, size_t len)
{
	int error;
	int n;
	unsigned short type = ntohs(h->type);
	DEB(netpp_log(DCLOG_DEBUG, "Send packet dest: %08lx, dport: %d len: %ld",
		peer->ip.ipaddr, peer->ip.dport, len));

	// Now send packet fragments
	int i = 0;

	h->id = htons(i);
	n = peer->packetsize;

	while (len > n) {
		h->type = htons(type | PP_FLAG_FRAG);
		DEB(netpp_log(DCLOG_DEBUG, "Clear to send (%d left)", len));
		error = net_send_slice(peer, h, data, n);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Failed to send slice");
			return error;
		}
		len -= n;
		data += n;

		// Update length field
		h->sub.set.len = htonl((unsigned long) len);

		i++;
		h->id = htons(i);
	}

	h->type = htons(type);

	error = net_send_slice(peer, h, data, (unsigned short) len);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Failed to send slice");
	}

	return error;
}


////////////////////////////////////////////////////////////////////////////
// Client methods:


int udp_open(RemoteDevice *d, int portindex)
{
	int error;
	const char *addr;

	addr = getname(&d->peer, portindex);
	if (!addr) return DCERR_PROPERTY_UNKNOWN;

	error = net_openbyname(d, addr, udp_connection_init);
	if (error < 0) return error;

	// Begin session:
	// We are aware that we call back and again to a callback :-)
	error = protocol_request(d, REQUEST_SESSION_BEGIN);
	if (error < 0) {
		net_peer_exit(&d->peer, 0);
		return error;
	}
	return 0;
}

int udp_close(RemoteDevice *d)
{
	int error;

	Peer *p = &d->peer;

	DEB(netpp_log(DCLOG_VERBOSE, "Disconnect: %08x:%d",
		p->ip.ipaddr,
		p->ip.sport));

	error = protocol_request(d, REQUEST_SESSION_END);
	if (error < 0) return error;


	error = udp_connection_exit(p);

	if (p->sendbuf) buf_free(p->sendbuf);
	return error;
}

int udp_poll_packet(Peer *p, PropertyPacketHdr **h, unsigned short timeout)
{
	int n = 0;
	fd_set fds;
	struct timeval tv;

	FD_ZERO(&fds);
	FD_SET(p->fd, &fds); // select() on socket

	tv.tv_sec = timeout / 1000; tv.tv_usec = (timeout % 1000) * 1000;

	select(p->fd + 1, &fds, NULL, NULL, &tv);
	if (FD_ISSET(p->fd, &fds)) {
		n = read_packet(p);
		*h = (PropertyPacketHdr *) p->recvbuf;
		if (n == 0) n = -1;
	}
	return n;
}


#ifndef C99_COMPLIANT
struct protocol_methods udp_methods = {
#ifdef CONFIG_NOPROBE
	0,
#else
	&net_probe,
#endif
	&net_create,
	&udp_open,
	&udp_close,
	&udp_send_packet,
	&udp_poll_packet
};
#else
struct protocol_methods udp_methods = {
#ifdef CONFIG_NOPROBE
	.probe   = 0,
#else
	.probe   = &net_probe,
#endif
	.create  = &net_create,
	.open    = &udp_open,
	.close   = &udp_close,
	.send    = &udp_send_packet,
	.recv    = &udp_poll_packet,
	.release = 0
};
#endif

////////////////////////////////////////////////////////////////////////////
// Server methods:
//


static
int servercontext_init(ServerContext *serv, const char *name)
{
	// Reset user count

	serv->flags = 0;
	serv->usercount = 0;
	serv->peer_init = peer_init;
 	serv->hub = hub_reuse(name, &udp_methods);
	if (serv->hub == TOKEN_INVALID) return DCERR_MALLOC;
	// Reserve device list:
	serv->peerdevs = Hub(GET_PORTROOT(), UDP_PEERS_NAME, 0);
	// But we don't append this to the global portbay.

	if (serv->peerdevs == TOKEN_INVALID) return DCERR_MALLOC;

	serv->devices =
		portbay_fromtoken(desc_fromtoken(serv->peerdevs)->hub.ports);

	serv->slave_handler = slave_handler;
	serv->cleanup = NULL;
	return 0;
}

int udp_server_init(int listen_port, ServerContext *serv, const char *name)
{
	int error;
	SockAddrIn sin;
	SOCKET sock;

#ifdef __WIN32__
	WSADATA data;
#endif

	error = servercontext_init(serv, name);
	if (error < 0) return error;


#ifdef __WIN32__
	error = WSAStartup(MAKEWORD(2, 2), &data);
	if (error != 0) {
		return DCERR_COMM_INIT;
	}
#endif

	// Set up socket:
	sin.sin_port = htons(listen_port);
	error = udp_sock_init(&sock, &sin);
	if (error >= 0) {
		error = bind(sock, (SockAddr *) &sin, sizeof(SockAddr));
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Could not bind UDP port");
		}

		if (error == 0) {
			serv->listener = sock;
			// Add to global server list:
			error = add_server_entry(serv, listen_port);
		}
	}
#ifdef __WIN32__
	if (error < 0) {
		WSACleanup();
	}
#endif
	return error;
}

int udp_serve(ServerContext *serv)
{
	int error;
	if (net_serve_users(serv) <= 0) {
		error = udp_listen_incoming(serv, TIMEOUT_CHECK);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Error when listening to UDP socket: %s",
#ifdef LWIP_COMPAT_SOCKETS
				"FIXME"
#warning "COMPAT_SOCKETS"
#else
				strerror(errno)
#endif
			);
			return error;
		}
	}
	return 0;
}

/* These are the server codes we know. If a server id matches one in this
 * list, we send a response packet on discovery request.
 */

static const char *serv_codes[3] = {
	UDP_SERVER_NAME,
	TCP_SERVER_NAME,
	PRX_SERVER_NAME,
};

static
int udp_send_info(PropertyPacketHdr *h, SOCKET s, SockAddrIn *from)
{
	int error;
	unsigned short i;
	const char *cmp = "";
	char *name;
	int port;
	DynPropertyDesc *pd;
	PortBay *b;

	i = ntohs(h->sub.status.code);
	if (i > 2) {
		netpp_log(DCLOG_ERROR, "Wrong server code received");
		return 0;
	}

	cmp = serv_codes[i];

	// We respond with an 'answer' token:
	h->sub.status.token = htonl(REQUEST_PROBE_ANSWER);

	b = GET_PORTROOT();

	PortToken d = ITERATOR_FIRST(b);
	PortToken n = PORTTOKEN_NIL;

	while (ITERATOR_VALID(d)) {
		pd = desc_fromtoken(d);
		DEB(netpp_log(DCLOG_VERBOSE, "Node: %s", pd->name));
		if (strcmp(pd->name, cmp) == 0) {
			// Send a packet for each Server node that matches:
			error = port_select(d, n, &n);
			while (error == 0) {
				name = desc_fromtoken(n)->name;
				if (name[0] == ':') {
					port = atoi(&name[1]);
				} else {
					port = 0;
				}
				if (port != DISCOVERY_PORT) {
					h->sub.status.code = htons(port);
					DEB(netpp_log(DCLOG_DEBUG,
						"Send %s discovery packet, port: %d",
						cmp, port));
					error = sendto(s, (char *) h, HDR_SIZE, 0,
						(SockAddr *) from, sizeof(SockAddrIn));
					if (error < 0)
						return handle_sockerr(error, "UDP socket sendto()");
				}
				error = port_select(d, n, &n);
			}
		}
#ifdef DEBUG
		// else netpp_log(DCLOG_ERROR,
		// "Not sending answer to %s (query code %d)", pd->name, i);
#endif
		d = ITERATOR_NEXT(b, pd);
	}
	return 0;
}

static
int parse_request(SOCKET s, SockAddrIn *from)
{
	socklen_t len = sizeof(SockAddrIn);
	int n, error;
	static PropertyPacketHdr h;

	error = recvfrom(s, (char *) &h, HDR_SIZE, 0, (SockAddr *) from, &len);
	if (error < 0) return handle_sockerr(error, "socket write()");
	n = ntohl(h.sub.status.token);

	if (n == REQUEST_PROBE) {
		error = udp_send_info(&h, s, from);
	} else
	if (n == REQUEST_PROBE_ANSWER) {
		// Hack peer answer data into 'from':
		from->sin_port = h.sub.status.code;
	} else {
		// Clear return code (no error):
		h.sub.status.token = 0;
		h.sub.status.code = htons(0);
		error = sendto(s, (char *) &h, HDR_SIZE, 0, (SockAddr *) from, len);
	}

	if (error < 0) return handle_sockerr(error, "UDP socket sendto()");

	return n;
}

int udp_listen_incoming(ServerContext *serv, unsigned int timeout_ms)
{
	int error;
	int rq;
	SockAddrIn from;
	fd_set fds;
	SOCKET sock = -1;
	SOCKET listener = serv->listener;
	struct timeval tv;

	// Listen for new users coming in
	FD_ZERO(&fds);
	FD_SET(listener, &fds);
	tv.tv_sec = 0; tv.tv_usec = timeout_ms * 1000;

	error = select(listener + 1, &fds, NULL, NULL, &tv);
	if ((error > 0) && FD_ISSET(listener, &fds)) {

		// If we got a SESSION_BEGIN PP_REQUEST, initialize peer
		rq = parse_request(listener, &from);
		switch (rq) {
			case REQUEST_PROBE:
				// No need to do anything, parse_request already answered.
				error = 0;
				DEB(netpp_log(DCLOG_DEBUG, "probe request from: %08x:%d",
					from.sin_addr.s_addr,
					ntohs(from.sin_port)));
				break;
			case REQUEST_PROBE_ANSWER:
				DEB(netpp_log(DCLOG_DEBUG, "got probe answer: %08x:%d",
					from.sin_addr.s_addr,
					ntohs(from.sin_port)));

				error = remotedevice_add(serv, sock, &from, DC_PORT);
				if (error >= 0) error = DCWARN_SLAVE_HANDLED;
				break;

			case REQUEST_SESSION_BEGIN:
				// TODO: Find out first, whether we are in the device
				// list. If yes, reuse entry (reconnect)
				// Connect fd socket:

				// Create a copy of that socket
#if defined(__WIN32__) || defined(LWIP_COMPAT_SOCKETS)
				sock = serv->listener;
#else
				sock = dup(serv->listener);
#endif
				// sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP); 

				error = connect(sock, (SockAddr *) &from, sizeof(from));
				if (error < 0)
					error = handle_sockerr(error, "connecting UDP socket");

				error = remotedevice_add(serv, sock, &from, DC_DEVICE);
				if (error < 0) return error;

				DEB(netpp_log(DCLOG_VERBOSE, "UDP Sign on: %08x:%d",
					from.sin_addr.s_addr,
					ntohs(from.sin_port)
				));
				break;
			default:
				netpp_log(DCLOG_DEBUG, "Query command not acknowledged");
				return DCERR_COMM_NAK;
		}
	}
	return error;
}

int udp_server_exit(ServerContext *serv)
{
	int error;

	net_disconnect_users(serv);
#ifdef __WIN32__
	error = shutdown(serv->listener, SD_BOTH);
	closesocket(serv->listener);
	WSACleanup();
#else
	error = shutdown(serv->listener, SHUT_RDWR);
	close(serv->listener);
#endif


	// Release reserved pools:
	// netpp_log(DCLOG_ERROR, "FREE SERVER");
	portdesc_free(serv->hub);
	// netpp_log(DCLOG_ERROR, "FREE PEERS");
	portdesc_free(serv->peerdevs);

	return error;
}

