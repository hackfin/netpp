/** \file version.h
 * Version query of DLL
 *
 * $Id: version.c 2 2007-01-26 20:14:02Z strubi $
 */

#include "devlib.h"

extern const char g_build[];
extern int        g_version[];

const char * APIDECL
dcDeviceGetDllVersion(DEVICE d, int *major, int *minor)
{
	*major = g_version[0];
	*minor = g_version[1];
	return g_build;
}

