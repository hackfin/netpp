/** \file rawio.h
 *
 * Functions to be implemented by raw I/O layer.
 *
 */

/** Open board specific netpp device */
int my_open(const char *name);

/** Close netpp device */
int my_close(int fd);

/** Read from netpp device */
int my_read(int fd, char *buf, int len);

/** Write to netpp device */
int my_write(int fd, const char *buf, int len);

/** Poll netpp device for data */
int my_poll(int fd, int dummy, unsigned short timeout);
