/** Dynamic property implementation
 *
 * (c) 2012, Martin Strubel // <hackfin@section5.ch>
 *
 * Dynamic properties are like standard properties, but they can be
 * created on the fly. They are just an extension of the already existing
 * dynamic port, hub and device nodes.
 *
 *
 */

#include <stdio.h> // XXX
#include "property_types.h"
#include "property_protocol.h"
#include "dynprops.h"
#include "devlib_error.h"

#define INDEX_FROM_TOKEN(t) (t & PROP_MASK)
#define BUILD_DYN_TOKEN(index) (DYNAMIC_PROPERTY | index)
#define GET_PROP(t) &g_dyn.pool.base[INDEX_FROM_TOKEN(t)]

struct dynprops {
	DlistPool pool;
} g_dyn;


// XXX

DynPropertyDesc *getDynProperty(TOKEN t)
{
	DynPropertyDesc *dp;
	t = INDEX_FROM_TOKEN(t);
	if (t >= g_dyn.pool.n) return NULL;
	dp = &g_dyn.pool.base[t];

	if (dp->type == DC_NULL) return NULL;
	return dp;
}

int dynprop_init(int maxprops)
{
	int error;
	error = pool_new(&g_dyn.pool, maxprops);
	g_dyn.pool.first =
	g_dyn.pool.last = 0;
	return error;
}

void dynprop_exit(void)
{
	pool_free(&g_dyn.pool);
}

/** Creates a dynamic property from a static property entry.
 * Note that this static property can be owned only by ONE dynprop
 */

DynPropToken new_dynprop(const char *name, PropertyDesc *prop)
{
	int n;
	DynPropertyDesc *pd;
	int i;

	if (name) {
		n = (int) strlen(name) + 1;
		if (n > NAMELENGTH) return INVALID_TOKEN;
	} else {
		n = 0;
	}

	i = dlist_new(&g_dyn.pool, &pd);
	if (i < 0) return INVALID_TOKEN;

	pd->prop.desc = prop;

	// Hack for id when not specified:
	if (!n) {
		sprintf(pd->name, "prop_%04x", i & 0xffff);
	} else {
		string_copy(pd->name, name, n);
	}

	// Link name:
	prop->name = pd->name;
	pd->type = DC_DPROP;
	pd->prop.first =
	pd->prop.last = -1;
	pd->prev =
	pd->next = -1;

	// Track last reserved token:
	g_dyn.pool.last = i;

	return BUILD_DYN_TOKEN(i);
}

/* Walks only local (dynamic) nodes */

static
TOKEN traverse(DynPropertyDesc *dpd, DynPropToken parent, DynPropToken prev)
{
	TOKEN t;
	if ((prev == parent) || (prev == TOKEN_INVALID)) {
		if (dpd->prop.first == -1) {
			t = TOKEN_INVALID;
		} else {
			t = BUILD_DYN_TOKEN(dpd->prop.first);
		}
	} else {
		dpd = getDynProperty(prev);
		if (!dpd || dpd->next == -1) t = TOKEN_INVALID;
		else t = BUILD_DYN_TOKEN(dpd->next);
	}
	return t;
}

/* Dynamical version of property_select. Called by the latter. */

TOKEN dynprop_select(DynPropToken parent, DynPropToken prev)
{
	TOKEN t;
	TOKEN index;
	int i;
	DynPropertyDesc *dpd;
	const PropertyDesc *pd;
	dpd = getDynProperty(parent);
	if (!dpd) return INVALID_TOKEN;

	if (prev & DYNAMIC_PROPERTY) {

		t = traverse(dpd, parent, prev);

		// When done searching, see if there is a base class
		if (t == TOKEN_INVALID) {
			pd = dpd->prop.desc;
			if (pd->flags & F_LINK) {
				t = follow_link(&pd);
				t = nextVisibleToken(t, pd, 0, 0);
			}
		}
	} else {
		// FIXME: Dynamic property roots can handle ONLY ONE level
		// of derivation just now.
		// Parent is dynamic, but child static, means we're in the base class:
		pd = dpd->prop.desc;

		// Move to base class:
		t = follow_link(&pd);

		i = 0;
		// Find position of member 'prev':
		index = pd->members[i++];

		while (index != INDEX_FROMTOKEN(prev)) {
			index = pd->members[i++];
			if (index == INVALID_INDEX) {
				netpp_log(DCLOG_ERROR, "Proplist error: empty members array in %08x", t);
			}
		}

		t = nextVisibleToken(t, pd, i, 0);
	}

	return t;
}

PropertyDesc *dynprop_get(DynPropToken t)
{
	DynPropertyDesc *pd;

	pd = getDynProperty(t);
	if (!pd) {
		netpp_log(DCLOG_ERROR, "Illegal dynamic property TOKEN (%08x)", t);
		return NULL;
	}
	if (!pd->prop.desc)
		netpp_log(DCLOG_ERROR, "Warning: No property desc\n");
	return pd->prop.desc;
}

int dynprop_append(DynPropToken parent, DynPropToken elem)
{
	DynPropertyDesc *pd;
	pd = GET_PROP(parent);
	elem = INDEX_FROM_TOKEN(elem);
	// Do we already have children?
	if (pd->prop.last != -1) {
		dlist_link(&g_dyn.pool, pd->prop.last, elem);
		pd->prop.last = elem;
	} else {
		pd->prop.first =
		pd->prop.last = elem;
	}
	return 0;
}

int dynprop_unlink(DynPropToken parent, DynPropToken elem)
{
	// netpp_log(DCLOG_DEBUG, "Free %08x from %08x", elem, parent);
	DynPropertyDesc *pd, *cur;
	DlistPool *pool = &g_dyn.pool;
	pd = GET_PROP(parent);
	elem = INDEX_FROM_TOKEN(elem);
	cur = dlentry_as_pointer(pool, elem);

	cur->type = DC_NULL;

//	printf("first: %d last: %d unlink: %d\n",
//		pd->prop.first, pd->prop.last, elem);

	if (pd->prop.last == elem) {
		// printf("new last: %d\n", cur->prev);
		pd->prop.last = cur->prev;
	}
	if (pd->prop.first == elem) {
		// printf("new first: %d\n", cur->prev);
		pd->prop.first = cur->next;
	}
	dlist_remove(pool, elem);

	// Dirty workaround: because we used our own last/first pointers
	// to maintain the hierarchy, we have to run some fixups past
	// the dlist_remove in order to keep the pool last/first pointers
	// sane.
	if (pool->first == INDEX_NIL && pool->last == INDEX_NIL)
		pool->last = elem;
	else if (pool->last == INDEX_NIL) pool->last = pd->prop.last;
	dlist_free(pool, elem);

	return DCWARN_PROPERTY_DESTROYED;
}

int dynproperty_crc(DCValue *val)
{
	INDEX i;
	TOKEN t;
	uint16_t c = 0xface;
	const char *s;

	DynPropertyDesc *pd;
	DlistPool *pool = &g_dyn.pool;

	i = pool->first;

	// Build CRC16 over token and name
	while (i != pool->last) {
		pd = GET_PROP(i);
		s = pd->name;
		c = crc16((unsigned char *) s, strlen(s), c);
		t = BUILD_DYN_TOKEN(i);
		c = crc16((unsigned char *) &t, sizeof(t), c);
		i++;
	}
	val->type = DC_UINT;
	val->value.u = c;
	return 0;
}

int dyntree_free(DEVICE d, DynPropToken root, prop_release_func propfree)
{
	TOKEN t, next;

	t = property_select(LOCAL_DEVICE(), root, root);

	if (t == TOKEN_INVALID) { // We have no children.
		if (root & DYNAMIC_PROPERTY) {
			PropertyDesc *p = dynprop_get(root);
			propfree(p);
		}
		return 1;
	}

	do {
		 // get next before removing current
		next = traverse(getDynProperty(root), root, t);
		if (t & DYNAMIC_PROPERTY) {
			PropertyDesc *p = dynprop_get(t);
			if (p) {
				dyntree_free(d, t, propfree);
				dynprop_unlink(root, t); // Remove current
			}
		} else {
			netpp_log(DCLOG_ERROR, "Foul node %08x, broken tree?\n", t);
			break;
		}
		t = next;
	} while (next != TOKEN_INVALID);
		
	return 0;
}

////////////////////////////////////////////////////////////////////////////
// Extended dyn prop API:

#ifndef NO_MALLOC
PropertyDesc *property_desc_new(const PropertyDesc *template)
{
	PropertyDesc *p;
	p = (PropertyDesc*) malloc(sizeof(PropertyDesc));
	if (p) {
		memcpy(p, template, sizeof(PropertyDesc));
	}
	return p;
}
#endif

TOKEN dynprop_from_entity(TOKEN parent, void *entity, 
	TOKEN template, const char *name)
{
	TOKEN t, walk;
	PropertyDesc *p;
	const PropertyDesc *child;
	const PropertyDesc *tdesc = getProperty_ByToken(template);

	p = property_desc_new(tdesc);
	if (!p) return TOKEN_INVALID;

	switch (p->where) {
		case DC_CUSTOM:
			// Story entity handle in custom pointer
			p->access.custom.p = entity;
			break;
		case DC_STATIC:
		case DC_HANDLER:
			// Special case for struct handler:
			if (p->type == DC_STRUCT) {
				p->access.custom.p = entity;
			}
			break;
		case DC_VAR:
			// Dirty. But we know all pointers of the various
			// variable types live at the same offset:
			if (entity) {
				p->access.buf.p = &((unsigned char *) entity)[p->access.s_uint];
			} else {
				p->access.buf.p = (void *) tdesc->access.varp_int;
			}
			break;
		default:
			netpp_log(DCLOG_ERROR,
				"Location type %d for new dynamic node '%s' not supported", p->where, name);
			return TOKEN_INVALID;
	}

	t = new_dynprop(name, p);
	// iterate children:

	if (t != TOKEN_INVALID) {
		dynprop_append(parent, t);
		// and create its children:
		// Select first child from template:
		walk = property_select(0, template, template);
		while (walk != TOKEN_INVALID) {
			child = getProperty_ByToken(walk);
			dynprop_from_entity(t, entity, walk, child->name);
			// Get successor:
			walk = property_select(0, template, walk);
		}
	}
	return t;
}


TOKEN dynprop_destroy(TOKEN parent, TOKEN prop, prop_release_func propfree)
{
	int ret;
	// Remove children of this node first:
	dyntree_free(NULL, prop, propfree);
	// Then commit suicide:
	ret = dynprop_unlink(parent, prop);
	return ret;
}

