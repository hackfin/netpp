#!/usr/bin/env python

import sys
import time

sys.path.append("../../Debug")
sys.path.append("../../python")
import netpp

d = netpp.connect("TCP:127.0.0.1:2008")

r = d.scan()

r.Client.set("TCP:localhost:2009")

r.Mode.set(1)
r.Stream.Start.set(1)
time.sleep(5)
r.Stream.Stop.set(1)

