<?xml version="1.0" encoding="ISO-8859-1"?>

<!--

$Id: errorlist.xsl 335 1970-01-06 02:51:45Z strubi $

Transformation style sheet DEVDESC-XML -> Error code C header source

(c) 2004, 2005 section5 // Martin Strubel <strubel@section5.ch>

-->

<xsl:stylesheet version="1.0" 
	xmlns:my="http://www.section5.ch/dclib/schema/devdesc"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

	<xsl:import href="errorenum.xsl"/>

	<xsl:output method="text" encoding="ISO-8859-1"/>


	<xsl:template match="/">
/**************************************************************************
 *
 * (c) 2005-2009 section5 // Martin Strubel
 *
 * THIS IS A GENERATED FILE. DO NOT MODIFY! ALL CHANGES WILL BE LOST
 *
 *
 **************************************************************************/

/** \ingroup ErrorHandling */

enum {
	/* Errors: */
<xsl:apply-templates select=".//my:errorspec" mode="enum"/>
	/* Warnings: */
<xsl:apply-templates select=".//my:warnspec" mode="enum"/>
};

<xsl:text>
</xsl:text>
	</xsl:template>
</xsl:stylesheet>

