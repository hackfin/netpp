/* Implementation of "DList pool".
 *
 * This is a double linked list using a "Pool" as memory base and
 * indices for the links.
 *
 * It is meant for allocation of linked objects that are often created,
 * destroyed and reallocated.
 *
 * (c) 2007 Martin Strubel <hackfin@section5.ch>
 *
 * BIG FAT WARNING: Since we can't do all the neat C++ smart pointer stuff,
 * the user MUST TAKE CARE of his pointers. Whenever a new list item is
 * created inside a pool, a reallocation may happen. In this case, external
 * pointers to a structure inside the pool are INVALIDATED.
 * If you want to avoid all this trouble, you must preinitialize the pool to
 * the expected amount of data and undefine ALLOW_REALLOC.
 *
 */

// #define DEBUG

#ifndef NO_MALLOC
#include <stdlib.h>
#endif
#ifdef DEBUG
#include <stdio.h>
#include <string.h>
#endif
#include "devlib_error.h"

#ifdef DEBUG
#include <assert.h>
#endif

#include "property_types.h"

#ifdef NO_MALLOC

// Standalone does not use malloc. Can be called only once.
int pool_new(DlistPool *pool, int initsize)
{
	pool->base = standalone_alloc_pool(initsize);
	pool->n = initsize;
	if (!pool->base) return DCERR_MALLOC;
	return pool_init(pool);
}

#else

int pool_new(DlistPool *pool, int initsize)
{
	pool->n = initsize;
	pool->base = (DLIST_ITEM *) malloc(pool->n * sizeof(DLIST_ITEM));
	if (!pool->base) return DCERR_MALLOC;

	return pool_init(pool);
}

#endif

void pool_free(DlistPool *pool)
{
#ifndef NO_MALLOC
	free(pool->base);
#else
	standalone_dealloc_pool(pool);
#endif
}

int pool_init(DlistPool *pool)
{
	int i;

	for (i = 0; i < pool->n; i++) {
		// pool->base[i].prev = i - 1;
		pool->base[i].next = i + 1;
	}

	// Last = NIL
	pool->base[i-1].next = INDEX_NIL;
	pool->first = pool->last = INDEX_NIL;
	// First free element at 0:
	pool->free = 0;

	return 0;
}

INDEX dlist_new(DlistPool *pool, DLIST_ITEM **pd)
{
	INDEX i;
	DLIST_ITEM * e;

	i = pool->free;

	if (i == INDEX_NIL) {
#ifdef ALLOW_REALLOC
		INDEX n;
		n = pool->n << 1;
		DEB(printf("Realloc %d bytes\n", n));
		e = (DLIST_ITEM *) realloc(pool->base, n * sizeof(DLIST_ITEM));
		if (!e) {
			return INDEX_NIL;
		}
		pool->base = e;
		e = &pool->base[pool->n]; // first free

		// Initialize new block:
		e[-1].next = pool->n;
		for (i = pool->n; i < n; i++) {
			e->prev = i - 1;
			e->next = i + 1;
			e++;
		}
		e[-1].next = INDEX_NIL;
		pool->free = pool->n;
		pool->n = n;
#else
		return INDEX_NIL;
#endif
	}

	// Did we get the last one?
	e = &pool->base[pool->free];
	if (e->next == INDEX_NIL) {
		pool->free = INDEX_NIL;
	} else {
		i = e->next;
		pool->base[i].prev = INDEX_NIL;
		i = pool->free;
		pool->free = e->next;
	}
	*pd = e;
	return i;
}

int dlist_free(DlistPool *pool, INDEX i)
{
	DLIST_ITEM *e;

	if (i == INDEX_NIL) return -1;

	e = &pool->base[i];
	
// We don't need the backward links in the "free" list. Therefore, a few
// lines are commented out here.

#if defined(DEBUG) && !defined(STANDALONE)
	assert(i != pool->free);
#endif

	e->next = pool->free;
	// e->prev = INDEX_NIL; // Mark as invalid
	DEB(printf("Free element %d, prev free: %d\n", i, pool->free));
	pool->free = i;
	return 0;
}

void dlist_link(DlistPool *pool, INDEX to, INDEX i)
{
	DLIST_ITEM *e;
	e = &pool->base[to];
	e->next = i;
	e = &pool->base[i];
	e->prev = to;
	e->next = INDEX_NIL;
}

void dlist_insert(DlistPool *pool, INDEX to, INDEX i)
{
	DLIST_ITEM *e, *ei;
	ei = &pool->base[i];
	e = &pool->base[to];
	if (to == pool->first) {
		pool->first = i;
		ei->prev = INDEX_NIL;
	} else {
		pool->base[e->prev].next = i;
		ei->prev = e->prev;
	}

	ei->next = to;
	e->prev = i;
}

/*
void dlist_unlink(DlistPool *pool, INDEX i)
{
	DLIST_ITEM *e;

	e = &pool->base[i];
	printf("Remove prop %d (p: %d, n: %d)\n", i, e->prev, e->next);

	if (pool->first == i) {
		pool->first = e->next;
	}

	if (e->prev != INDEX_NIL) pool->base[e->prev].next = e->next;

	if (pool->last == i) {
		pool->last = e->prev;
		if (e->prev == INDEX_NIL) printf("WARN: %d has prev NIL\n", i);
	}

	if (e->next != INDEX_NIL) pool->base[e->next].prev = e->prev;
}
*/

int dlist_append(DlistPool *pool, INDEX i)
{
	DLIST_ITEM *e;
	int nil = INDEX_NIL;

	if (pool->last == nil) {
		pool->first = pool->last = i;
		e = &pool->base[i];
		e->next = e->prev = INDEX_NIL;
	} else {
		dlist_link(pool, pool->last, i);
		pool->last = i;
	}

	return 0;
}


void dump(DLIST_ITEM *e); // XXX

int dlist_remove(DlistPool *pool, INDEX i)
{
	DLIST_ITEM *e;

	e = &pool->base[i];
	if (e->prev != INDEX_NIL) {
		pool->base[e->prev].next = e->next;
	} else if (pool->first == i) {
		pool->first = e->next;
		if (e->next != INDEX_NIL) pool->base[e->next].prev = INDEX_NIL;
	}

	if (e->next != INDEX_NIL) {
		pool->base[e->next].prev = e->prev;
	} else if (pool->last == i) {
		pool->last = e->prev;
		if (e->prev != INDEX_NIL) pool->base[e->prev].next = INDEX_NIL;
	}

	return 0;
}

void dlist_clear(DlistPool *d)
{
	// Insert first element into "free" list.
	dlist_free(d, d->first);
	d->first = d->last = INDEX_NIL;
}

DLIST_ITEM *dlentry_as_pointer(DlistPool *pool, INDEX i)
{
	return &pool->base[i];
}

#ifdef TEST_STRUCT

#include <stdio.h>

void dlist_dump(DlistPool *pool, dispfunc f)
{
	INDEX i;
	DLIST_ITEM *e;

	i = pool->first;

	while (i != INDEX_NIL) {
		e = &pool->base[i];
		if (f) f(e);
		else printf("[%i]: %s", i, e->name);
		i = e->next;
	}

	if (!f) {
		printf("Free:\n");

		i = pool->free;

		while (i != INDEX_NIL) {
			e = &pool->base[i];
			printf("[%i]\n", i);
			i = e->next;
		}
	}
}

#endif
