
#include <stdio.h>
#include <pthread.h>

struct gcontext {
	DEVICE peerdev;
	pthread_t thread;
	TOKEN t_data;
	char client[64];
	int mode;

	FILE *file;
	int blocksize;
	char *buffer;
	int cursize;
};

extern struct gcontext context;

