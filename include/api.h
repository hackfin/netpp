////////////////////////////////////////////////////////////////////////////
//
//  API DEFINITION FILE
//
//  Here the behaviour of the exported API is specified.
//
//
////////////////////////////////////////////////////////////////////////////

#ifndef API_H_INCLUDED
#define API_H_INCLUDED

#include "platform.h"

// DEBUGGING

#ifdef DEBUG
#include <stdio.h>
#define DEB(x) x
#else
#define DEB(x) 
#endif

#if defined (WIN32) && defined (USE_STDCALL)
#	define APIDECL WINAPI
#else
#	define APIDECL
#endif


#ifndef EXTERN_C
#ifdef __cplusplus
#	define EXTERN_C extern "C"
#else
#	define EXTERN_C 
#endif
#endif

#ifdef __cplusplus
#	define DC_INLINE inline
#else
#ifdef __GNUC__
#	define DC_INLINE static __inline__
#else
#	define DC_INLINE
#endif
#endif

#if defined (WIN32) && !defined(MSVC_STATIC)
#	if defined (DLL_EXPORTS)
#		define DLL_API EXTERN_C __declspec(dllexport)
#	else
#		define DLL_API EXTERN_C __declspec(dllimport)
#	endif
#else
#	define DLL_API EXTERN_C
#endif


#endif // API_H_INCLUDED
