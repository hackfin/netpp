#include "property_protocol.h"

/**
 * \defgroup SlaveAux Slave auxiliaries
 */

/**
 * \addtogroup SlaveAux
 * \{
 */


/** The netpp server main loop.
 * This example serves USB, Devices and TCP simultaneously (if configured)
 * 
 * Note that several users can connect via network protocols such as TCP.
 * Access and locking of resources must be sorted out on a property
 * level.
 *
 * The parameters are normally passed from the command line.
 *
 * \param argc         Number of arguments passed from command line
 * \param argv         String array
 *
 */

int start_server(int argc, char **argv);

/** Device array in property list proplist.c */
extern DeviceDesc g_devices[];

/** Size of g_devices array, also instanced in proplist.c */
extern int g_ndevices;

/** Initialize property tables (non C99 compilers only)
 *
 * Somewhat stupid compilers like MSVC can't use designated initializers.
 * So they need this workaround.
 */
#ifndef C99_COMPLIANT
	void init_properties(void);
#endif

/* \} */

