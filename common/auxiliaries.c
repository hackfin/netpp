/* Auxiliary functions */

#include "devlib.h"
#include "property_types.h"
#include "property_protocol.h"
#include "devlib_error.h"

#include <stdio.h>

#if _MSC_VER == 1400
#define snprintf sprintf_s
#elif defined(_MSC_VER)
#define snprintf _snprintf
#endif

int
dcValue_ToString(DCValue *val, char *outs, int len)
{
	switch (val->type) {
		case DC_BOOL:
			if (val->value.i) {
				snprintf(outs, len, "TRUE (%d)", val->value.i);
			} else {
				snprintf(outs, len, "FALSE (%d)", val->value.i);
			}
			break;
		case DC_INT:
			snprintf(outs, len, "%d", val->value.i);
			break;
		case DC_REGISTER:
			snprintf(outs, len, "0x%x", val->value.i);
			break;
		case DC_MODE:
			snprintf(outs, len, "#%d", val->value.i);
			break;
		case DC_FLOAT:
			snprintf(outs, len, "%e", val->value.f);
			break;
		case DC_STRING:
			snprintf(outs, len, "%s", (char *) val->value.p);
			break;
		case DC_BUFFER:
			snprintf(outs, len, "[ Buffer : size:%lu ]",
				(unsigned long) val->len);
			break;
		case DC_COMMAND:
			snprintf(outs, len, "[ Command : %s ]",
				(val->value.i) ? "BUSY" : "DONE");

			break;
		case DC_EVENT:
			snprintf(outs, len, "[ Event : 0x%08x ]", val->value.u);
			break;

		default:
			DEB(printf("Bad type field\n"));
			return DCERR_PROPERTY_TYPE_MATCH;
	}
	return 0;
}

static int parse_mode(DEVICE d, TOKEN p, DCValue *val, const char *s)
{
	char string[40];
	int error;
	sprintf(string, ".%s", s);
	DCValue mode;
	TOKEN m;
	m = p; // Query inside 'p' namespace:
	error = dcProperty_ParseName(d, string, &m);
	if (error == 0) {
		error = dcDevice_GetProperty(d, m, &mode);
		if (error == 0) *val = mode;
	}
	return error;
}

/** Set property by string.
 * Performs the proper parsing of the string
 *
 * */

int 
dcString_ToValue(DEVICE d, TOKEN p, DCValue *val, const char *string)
{
	int error = 0;

	int type;

	val->type = DC_INVALID;

	TOKEN elem;

	error = dcProperty_GetProto(d, p, val);
	type = val->type;
	if (type == DC_ARRAY) {
		// in case of an array, we have to query its element's
		// data type
		error = dcProperty_Select(d, p, p, &elem);
		if (error == 0) 
			error = dcProperty_Select(d, p, elem, &elem);

		if (error < 0) return error;
		dcProperty_GetProto(d, p, val);
		// and we just pass on this type to the 'guesser'.
		type = val->type;
	}

	switch (type) {
		case DC_MODE:
			if ( 
				sscanf(string, "0x%x", &val->value.i) == 1 ||
			    sscanf(string, "%d", &val->value.i) == 1 || 
			    sscanf(string, "#%d", &val->value.i) == 1   ) {
				val->type = type;
			} else {
				parse_mode(d, p, val, string);
			}
			break;
		case DC_REGISTER:
		case DC_BOOL:
		case DC_INT:
		case DC_COMMAND:
			if ( 
				sscanf(string, "0x%x", &val->value.i) == 1 ||
			    sscanf(string, "%d", &val->value.i) == 1 || 
			    sscanf(string, "#%d", &val->value.i) == 1   ) {
				val->type = type;
			} else
			if (type == DC_COMMAND) {
				val->type = type;
				// 'hardware' fallback:
				val->value.i = -1; // All bits set
			}
			break;
		case DC_FLOAT:
			if (sscanf(string, "%f", &val->value.f)) {
				val->type = type;
			}
			break;
		case DC_STRING:
			val->len = strlen(string);
			val->value.p = (void *) string;
			val->type = type;
			break;

		default:
			DEB(printf("This data type can not yet get processed by:\n"
			       "    dcDevice_SetProperty_String()\n"));
			return DCERR_PROPERTY_HANDLER;
	}

	// If data could not be parsed, report
	if (val->type == DC_INVALID)
		return DCERR_PROPERTY_TYPE_MATCH;

	return error;
}

int dcDevice_SetProperty_String(DEVICE d, TOKEN p, const char *string)
{
	DCValue val;
	int error;
	error = dcString_ToValue(d, p, &val, string);
	if (error < 0) return error;
	return dcDevice_SetProperty(d, p, &val);
}

int 
dcDevice_GetProperty_String(DEVICE d, TOKEN p, char *outs, int len)
{
	int error, warn;

	char string[256];

	DCValue val;
	val.value.p = &string[0];
	val.len = sizeof(string);

	len--;

	error = dcDevice_GetProperty(d, p, &val);
	if (error < 0) return error;
	else warn = error;
	error = dcValue_ToString(&val, outs, len);
	if (error < 0) return error;
	return warn;
}


