<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- (c) 2004, 2005 Martin Strubel // strubel@section5.ch
     Translates a non DTD device file such that an xpointer reference
     (implicit ID) refers to the 'id' attribute.
     This is a workaround for XXE not supporting xpointer() syntax.

-->


<xsl:stylesheet version="1.0" 
	xmlns:my="http://www.section5.ch/dclib/schema/devdesc"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xi="http://www.w3.org/2001/XInclude" >

	<xsl:output method="xml" encoding="ISO-8859-1" indent="no"/>	

	<!-- Re-construct xpointer specification -->
	  
	<xsl:template match="xi:include">
		<xi:include>
			<xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
			<xsl:attribute name="xpointer">xpointer(//*[@id ='<xsl:value-of select="@xpointer"/>'])</xsl:attribute>

		</xi:include>

	</xsl:template>

	<!-- And copy all the rest -->

	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>


</xsl:stylesheet>
