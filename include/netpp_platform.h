// NETPP platform dependent definitions

#ifdef __AVR__
// AVR 8 bit hack. Should only apply to 8 bit AVRs..
#define LE_8BIT_CPU
#endif

////////////////////////////////////////////////////////////////////////////
// LINUX (uClinux)
#if defined (__linux__) || defined(__APPLE__)
	
#define HAVE_SOCKET
#define HAVE_MULTIUSER
#define SOCKET int

#ifdef CONFIG_PTHREAD
#include <pthread.h>
#endif

#include <fcntl.h>
#include <unistd.h>

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

// Invalid socket:
#define INVALID_SOCKET 0xffffffff

#if defined(__ADSPBLACKFIN__) || defined (__MIPSEL__)
#	include "bfin-unaligned.h"
#elif defined (i386) || defined (__x86_64__)
#	define ALLOW_UNALIGNED_ACCESS
#	define get_unaligned(x) (*(x))
#	define store_unaligned_float(dst, src) (*(dst) = *( (float *) (src)))
#elif defined (__XTENSA__)
#	include "xtensa-unaligned.h"
#elif defined (__arm__) || defined(__aarch64__)
#	include "arm-unaligned.h"
#else
#	warning "Possibly unsupported (untested) architecture"
#endif

#include <string.h>
#define string_copy(d, s, n) strncpy(d, s, n)

#define __PACKED  __attribute__((packed))

#define NETPP_MUTEX pthread_mutex_t

////////////////////////////////////////////////////////////////////////////
// Windows32
#elif defined(_MSC_VER) || defined(__MINGW32__) || defined(__CYGWIN__) || defined(__BORLANDC__)
#ifndef __WIN32__
#	define __WIN32__ 1
#endif

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <process.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <io.h>

#define HAVE_SOCKET
#define HAVE_MULTIUSER

#if !defined(_MSC_VER) && !defined(__BORLANDC__)
#	define C99_COMPLIANT
#endif

#if defined (__CYGWIN__)
#	include <sys/types.h>
// Might be the cygwin compiler. FIXME:
#	define __FUNCTION__ "<unknown>"
#else
#	ifdef __MINGW32__
#		define __FUNCTION__ "<unknown>"
#	else
		typedef UINT16 uint16_t;
		typedef UINT32 uint32_t;
		typedef INT16 int16_t;
		typedef INT32 int32_t;
#	endif
#endif

#define get_unaligned(x) (*(x))
#define store_unaligned_float(dst, src) (*(dst) = *( (float *) (src)))


#if defined(_MSC_VER) || defined(__BORLANDC__)
#	define __PACKED
#else
#	define __PACKED  __attribute__((packed))
#endif

#define NETPP_MUTEX HANDLE

////////////////////////////////////////////////////////////////////////////
// Standalone / non-OS / shell OS
#else

char *string_copy(char *d, const char *s, unsigned short n);
// #define string_copy(d, s, n) strncpy(d, s, n-1)

#if defined(__ADSPBLACKFIN__) || defined (PLASMA) || \
	defined(__MSP430__)  || defined(__xtensa__)
#	include "bfin-unaligned.h"
#elif defined(__ZPU__)
#	include "zpu-unaligned.h"
#elif defined (__arm__) || defined(__aarch64__)
#	include "arm-unaligned.h"
#else
#	ifndef PARANOIA_MODE
#	define get_unaligned(x) (*(x))
#	endif
#	ifndef LE_8BIT_CPU
#	warning "Unsupported architecture. No unaligned access, possibly"
#	endif
#endif

#ifdef __PLASMA__
#include "rtos.h"
#endif

#define NETPP_MUTEX void

#if defined(LWIP)
#ifdef FREERTOS_SDK
#	include "lwip/sockets.h"
#	define HAVE_SOCKET
#	define SOCKET int
#	define INVALID_SOCKET 0xffffffff
#	ifdef LWIP_FIXES
		typedef uint32_t in_addr_t;
#	endif
#else
#	include "lwip/def.h"
#endif

// If we have UDP/TCP or a proxy service, we can do multi user
#if (defined(CONFIG_TCP) || defined(CONFIG_UDP) || defined(CONFIG_PROXY))
#	define HAVE_MULTIUSER
#endif

// Compatibility:
#define MSG_TRUNC      0x00

#endif // LWIP

#define __PACKED  __attribute__((packed))

#ifdef USE_LIBC_SUBST
#define snprintf(name, n, format, ...) my_sprintf(name, format, ##__VA_ARGS__)
#define printf my_printf
#define vsprintf my_vsprintf
#define vfprintf my_vfprintf
#endif


#endif // __linux__ || __APPLE__ / MSC_VER, etc.

// Insane compiler switch:

#ifdef _MSC_VER 
#	if _MSC_VER == 1400
#		define MSC_2005_SAFE_STRING
#		define string_copy(d, s, n) strncpy_s(d, n, s, n-1)
#	else 	
#		warning "Not safe."
#		define string_copy(d, s, n) strncpy_s(d, s, n)
#		define snprintf _snprintf
#	endif
#else
#	if defined(__WIN32__)
#		warning "Non safe string_copy implementation for win32"
#		define string_copy(d, s, n) strncpy(d, s, n)
#	endif
#endif // _MSC_VER	
	

#ifndef EXTERN_C_BEGIN
#ifdef __cplusplus
#	define EXTERN_C_BEGIN extern "C" {
#	define EXTERN_C_END }
#else
#	define EXTERN_C_BEGIN
#	define EXTERN_C_END
#endif
#endif

// If no native network OS, we need to define network integer
// conversion ourselves
#if !defined(__linux__) && !defined(__APPLE__) && !defined(LWIP) && !defined(__WIN32__)

#ifndef __bswap_16
#define __bswap_16(x)  \
	( ( ((x) >> 8) & 0x00FF) |		\
	  ( ((x) << 8) & 0xFF00) )
#endif

#ifndef __bswap_32
#define __bswap_32(x)  \
	( ( ((x) & 0x000000FF) << 24 ) |  \
	  ( ((x) & 0x0000FF00) <<  8 ) |  \
	  ( ((x) & 0x00FF0000) >>  8 ) |  \
	  ( (((x) >> 24) & 0xFF)) )
#endif

#if (!defined(CONFIG_TCP) && !defined(CONFIG_UDP)) || !defined (htons)
#if defined (__LITTLE_ENDIAN__) || \
	(defined(__BYTE_ORDER__) && (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__))
#if defined __ZPU__
#error "ZPU little endian config not supported"
#endif
#define htons(x) __bswap_16(x)
#define htonl(x) __bswap_32(x)
#define ntohs(x) __bswap_16(x)
#define ntohl(x) __bswap_32(x)
#else
#define htons(x) (x)
#define htonl(x) (x)
#define ntohs(x) (x)
#define ntohl(x) (x)
#endif
#endif

#endif

// Float conversion
#define ntohf(x) (x)
#define htonf(x) (x)

#ifdef __GNUC__
#define C99_COMPLIANT
#endif


