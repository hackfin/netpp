import ch.s5.netpp.Device;

public class Netpp
{
	private static Netpp self = new Netpp();

    private static boolean initialized = false;

    public static Netpp init()
    {
        if (!initialized) {
            initialized = true;
        } else {
            System.out.println("JNETPP present");
        }
        return self;
    }

	static {
		System.loadLibrary("jnetpp");
		System.out.println("JNETPP loaded");
	}

	public static Device device() { return new Device(); }

	public static Device connect(String dest) {
        Device dev = new Device();
		System.out.println("Connecting to device..");
        int res = dev.open(dest);
        if (res < 0) return null;
        return dev;
    }
}
