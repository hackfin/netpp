/* Simple netpp cache */

#include "devlib_types.h"
#include "cache.h"

int cache_init(Cache *cache, int n)
{
	return pool_new(&cache->pool, n);
}

int cache_clear(Cache *c)
{
	dlist_clear(&c->pool);
	return 0;
}

int cache_add(Cache *c, const char *id, TOKEN token)
{
	INDEX i;
	DynPropertyDesc *pd;

	i = dlist_new(&c->pool, &pd);
	if (i == INDEX_NIL) {
		i = c->pool.last;
		pd = dlentry_as_pointer(&c->pool, i);
	} else {
		dlist_append(&c->pool, i);
	}
	strncpy(pd->name, id, sizeof(pd->name));
	pd->cache.rank = 0;
	pd->cache.proptoken = token;
	return i;
}

TOKEN cache_find(Cache *c, const char *id)
{
	INDEX walk;
	DynPropertyDesc *pd, *ppd;
	DlistPool *p = &c->pool;

	walk = p->first;
	while (walk != INDEX_NIL) {
		pd = dlentry_as_pointer(p, walk);
		if (strcmp(pd->name, id) == 0) {
			if (pd->prev != INDEX_NIL) {
				ppd = dlentry_as_pointer(p, pd->prev);
				if (ppd->cache.rank < 255) pd->cache.rank++;
				if (pd->cache.rank >= ppd->cache.rank) {
					if (ppd->cache.rank > 0) ppd->cache.rank--;
					dlist_remove(p, walk);
					dlist_insert(p, pd->prev, walk);
				}
			}
			// printf("Cache hit '%s', rank: %d\n", pd->name, pd->cache.rank, c->root, pd->cache.proptoken);
			return pd->cache.proptoken;
		}
		walk = pd->next;
	}
	return TOKEN_INVALID;
}

