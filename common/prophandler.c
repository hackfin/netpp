/*
 * Property handler module
 *
 * This common module handles all property accesses of the device
 * This module can be separated from the Property API, in case of
 * an embedded back end using the Property Protocol.
 *
 * (c) 2004, 2005, 2006 Martin Strubel // <hackfin@section5.ch>
 * (c) 2007, 2008
 *     Added derived class support (F_LINK)
 * (c) 2009, 2010 
 *     Added crc16 checksum for property table synchronizing
 *     2011-2013
 *     Dynamic property support
 *
 * Wish list (TODO):
 *
 * - More zerocopy options for scatter/gather HW stacks
 * - New TARGET_BUFFER type: This is a preinitialized val->value.p,
 *   pointing to the packet payload buffer
 * Netpp 1.0 backports:
 * - Session management
 * - STRUCT handler
 *
 */

#include "platform.h"       // platform dependent settings
#include "devlib.h"
#include "property_types.h"
#include "devlib_error.h"
#ifdef ALLOW_DYNAMIC_PROPERTIES
#include "dynprops.h"
#endif

#ifndef WITHOUT_NETPP

#include "property_protocol.h"

// If we're configured as proxy, replace all property_ calls by local_
#	ifdef PROXY
#		define PROXY_MODE
#		include "property_api.h"
#	endif

#	ifdef DEBUG
#		define LOG_DEBUG(...) netpp_log(DCLOG_DEBUG, ##__VA_ARGS__)
#	else
#		define LOG_DEBUG(...)
#	endif

#endif // WITHOUT_NETPP

#include <string.h>
#include <stdlib.h>

#if !defined(STANDALONE)
#include <assert.h>
#endif

#define GET_MASK(cur) \
	CREATEMASK(cur->access.reg.lsb, cur->access.reg.msb)


static struct dc_context {
	DeviceDesc *devices;
	int ndevices;
} g_c = { NULL, 0 };


#ifdef ALLOW_DYNAMIC_PROPERTIES
TOKEN dynprop_select(TOKEN parent, TOKEN prev);
#endif

/* int APIDECL dcFunction(DEVICE d, ... */

#ifdef USE_CRC16

int property_crc(DCValue *val)
{
	uint16_t c;
	const char *s;
	DeviceDesc *d;
	const PropertyDesc *p;
	TOKEN t;
	int i, j;

	c = 0xbeef;

	i = 0;
	for (d = &g_c.devices[0]; d < &g_c.devices[g_c.ndevices]; d++) {
		j = 0;
		for (p = &d->root[0]; p < &d->root[d->nproperties]; p++) {
			s = p->name;
			c = crc16((unsigned char *) s, strlen(s), c);
			t = BUILD_TOKEN(DEVICE_TOKEN(i), j);
			c = crc16((unsigned char *) &t, sizeof(t), c);
			j++;
		}
		i++;
	}

	val->type = DC_UINT;
	val->value.i = c;
	// Special hack to communicate total length of props:
	// See if we can pass this on in v1.0. Not working in 0.x.
	// val->len = n;

	return 0;
}

#endif

////////////////////////////////////////////////////////////////////////////
// PROTOTYPES
//
////////////////////////////////////////////////////////////////////////////

TOKEN findProperty_ByName(DEVICE d, TOKEN parent, const char *propname);


////////////////////////////////////////////////////////////////////////////

void register_proplist(DeviceDesc *d, int n)
{
	g_c.devices = d;
	g_c.ndevices = n;
}

TOKEN device_select(TOKEN d)
{
	TOKEN t;
	unsigned short di;
	if (d == TOKEN_INVALID) {
		di = 0;
	} else {
		di = DEVICE_FROMTOKEN(d);
		di++;
	}
	if (di >= g_c.ndevices) return TOKEN_INVALID;
	t = BUILD_TOKEN(DEVICE_TOKEN(di), 0);
	return t;
}

const PropertyDesc *getProperty_ByIndex(TOKEN parent, PROPINDEX pi)
{
	unsigned short di;
	di = DEVICE_FROMTOKEN(parent);
	if (di >= g_c.ndevices) return NULL;
	if (pi >= g_c.devices[di].nproperties) return NULL;
	return &g_c.devices[di].root[pi];
}

/* This is the one and only certified property token to descriptor
 * decoder. Do not use anything else!
 */

const PropertyDesc *getProperty_ByToken(TOKEN t)
{
#ifdef ALLOW_DYNAMIC_PROPERTIES
	if (t & DYNAMIC_PROPERTY) {
		PropertyDesc *p = dynprop_get(t);
		if (!p)
			LOG_DEBUG("Illegal property %08x", t);
		return p;
	}
#endif

	// If TOKEN is invalid, return pointer to root node
#ifndef STANDALONE
	assert(g_c.devices != NULL);
#endif
	return getProperty_ByIndex(t, t & PROP_MASK);
}

TOKEN follow_link(const PropertyDesc **cur)
{
	TOKEN t;
	int index;

	index = (*cur)->access.base;
	// Get new (linked) root node:
	*cur = &g_c.devices[index].root[0];
	t = BUILD_TOKEN(DEVICE_TOKEN(index), 0);
	LOG_DEBUG("follow link %08x", t);
	return t;
}

TOKEN nextVisibleToken(TOKEN parent,
	const PropertyDesc *cur, int i, char visible)
{
	PROPINDEX index;
	TOKEN t;
	
	t = parent;
	LOG_DEBUG("Iterating in parent %08x, index %d", parent, i);
	index = cur->members[i];
	while (index != INVALID_INDEX) {
		if (getProperty_ByIndex(t, index)->flags & F_PRIVATE && !visible) {
			LOG_DEBUG("Private property %08x", index);
			i++;
		} else {
			t = BUILD_TOKEN(t, index);
			return t;
		}

		index = cur->members[i];
		if ( (index == INVALID_INDEX) && (cur->flags & F_LINK) ) {
			t = follow_link(&cur); i = 0;
			index = cur->members[i];
		}
	}
	return TOKEN_INVALID;
}

static
TOKEN 
select_property(DEVICE d, TOKEN parent, TOKEN prev, char visible)
{
	const PropertyDesc *cur;
	int i;
	TOKEN t;
	unsigned short index;

#ifdef ALLOW_DYNAMIC_PROPERTIES
	if (parent & DYNAMIC_PROPERTY) {
		return dynprop_select(parent, prev);
	}
#endif

	// We make sure we only reference within the current
	// device context by taking the device index from 'prev'.
	// This is necessary in case we switched base class for
	// derived devices (follow_link()).
	// Get parent descriptor:
	cur = getProperty_ByIndex(prev, parent & PROP_MASK);
	if (!cur) return TOKEN_INVALID;

	/* If invalid 'prev', return first children node */
	if (prev == TOKEN_INVALID || prev == parent) {
		if (cur->members) {
			// Use device field from prev:
			parent = BUILD_TOKEN(prev, 0);
			return nextVisibleToken(parent, cur, 0, visible);
		} else {
			return TOKEN_INVALID;
		}
	}

	// Our structure is not very nice, so children
	// are not aware of their predecessors/descendants. Thus, we have
	// to 'walk'.

	if (!cur->members)
		return TOKEN_INVALID;
	
	i = 0;
	index = cur->members[i++];

	while (index != INDEX_FROMTOKEN(prev)) {
		index = cur->members[i++];
		if (index == INVALID_INDEX) {
			netpp_log(DCLOG_ERROR,
				"Proplist error: empty members array");
		}
	}

	index = cur->members[i];

	if (index == INVALID_INDEX && (cur->flags & F_LINK)) {
		parent = follow_link(&cur); i = 0;
	} else {
		// Device field from prev, keep parent index
		parent = BUILD_TOKEN(prev, parent);
	}

	t = nextVisibleToken(parent, cur, i, visible);

	return t;
}

TOKEN 
property_select(DEVICE d, TOKEN parent, TOKEN prev)
{
	return select_property(d, parent, prev, 0);
}

// This function does the proper endian safe integer conversion
//
int32_t Int_FromBuf(void *buf, const PropertyDesc *p)
{
	int size = p->access.reg.size;
	long value = 0;

	unsigned char *b = (unsigned char* ) buf;

	if (p->flags & F_BIG) {  // Big endian format
		while (size--) {
			value <<= 8;
			value |= *b++;
		}
	} else {                 // little endian
		while (size--) {
			value <<= 8;
			value |= b[size];
		}
	}
	// Sign extension for smaller types:
	if (p->type == DC_INT) {
		switch (p->access.reg.size) {
			case 1:
				value = (int8_t) value;
			case 2:
				value = (int16_t) value;
		}
	}
	return value;
}

void Buf_FromInt(void *buf, long value, const PropertyDesc *p)
{
	int size = p->access.reg.size;

	unsigned char *b = (unsigned char* ) buf;

	if (p->flags & F_BIG) {  // Big endian format
		while (size--) {
			b[size] = (unsigned char) value;
			value >>= 8;
		}
	} else {                 // little endian
		while (size--) {
			*b++ = (unsigned char) value;
			value >>= 8;
		}
	}
}

#ifndef NO_MALLOC
int handle_device_buffer(DEVICE d, uint32_t address, DCValue *val, int size)
{
	int error;
	// if buffer lengths don't match, report it
	// to the user.

	if (val->type == DC_COMMAND) {
		DEB(netpp_log(DCLOG_DEBUG, "Release buffer"));
		free(val->value.p);
		return 0;
	}


	if (val->len < size) {
		val->len = size;
		return DCERR_PROPERTY_SIZE_MATCH;
	}
	unsigned char *p;
	p = (unsigned char *) malloc(size);
	if (!p) return DCERR_MALLOC;
	error = device_read(d, address, p, size);
	if (error < 0) {
		free(p);
	}

	DEB(netpp_log(DCLOG_DEBUG, "Read from 0x%08x: '%s'", address, p));

	val->value.p = p;
	// TODO: if buffer size is too small, issue warning.
	return error;
}

#endif

int 
property_getitem(DEVICE d, const PropertyDesc *cur, DCValue *val)
{
	int error = 0;
	int size;
	int address = val->index + cur->access.reg.id;
	unsigned char buf[8]; // value buffer

	size = cur->access.reg.size;

	switch (cur->type) {
		// if we have an int, it is a max 4 byte register.
		// Endianness is handled by Int_FromBuf()
		case DC_COMMAND:
		case DC_BOOL:
		case DC_UINT:
		case DC_INT:
		case DC_MODE:
			error = device_read(d, address, buf, size);
			val->value.u = Int_FromBuf(buf, cur);
			// apply mask to int:
			val->value.u &= GET_MASK(cur);
			val->value.u >>= cur->access.reg.lsb;
			val->len = size;
			break;
#ifndef NO_MALLOC
		case DC_STRING:
		case DC_BUFFER:
			error = handle_device_buffer(d, address, val, size);
			val->type = cur->type;
			break;
#endif
		default:
			return DCERR_PROPERTY_HANDLER;
	}
	return error;
}

int property_setitem(DEVICE d, const PropertyDesc *cur,
		const DCValue *val)
{
	int error = 0;
	ADDRESS address = val->index + cur->access.reg.id;
	unsigned char buf[8]; // value buffer
	uint32_t v;
	BITMASK mask;

	switch (cur->type) {
		case DC_BOOL:
		case DC_MODE:
		case DC_UINT:
			// FIXME: use val->value.u
		case DC_INT:
		case DC_COMMAND:
			if (cur->access.reg.lsb != 0 ||
			    cur->access.reg.msb != LAST_BIT) {
				mask = GET_MASK(cur);
				error = device_read(d, address,
					buf, cur->access.reg.size);
				if (error < 0) return error;
				// get old value:
				v = Int_FromBuf(buf, cur);

				if (cur->type == DC_BOOL) {
					v = (v & ~mask) | (val->value.i ? mask : 0);
				} else {
					v = (v & ~mask) |
						( (val->value.i << cur->access.reg.lsb) & mask);
				}
			} else {
				v = val->value.i;
			}

			Buf_FromInt(buf, v, cur);

			error = device_write(d, address,
				buf, cur->access.reg.size);
			break;
		case DC_FLOAT:
			// not yet supported, needs mapping specification
			// we could use the IEEE float format..
			return DCERR_PROPERTY_HANDLER;
		case DC_BUFFER:
			// if buffer sizes don't match, complain.
			if (val->len != cur->access.reg.size)
				return DCERR_PROPERTY_SIZE_MATCH;
			error = device_write(d, address, val->value.p, val->len);
			break;
		case DC_STRING:
			// LOG_DEBUG("dummy write string '%s'\n", val->value.p);
			error = device_write(d, address,
				val->value.p, cur->access.reg.size);
			break;
		// TODO: implement DC_ARRAY
		default:
			return DCERR_PROPERTY_HANDLER;
	}
	// TODO: For blocking commands, implement polling function
	// For now, a handler needs to be implemented.
	return error;
}

static int
count_properties(DEVICE d, DCValue *val)
{
	DeviceDesc *desc;
	const PropertyDesc *p;
	int i = 0;

	for (desc = &g_c.devices[0]; desc < &g_c.devices[g_c.ndevices]; desc++) {
		for (p = &desc->root[0]; p < &desc->root[desc->nproperties]; p++) {
			i++;
		}
	}
	val->value.i = i;
	val->type = DC_INT;
	return 0;
}

int property_get(DEVICE d, TOKEN p, DCValue *val)
{
	const PropertyDesc *cur;
	int error = 0;
	size_t len, aux;
	unsigned short index;

	if (IS_RESERVED(p)) {
		val->flags = F_RO; // Default read only
		val->type = DC_UINT;

		switch (p) {
			case TOKEN_NPROPS:
				return count_properties(d, val);
			case TOKEN_CHECK:
#ifdef USE_CRC16
#ifdef ALLOW_DYNAMIC_PROPERTIES
				if (local_getroot(d) & DYNAMIC_PROPERTY) {
					DCValue tmp;
					dynproperty_crc(val);
					error = property_crc(&tmp);
					val->value.u += tmp.value.u;
					return error;
				}
#endif
				return property_crc(val);
#else
				val->value.u = 0xbeef;
				return 0;
#endif
				break;
			default:
#ifdef CONFIG_CUSTOMHANDLER
				return property_custom(p, val);
#else
				return DCERR_PROPERTY_UNKNOWN;
#endif
		}
	}

	cur = getProperty_ByToken(p);
	if (!cur) return DCERR_PROPERTY_UNKNOWN;
	val->flags = cur->flags;
	index = OFFSET_FROMTOKEN(p);
	val->index = index;
	// if property is write only, return.
	if (cur->flags & F_WO) return DCERR_PROPERTY_ACCESS;

	switch (cur->where) {
		case DC_STATIC:
			val->type = cur->type;  // Auto-Assign type
			switch (cur->type) {
				case DC_BOOL:
				case DC_MODE:
				case DC_UINT:
				case DC_INT:
					val->value.i = cur->access.s_int;
					break;
#ifndef NO_FLOAT
				case DC_FLOAT:
					val->value.f = cur->access.s_float;
#endif
					break;
				case DC_STRING:
					len = strlen(cur->access.s_string) + 1;
					val->len = len;
					if (val->len < len) {
						val->type = DC_STRING;
						return DCERR_PROPERTY_SIZE_MATCH;
					}
					val->value.p = (void *) cur->access.s_string;
					break;
				case DC_EVENT:
					val->value.i = cur->access.event.target;
					if (cur->access.event.s_evttype == DC_DYNAMIC) {
						val->value.i |= DYNAMIC_PROPERTY;
					}
					break;
				default:
					return DCERR_PROPERTY_TYPE_UNKNOWN;
			}
			break;

		case DC_ADDR:
			if (cur->type != DC_BUFFER && cur->type != DC_STRING) {
				val->type = cur->type;  // Auto-Assign type
			}
			error = property_getitem(d, cur, val);
			break;
			
		case DC_HANDLER:
			if (cur->type == DC_ARRAY ) {
				val->type = cur->type;
				return DCERR_PROPERTY_HANDLER;
			} else {
				if (val->type != DC_COMMAND)
					val->type = cur->type;
				if (!cur->access.func.get) {
					return DCERR_PROPERTY_HANDLER;
				}
				error = cur->access.func.get(d, val);
			}
			break;

		case DC_VAR:
			val->type = cur->type;  // Auto-Assign type
			switch (cur->type) {
				case DC_BOOL:
					val->value.i = cur->access.varp_bool[index];
					break;
				case DC_MODE:
					val->value.i = cur->access.varp_mode[index];
					break;
#ifndef NO_FLOAT
				case DC_FLOAT:
					val->value.f = cur->access.varp_float[index];
					break;
#endif
				case DC_UINT:
					val->value.u = cur->access.varp_uint[index];
					break;
				case DC_INT:
					val->value.i = cur->access.varp_int[index];
					break;
				case DC_STRING:
					val->value.p = (void *) cur->access.buf.p;
					// Size check
					len = strlen(val->value.p) + 1;
					aux = val->len;
					val->len = (unsigned long) len;
					if (len > aux) error = DCERR_PROPERTY_SIZE_MATCH;
					break;
				case DC_BUFFER:
					val->value.p = (void *) cur->access.buf.p;
					// Size check
					len = cur->access.buf.size;
					aux = val->len;
					val->len = (unsigned long) len;
					if (len > aux) error = DCERR_PROPERTY_SIZE_MATCH;
					break;
				default:
					return DCERR_PROPERTY_HANDLER;
			}
			break;
		case DC_CUSTOM:
			if (val->type != DC_COMMAND) val->type = cur->type;
			// Struct burst not supported
			if (val->type == DC_STRUCT) {
				return DCERR_PROPERTY_HANDLER;
			}
			if (!cur->access.custom.p) {
				netpp_log(DCLOG_WARN,
					"Warning: Null pointer to custom handler passed");
			}
			if (cur->access.custom.handler) {
				error =
				cur->access.custom.handler(cur->access.custom.p, 0, val);
			} else {
				netpp_log(DCLOG_ERROR,
					"Error: Undefined custom handler");

				return DCERR_PROPERTY_HANDLER;
			}
			break;
		default:
			LOG_DEBUG("Not supported, sorry");
			return DCERR_PROPERTY_TYPE_UNKNOWN;
	}

	if (error)
		return error;
	else if (cur->flags & F_INACTIVE) {
		return DCWARN_PROPERTY_INACTIVE;
	}
	else
		return error;

}

/* Validate a value range using the limits (min, max), if
 * specified.
 */

int property_validate(DEVICE d, TOKEN p, const DCValue *v)
{
	DCValue min, max;
	TOKEN child;
	min.type = max.type = v->type;
	
	child = findProperty_ByName(d, p, "Min");
	if (child == TOKEN_INVALID) return 1;

	property_get(d, child, &min);

	child = findProperty_ByName(d, p, "Max");
	if (child == TOKEN_INVALID) return 1;

	property_get(d, child, &max);

	switch (v->type) {
#ifndef NO_FLOAT
		case DC_FLOAT:
			if (v->value.f >= min.value.f &&
			    v->value.f <= max.value.f) return 1;
			else return 0;
			break;
#endif
		case DC_MODE:
		case DC_UINT:
			// FIXME: use val->value.u (some day)
		case DC_INT:
			if (v->value.i >= min.value.i &&
			    v->value.i <= max.value.i) return 1;
			else return 0;
			break;
		default:
			return 1;
	}
}

int property_getbuf(DEVICE d, TOKEN p, DCValue *val)
{
	const PropertyDesc *cur;
	int error = 0;

	cur = getProperty_ByToken(p);
	if (!cur) return DCERR_PROPERTY_UNKNOWN;

	if (cur->where == DC_VAR) {
		val->value.p = (void *) cur->access.buf.p;
		val->len     =          cur->access.buf.size;
	} else {
		error = DCERR_PROPERTY_TYPE_MATCH;
	}

	return error;
}

int property_set(DEVICE d, TOKEN p, DCValue *val)
{
	const PropertyDesc *cur;
	int error = 0;
	char type_mismatch = 0;
	unsigned short index;

	cur = getProperty_ByToken(p);
	if (!cur) return DCERR_PROPERTY_UNKNOWN;

	// Check data types:
	// We allow differing data types when using handlers.
	// Responsability for parsing the data correctly is up to the
	// handler!
	if (cur->type != val->type && val->type != DC_COMMAND) {
		LOG_DEBUG("%s: Token %08x type expected: %d, was: %d", __FUNCTION__,
				p, cur->type, val->type);
		type_mismatch = 1;
	}

	// if property is read only, return.
	if (cur->flags & F_RO)
		return DCERR_PROPERTY_ACCESS;

	if (!property_validate(d, p, val))
		return DCERR_PROPERTY_RANGE;

	index = OFFSET_FROMTOKEN(p);
	val->index = index;

	switch (cur->where) {
		case DC_STATIC: 
			// writing static values is prohibited:
			return DCERR_PROPERTY_ACCESS;
		case DC_ADDR:
			if (type_mismatch) return DCERR_PROPERTY_TYPE_MATCH;
			error = property_setitem(d, cur, val);
			break;
		case DC_HANDLER:
			if (cur->type == DC_ARRAY ) {
				return DCERR_PROPERTY_HANDLER;
			} else {
				if (!cur->access.func.set) {
					return DCERR_PROPERTY_HANDLER;
				}
				error = cur->access.func.set(d, val);
			}
			break;
		case DC_CUSTOM:
			if (!cur->access.custom.p) {
				netpp_log(DCLOG_WARN,
					"Warning: Null pointer to custom handler passed");
			}
			if (cur->access.custom.handler) {
				error =
					cur->access.custom.handler(cur->access.custom.p, 1, val);
			} else {
				error = DCERR_PROPERTY_HANDLER;
			}
			break;
		case DC_VAR:
			if (type_mismatch) return DCERR_PROPERTY_TYPE_MATCH;
			switch (cur->type) {
				case DC_BOOL:
					cur->access.varp_bool[index] = (char) val->value.i;
					break;
				case DC_MODE:
					cur->access.varp_mode[index] = (short) val->value.i;
					break;
#ifndef NO_FLOAT
				case DC_FLOAT:
					cur->access.varp_float[index] = val->value.f;
					break;
#endif
				case DC_UINT:
					// FIXME: use val->value.u
				case DC_INT:
					cur->access.varp_int[index] = val->value.i;
					break;
				case DC_BUFFER:
				case DC_STRING:
					// No support for indexed strings
					if (index > 0) return DCERR_PROPERTY_HANDLER;
					if (val->len > cur->access.buf.size)
						return DCERR_PROPERTY_SIZE_MATCH;
					val->value.p = cur->access.buf.p;
					break;
				case DC_COMMAND:
					// We don't support a string update function
					// here.
					break;

				default:
					return DCERR_PROPERTY_HANDLER;
			}
			break;
		case DC_USERDEFINED:
			return DCERR_PROPERTY_HANDLER;
	}
	if (error)                         return error;
	else if (cur->flags & F_INACTIVE)  return DCWARN_PROPERTY_INACTIVE;

	return error;
}

static
TOKEN scan_namespace(DEVICE d, TOKEN parent, const char *propname)
{

	const PropertyDesc *p, *cur;
	TOKEN t;

	p = getProperty_ByToken(parent);
	if (!p) return DCERR_PROPERTY_UNKNOWN;

	t = select_property(d, parent, parent, 1); // get child
	
	while (t != TOKEN_INVALID) {
		cur = getProperty_ByToken(t);
		if (strcasecmp(cur->name, propname) == 0) {
			return t;
		}
		t = select_property(d, parent, t, 1);
	}
	return TOKEN_INVALID;
}

/* This function parses a property access code and returns a
 * property proxy (token) to the property instance
 *
 */

TOKEN findProperty_ByName(DEVICE d, TOKEN parent, const char *propname)
{
	const PropertyDesc *p;
	TOKEN t;

	t = scan_namespace(d, parent, propname);
	if (t != TOKEN_INVALID) return t;

	// Token was not found, if we are an inherited property,
	// follow the link.
	p = getProperty_ByToken(parent);
	if (p->flags & F_LINK) {
		t = BUILD_TOKEN(DEVICE_TOKEN(p->access.base), parent);
		return findProperty_ByName(d, t, propname);
	}
	return TOKEN_INVALID;
}

/** Encodes an array token via the array specifications and
 * the attributes of the contained property. This token
 * merely contains a property pointer and an encoded
 * address offset
 */

TOKEN encode_arraytoken(TOKEN parent, TOKEN child, unsigned long i)
{
	const PropertyDesc *cur = getProperty_ByToken(parent);
	if (!cur) return DCERR_PROPERTY_UNKNOWN;
	i *= cur->access.array.banksize;

	return BUILD_ARRAYTOKEN(child, i /* reg offset */);
}

int array_getlimit(DEVICE d, const PropertyDesc *this)
{
	DCValue v;
	int error;

	switch (this->where) {
		case DC_STATIC:
			return this->access.s_int;
		case DC_HANDLER:
			v.type = DC_INT;
			error = this->access.func.get(d, &v);
			if (error < 0) return error;

			return v.value.i;
		default:
			LOG_DEBUG("Unhandled array type %d", this->type);
			return -1;
	}
}

/** Parse given name in the namespace of the property 'parent'.
 *
 * \param propname    Property name looked for
 * \param parent      Namespace root, normally device root node
 */

TOKEN property_parsename(DEVICE d, const char *propname, TOKEN parent)
{
	char name[NAMELENGTH];
	const char *c;
	char *walk;
	int index = 0;
	int depth = 0; // nested array depth
	const PropertyDesc *this;
	TOKEN child;
	int size;

	enum {
		S_START,
		S_WORD,
		S_INDEX,
		S_STRUCT, 
		S_ARRAY
	} state = S_START;

	c = propname;
	walk = name;

	while (*c && walk < &name[NAMELENGTH - 1]) {
		switch (*c) {
			case '.':
				*walk = '\0';
				walk = name;
				switch (state) {
					case S_WORD:
						parent = findProperty_ByName(d, parent, name);
						if (parent == TOKEN_INVALID) {
							return parent;
						}
						break;
					case S_START:
					case S_INDEX:
						LOG_DEBUG("Semantic error");
						return TOKEN_INVALID;
					case S_ARRAY:
						state = S_STRUCT;
						break;
					case S_STRUCT:
						LOG_DEBUG("Nested struct arrays not allowed");
						return TOKEN_INVALID;
						break;
				}

				break;
			case '[':
				if (depth > 0) {
					LOG_DEBUG("Two-Dimensional Arrays not yet supported");
					return TOKEN_INVALID;
				}

				if (state != S_START) {
					*walk='\0';
					parent = findProperty_ByName(d, parent, name);
					if (parent == TOKEN_INVALID) {
						return parent;
					}
				}

				this = getProperty_ByToken(parent);

				if (!this || this->type != DC_ARRAY) {
					LOG_DEBUG("This is not an array type!");
					return TOKEN_INVALID;
				}

				// Now, we have the TOKEN to the array property in
				// 'parent'
				walk = name;
				state = S_INDEX;
				break;
			case ']':
				if (state != S_INDEX) return TOKEN_INVALID;
				*walk='\0';
				index = atoi(name);
				if (index >= MAX_INDEX || index < 0) {
					return TOKEN_INVALID;
				}
			
				depth++;
				state = S_ARRAY;
				break;
			default:
				*walk++ = *c;
				if (state == S_START) state = S_WORD;
		}
		c++;
	}
	*walk = *c;
	switch (state) {
		case S_WORD:
			parent = findProperty_ByName(d, parent, name);
			break;
		case S_ARRAY:
		case S_STRUCT:
			// Check 'Size' child:
			child = property_select(d, parent, parent); // select first child

			this = getProperty_ByToken(child);

			size = array_getlimit(d, this);

			if (index >= size) {
				LOG_DEBUG("Array '%s' size exceeded: %d",
					this->name,
					size);
				return TOKEN_INVALID;
			}

			// select child member and go on...

			child = property_select(d, parent, child); // select 2nd child

			if (state == S_STRUCT) {
				child = findProperty_ByName(d, child, name);
			}

			parent = encode_arraytoken(parent, child, index);

			break;
		default:
			break;
	}

	return parent;
}

int property_getname(DEVICE d, TOKEN t, DCValue *value)
{
	const PropertyDesc *p;
	p = getProperty_ByToken(t);
	if (!p) return DCERR_PROPERTY_UNKNOWN;

	// Are we the the owner of the string?
	// FIXME: This does not work for proxies without cache (that owns the
	// string)
	if (value->len == 0 /* && d == DC_PORT_ROOT */) {
		value->value.p = (void *) p->name;
		value->type = DC_STRING;
	} else {
		// Caller is owner, fill:
		string_copy(value->value.p, p->name, value->len);
	}
	value->len = strlen( (char *) p->name) + 1;

	return 0;
}

int property_getdesc(DEVICE d, TOKEN t, DCValue *val)
{
	int error = 0;
	size_t len = 0;
	const PropertyDesc *p = getProperty_ByToken(t);
	if (!p) return DCERR_PROPERTY_UNKNOWN;

	val->type = p->type;

	len = 0;
	switch (p->where) {
		case DC_STATIC:
			switch (p->type) {
				case DC_STRING:
					len = strlen((char *) p->access.s_string) + 1;
					break;
				case DC_BUFFER:
					len = p->access.buf.size;
					break;
				// TBD: DC_EVENT handling for 1.0
				default:
					break;
			}
		case DC_VAR:
			switch (p->type) {
				case DC_STRING:
					len = strlen((char *) p->access.buf.p) + 1;
					break;
				case DC_BUFFER:
					len = p->access.buf.size;
					break;
				default:
					break;
			}
			break;
		case DC_HANDLER:
			// Retrieve length of dynamic buffers:
			switch (p->type) {
				case DC_STRING:
				case DC_BUFFER:
					val->len = 0;
					if (p->access.func.set) {
						error = p->access.func.set(d, val);
						len = val->len;
						break;
					}
				default:
					break;
			}
			break;
		case DC_ADDR:
			len = p->access.reg.size;
			break;
		default:
			break;
	}
	val->flags = (p->flags & FLAG_MASK_PUBLIC);
	val->len = len;
	return error;
}

#if UNUSED_CODE
TOKEN iterate_children(DEVICE d, TOKEN t, PropCallback func)
{
	TOKEN child;
	child = property_select(d, t, t);
	while (child) {
		// apply function on node:
		if (func(child) == 0) return child;
		child = property_select(d, t, child); // iterate to next
	}
	return TOKEN_INVALID;
}
#endif
////////////////////////////////////////////////////////////////////////////
