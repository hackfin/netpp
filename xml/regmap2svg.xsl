<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- registermap to SVG converter

(c) 2010, Martin Strubel <hackfin@section5.ch>

-->

<xsl:stylesheet version="1.0" 
	xmlns:my="http://www.section5.ch/dclib/schema/devdesc"
	xmlns:svg="http://www.w3.org/2000/svg"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

<xsl:import href="reg2svg.xsl"/>

<xsl:param name="registermap">-1</xsl:param>


<xsl:template match="my:registermap" mode="reg_draw">
<!-- Only for debugging:
	<svg:g transform="translate(0,8)">
	<svg:text>
		<xsl:attribute name="text-anchor">start</xsl:attribute>
		<xsl:attribute name="font-family">Courier</xsl:attribute>
		<xsl:attribute name="font-size">12</xsl:attribute>
		<xsl:value-of select="@name"/>
	</svg:text>
	</svg:g>
	  <xsl:if test="@offset">
		<svg:g transform="translate(0,20)">
		<svg:text>
			<xsl:attribute name="text-anchor">start</xsl:attribute>
			<xsl:attribute name="font-family">Courier</xsl:attribute>
			<xsl:attribute name="font-size">8</xsl:attribute>
			Offset: <xsl:value-of select="@offset"/>
		</svg:text>
		</svg:g>
	  </xsl:if>
-->
		<xsl:apply-templates select=".//my:register[not(@size) or @size &lt;= 2]" mode="reg_draw"/>
</xsl:template>

<xsl:template match="/">
	<svg:svg xmlns="http://www.w3.org/2000/svg">

<xsl:apply-templates select=".//my:registermap[@name=$registermap]" mode="reg_draw"/>

	</svg:svg>
</xsl:template>

</xsl:stylesheet>

