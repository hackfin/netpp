import ch.s5.netpp.Property;

class NetppProperty
{
	Property property;

	NetppProperty(Property p)
	{
		property = p;
	}

	int setValue(int val)
	{
		return property.setValue(val);
	}

}

