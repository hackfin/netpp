import netpp
import time

from colorcurves import *
import sys


SUNSET = [
	(0x0,    (0x000, 200, 0)),
	(0x4000, (0x200, 0, 128)),
	(0x5000, (0x200, 127, 0)),
	(0x6000, (0x300, 127, 0)),
	(0x8000, (0x3c0, 0, 0)),
	(0xc000, (0x500, 0, 127)),
	(60000, (0x5ff, 0, 255)),
]


SUMMERDAY = [
( 0x0000, ( 1024 ,   0, 128)),
( 0x2000, ( 640 ,  20, 127)),
( 0x3a98, ( 832 ,   0, 200)),
( 0x5000, ( 1024 ,   0, 100)),
( 0x6000, ( 384 ,  30, 150)),
( 0xa000, ( 544 ,  10, 220)),
( 0xea60, ( 768 ,   0, 255)),
]

# ALT = (0x280,   0, 250) 
# HI = (0x320,   20, 120) 
# LO = (0x380,    0, 250) 

ALT = (0x080,   0, 250) 
HI = (0x120,   20, 120) 
LO = (0x280,    0, 250) 

DELTA = 110

WAVE = [
	(8 * 0x0000, HI),
	(8 * (0x0100-DELTA), LO),
	(8 * (0x0200+DELTA), LO),
	(8 * (0x0300-DELTA), HI),
	(8 * (0x0400+DELTA), LO),
	(8 * (0x0500-DELTA), LO),
	(8 * (0x0600+DELTA), HI),
	(8 * (0x0700-DELTA), LO),
	(8 * (0x0800+DELTA), LO),
	(8 * (0x0900-DELTA), HI),
	(8 * (0x0a00+DELTA), LO),
	(8 * (0x0b00-DELTA), LO),
	(8 * (0x0c00+DELTA), HI),
	(8 * (0x0d00-DELTA), LO),
	(8 * (0x0e00+DELTA), LO),
	(60000, ALT),
]


SUNRISE = [
	(0x0, (0x5ff, 0, 255)),
	(0x2000, (0x200, 100, 255)),
	(0x3000,    (0x500, 127, 100)),
	(0x5000, (0x400, 200, 0)),
	(0x6000, (0x200, 230, 0)),
	(0x8000, (0x000, 100, 50)),
	(60000, (0x200, 180, 0)),
]

def set_table(dev, r, table):
	n = len(table)
	j = 0
	d = dev.device
	for i in table:
		print "Setting Pivot %d" % j
		pV = d.getToken("Pivots[%d].Val" % j)
		val = i[0]
		if val > 60000:
			val = 60000
		pH = d.getToken("Pivots[%d].H" % j)
		pW = d.getToken("Pivots[%d].W" % j)
		pB = d.getToken("Pivots[%d].B" % j)
		pV.set(val)
		pH.set(i[1][0])
		pW.set(i[1][1])
		pB.set(i[1][2])
		r.Update.set(1)

		j += 1

	r.NumPivots.set(n)

def dumpcols(r):

	n = r.NumPivots.get()

	p0 = a.device.getToken("Pivots[0]")
	p1 = a.device.getToken("Pivots[1]")

	print p0, p1

	for i in range(n):
		# HACK since shiney does not yet support local namespace array struct
		# queries:
		pV = a.device.getToken("Pivots[%d].Val" % i)
		pH = a.device.getToken("Pivots[%d].H" % i)
		pW = a.device.getToken("Pivots[%d].W" % i)
		pB = a.device.getToken("Pivots[%d].B" % i)
		v = pV.get()
		h = pH.get()
		w = pW.get()
		b = pB.get()

		print "( 0x%04x, ( %3d , %3d, %3d))," % (v, h, w, b)

def do_curve(a, r, curve):
	# r.Mode.set(1)
	r.Brightness.set(31)
	r.Step.set(1000)
	r.Speed.set(0)
	r.Position.set(0)
	# r.Update.set(1)
	set_table(a, r, curve)
	# r.Step.set(100)
	r.Speed.set(50)
	r.Mode.set(2)


# ALL_CURVES = [ WARM, FIRE, SUNRISE, DAYLIGHT1, SUNSET ]
ALL_CURVES = [ WAVE ]

target = sys.argv[1]

a = netpp.connect("PRX:" + target + "+DEV:0")
# a = netpp.connect("PRX:localhost:2012+DEV:0")

r = a.sync()

# dumpcols(r)
if len(sys.argv) > 2:
	curve = eval(sys.argv[2])
	do_curve(a, r, curve)
else:
	for curve in ALL_CURVES:
		do_curve(a, r, curve)
		time.sleep(50)
