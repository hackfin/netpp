/* Property API wrapper
 *
 * (c) 2005-2014 <hackfin@section5.ch>
 */

#include "devlib.h"
#include "devlib_error.h"
#include "property_protocol.h"

#include <stdio.h> // snprintf
#include <stdlib.h>

// PROTOS

const char version_string[] = VERSION_STRING;

#ifdef __cplusplus
extern "C" {
#endif

#define NULLPTR_CHECK(x)  { if (!(x)) return DCERR_BADPTR; }

#ifdef CONFIG_PTHREAD
	int mutex_lock(NETPP_MUTEX *mutex);
	int mutex_unlock(NETPP_MUTEX *mutex);

#	define ENTER_CRITICAL \
		if (mutex_lock(&d->mutex) < 0) return DCERR_MUTEX;

#	define LEAVE_CRITICAL \
		if (mutex_unlock(&d->mutex) < 0) return DCERR_MUTEX;

#	define NETPP_MUTEX_TIMEOUT 10000
#else
#	define ENTER_CRITICAL
#	define LEAVE_CRITICAL
#endif // CONFIG_PTHREAD


////////////////////////////////////////////////////////////////////////////
// MUTEX HANDLING

#ifdef CONFIG_PTHREAD

#if defined(__WIN32__)

int mutex_init(NETPP_MUTEX *mutex)
{
	*mutex = CreateMutex(NULL, FALSE, NULL);
	if (!*mutex) return DCERR_MUTEX;
	return 0;
}

int mutex_exit(NETPP_MUTEX *mutex)
{
	CloseHandle(*mutex);
	return 0;
}

int mutex_lock(NETPP_MUTEX *mutex)
{
	int error;

	int tries = 100;

	do {
		tries--;
		error = WaitForSingleObject(*mutex, NETPP_MUTEX_TIMEOUT / 100);
	} while (error != WAIT_OBJECT_0 && tries);

	if (error != WAIT_OBJECT_0) return DCERR_MUTEX;

	return 0;
}

int mutex_unlock(NETPP_MUTEX *mutex)
{
	if (!ReleaseMutex(*mutex)) return DCERR_MUTEX;
	return 0;
}

#elif defined (__linux__) || defined(__APPLE__)

int mutex_init(NETPP_MUTEX *mutex)
{
	int error;
	error = pthread_mutex_init(mutex, NULL);
	if (error) return DCERR_MUTEX;
	return 0;
}

int mutex_exit(NETPP_MUTEX *mutex)
{
	pthread_mutex_destroy(mutex);
	return 0;
}

int mutex_lock(NETPP_MUTEX *mutex)
{
	if (pthread_mutex_lock(mutex) < 0) return DCERR_MUTEX;
	return 0;
}

int mutex_unlock(NETPP_MUTEX *mutex)
{
	if (pthread_mutex_unlock(mutex) < 0) return DCERR_MUTEX;
	return 0;
}

#else
#warning "This platform does not provide Thread support"
#endif

#endif // CONFIG_PTHREAD

/** Requests something specific from the protocol layer
 */

int protocol_request(RemoteDevice *d, int request)
{
	int error;
	PropertyPacketHdr *h;
	TOKEN t;

	NULLPTR_CHECK(d);
	REQUEST_PACKET_HDR(&d->peer, h, 0);

	h->type = htons(PP_REQUEST);
	h->sub.status.token = htonl((TOKEN) request);
	h->sub.status.code = htons(NETPP_PROTOCOL_VERSION);
	h->sub.status.len = 0;
	h->sub.status.stat = 0;

	ENTER_CRITICAL

	send_packet(&d->peer, h, NULL, 0);
	error = get_status_packet(&d->peer, &t);

	LEAVE_CRITICAL

	if (error < 0)
		return error;
	else
		return t;
}

int
dcDevice_GetRoot(RemoteDevice *d, TOKEN *t)
{
	PropertyPacketHdr *h;
	int error;


	if (d == DC_PORT_ROOT) {
		ports_init(NULL); // To be sure
		*t = TOKEN_INVALID;
		return 0;
	}

	NULLPTR_CHECK(d);
	REQUEST_PACKET_HDR(&d->peer, h, 0);

	// Downward compatibility:
	if (d->proto_version == 0) {
		h->type = htons(PP_GETROOT);
	} else {
		h->type = htons(PP_SELECT);
		h->sub.select.parent = htonl(TOKEN_INVALID);
		h->sub.select.prev = htonl(TOKEN_INVALID);
	}

	ENTER_CRITICAL

	send_packet(&d->peer, h, NULL, 0);
	error = get_status_packet(&d->peer, t);

	LEAVE_CRITICAL

	return error;
}

int 
dcProperty_Select(RemoteDevice *d, TOKEN parent, TOKEN prev, TOKEN *out)
{
	int error;
	PropertyPacketHdr *h;
	TOKEN t;


	if (d == DC_PORT_ROOT) {
		PortToken next;

		error = port_select(parent, prev, &next);
		*out = (TOKEN) next;
		return error;
	}

	NULLPTR_CHECK(d);
	REQUEST_PACKET_HDR(&d->peer, h, 0);

	h->type = htons(PP_SELECT);
	h->sub.select.parent = htonl(parent);
	h->sub.select.prev = htonl(prev);

	ENTER_CRITICAL

	send_packet(&d->peer, h, NULL, 0);
	error = get_status_packet(&d->peer, &t);

	LEAVE_CRITICAL

	*out = t;
	return error;
}

int
dcProperty_GetProto(DEVICE d, TOKEN t, DCValue *val)
{
	PropertyPacketHdr *h;

	int error;

#ifndef TINYCPU
	if (d == DC_PORT_ROOT) {
		DynPropertyDesc *pd;
		val->type = DC_INVALID;
		if (t == 0) {
			val->value.i = NETPP_PROTOCOL_VERSION;
		} else {
			pd = desc_fromtoken(t);
			NULLPTR_CHECK(pd);
			switch (pd->type) {
				case DC_DEVICE:
					val->value.i = 'D'; break;
				case DC_PORT:
					val->value.i = 'P'; break;
				case DC_HUB:
					val->value.i = 'H'; break;
				default:
					val->value.i = 'p'; break;
			}
		}
		return 0;
	}
#endif

	NULLPTR_CHECK(d);
	REQUEST_PACKET_HDR(&d->peer, h, 0);

	h->type = htons(PP_DESC);
	h->sub.status.token = htonl(t);

	ENTER_CRITICAL

	send_packet(&d->peer, h, NULL, 0);
	error = handle_request(d, t, val);

	LEAVE_CRITICAL

	return error;
}

int
dcProperty_GetName(DEVICE d, TOKEN t, DCValue *val)
{
	PropertyPacketHdr *h;
	int error;

	if (d == DC_PORT_ROOT) {
		const char *name = 0;
		error = portdesc_getname((PortToken) t, &name);
		// We never fail here (we know):
		// if (name) {
			val->type = DC_STRING;
			val->value.p = (void *) name;
			val->len = strlen(name) + 1;
		// }
		return error;
	}

	NULLPTR_CHECK(d);
	REQUEST_PACKET_HDR(&d->peer, h, 0);

	h->type = htons(PP_NAME | PP_FLAG_PROTO); // A GetName() request
	if (d->proto_version > 0) {
		h->sub.name.token = htonl(t);
	} else {
		h->sub.status.token = htonl(t);
	}

	ENTER_CRITICAL

	send_packet(&d->peer, h, NULL, 0);
	error = handle_request(d, t, val);

	LEAVE_CRITICAL

	return error;
}

int dcProperty_Lookup(RemoteDevice *d, const char *propname, TOKEN ns, TOKEN *t)
{
	PropertyPacketHdr *h;
	int error;
	unsigned short l = (unsigned short) strlen(propname);

	if (d == DC_PORT_ROOT) {
		// return port_parsename(propname, t);
		return DCERR_PROPERTY_HANDLER;
	}

	NULLPTR_CHECK(d);

	l++;

	REQUEST_PACKET_HDR(&d->peer, h, l);

	h->type = htons(PP_NAME);
	if (d->proto_version > 0) {
		h->sub.name.token = htonl(ns);
		h->sub.name.len = htons(l);
	} else {
		// Legacy v0 PP_PARSE request
		h->sub.parse.len = htons(l);
	}

	ENTER_CRITICAL

	send_packet(&d->peer, h, propname, l);
	error = get_status_packet(&d->peer, t);

	LEAVE_CRITICAL

	return error;
}

/** Send property name and receive token.
 */

int
dcProperty_ParseName(RemoteDevice *d, const char *propname, TOKEN *t)
{
	int error;

	// HACK:
	// If first character is a '.', we look for a local name.
	// In this case, we use *t as namespace parent.
	if (propname[0] == '.') {
		error = dcProperty_Lookup(d, &propname[1], *t, t);
	} else {
		error = dcProperty_Lookup(d, propname, TOKEN_INVALID, t);
	}

	return error;
}

int 
dcDevice_GetProperty(RemoteDevice *d, TOKEN p, DCValue *val)
{
	int error;
	PropertyPacketHdr *h;


	if (d == DC_PORT_ROOT) {
		return DCERR_PROPERTY_HANDLER;
	}

	NULLPTR_CHECK(d);
	REQUEST_PACKET_HDR(&d->peer, h, 0);

	h->type = htons(PP_GET);
	h->sub.get.token = htonl(p);
	h->sub.get.len = htonl((unsigned long) val->len);

	ENTER_CRITICAL

	send_packet(&d->peer, h, NULL, 0);
	error = handle_request(d, p, val);

	LEAVE_CRITICAL

	return error;
}

int 
dcDevice_SetProperty(RemoteDevice *d, TOKEN p, DCValue *val)
{
	int error;

	if (d == DC_PORT_ROOT) {
		return DCERR_PROPERTY_HANDLER;
	}

	NULLPTR_CHECK(d);

	if (val->type == DC_STRING) {
		val->len = (int) strlen((char*) val->value.p) + 1;
	}

	ENTER_CRITICAL

	error = handle_setproperty(d, p, val);

	LEAVE_CRITICAL

	return error;

}

int
dcDeviceOpen(const char *port, DEVICE *pDev)
{
	RemoteDevice *d;
	int error;
	PORTID portid;
#ifdef CONFIG_TCP
	char fallback[64];
#endif
	error = ports_init(NULL);
	if (error < 0) return error;

	// No longer call this. It is not really necessary.
	// error = ports_probe();
	// if (error < 0) return error;

	d = (RemoteDevice *) malloc(sizeof(RemoteDevice));
	if (!d) return DCERR_MALLOC;

	d->proto_version = 0;
	d->flags = 0;
	d->errcount = 0;

#ifdef CONFIG_PTHREAD
	mutex_init(&d->mutex);
#endif

	error = portid_fromname(port, &portid);
	if (error == 0) {
		error = port_open(d, portid);
	}
#ifdef CONFIG_TCP
	else if (error == DCERR_PROPERTY_UNKNOWN) {
		snprintf(fallback, sizeof(fallback), "TCP:%s", port);
		netpp_log(DCLOG_NOTICE, "Port '%s' not found, trying '%s'...",
			port, fallback);
		error = portid_fromname(fallback, &portid);
		if (error == 0) {
			error = port_open(d, portid);
		}
	}
#endif
	else {
		free(d); return error;
	}

	if (error == 0) {
		error = protocol_request(d, REQUEST_VERSION);
		if (error >= 0) {
#ifdef DEBUG
			netpp_log(DCLOG_VERBOSE, "Protocol Version %d", error);
#endif
			d->proto_version = error; error = 0;
		} else {
			port_close(d);
		}
	} else {
		portid = TOKEN_INVALID;
	}
		
	if (error < 0) {
		// We failed, remove port ID. Note it could be invalid from above.
		if (portid != TOKEN_INVALID) {
			portdesc_free(portid);
		}
		ports_exit(0);
		free(d);
		return error;
	}

	// init cleanup function with zero
	d->cleanup = NULL;
	*pDev = d;
	return error;
}

int
dcDeviceClose(RemoteDevice *d)
{
	if (d == DC_PORT_ROOT) {
		ports_exit(1);
		return 0;
	}

	NULLPTR_CHECK(d);
#ifdef CONFIG_PTHREAD
	mutex_exit(&d->mutex);
#endif
	port_close(d);
	ports_exit(0);

	free(d);
	return 0;
}

int
dcDevice_GetHubName(RemoteDevice *d, const char **name)
{
	DynPropertyDesc *hub;
	NULLPTR_CHECK(d);

	hub = d->peer.hub;

	if (hub) {
		*name = hub->name;
	} else return DCERR_BADPTR;
	return 0;
}

#ifdef __cplusplus
}
#endif

