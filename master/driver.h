
#ifdef __WIN32__
#include <windows.h>
#define FILEDES HANDLE
#else
#define FILEDES int
#endif
FILEDES input_init(int ch);
int input_poll(FILEDES fd, char *buf, int n);
int input_exit(FILEDES fd);
