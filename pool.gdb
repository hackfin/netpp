# GDB script to dump memory pools:

if sizeof(PROPINDEX) == 1
	set $NIL = 255
else
	set $NIL = -1
end

define dump_list
	set $r = $arg0.hub.ports
	set $i = ($r >> 16) & 0xf
	print $i
end

define dump_pool
	set $p = $arg0
	printf "NODES:\n"
	set $walk = $p.first
	while $walk != $NIL
		set $a = $p.base[$walk]
		printf "-------------------------------------\n"
		print $a
		set $walk = $a.next
		dump_list $a
	end

	set $i = 0
	set $walk = $p.free
	while $walk != $NIL
		set $a = $p.base[$walk]
		set $i = $i + 1
		set $walk = $a.next
	end
	printf "Free elements: %d\n", $i

	set $i = 0
	while $i < $p.n
		if $p.base[$i].next == $NIL && $p.base[$i].prev == $NIL && $p.first != $i
			echo \033[31m
			printf "unconnected: %d ('%s')\n", $i, $p.base[$i].name
			echo \033[0m
		end
		set $i = $i + 1
	end
end

define dump_dynprop
	set $p = g_dyn.pool
	set $walk = $p.base[$arg0].prop.first

	printf "==================================================\n"
	print $arg0
	printf "--------------------------------------------------\n"

	while $walk != $NIL
		set $a = $p.base[$walk]
		print $a
		set $walk = $a.next
	end

	printf "==================================================\n"


end

define dump_dynprops
	dump_dynprop 0
end

define dump_ports
	set $p = g_port.pools[$arg0]
	set $rootpool = g_port.pools[0]

	printf "\n---------------------------------------------------\n"
	echo \033[32m
	printf "Pool name '%s' size %d\n", $rootpool.base[$arg0-1].name, $p.n
	echo \033[0m
	dump_pool $p
end


