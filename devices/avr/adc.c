/* ADC driver
 *
 * (c) 2016, <hackfin@section5.ch>
 *
 */

#include <avr/io.h>

#include "adc.h"

void adc_init(char enable)
{
    // AREF = 2.56
    ADMUX = 3 << REFS0;

    // ADC Enable and prescaler of 128
    // 16000000/128 = 125000
    uint8_t v = _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);
    if (enable) v |= _BV(ADEN);

    ADCSRA = v;
}

// read adc value
uint16_t adc_read(uint8_t ch)
{
    if (ch < 128) {
		set_adc_mux(ch);
	}
    // start single conversion
    ADCSRA |= _BV(ADSC);

    // wait for conversion to complete
    // ADSC becomes '0' again
    // till then, run loop continuously
    while (ADCSRA & _BV(ADSC));

    return ADC;
}

void set_adc_mux(uint8_t val)
{
	uint8_t v = ADMUX;
	v &= ~0x1f; v |= (val & 0x1f);
	ADMUX = v;

	val &= 0x20;
	if (val) {
		ADCSRB |= val;
	} else {
		ADCSRB &= ~0x20;
	}
}


uint8_t get_adc_mux(void)
{
	return (ADMUX & 0x1f ) | (ADCSRB & 0x20);
}
