
void adc_init(char enable);
uint16_t adc_read(uint8_t ch);
void set_adc_mux(uint8_t val);
uint8_t get_adc_mux(void);
