/* AVR Uart implementation for netpp
 *
 * (c) 2015, Martin Strubel <hackfin@section5.ch>
 *
 */


#include "uart.h"
#include "devlib_error.h"
#include <avr/interrupt.h>

extern struct fifo g_rxfifo;

#if 0
struct fifo g_txfifo = {
	0, 0,
};
#endif

#define ENCODE_ERR(x) (x - DCERR_COMM_GENERIC)
#define DECODE_ERR(x) (x + DCERR_COMM_GENERIC)

ISR(UART1_RECEIVE_INTERRUPT)
{
    uint8_t i;
    uint8_t data;
    uint8_t usr;
    int8_t err;
 
    /* read UART status register and UART data register */ 
    usr  = UART1_STATUS;
    data = UART1_DATA;
    
    err = (usr & (_BV(FE1) | _BV(DOR1)));

	if (err & _BV(FE1)) g_rxfifo.err = DCERR_COMM_FRAME;
	else if (err & _BV(DOR1)) g_rxfifo.err = DCERR_COMM_OVERRUN;
        
    /* calculate buffer index */ 
    i = (g_rxfifo.head + 1) & FIFO_BUFFER_MASK;
    
    if (i == g_rxfifo.tail) {
		g_rxfifo.err = DCERR_COMM_OVERRUN;
    } else {
        g_rxfifo.head = i;
        g_rxfifo.buf[i] = data;
    }

}

#if 0


{
	uint16_t i;

	if (g_txfifo.head != g_txfifo.tail) {
		i = (g_txfifo.tail + 1) & FIFO_BUFFER_MASK;
		g_txfifo.tail = i;
		UART1_DATA = g_txfifo.buf[i];
	} else {
		// Turn off irq when drained
		UART1_CONTROL &= ~_BV(UART1_UDRIE);
	}
}

void Xuart1_putc(uint8_t data)
{
	uint16_t i;

	i  = (g_txfifo.head + 1) & FIFO_BUFFER_MASK;

	// XXX Fixme:
	while (i == g_txfifo.tail);

	g_txfifo.buf[i] = data;
	g_txfifo.head = i;

	// Kick on IRQ:
	UART1_CONTROL |= _BV(UART1_UDRIE);

}

#endif

void uart1_putc(uint8_t data)
{
	UART1_STATUS = _BV(TXC1);
	UART1_DATA = data;
	while (!(UART1_STATUS & _BV(TXC1)));
}

void uart1_puts(const char *str)
{
	char c;
	int n = 0;
	while ((c = *str++) !='\000' ) {
		uart1_putc(c);
		if (c == '\n') uart1_putc('\r');
		n++;
		if (n > 80) break;
	}
}

void uart1_init(uint16_t baudrate)
{
	g_rxfifo.head = 0; g_rxfifo.tail = 0;
	// g_txfifo.head = 0; g_txfifo.tail = 0;

	if (baudrate & 0x8000) {
		UART1_STATUS = _BV(U2X1);
		baudrate &= ~0x8000;
	}
	UBRR1H = (uint8_t) (baudrate >> 8);
	UBRR1L = (uint8_t) baudrate;

	UART1_CONTROL = _BV(RXCIE1) | _BV(RXEN1) | _BV(TXEN1);

	UCSR1C = (3 << UCSZ10);
}
