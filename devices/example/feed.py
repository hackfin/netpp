#!/usr/bin/env python

import random
import sys
import time

sys.path.append("../../Debug")
import device

# d = device.open("TCP:10.2.6.54:2009")
d = device.open("UDP:127.0.0.1:2008")
# d = device.open("UDP:192.168.2.12:2008")
# d = device.open("TCP:169.254.80.251:2008")

buf = ""
for i in range(0x20000):
	buf += (chr(random.randint(0, 255)))

buf = buffer(buf)

d.set("Stream.HandledData", buf)

b = d.get("Stream.HandledData")

print "Buffer length:", len(b)
if b != buf:
	print "Buffers dont match"

# print d.get("StaticString")

import time

while (1):
	t0 = time.time()
	b = d.get("Stream.StaticData")
	t1 = time.time()

	t = t1 - t0
	print "%f kb/s" % (len(b) / t / 1000)
	time.sleep(0.2)
