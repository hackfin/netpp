# Demo script to access example server through Python
#
# (c) 2013, 2014 <hackfin@section5.ch>
#

import sys
import netpp
import random
import time
import subprocess
import pytest

# import intelhex
# 
# def dump(data, fname):
# 	ih = intelhex.IntelHex()
# 	ih.puts(0, str(data))
# 	f = open(fname, "w")
# 	ih.dump(f)
# 	f.close()

def verify_lut_values(l):
	"LUT array test"
	e1 = l[0]
	e2 = l[1]
	n = len(l)
	print e1.children()
	# print "%08x, %08x" % (e1.token(), e2.token())
	for i in range(n):
		elem = l[i]
		print "children:", elem.children(), hex(elem.token())
		elem.Y.set(i * 4)

	val = l[4].Y.get()
	if val == 16:
		return True

	print "Wrong return value:", val
	
	return False


def verify_lut_array(l):
	"LUT array test"
	for i in range(l.Size.get()):
		l[i].set(i * 4)

	val = l[4].get()
	if val == 16:
		return True
	
	return False

def verify_buffer(root):
	"Test (fixed) buffer transfer"
	buf = netpp.Buffer(0x40000)
	root.Stream.HandledData.get(buf) # Supply buffer reference
	print "Filled buffer, size: %d" % (len(buf))
	return True

def verify_integer(root):
	"Signed integer range test"
	i = root.Integer1
	i.set(42)
	if i.get() != 42:
		return False

	max_i = i.Max.get()
	
	try:
		i.set(max_i + 1)
		print "Maximum test failed"
		return False

	except ValueError:
		print "Maximum exceeded, test successful"

	i = root.Demo
	# Test signed integer value range:
	for val in [ -1, 0x7fffffff, -2147483648 ]:
		print "Setting value: %d" % val
		i.set(val)
		v = i.get()
		if v != val:
			print "Value mismatch: %x != %x" % (v, val)
			return False

	i.set(144) # Set dynamic limit
	try:
		root.DynLimits.set(145) # Must throw exception
		return False
	except ValueError:
		pass

	return True

def verify_handled_data(stream):
	"Handled data test"

	hd = stream.HandledData

	SIZE = 0x2000

	stream.BufSize.set(SIZE)

	buf = ""
	for i in range(SIZE):
		buf += (chr(random.randint(0, 255)))

	hd.set(buffer(buf))
	b = hd.get()
	if len(b) != SIZE:
		print "Error: expected buffer length 0x%x, got 0x%x" % (SIZE, len(b))
		return False

	if str(b) != buf:
		print "Buffers don't match"
#		dump(b, "/tmp/b.hex")
#		dump(buf, "/tmp/buf.hex")
		return False

	# Now try different size GET:
	b2 = netpp.Buffer(0x1000)
	ret = hd.get(b2)
	if ret != 0:
		print "Return", ret

	return True

def verify_logbuf(buf):
	"Verify log buffer handling"
	# Scenario: Buffer too short

	EXPECTED = [ [ "<0000> This is a fake log message",
		"<idle>",
		"<idle>",
		"<idle>",
		"<idle>" ],
		[ "<0005> This is a fake log message",
		"<idle>",
		"<idle>",
		"<idle>",
		"<idle>" ]
	]

	
	b = 4 * '\0'
	ret = buf.get(b)
	if ret > 0:
		print "Bytes to effectively reserve: %d " % ret
	# Try again:
	b = ret * '\0'
	# Tricky: Previous content can be in buffer.
	# Since python strings are not terminated by a \0 character,
	# (and may contain it, too) we need to slice in case of
	# a reference:
	
	for i in range(5):
		errcode = buf.get(b) # By reference, expect to fail

		print "%d> '%s' (string len: %d)" % (i, b[:errcode-1], errcode)

		if EXPECTED[0][i] != b[:errcode-1]:
			print "'%s' : '%s'" % ( EXPECTED[0][i], b[:errcode-1])
			return False

	for i in range(5):
		msg0 = buf.get()
		print "%d> '%s' " % (i, msg0)
		if EXPECTED[1][i] != msg0:
			print "'%d' : '%d'" % ( len(EXPECTED[0][i]), len(msg0))
			print "'%s': '%s'" % ( EXPECTED[1][i], msg0)
			return False

	return True

def verify_handled_array(ar):
	"Handled array test, see slave console"
	n = ar.Size
	ar[0].set("gna")
	v = ar[1].get()
	if v != "item_1":
		print "Got", v
		return False

	# Second method with pre-reserved string reference:
	s = 2 * ' ' # Too short string
	t = ar[0].get(s)
	if type(t) == type(0):
		print "Got return code", t
		pass
	elif s[:6] != "item_0":
		print "Got return value '%s'" % s
		print "len:", len(s)
		return False

	s = 10 * ' ' # Sufficient string
	ar[0].get(s)
	if s[:6] != "item_0":
		print "Got return value '%s'" % s
		print "len:", len(s)
		return False

	return True


def verify_dyndata(dd):
	"Dynamic data test"
	s = """
	Pesudo warpi salmu it perpensel den Sackenwuck.
	Kneisi wambo iegen zietsel floerde memso toi,
	Wakambe seibl osmo ilte pansi gorgel sampo!
	"""
	dd.set(s)

	a = dd.get()
	print "Got in return '%s'" % a
	return True

def verify_register(root):
	"Register endian test"
	root.ControlReg.set(0xaa55)
	low = root.ControlRegL.get()
	high = root.ControlRegH.get()

	if low != 0x55 or high != 0xaa:
		print "False endian order"
		return False
	return True

def verify_staticstring(s):
	"Static string test"
	try:
		s.set("honk")
		return False
	except TypeError:
		return True

def verify_varstring(s):
	"Variable string test"
	s.set("honk")
	v = s.get()
	s.set("Default") # Reset to Default
	if v != "honk":
		print "Return value does not match"
		return False

	return True

def verify_float(f):
	"Float test"
	min_f = f.Min.get()
	max_f = f.Max.get()

	testval = float(2.55999994278)

	try:
		f.set(0.4)
		return False
	except ValueError:
		pass

	f.set(testval)

	fval = f.get()
	testval = testval - fval

	if testval > 0.0000001:
		print "Float return value does not match, got %f" % fval
		return False

	return True

def verify_cmd(root):
	"DumpUser command"
	root.DumpUsers.set(0)
	print "Check slave console for dump"
	return True

def verify_hidden(dev):
	"Hidden property test"
	name = "Password"

	t = dev.device.getToken(name)
	p = netpp.Property(t, name)
	b = p.get()
	if b != "Default":
		return False
	# Make sure it's hidden:
	root = dev.sync()
	try:
		b = root.Password.get()
	except KeyError:
		return True

	print "Password property was not hidden"
	return False

############################################################################
# Cleanup

@pytest.yield_fixture(autouse=True, scope='session')
def test_cleanup():
	global slave, root, dev
	slave = subprocess.Popen(["devices/example/slave", "--port=5000"])
	time.sleep(0.5)
	dev = netpp.connect("TCP:localhost:5000")

	root = dev.sync()

	yield
	if slave:
		slave.terminate()
	time.sleep(1.0)

def test_hidden():
	t = [ verify_hidden, dev ]
	ret = apply(t[0], [t[1]])
	return ret

def run_tests(tests):

	fail = False

	for t in tests:
		ret = apply(t[0], [t[1]])
		if ret != True:
			fail = True

	assert fail != True

############################################################################
# Tests

def test_simple_properties():

	tests = [
		[ verify_integer, root ],
		[ verify_register, root ],
		[ verify_float, root.FloatVal ],
		[ verify_cmd, root ],
		[ verify_handled_array, root.HandledArray ],
	]

	run_tests(tests)

def test_string_properties():

	tests = [
		[ verify_staticstring, root.StaticString ],
		[ verify_varstring, root.VariableString ],
	]

	run_tests(tests)

def test_handlers():
	tests = [
		[ verify_lut_array, root.LUTarray ],
		[ verify_lut_values, root.LUTvalues ],
	]

	run_tests(tests)

def test_buffer_properties():
	tests = [
		[ verify_handled_data, root.Stream ],
		[ verify_dyndata, root.DynamicData ],
	]

	run_tests(tests)

def test_progress():
	ret = root.Stream.Start.set(1)
	retries = 20
	if ret > 0:
		progress = root.Stream.Start.children()[0]
		pr = progress.get()
		while pr != 100 and retries > 0:
			retries -= 1
			print pr
			pr = root.Progress.get()
		if pr != 100:
			raise ValueError, "Progress stuck at %d" % pr

	else:
		raise ValueError, "No event received"


############################################################################
# Should be the last test, as it terminates the slave:
#
def test_terminate():
	if slave:
		slave.terminate()
		try:	
			# When slave terminated, this MUST fail
			dev = netpp.connect("TCP:localhost:5000")
			assert False
		except:
			# Failure is expected!
			pass


############################################################################
# Main program:

if __name__ == '__main__':
	
	if len(sys.argv) == 2:
		print(sys.argv[1])
		dev = netpp.connect(sys.argv[1])
		slave = None
	else:
		slave = subprocess.Popen(["./slave", "--port=5000"])
		time.sleep(0.5)
		dev = netpp.connect("TCP:localhost:5000")

	root = dev.sync()

	root.prettyprint()

	tests = [
		[ verify_hidden, dev ],
		[ verify_integer, root ],
		[ verify_buffer, root ],
		[ verify_handled_data, root.Stream ],
		[ verify_logbuf, root.LogBuffer ],
		[ verify_dyndata, root.DynamicData ],
		[ verify_register, root ],
		[ verify_varstring, root.VariableString ],
		[ verify_float, root.FloatVal ],
		[ verify_cmd, root ],
		[ verify_handled_array, root.HandledArray ],
		[ verify_lut_array, root.LUTarray ],
		[ verify_lut_values, root.LUTvalues ],
	]

	if sys.platform == "win32":
		REDBG = "!!!     "
		GREEN = ""
		OFF = ""
	else:
		REDBG = "\033[7;31m"
		GREEN = "\033[32m"
		OFF = "\033[0m"

	for t in tests:
		print 76 * '-'
		print "Running %s" % t[0].__doc__
		ret = apply(t[0], [t[1]])
		if not ret:
			print REDBG + "TEST FAILED" + OFF
		else:
			print GREEN + "Test passed" + OFF


	slave.terminate()
