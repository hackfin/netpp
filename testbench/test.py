import sys
import random

sys.path.append("../Debug")
sys.path.append("../python")

import netpp

def test_hi_lo(r):
	r.ControlReg.set(0xaa55)
	low = r.ControlRegL.get()
	high = r.ControlRegH.get()
	if low != 0x55 or high != 0xaa:
		raise RuntimeError, "Hi/Low failed"

def test_string(r):
	s = """
Pesudo warpi salmu it perpensel den Sackenwuck.
Kneisi wambo iegen zietsel floerde memso toi,
Wakambe seibl osmo ilte pansi gorgel sampo!
"""
	r.Stream.DynamicData.set(s)
	ret = r.Stream.DynamicData.get()

	if ret != "<This string doesn't change>":
		raise RuntimeError, "Wrong string: '%s'" % ret

	r.VariableString.set("honk")
	s = r.VariableString.get()
	if s != "honk":
		raise RuntimeError, "VariableString: compare failed"

	try:
		r.StaticString.set("gna")
	except TypeError:
		print "Writeonly test passed"
		return 0
	raise RuntimeError, "Failed access test"

def gen_random_buf(n):
	buf = ""
	for i in xrange(n):
		buf += chr(random.randint(0, 255))
	return buffer(buf)
	

def test_buffer(r):
	n = 32768
	buf = gen_random_buf(n)

	buf = buffer(buf)
	r.Stream.HandledData.set(buf)
	size = r.Stream.MaxBufSize.get()

	retbuf = r.Stream.HandledData.get()
	if len(retbuf) != size:
		raise RuntimeError, "Wrong buffer size"

	retbuf = retbuf[:n] # Truncate buffer

	if str(buf) != str(retbuf):
		raise RuntimeError, "Truncated buffer mismatch"

	n = size
	buf = gen_random_buf(n)
	r.Stream.HandledData.set(buf)

	retbuf = r.Stream.HandledData.get()
	buf = buf[:n]

	if str(buf) != str(retbuf):
		raise RuntimeError, "Buffer data mismatch"


def test_array(r):
	a = r.LUTvalues

	s = r.LUTvalues.Size.get()

	for i in range(s):
		a[i].X.set(i)

	for i in range(s):
		if a[i].X.get() != i:
			raise RuntimeError, "Array test failed"

	try:
		# Provoke index-excess:
		a[s+1].X.set(0)
	except SystemError:
		print "Index guard test passed"
		return 0

	raise RuntimeError, "Did not guard bad index"

def test_integer(r):
	a = r.Integer1.get()
	max = r.Integer1.Max.get()

	try:
		r.Integer1.set(max + 1)
	except SystemError:
		min = r.Integer1.Min.get()
		try:
			r.Integer1.set(min - 1)
		except SystemError:
			return 0

	raise RuntimeError, "Did not guard limits"

def test_mode(r):
	m = r.Mode

	slow = m.Slow.get()

	m.set(slow)
	if m.get() != slow:
		raise RuntimeError, "Failed to set Mode"

	
	

def run_tests(r, tests):
	for i in tests:
		print 76 * "-"
		print "Running", i[0]
		i[1](r)
		print "- OK"

	print 76 * "-"

dev = netpp.connect("TCP:localhost:2008")

# Close:
del dev
# Reopen:
dev = netpp.connect("TCP:localhost:2008")


root = dev.sync()

tests = [
	[ "Buffer tests", test_buffer ],
	[ "High byte/Low byte test (Endianness)", test_hi_lo ],
	[ "Static/dynamic string test", test_string ],
	[ "Integer test", test_integer ],
	[ "Mode test", test_mode ],
	[ "Array test", test_array ],
]

run_tests(root, tests)
