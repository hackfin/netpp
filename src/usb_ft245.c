/** FT245 USB FIFO control frontend for NETPP library
 *
 * 04/2007 Martin Strubel <strubel@section5.ch>
 *
 */

// #define DEBUG

#define DEFAULT_VID DD_FT245_VID
#define DEFAULT_PID DD_FT245_PID

#ifdef WIN32
#include <windows.h>
#endif

#ifdef FTDI_NATIVE
#include <ftd2xx.h>
#else
#include <ftdi.h>
#endif

#include "devlib.h"
#include "devlib_error.h"

#include "property_protocol.h"

#include <stdlib.h>
#include <stdio.h>

#define MAX_PACKET_SIZE   63488
#define PKT_CHUNK_SIZE (MAX_PACKET_SIZE - HDR_SIZE)
#define HDR_SIZE sizeof(PropertyPacketHdr)

#define DD_FT245_VID 0x131E
#define DD_FT245_PID 0x0010

#ifndef FT245_VID
#define FT245_VID 0x0403
#endif

#ifndef FT245_PID
#define FT245_PID 0x6001
#endif
////////////////////////////////////////////////////////////////////////////

int handle_usberr(int error, const char *txt)
{
	netpp_log(DCLOG_ERROR, "USB error: %d: '%s'", error, txt);
	error = DCERR_COMM_GENERIC;
	return error;
}

#ifdef FTDI_NATIVE

static
int ft245_write(FT_HANDLE ft, const char *buf, int len)
{
	FT_STATUS s;
	DWORD n;
	DEB(printf("write %d bytes to FTDI\n", len));
	if (len == 0) {
		return 0;
	}
	s = FT_Write(ft, (void *) buf, len, &n);
	DEB(printf("wrote %d/%d bytes to FTDI\n", n, len));
	if (s < 0) return -1;
	return (int) n;
}

static
int ft245_read(FT_HANDLE ft, char *buf, int len)
{
	FT_STATUS s;
	DWORD n;
	s = FT_Read(ft, buf, len, &n);
	if (s != FT_OK) {
		return -1;
		netpp_log(DCLOG_ERROR, "FTDI error: %d", s);
	}
	DEB(printf("Read %ld bytes\n", n));
	if (n != len) return DCERR_COMM_TIMEOUT;
	return (int) n;
}

static
int ft245_read_packet(FT_HANDLE ft, char *buf)
{
	int n;
	int len;
	int i;

	n = ft245_read(ft, buf, HDR_SIZE);
	if (n != HDR_SIZE) return handle_usberr(n, "reading packet");

	// printf("%s: read %d bytes\n", __FUNCTION__, n);

	len = get_packet_length(buf);
	if (!len) return n;
	DEB(printf("Len: %d\n", len));
	

	if (len > PKT_CHUNK_SIZE) {
		len = PKT_CHUNK_SIZE;
	}

	buf += n;
	while (len > 0) {
		// Timeout handled by FTD2XX lib
		i = ft245_read(ft, (char*) buf, len);
		if (i < 0) {
			handle_usberr(i, "reading from USB");
			break;
		}
		DEB(printf("read %d bytes\n", i));
		buf += i; n += i; len -= i;
	}

	return n;
}


static
int ft245_poll_packet(Peer *p, PropertyPacketHdr **h, unsigned short timeout)
{
	int error = 0;
	unsigned long time;
	DWORD n = 0;

	FT_HANDLE ft = p->user.p;

#ifdef WIN32
	time = GetTickCount();
#endif

	while (n == 0) {
		error = FT_GetQueueStatus(ft, &n);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Queue status error #%d", error);
			return error;
		}
		DEB(printf("%d bytes in queue\n", n));
		if (n >= HDR_SIZE) {
			n = ft245_read_packet(ft, p->recvbuf);
			DEB(printf("in packet: %d\n", n));
			*h = (PropertyPacketHdr *) p->recvbuf;
		} else {
			n = 0; // Tell loop we're not yet happy.
			// FIXME Bad. When we wrap, we timeout, too. Catch this,
			// even if it may happen 1:10e5
#ifdef WIN32
			if ((GetTickCount() - time) > 2000) {
				DEB(printf("Timeout\n"));
				return 0;
			}
#else
			// No good and only experimental (Linux):
			// This can hang.
			Sleep(1);
#endif
		}
	}
	return n;
}

#else

static
int ft245_write(struct ftdi_context *ft, const char *buf, int len)
{
	int n;
	if (len) n = ftdi_write_data(ft, (unsigned char *) buf, len);
	else n = 0;
	DEB(printf("Wrote %d bytes to USB\n", n));
	return n;
}

static
int ft245_read(struct ftdi_context *ft, char *buf, int len)
{
	int size = 0;
	int n;
	int retry = 0;

	while (len > 0) {
		n = ftdi_read_data(ft, (unsigned char *) buf, len);
		buf += n; len -= n;
		size += n;
		if (n == 0) {
			usleep(10);
			retry++;
			if (retry == 200) return 0;
		}
		DEB(printf("Read %d bytes\n", n));
	}
	return size;
}

static
int ft245_read_packet(struct ftdi_context *ft, char *buf)
{
	int n;
	int i;
	int len;

	n = ft245_read(ft, buf, HDR_SIZE);
	if (n != HDR_SIZE) return handle_usberr(n, "reading packet");

	len = get_packet_length(buf);
	// printf("Len: %d\n", len);

	if (len > PKT_CHUNK_SIZE) {
		len = PKT_CHUNK_SIZE;
	}

	buf += n;
	while (len > 0) {
		i = ft245_read(ft, (char*) buf, len);
		if (i < 0) {
			handle_usberr(i, "reading from USB");
			break;
		}
		DEB(printf("read %d bytes\n", i));
		buf += i; n += i; len -= i;
	}

	return n;
}

static
int ft245_poll_packet(Peer *p, PropertyPacketHdr **h, unsigned short mode)
{
	int error;

	error = ft245_read_packet((struct ftdi_context *) p->user.p, p->recvbuf);
	*h = (PropertyPacketHdr *) p->recvbuf;

	return error;
}

#endif

static
int send_slice(Peer *peer, PropertyPacketHdr *h, const char *data,
		unsigned short len)
{
	int n;
	int error;

	n = ft245_write(peer->user.p, (char *) h, HDR_SIZE);

	error = ft245_write(peer->user.p, data, len);
	if (error < 0) {
		return handle_usberr(error, "usb write()");
	}

	return 0;
}

static
int ft245_send_packet(Peer *peer, PropertyPacketHdr *h,
		const char *data, size_t len)
{
	int error;
	unsigned short type = ntohs(h->type);

	// Now send packet fragments
	int i = 0;

	h->id = htons(i);

	while (len > PKT_CHUNK_SIZE) {
		h->type = htons(type | PP_FLAG_FRAG);
		// netpp_log(DCLOG_DEBUG, "Send slice (%d left)", len);
		error = send_slice(peer, h, data, PKT_CHUNK_SIZE);
		if (error < 0) {
			netpp_log(DCLOG_ERROR, "Failed to send slice");
			return error;
		}
		len -= PKT_CHUNK_SIZE;
		data += PKT_CHUNK_SIZE;

		// Update length field
		h->sub.set.len = htonl((unsigned long) len);

		i++;
		h->id = htons(i);
	}

	h->type = htons(type);

	error = send_slice(peer, h, data, (unsigned short) len);
	if (error < 0) {
		netpp_log(DCLOG_ERROR, "Failed to send slice");
		return error;
	}

	return error;
}

#ifdef FTDI_NATIVE

static
int ft245_probe(PortToken hub)
{
	PortToken port;
	FT_STATUS s;
	DWORD i;
	DWORD n;
	char id[8];
	FT_DEVICE_LIST_INFO_NODE *devinfo;

	// Linux will only find the FTDI when this call is used
#ifdef defined(__linux__) || defined(__APPLE__)
	FT_SetVIDPID(DEFAULT_VID, DEFAULT_PID);
#endif

	s = FT_CreateDeviceInfoList(&n);
	if (s != FT_OK) return DCERR_COMM_GENERIC;

	printf("Found %d FTDI devices\n", n);

	devinfo = (FT_DEVICE_LIST_INFO_NODE *)
		malloc(sizeof(FT_DEVICE_LIST_INFO_NODE) * n);

	if (!devinfo) return DCERR_MALLOC;

	s = FT_GetDeviceInfoList(devinfo, &n);
	if (s == FT_OK) {
		for (i = 0; i < n; i++) {
			if (devinfo[i].ID == (((FT245_VID) << 16) | FT245_PID)) {
				sprintf(id, "ftdi%d", i);
				port = Port(id, 0, PROPERTY_NIL);
			} else
			if (devinfo[i].ID == (((DD_FT245_VID) << 16) | DD_FT245_PID)) {
				sprintf(id, "perio%d", i);
				port = Port(id, 0, PROPERTY_NIL);
			} else {
				port = NULL;
			}
			if (port)
				hub_append(hub, port);

		}
	}

	free(devinfo);
	return 0;
}

int ft245_connection_init(Peer *p, int index)
{
	FT_HANDLE ft;
	FT_STATUS s;

#ifdef defined(__linux__) || defined(__APPLE__)
	FT_SetVIDPID(DEFAULT_VID, DEFAULT_PID);
#endif

	s = FT_Open(index, &ft);
	if (s != FT_OK) return DCERR_COMM_INIT;

	s = FT_ResetDevice(ft);
	if (s != FT_OK) return DCERR_COMM_INIT;

	s = FT_Purge(ft, FT_PURGE_RX | FT_PURGE_TX);
	if (s != FT_OK) return DCERR_COMM_INIT;

	s = FT_SetTimeouts(ft, 2000, 1000);
	if (s != FT_OK) return DCERR_COMM_INIT;

	s = FT_SetUSBParameters(ft, 0x10000, 0);
	if (s != FT_OK) return DCERR_COMM_INIT;

	s = FT_SetLatencyTimer(ft, 8);
	if (s != FT_OK) { 
		return DCERR_COMM_INIT;
	}

	// This finally enables the FIFO full control
	s = FT_SetFlowControl(ft, FT_FLOW_RTS_CTS, 0, 0);
	// s = FT_SetFlowControl(ft, FT_FLOW_NONE, 0, 0);
	if (s != FT_OK) return DCERR_COMM_INIT;


	p->user.p = ft;
	p->recvbuf = (unsigned char *) malloc(MAX_PACKET_SIZE);
	// We don't need these (yet)
	// p->recvtimeout_check = TIMEOUT_CHECK;
	// p->recvtimeout_wait = TIMEOUT_WAIT;
	if (!p->recvbuf) return DCERR_MALLOC;
	return 0;
}

int ft245_connection_exit(Peer *p)
{
	FT_Close(p->user.p);
	free(p->recvbuf);
	return 0;
}



#else

// libfdti variant:

static
int ft245_probe(PortToken hub)
{
	char id[8];
	int i;
	PortToken port;
	struct ftdi_context ft;
	struct ftdi_device_list *devlist;
	struct ftdi_device_list *walk;

 	i = ftdi_usb_find_all(&ft, &devlist, DEFAULT_VID, DEFAULT_PID);

	if (i <= 0) return i;

	walk = devlist;
	i = 0;
	while (walk) {
		sprintf(id, "%s:%d", walk->dev->filename, i);
		port = Port(id, 0, PROPERTY_NIL);
		hub_append(hub, port);
		i++;
		walk = walk->next;
	}

	ftdi_list_free(&devlist);
	return 0;
}

int ft245_connection_init(Peer *p, int index)
{
	struct ftdi_context *ft;
	int error;

	ft =  (struct ftdi_context *) malloc(sizeof(struct ftdi_context));
	if (!ft) return DCERR_MALLOC;

	if (ftdi_init(ft) != 0) return DCERR_COMM_INIT;

	error = ftdi_usb_open(ft, DEFAULT_VID, DEFAULT_PID);
	if (error < 0) return DCERR_COMM_INIT;

	p->user.p = ft;
	p->recvbuf = (char *) malloc(MAX_PACKET_SIZE);
	if (!p->recvbuf) return DCERR_MALLOC;
	return 0;
}

int ft245_connection_exit(Peer *p)
{
	ftdi_usb_close(p->user.p);
	ftdi_deinit(p->user.p);
	free(p->user.p);
	if (p->recvbuf) free(p->recvbuf);
	return 0;
}


#endif
///
/**
*/

static
int ft245_open(RemoteDevice *d, int portindex)
{
	return ft245_connection_init(&d->peer, portindex);
}

static
int ft245_close(RemoteDevice *d)
{
	return ft245_connection_exit(&d->peer);
}

#ifndef C99_COMPLIANT

struct protocol_methods usb_methods = {
	&ft245_probe,
	0,
	&ft245_open,
	&ft245_close,
	&ft245_send_packet,
	&ft245_poll_packet
};

#else

struct protocol_methods usb_methods = {
	.probe   = &ft245_probe,
	.create  = 0,
	.open    = &ft245_open,
	.close   = &ft245_close,
	.send    = &ft245_send_packet,
	.recv    = &ft245_poll_packet
};

#endif
