/** \file slave.c
 *
 * Slave property dispatcher
 * Property protocol back end (embedded system, etc.)
 *
 * (c) 2006 Martin Strubel <hackfin@section5.ch>
 *
 */

#ifdef DEBUG
#define DEB(x) x
#endif

#include "netpp.h"
#include "slave.h"
#include "example.h"
#include <stdio.h>

#ifdef ALLOW_DYNAMIC_PROPERTIES
#include "dynprops.h"
#endif

// For local_getroot(), see src/server.c

static unsigned char _dummy_registers[256] = {
	0xaa, 0x55, 
	[0x10] = 0x01, 0x20, 0x02, 0x21, 0x03, 0x24
};

/** Device flat address register map write access.
 * For low level device access (SPI, I2C, etc.) this normally wants to
 * be implemented */

#ifdef __ZPU__

int device_write(RemoteDevice *d,
		uint32_t addr, const unsigned char *buf,
		unsigned long size)
{
	return 0;
}

int device_read(RemoteDevice *d,
		uint32_t addr, unsigned char *buf, unsigned long size)
{
	return 0;
}

#else

int device_write(RemoteDevice *d,
		uint32_t addr, const unsigned char *buf,
		unsigned long size)
{
	addr &= 0xfff;
	if (addr > 255) {
		printf("Address 0x%x out of range.\n", addr);
		return DCERR_PROPERTY_RANGE;
	}
	printf("Write to register %04x:", addr);
	memcpy(&_dummy_registers[addr & 0xff], buf, size);
	while (size--) {
		printf(" %02x", *buf++);
	}
	printf("\n");
	return 0;
}


/** Device flat address register map read access.
 * For low level device access (SPI, I2C, etc.) this normally wants to
 * be implemented */

int device_read(RemoteDevice *d,
		uint32_t addr, unsigned char *buf, unsigned long size)
{
	addr &= 0xfff;
	if (addr > 255) {
		printf("Address 0x%x out of range.\n", addr);
		return DCERR_PROPERTY_RANGE;
	}
	printf("Read from register %04x (%lu bytes)\n", addr, size);
	memcpy(buf, &_dummy_registers[addr & 0xff], size);
	return 0;
}

#endif

#ifdef __MINGW32__
TOKEN local_getroot(RemoteDevice *d)
{
	return DEVICE_TOKEN(DEFAULT_DEVICE_INDEX);
}
#endif

////////////////////////////////////////////////////////////////////////////
//
// TODO: Install a SIGPIPE handler


/* This structure is a template of a dynamic class node deriving from
 * a static class, in this case the default device index DEVICE_INDEX
 */


#ifdef ALLOW_DYNAMIC_PROPERTIES
PropertyDesc s_rootprop = {
	.type = DC_ROOT,
	.flags = F_RO | F_LINK /* Derived ! */,
	.access = { .base = 0 }
};
#endif

extern TOKEN g_t_templ_obj;

int main(int argc, char **argv)
{
	int error;
	TOKEN root, t;

	struct Object obj0, obj1;

	obj0.count = 0;
	obj1.count = 1;
	// Lazy and 'broken' string copy:
	strncpy(obj0.name, "u0", sizeof(obj0.name)-1);
	strncpy(obj1.name, "u1", sizeof(obj0.name)-1);

	// Important: Register property list
	register_proplist(g_devices, g_ndevices);
//
// This function must be called for example from MSVC
#ifndef C99_COMPLIANT
	init_properties();
#endif

#ifdef ALLOW_DYNAMIC_PROPERTIES

	error = dynprop_init(20);
	if (error < 0) return error;

	// Create a dynamic root node:
	root = new_dynprop("DynDevice", &s_rootprop);
	// Insert some properties into the root node:

	t = dynprop_from_entity(root, &obj0, g_t_templ_obj, "Obj0");
	if (t == TOKEN_INVALID) {
		netpp_log(DCLOG_ERROR, "Unable to create Obj0");
	}

	t = dynprop_from_entity(root, &obj1, g_t_templ_obj, "Obj1");
	if (t == TOKEN_INVALID) {
		netpp_log(DCLOG_ERROR, "Unable to create Obj1");
	}

#endif

	error = start_server(argc, argv);
	if (error)
		netpp_log(DCLOG_ERROR, "Server terminated with error:\n"
			"'%s'\n", dcGetErrorString(error));
	return error;
}

