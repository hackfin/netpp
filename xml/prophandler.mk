# Common makefile for property handler source generation
#
# (c) 2005-2014 Martin Strubel <hackfin@section5.ch>
#

# Must define:
#
# DEVICEFILE: device file to process
# NETPP  : NETPP top level directory

XSLT = $(NETPP)/xml
XP   = xsltproc

HTMLDOCBOOK = /usr/share/xml/docbook/stylesheet/nwalsh/html/docbook.xsl

HTMLOPTIONS ?= --stringparam html.stylesheet css/style.css

OUTPUT_IMG ?= img

COMMON = $(NETPP)/common

COMMONSRCDIR ?= $(COMMON)

XPTRFILE = xptr_$(notdir $(DEVICEFILE))
WRAPPEDFILE ?= wrapped_$(notdir $(DEVICEFILE))

# If WRAPPER is defined, use temporarely property-wrapped file instead
# explicit device file

ifdef WRAPPER
USERFILE=$(WRAPPEDFILE)
else
USERFILE=$(XPTRFILE)
endif

# Global error header generation:
$(NETPP)/include/devlib_error.h: $(COMMONSRCDIR)/errorcodes.xml
	$(XP) -o $@ $(XSLT)/errorlist.xsl $(COMMONSRCDIR)/errorcodes.xml

# Header generation for device specific errors:
device_error.h: $(DEVICEFILE)
	$(XP) -o $@ $(XSLT)/errorlist.xsl $<

# Error handler generation:
$(COMMONSRCDIR)/errorhandler.c: $(NETPP)/include/devlib_error.h
	$(XP) -o $@ $(XSLT)/errorhandler.xsl $(COMMONSRCDIR)/errorcodes.xml

# Create xpointer meta file of device file
xptr_%.xml: %.xml \
	      $(DEVICEFILE_DEPS) \
	      $(COMMONSRCDIR)/errorhandler.c
	$(XP) -o $@ $(XSLT)/xpointer.xsl $<

$(XPTRFILE): $(DEVICEFILE) \
	      $(DEVICEFILE_DEPS) \
	      $(COMMONSRCDIR)/errorhandler.c
	$(XP) -o $@ $(XSLT)/xpointer.xsl $<

# Generate register header of first device in DEVICEFILE
# (unless specified different in REGISTER_OUTPUT_OPTIONS)
register.h: $(USERFILE)
	$(XP) -o $@ \
		--stringparam srcfile $(DEVICEFILE) \
		$(REGISTER_OUTPUT_OPTIONS) \
		--xinclude $(XSLT)/registermap.xsl $<

# Create register header for device specified by device node id
%_register.h: xptr_%.xml
	$(XP) -o $@ \
		--stringparam srcfile $(patsubst xptr_%.xml,%.xml,$<) \
		--stringparam selectDevice $(patsubst %_register.h,%,$@) \
		$(REGISTER_OUTPUT_OPTIONS) \
		--xinclude $(XSLT)/registermap.xsl $<

# Create mode header from mode selection values
register_modes.h: $(USERFILE)
	$(XP) -o $@ \
		$(REGISTER_OUTPUT_OPTIONS) \
		--xinclude $(XSLT)/values.xsl $<

%_register_modes.h: xptr_%.xml
	$(XP) -o $@ \
		--stringparam srcfile $(patsubst xptr_%.xml,%.xml,$<) \
		--stringparam selectDevice $(patsubst %_register_modes.h,%,$@) \
		$(REGISTER_OUTPUT_OPTIONS) \
		--xinclude $(XSLT)/values.xsl $<

reg8051.h: $(USERFILE)
	$(XP) -o $@ \
		--xinclude $(XSLT)/reg8051.xsl $<

# Property tree structure for compilation:
%_proptable.c: xptr_%.xml
	$(XP) -o $@ \
		$(PROPLIST_OPTIONS) \
		--xinclude $(XSLT)/proplist.xsl $<

proplist.c: $(USERFILE)
	$(XP) -o $@ \
		$(PROPLIST_OPTIONS) \
		--xinclude $(XSLT)/proplist.xsl $<

# Handler skeleton file. Use this as a template.
handler_skeleton.c: $(USERFILE)
	$(XP) -o $@ \
		--xinclude $(XSLT)/userhandler.xsl $<

# Generation of documentation:
device_properties.xml: $(USERFILE) $(XSLT)/proptable.xsl
	$(XP) -o $@ \
		$(DOC_CONVERSION_FLAGS) \
		--xinclude $(XSLT)/proptable.xsl $<

# Generation of register bit mapping graphics:
$(OUTPUT_IMG)/reg_%.svg: $(USERFILE) $(XSLT)/reg2svg.xsl
	$(XP) -o $@ \
		--stringparam register $(patsubst $(OUTPUT_IMG)/reg_%.svg,%,$@) \
		--xinclude $(XSLT)/reg2svg.xsl $<

# Generation of entire register map graphics:
$(OUTPUT_IMG)/regmap_%.svg: $(USERFILE) $(XSLT)/regmap2svg.xsl
	$(XP) -o $@ \
		$(REGMAP_OUTPUT_OPTIONS) \
		--stringparam registermap $(patsubst $(OUTPUT_IMG)/regmap_%.svg,%,$@) \
		--xinclude $(XSLT)/regmap2svg.xsl $<

# Wrap pure register properties:
# Specifiy 'WRAPPER_OUTPUT_OPTIONS = --stringparam selectDevice <id>' to wrap
# properties of a specific device

wrapped_%.xml: xptr_%.xml
	$(XP) -o $@ \
		$(WRAPPER_OUTPUT_OPTIONS) \
		--xinclude $(XSLT)/regwrap.xsl $<

%.html: %.xml
	$(XP) $(HTMLOPTIONS) -o $@ $(HTMLDOCBOOK) $<


debug:
	echo $(DEVICEFILE)

doc: device_properties.xml

clean::
	-rm -f $(XPTRFILE)
	-rm -f reg_*.svg regmap_*.svg
	-rm -f $(COMMONSRCDIR)/errorhandler.c
