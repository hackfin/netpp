#include <avr/io.h>

#define FIFO_BUFFER_SIZE 64

#define FIFO_BUFFER_MASK (FIFO_BUFFER_SIZE - 1)

struct fifo {
	volatile unsigned char head;
	volatile unsigned char tail;
	short err;
	unsigned char buf[FIFO_BUFFER_SIZE];
};

#define UART1_RECEIVE_INTERRUPT   USART1_RX_vect
#define UART1_TRANSMIT_INTERRUPT  USART1_UDRE_vect
#define UART1_STATUS   UCSR1A
#define UART1_CONTROL  UCSR1B
#define UART1_DATA     UDR1
#define UART1_UDRIE    UDRIE1

extern struct fifo g_rxfifo;

void uart1_putc(uint8_t data);
void uart1_puts(const char *str);
void uart1_init(uint16_t baudrate);
void toggle_led(char which);
