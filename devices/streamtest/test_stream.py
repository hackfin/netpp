#!/usr/bin/env python

# Test script to test stream server

import sys
import time
import netpp
import pytest
import subprocess


def stream_test(root, block, bsize):

	root.Stream.BlockSize.set(bsize)
	root.Stream.Start.set(1)
	# Send block three times:
	sum = 0
	for i in range(8):
		t0 = time.time()
		root.Stream.Data.set(block)
		t1 = time.time()

		t = t1 - t0
		kbs = len(block) / t / 1000
		print("            %f kb/s" % kbs)
		sum += kbs

	root.Stream.Stop.set(1)
	return sum / 8


def test_stream_tcp():

	n = 1024 * 1024
	m = 2

	block = chr(0xff)
	block += (n / 2 - 1) * " "
	block += block

	block = buffer(block)


	print(80 * "-")
	print("TCP test")
	print(80 * "-")

	#########

	sum = 0
	for i in range(m):
		sum += stream_test(root_tcp, block, n)

	print("TCP average: %f" % (sum / m))

def test_stream_udp():
	n = 1024
	m = 1

	block = chr(0xff)
	block += (n / 2 - 1) * " "
	block += block

	block = buffer(block)

	print(80 * "-")
	print("UDP test")
	print(80 * "-")

	sum = 0
	for i in range(m):
		sum += stream_test(root_udp, block, n)

	print("UDP average: %f" % (sum / m))




def runtest_stream_remote_tcp(proto, r):
	r.Client.set(proto + ":localhost:5010")
	# Go into feeder mode which throws buffers at the receiver ('Client')
	r.Mode.set(1)

	r.Stream.Stop.set(1)
	r.Stream.Start.set(1)
	time.sleep(2.0)
	r.Stream.Stop.set(1)



def test_stream_remote_tcp():
	global receiver_log, receiver
	receiver_log = open("/tmp/receiver.log", "w")
	receiver = subprocess.Popen([RECEIVER, "--port=5010", "--hide"],
		stdout=receiver_log)
	time.sleep(0.5)

	try:
		runtest_stream_remote_tcp("TCP", root_tcp)
	except:
		receiver.terminate()

def test_stream_remote_udp():
	global receiver_log, receiver

	runtest_stream_remote_tcp("UDP", root_tcp)
	receiver.terminate()
	receiver_log.close()

@pytest.yield_fixture(autouse=True, scope='session')
def test_cleanup():
	global slave, root_udp, root_tcp

	slave = subprocess.Popen([SLAVE, "--port=5002"])
	time.sleep(0.5)
	dev_tcp = netpp.connect("TCP:localhost:5002")
	dev_udp = netpp.connect("UDP:localhost:5002")

	root_udp = dev_udp.sync()
	root_tcp = dev_tcp.sync()

	yield
	if slave:
		slave.terminate()

if __name__ == '__main__':
	RECEIVER = "../example/slave"
	SLAVE = "./slave"


	slave = subprocess.Popen([SLAVE, "--port=5002"])
	time.sleep(0.5)
	dev_tcp = netpp.connect("TCP:localhost:5002")
	dev_udp = netpp.connect("UDP:localhost:5002")

	root_udp = dev_udp.sync()
	root_tcp = dev_tcp.sync()

	test_stream_tcp()
	test_stream_remote_tcp()
	test_stream_udp()
	slave.terminate()

else:
	SLAVE = "devices/streamtest/slave"
	RECEIVER = "devices/example/slave"




