/* Main test program for host (master)
 *
 * (c) 2006 Martin Strubel <hackfin@section5.ch>
 *
 */

#include "devlib.h"
#include "devlib_error.h"
#include "device_error.h"
#include "property_protocol.h"

#include "fileops.h"

#include <stdio.h> // DEBUG
#include <stdlib.h>

#include "timing.h"


struct tokens {
	TOKEN filename;
	TOKEN filesize;
	TOKEN filedata;
	TOKEN fileopen;
	TOKEN fileclose;
} g_tokens;

int handle_error(int error)
{
	switch (error) {
		case DCERR_FILE_NOT_EXISTENT:
			printf("File does not exist\n");
			break;
		case DCERR_FILE_OPEN:
			printf("File open error\n");
			break;
		case DCERR_FILE_READ:
			printf("File read error\n");
			break;
		case DCERR_FILE_ACCESS:
			printf("File permission error\n");
			break;
		case DCERR_FILE_GENERIC:
			printf("Generic File handling error (unhandled)\n");
			break;
		case 0: break;
		default:
			printf("Error: %s\n", dcGetErrorString(error));
	}
	return error;
}

static struct _tdesc {
	TOKEN *t;
	char *name;
} init_tokens[] = 
{
	{ &g_tokens.filename, "File.Name" },
	{ &g_tokens.filesize, "File.Size" },
	{ &g_tokens.filedata, "File.Data" },
	{ &g_tokens.fileopen, "File.Open" },
	{ &g_tokens.fileclose, "File.Close" },
	{ 0 }
};

int token_init(RemoteDevice *d)
{
	int error;
	struct _tdesc *t = init_tokens;
	
	while (t->name) {
		error = dcProperty_ParseName(d, t->name, t->t);
		if (error < 0) {
			handle_error(error);
			return 0;
		}
		t++;
	}
	return 1;
}

int get_prop(RemoteDevice *d, TOKEN p, DCValue *v)
{
	int error;
	error = dcDevice_GetProperty(d, p, v);
	if (error < 0) handle_error(error);
	return error;
}

int receive_file(RemoteDevice *d, const char *name)
{
	int n = 0x80000; // chunk size
	int error;
	char *buf;
	FILE *f;
	DCValue v;

	TIMEVAL t0, t1;
	float t;

	printf("Receiving file...\n");

	buf = (char *) malloc(n);
	if (!buf) {
		printf("Malloc error, size too big?\n");
		return 0;
	}


	f = fopen(name, "wb");
	if (!f) return DCERR_FILE_OPEN;

	// Request <chunksize> bytes from property 'File.Data' until
	// a return value of PROPERTY_MODIFIED occurs.
	// This means, that less than the
	// requested number of bytes was read, thus the file end was reached.
	do {
		v.value.p = buf;
		v.len = n; v.type = DC_BUFFER;
		get_time(&t0);
		printf("------------------- GET CHUNK -----------------------\n");
		error = dcDevice_GetProperty(d, g_tokens.filedata, &v);
		get_time(&t1);
		t = took_time(&t0, &t1);
		printf("rate: %f kB/s\n", v.len / 1000.0 / t);

		// Real number of bytes delivered is possibly updated in v.len:
		if (error < 0) {
			return handle_error(error);
		}

		printf("Got %d bytes\n", v.len);
		if (fwrite(buf, 1, v.len, f) == 0) break;

	} while (error != DCWARN_PROPERTY_MODIFIED);



	fclose(f);
	printf("Close file\n");

	free(buf);
	return 0;
}

int get_file(RemoteDevice *d, const char *filename)
{
	int error;
	DCValue v;

	printf("Getting file %s\n", filename);
	v.type = DC_STRING;
	v.value.p = (void *) filename;
	// Setting of length field is not necessary
	
	// First, we set the filename of the file we want to retrieve.
	error = dcDevice_SetProperty(d, g_tokens.filename, &v);
	if (error < 0) {
		return handle_error(error);
	}

	v.type = DC_COMMAND;
	v.value.i = FILE_OPEN_READ;
	error = dcDevice_SetProperty(d, g_tokens.fileopen, &v);
	if (error < 0) return handle_error(error);
	error = dcDevice_GetProperty(d, g_tokens.filesize, &v);
	if (error < 0) return handle_error(error);
	printf("File size: %ld bytes\n", v.value.i);

	// Now receive file in chunks
	error = receive_file(d, "received.dat");

	v.type = DC_COMMAND;
	error = dcDevice_SetProperty(d, g_tokens.fileclose, &v);
	if (error < 0) handle_error(error);

	return 0;
}


int main(int argc, char **argv)
{
	int error;
	int i;

	RemoteDevice *d;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <netpp target>\n", argv[0]);
		return -1;
	}
	
	error = dcDeviceOpen(argv[1], &d);
	if (error < 0) {
		handle_error(error);
		return -1;
	}

	if (token_init(d)) {
		for (i = 0; i < 200; i++) {
			get_file(d, "test.dat");
		}
	} else {
		printf("Failed to initialize Property tokens. Wrong backend?\n");
	}

	dcDeviceClose(d);
	return error;
}


