/** \file zpu-unaligned.h
 *
 * Specific blackfin unaligned accesses.
 *
 * Replacement for asm-generic/unaligned.h. Reason to include here: some
 * toolchain installations don't have it, and it's buggy when accessing
 * floats.
 *
 */

/* Supports only datatypes of size up to 4. Watch it! */

// TODO: Even more elegant solution: allocate the wanted datatype
// instead of uint32 to support generic sizes.

#define get_unaligned(x) __get_unaligned((x), sizeof(*(x)))

/* Sidenote: Very nice pitfall: while(n--) instead of introducing a counter
 * variable 'i' would optimize the loop away...and not show a warning!
 */

#define __get_unaligned(p, n)   (       \
	{                                   \
		int i = n;                      \
		__typeof__(*(p)) v[1];          \
		unsigned char *d, *s;           \
		d = (unsigned char *) v;        \
		s = (unsigned char *) p;        \
		while(i--) {                    \
			*d++ = *s++;                \
		}                               \
		(__typeof__(*(p))) v[0];        \
	} )
		
#define store_unaligned_float(dst, src) \
	{                                   \
		unsigned char *d, *s;           \
		d = (unsigned char *) dst;      \
		s = (unsigned char *) src;      \
		*d++ = *s++;                    \
		*d++ = *s++;                    \
		*d++ = *s++;                    \
		*d++ = *s++;                    \
	}



