/** \file local_api.c
 *
 * Property/Device control library API
 *
 * These are wrapped calls
 *
 */

#include "devlib.h"
#include "devlib_types.h"
#include "slave.h"
#include "devlib_error.h"

#include <stdio.h>
#include <assert.h>

const char version_string[] = VERSION_STRING;

#define IS_BUFFER(x)      (x == DC_BUFFER || x == DC_STRING)

////////////////////////////////////////////////////////////////////////////

struct DCDevice g_this_device = {
	.proto_version = 0,

};

int
dcDeviceOpen(const char *port, DEVICE *pDev)
{
	*pDev = &g_this_device;
#ifndef WIN32
	register_proplist(g_devices, g_ndevices);
#endif
	return ports_init(NULL);
}

int
dcDeviceClose(RemoteDevice *d)
{
	return 0;
}

////////////////////////////////////////////////////////////////////////////

int
dcDevice_GetRoot(RemoteDevice *d, TOKEN *t)
{
	*t = local_getroot(d);
	return 0;
}

int 
dcProperty_Select(DEVICE d, TOKEN parent, TOKEN prev, TOKEN *out)
{
	int error = 0;
	TOKEN t;
	if (d == DC_PORT_ROOT) {
		PortToken next;

		error = port_select(parent, prev, &next);
		t = (TOKEN) next;
	} else {
		t = property_select(d, parent, prev);
	}
	if (t == TOKEN_INVALID) error = DCWARN_PROPERTY_NIL;
	*out = t;
	return error;
}

int
dcProperty_GetProto(DEVICE d, TOKEN t, DCValue *val)
{
	if (d == DC_PORT_ROOT) {
		val->value.i = 'D';
		return 0;
	} else {
		return property_getdesc(d, t, val);
	}
}

/** Gets a property value from a device property
 *
 * \param d      The device handle
 * \param p      The property token
 * \param value  Pointer to the DCValue struct
 *
 */

int 
dcDevice_GetProperty(DEVICE d, TOKEN p, DCValue *val)
{
	int error = 0;
	DCValue proxy;
	int type;
	proxy.len = val->len;
	error = property_get(d, p, &proxy);
	type = proxy.type;
	if (IS_BUFFER(type)) {
		if (IS_BUFFER(val->type)) {
			if (error != DCERR_PROPERTY_SIZE_MATCH) {
				memcpy(val->value.p, proxy.value.p, proxy.len);
				// Release buffer, possibly:
				proxy.type = DC_COMMAND;
				error = property_get(d, p, &proxy);
			}
		} else {
			error = DCERR_PROPERTY_TYPE_MATCH;
		}
	} else {
		val->value = proxy.value;
	}
	val->type = type;
	return error;
}

/** Sets a property value of a device property
 *
 * \param d      The device handle
 * \param p      The property token
 * \param value  const Pointer to the DCValue struct
 *
 */

int 
dcDevice_SetProperty(DEVICE d, TOKEN p, DCValue *val)
{
	int error;
	DCValue proxy;
	if (IS_BUFFER(val->type)) {
		proxy.type = val->type;
		proxy.len = val->len;
		error = property_set(d, p, &proxy); // where does the data go?
		if (error >= 0) {
			memcpy(proxy.value.p, val->value.p, proxy.len);
			proxy.type = DC_COMMAND;
			error = property_set(d, p, &proxy);
		}
	} else {
		error = property_set(d, p, val);
	}
	return error;
}


int dcProperty_ParseName(DEVICE d, const char *propname, TOKEN *out)
{
	TOKEN t = local_getroot(d);

	t = property_parsename(d, propname, t);

	*out = t;
	return 0;
}


/** Get property value as string. Note that the caller owns the
 * string. 
 */

PropertyType dcProperty_GetType(DEVICE d, TOKEN t)
{
	const PropertyDesc *p = getProperty_ByToken(t);
	assert(p != 0);
	return p->type;
}

unsigned long dcProperty_GetFlags(DEVICE d, TOKEN t)
{
	const PropertyDesc *p = getProperty_ByToken(t);
	assert(p != 0);
	return p->flags;
}

int dcProperty_GetName(DEVICE d, TOKEN t, DCValue *val)
{
	int n;
	int error = 0;
	const char *name;
	if (d == DC_PORT_ROOT) {
		error = portdesc_getname((PortToken) t, &name);
		if (error >= 0) {
			val->type = DC_STRING;
			val->value.p = (void *) name;
			val->len = strlen(name) + 1;
		}
	} else {
		const PropertyDesc *p = getProperty_ByToken(t);
		assert(p != 0);
		name = p->name;
		n = strlen(name) + 1;
		if (val->len < n) return DCERR_PROPERTY_SIZE_MATCH;
		strncpy(val->value.p, name, val->len);
	}
	return error;
}

int
dcDevice_GetHubName(RemoteDevice *d, const char **name)
{
	*name = "LOCAL_API";
	return 0;
}

////////////////////////////////////////////////////////////////////////////
// Methods

int protocol_request(RemoteDevice *d, int request)
{
	return NETPP_PROTOCOL_VERSION;
}

int loc_probe(DynPropertyDesc *h)
{
	PortToken p;
	PortBay *b;
	b = portbay_fromtoken(h->hub.ports);
	p = PortNode(b, "0", DC_PORT);
	portbay_append(b, p);
	return 0;
}

int loc_open(RemoteDevice *d, int portindex)
{
	return 0;
}

int loc_close(RemoteDevice *d)
{
	return 0;
}


int loc_send_packet(Peer *peer, PropertyPacketHdr *h,
		const char *data, size_t len)
{
	return 0;
}

int loc_poll_packet(Peer *p, PropertyPacketHdr **h, unsigned short timeout)
{
	return 0;
}


struct protocol_methods loc_methods = {
	.probe   = &loc_probe,
	.open    = &loc_open,
	.close   = &loc_close,
	.send    = &loc_send_packet,
	.recv    = &loc_poll_packet
};

