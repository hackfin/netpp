/** \file devlib_types.h
 * Devlib API data types
 *
 * These are datatypes that have to be exported to the API.
 *
 * WARNING: Changes in these data structures may affect
 * API binary compatibility. If this is the case, the major library
 * version number must be increased.
 *
 * $Id: devlib_types.h 333 2014-10-19 13:03:39Z strubi $
 *
 */

#ifndef DCTYPES_H_INCLUDED_260872
#define DCTYPES_H_INCLUDED_260872

#include "api.h"

// Forward declaration of device struct. Never expose to the user.

struct DCDevice;
#define DEVICE struct DCDevice*


// Property token (handle)
// TODO: Revert to 16 bit tokens for small CPUs.
// #ifdef TINYCPU
// #warning "Still using 32 bit tokens for TINYCPU"
// #define TOKEN uint32_t
// #else
#define TOKEN uint32_t
// #endif

/** Invalid token value. When returned from dcProperty_ParseName(),
 * a property of that name was not found. */
#define TOKEN_INVALID          0xffffffff
/** Reserved for packet size by some protocols */
#define TOKEN_PKTSIZE          0xfffffffe
/** Number of properties */
#define TOKEN_NPROPS           0xfffffffd
/** Special CRC16 property checksum query token. See property_crc() */
#define TOKEN_CHECK            0xfffffffc

// All other tokens in the range from 0xf0000000 - 0xfffffff0 can be
// used by custom handlers when CONFIG_CUSTOMHANDLERS is defined.

#define RESERVED_MASK          0xf0000000

// Legacy that does not hurt.
#define INVALID_TOKEN          TOKEN_INVALID

#define INVALID_INDEX ((PROPINDEX) TOKEN_INVALID)

#define IS_RESERVED(x)   ((x & RESERVED_MASK) == RESERVED_MASK)
#define IS_VALIDTOKEN(x) (x != TOKEN_INVALID)

#define DC_LOCAL_DEVICE  (struct DCDevice *) NULL
/** Pseudo device handle for port hub structure */
#define DC_PORT_ROOT     (struct DCDevice *) -1

#define HAS_BASECLASS

#define DC_UNSIGNED 0x0080    ///< Property is unsigned

/** 
 *  Property data types
 */

enum property_type_enum {
	DC_INVALID,
	DC_ROOT,     ///< ROOT NODE TYPE
	DC_INT,      ///< A 32 bit signed integer
	DC_FLOAT,    ///< A 4 byte float, single precision
	DC_BOOL,     ///< A boolean value (1: true, 0: false)
	DC_MODE,     ///< A mode value. Only the values in the choice are valid.
	DC_RESERVED, 
	DC_STRING,   ///< A string value (constant or fixed lengh, 0 terminated)
	DC_BUFFER,   ///< A buffer value. The length is specified in the len field.
	// Meta types:
	DC_STRUCT,   ///< A struct value containing other properties
	DC_ARRAY,    ///< An array value, containing a struct or property
	// Special:
	DC_COMMAND,  ///< A command
	DC_EVENT,    ///< An event node.
	DC_UINT = DC_INT | DC_UNSIGNED  ///< An unsigned integer
	// Special types:
};

#ifdef TINYCPU
typedef uint8_t PropertyType;
#else
typedef enum property_type_enum PropertyType;
#endif


#define DC_REGISTER   DC_UINT

// Undefined (unknown) data type
#define DC_UNDEFINED  DC_INVALID

/** Property flags
 *
 * All other bits are reserved for internal purposes.
 *
 */


/* These flags are private. Can be changed. */
#define F_PRIVATE  0x0001    ///< Property is private ('hidden' attribute)
#define F_BIG      0x0002    ///< Big endian, if Register node
#define F_BLOCKING 0x0010    //   command is blocking (RESERVED)
#define F_DIRTY    0x0020    //   Property was modified (RESERVED)
#define F_ACTION   0x0040    //   Property changes cause events
#define F_LINK     0x0080    ///< Node is a link (members are instanced)

/* These flags are exposed to the protocol. DO NOT CHANGE */
#define F_RW       0x0000    ///< Readable/Writeable
#define F_RO       0x0100    ///< Readonly
#define F_WO       0x0200    ///< Writeonly
#define F_INACTIVE 0x0400    ///< Property is currently inactive
#define F_VOLATILE 0x2000    ///< Property is volatile

#define FLAG_MASK_PUBLIC     0xff00

// see property_types.h for internal flags

/** Run time information data type.
 *
 * This contains a value union and a type field
 *
 * A DCValue is to be initialized as follows:
 * 
   \code v.type = <type>; v.value.<valuefield> = <value> \endcode
 *
 * For example, see macros #SET_FLOAT, etc.
 *
 * In case of a string or buffer value, it is important to know that
 * the data buffer is not owned by the property structure, i.e. the
 * DCValue only serves as a descriptor and no value copying actually
 * happens.
 * Therefore, the programmer has to distinguish between two cases
 * before querying a property via dcDevice_GetProperty()
 *
 * \li The property is locally static, i.e. lives on the client's heap:
 *     The caller does not have to worry about pointer initialization of
 *     the DCValue before calling dcDevice_GetProperty()
 * \li The property is dynamic (read/write) and the caller *must*
 *     initialize the value buffer.
 *
 * If in doubt, ALWAYS initialize a string buffer. If value.p was
 * changed within the call, you can release the buffer and assume that the
 * property is static.
 *
 * For a dynamic string, the initialization would look as follows:
 *
\code 
#define BUF_LEN 80
	DCValue v;
	char buffer[BUF_LEN];
	v.type = DC_STRING; v.len = BUF_LEN; v.value.p = buffer;

	dcDevice_GetProperty(d, property, &v);
	// ...
	//
\endcode
 * Note that in this example, the (zero terminated) string has a maximum
 * length of 79.
 *
 * If there was not enough space reserved for the property buffer,
 * an error #DCERR_PROPERTY_SIZE_MATCH is returned from dcGetProperty()
 *
 * Data type checking is performed when setting properties.
 */

typedef struct 
{
	union {
		uint32_t       u;   ///< Union: Unsigned 32 bit value
		int32_t        i;   ///< Union: Integer value
		float          f;   ///< Union: Float value
		void          *p;   ///< Union: Generic Pointer value
	} value;
	PropertyType    type;   ///< Value Type
	size_t len;             ///< length of array, if buffer type
	unsigned short index;   ///< Access index for array types
	uint16_t       flags;   ///< Public Flags
} DCValue;

/**
 * \ingroup Misc
 *
 * Initialize float DCValue */
#define SET_FLOAT(v, f) v.type = DC_FLOAT; v.value.f = f
/**
 * \ingroup Misc
 *
 * Initialize integer DCValue */
#define SET_INT(v, i)   v.type = DC_INT; v.value.f = i

/**
 * \ingroup Misc
 *
 * Initialize string DCValue */
#define SET_STRING(v, s, l)   v.type = DC_STRING; v.value.p = (void *) s; \
                              v.len = l;


/** 
 * \ingroup Misc
 *
 * Property callback function for recursive node walker.
 * See source code for detailed information.
 *
 * \param t      The current node token
 *
 * \return 1: success, continue walking
 *         0: quit walking.
 */

typedef int (PropCallback)(TOKEN t);

/**
 * Feedback function pointer definition
 *
 * See dcSetFeedback() in devlib.h
 */

typedef int (*FeedbackFuncP)(int i);

/** Logging function type */
typedef void (*LogFuncP)(int log_level, const char *fmt, ...);

/** Log levels */

enum {
	DCLOG_ERROR,    ///< Error level
	DCLOG_DEBUG,    ///< Debug level
	DCLOG_NOTICE,   ///< Notice level
	DCLOG_WARN,     ///< Warn level
	DCLOG_VERBOSE   ///< Verbose level
};

#endif // DCTYPES_H_INCLUDED_260872
