/** \file conffile.c
 *
 * Configuration file parser
 *
 * Configuration files have the typical INI format:
 *

# Comment
[config1]
Property = "Value"
List = Value1,Value2,Value3;
IntegerProperty = 2
Struct.Value = 4

[config2]
 ...


 *
 */

// #include "devlib.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "conffile.h"

#ifdef DEBUG
#define DEB(x) (x)
#else
#define DEB(x)
#endif

#define MAXARGS 4

typedef enum  {
	T_OVERFLOW = -127,
	T_UNKNOWN = 0,
	T_WORD,
	T_GROUP,
	T_ASSIGN,
	T_SEMI,
	T_NL,
	T_UNTERMINATED,
	T_FRAG,
	T_EOF
} ParserToken;


#define PARSE_ERROR(s, t) parse_error(line, s, t)

/** Fill buffer */

int fill_buffer(BufStream *s, char *remainder)
{
	int i;
	int size;

	if (!s->file) return 0;

	i = 0;
	size = s->end - remainder;
	if (remainder) {
		DEB(printf("Prepend: "));
		while (i < size) {
			DEB(printf("%c", *remainder));
			s->buf[0][i] = *remainder;
			i++; remainder++;
		}
		DEB(printf("\n"));
	}

	size = s->bufsize - i;
	if (size == 0) return -1;

	size = fread(&s->buf[0][i], 1, size, s->file);
	if (size < 0) return size;

	size += i;

	s->pos = s->buf[0];
	s->end = s->pos + size;
	return size;
}

void parse_error(int line, int state, char *txt)
{
	printf("[Line %d] Parse error: %s [state: %d]\n", line, txt, state);
}

static ParserToken gettoken(BufStream *s, char *w, int max)
{
	enum { S_NEUTRAL, S_COMMENT, S_INBRACKET,
		S_INQUOTE, S_ESCAPE, S_INWORD } state = S_NEUTRAL;
	int c;
	char *endw = &w[max];

	while (s->pos < s->end) {
		if (w > endw) {
			return T_OVERFLOW;
		}
		c = *s->pos++;
		switch(state) {
		case S_NEUTRAL:
			switch(c)
			{
			case '=':
				return T_ASSIGN;
			// case ',':
				// return T_COMMA;
			// case ';':
				// return T_SEMI;
			case '[':
				state = S_INBRACKET;
				continue;
			case '\r':
				return T_NL;
			case '\n':
				return T_NL;
			case ';':
			case '#':
				state = S_COMMENT;
				continue;
			case ' ':
			case '\t':
				continue;
			case '"':
				state = S_INQUOTE;
				continue;
			default:
				state = S_INWORD;
				*w++ = c;
				continue;
			}
		case S_COMMENT:
			switch(c) {
				case '\n':
					return T_NL;
			}
			break;
		case S_INBRACKET:
			switch(c) {
				case ']':
					*w = '\0';
					return T_GROUP;
					break;
				case '\n':
					return T_UNTERMINATED;
				default:
					*w++ = c;
					continue;
			}
			break;
		case S_INQUOTE:
			switch(c) {
				case '\\':
					state = S_ESCAPE;
					continue;
				case '\n':
					return T_UNTERMINATED;
				case '"':
					*w = '\0';
					return T_WORD;
				default:
					*w++ = c;
					continue;
			}
		case S_ESCAPE:
			*w++ = c;
			state = S_INQUOTE;
			break;
		case S_INWORD:
			switch(c) {
				case '{':
				case '}':
				// Index brackets are allowed inside property!
				// case '[':
				// case ']':
				case '=':
				case '\n':
				case ' ':
				case '\t':
					s->pos--;
					*w = '\0';
					return T_WORD;
				default:
					*w++ = c;
					continue;
			}
		}
	}
	if (state == S_NEUTRAL) {
		return T_EOF;
	} else {
		*w = '\0';
		return T_FRAG;
	}
}


#define WORDBUF_SIZE 40

#define COPYWORD(dst, src, n)   \
	while (n < (sizeof(dst)-1) && *src) {     \
		dst[n] = *src++; n++;                \
	}                                        \
	dst[n++] = '\0';


int conf_parse(BufStream *s, ConfHandler *h)
{
	static
	char buf[128];
	static
	char word[WORDBUF_SIZE];
	char *w;
	char *start;
	int retcode;

	char *argv[MAXARGS];
	int argc = 0;
	int i = 0;
	int line = 1;

	enum { S_NEUTRAL, S_PROPERTY, S_ASSIGN, S_ARG, S_SKIP } state = S_NEUTRAL;

	retcode = fill_buffer(s, NULL);
	if (retcode <= 0) return retcode;

	while (1) {
		start = s->pos;
		switch (gettoken(s, word, WORDBUF_SIZE)) {
			case T_WORD:
				switch (state) {
					case S_SKIP:
						break;
					case S_NEUTRAL:
						argc = 0;
						i = 0;
						w = word;
						argv[argc++] = &buf[i];
						COPYWORD(buf, w, i);
						state = S_PROPERTY;
						break;
					case S_ASSIGN:
						// Keep trailing '0' into account:
						if (i >= (sizeof(buf) - 1)) {
							PARSE_ERROR(state, "Buffer overflow");
							return -1;
						}
						argv[argc++] = &buf[i];
						w = word;
						COPYWORD(buf, w, i);
						state = S_ARG;
						break;
					default:
						PARSE_ERROR(state, "Unexpected string");
						return -1;
						break;
				}
				break;
			case T_GROUP:
				switch (state) {
					case S_PROPERTY:
					case S_NEUTRAL:
						h->add_group(h->user, word);
						break;
					default:
						PARSE_ERROR(state, "Unexpected group");
				}
				break;
			case T_ASSIGN:
				if (state == S_PROPERTY) {
					state = S_ASSIGN;
				} else if (state != S_SKIP) {
					PARSE_ERROR(state, "Unexpected '='");
					state = S_NEUTRAL;
				}
				break;
// Not used in this implementation
#if 0
			case T_COMMA:
				switch (state) {
					case S_ARG:
						state = S_LIST;
						break;
					default:
						PARSE_ERROR(state, "Unexpected ','");
						state = S_NEUTRAL;
				}
				break;
			case T_SEMI:
				switch (state) {
					case S_ARG:
						state = S_NEUTRAL;
						break;
					default:
						PARSE_ERROR(state, "Unexpected ';'");
				}
				break;
#endif
			case T_NL:
				switch (state) {
					case S_SKIP:
					case S_NEUTRAL:
						state = S_NEUTRAL;
						break;
					case S_ARG:
						if (argc == 2) {
							h->set_property(h->user, argv[0], argv[1]);
						}
						state = S_NEUTRAL;
						break;
					default:
						PARSE_ERROR(state, "Unexpected CR");
						return -1;
				}
				line++;
				DEB(printf("NEXT LINE: %d\n", line));

				break;
			case T_UNTERMINATED:
				PARSE_ERROR(state, "Unterminated string");
				line++;
				return -1;
				break;
			case T_FRAG:
				// printf("Premature end in state %d\n", state);
				// Load next buffer from file:
				retcode = fill_buffer(s, start);
				if (retcode == -100 || retcode == 0) {
					return 1;
				} else if (retcode < 0) {
					PARSE_ERROR(state, "Line too long/Read error");
					return -1;
				}
				break;
			case T_EOF:
				retcode = fill_buffer(s, 0);
				if (retcode == -100 || retcode == 0) {
					DEB(printf("End\n"));
					return 1;
				} else if (retcode < 0) {
					PARSE_ERROR(state, "Line too long/Read error");
					return -1;
				}
				break;
			case T_OVERFLOW:
				PARSE_ERROR(state, "Property word overflow, ignoring line");
				state = S_SKIP;
				break;
			default:
				PARSE_ERROR(state, "Unknown token");
		}
	}

	return 0;
}


#ifdef TEST

int my_add_group(void *c, const char *name)
{
	printf("ADD GROUP: %s\n", name);
	return 0;
}

int my_set_property(void *c, const char *name, const char *val)
{
	printf("Set Property: %s = '%s'\n", name, val);
	return 0;
}

int main(int argc, char **argv)
{
	BufStream s;
	ConfHandler h;
	int error;

	h.add_group = my_add_group;
	h.set_property = my_set_property;

	s.file = fopen("test.ini", "r");
	s.bufsize = 128;
	s.buf[0] = (char *) malloc(s.bufsize);

	error = conf_parse(&s, &h);
	if (error <= 0) {
		printf("Failed to parse conf file\n");
	}

	fclose(s.file);
	free(s.buf[0]);
	return 0;
}

#endif
