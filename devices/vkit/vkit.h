/**
 * Video configuration
 */

struct video_config_t {
	int width;
	int height;
	short videomode;
	int low;
	int high;
};

extern struct video_config_t g_config;

enum {
	VIDEOMODE_RAW,
	VIDEOMODE_8BIT,
	VIDEOMODE_8BIT_LUT,
	VIDEOMODE_REGION
};

extern unsigned char g_lut[1024];
extern char g_ip[16];
