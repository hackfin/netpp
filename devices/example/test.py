# Demo script to access example server through Python
#
# (c) 2013, 2014 <hackfin@section5.ch>
#

import sys
import netpp
import random
# import intelhex
# 
# def dump(data, fname):
# 	ih = intelhex.IntelHex()
# 	ih.puts(0, str(data))
# 	f = open(fname, "w")
# 	ih.dump(f)
# 	f.close()

def test_lut_array(l):
	"LUT array test"
	for i in range(l.Size.get()):
		l[i].set(i * 4)

	val = l[4].get()
	if val == 16:
		return True
	
	return False

def test_buffer(data):
	"Test (fixed) buffer transfer"
	buf = netpp.Buffer(0x40000)
	data.get(buf) # Supply buffer reference
	print "Filled buffer, size: %d" % (len(buf))
	return True

def test_integer(root):
	i = root.Integer1
	"Signed integer range test"
	i.set(42)
	if i.get() != 42:
		return False

	max_i = i.Max.get()
	
	try:
		i.set(max_i + 1)
		print "Maximum test failed"
		return False

	except ValueError:
		print "Maximum exceeded, test successful"

	i = root.Demo
	# Test signed integer value range:
	for val in [ -1, 0x7fffffff, -2147483648 ]:
		print "Setting value: %d" % val
		i.set(val)
		v = i.get()
		if v != val:
			print "Value mismatch: %x != %x" % (v, val)
			return False

	i.set(144) # Set dynamic limit
	try:
		root.DynLimits.set(145) # Must throw exception
		return False
	except ValueError:
		pass

	return True

def test_handled_data(hd):
	"Handled data test"
	SIZE = 0x20000
	buf = ""
	for i in range(SIZE):
		buf += (chr(random.randint(0, 255)))

	hd.set(buffer(buf))
	b = hd.get()
	if len(b) != SIZE:
		print "Error: expected buffer length 0x%x, got 0x%x" % (SIZE, len(b))
		return False

	if str(b) != buf:
		print "Buffers don't match"
#		dump(b, "/tmp/b.hex")
#		dump(buf, "/tmp/buf.hex")
		return False

	return True

def test_handled_array(ar):
	"Handled array test, see slave console"
	n = ar.Size
	ar[0].set("gna")
	v = ar[1].get()
	if v != "item_1":
		print "Got", v
		return False

	# Second method with pre-reserved string reference:
	s = 2 * ' ' # Too short string
	t = ar[0].get(s)
	if type(t) == type(0):
		print "Got return code", t
		pass
	elif s[:6] != "item_0":
		print "Got return value '%s'" % s
		print "len:", len(s)
		return False

	s = 10 * ' ' # Sufficient string
	ar[0].get(s)
	if s[:6] != "item_0":
		print "Got return value '%s'" % s
		print "len:", len(s)
		return False

	return True


def test_dyndata(dd):
	"Dynamic data test"
	s = """
	Pesudo warpi salmu it perpensel den Sackenwuck.
	Kneisi wambo iegen zietsel floerde memso toi,
	Wakambe seibl osmo ilte pansi gorgel sampo!
	"""
	dd.set(s)

	a = dd.get()
	print "Got in return '%s'" % a
	return True

def test_register(root):
	"Register endian test"
	root.ControlReg.set(0xaa55)
	low = root.ControlRegL.get()
	high = root.ControlRegH.get()

	if low != 0x55 or high != 0xaa:
		print "False endian order"
		return False
	return True

def test_varstring(s):
	"Variable string test"
	s.set("honk")
	v = s.get()
	s.set("Default") # Reset to Default
	if v != "honk":
		print "Return value does not match"
		return False

	return True

def test_float(f):
	"Float test"
	min_f = f.Min.get()
	max_f = f.Max.get()

	testval = float(2.55999994278)

	try:
		f.set(0.4)
		return False
	except ValueError:
		pass

	f.set(testval)

	fval = f.get()
	testval = testval - fval

	if testval > 0.0000001:
		print "Float return value does not match, got %f" % fval
		return False

	return True

def test_cmd(root):
	"DumpUser command"
	root.DumpUsers.set(0)
	print "Check slave console for dump"
	return True

def test_hidden(dev):
	"Hidden property test"
	name = "Password"

	t = dev.device.getToken(name)
	p = netpp.Property(t, name)
	b = p.get()
	if b != "Default":
		return False
	# Make sure it's hidden:
	root = dev.sync()
	try:
		b = root.Password.get()
	except KeyError:
		return True

	print "Password property was not hidden"
	return false

############################################################################
# Main program:
	
if len(sys.argv) > 1:
	dev = netpp.connect(sys.argv[1])
else:
	dev = netpp.connect("TCP:localhost:2008")

root = dev.sync()

root.prettyprint()

tests = [
	[ test_hidden, dev ],
	[ test_integer, root ],
	[ test_buffer, root.Stream.HandledData ],
	[ test_handled_data, root.Stream.HandledData ],
	[ test_dyndata, root.Stream.DynamicData ],
	[ test_handled_array, root.HandledArray ],
	[ test_register, root ],
	[ test_varstring, root.VariableString ],
	[ test_float, root.FloatVal ],
	[ test_cmd, root ],
	[ test_lut_array, root.LUTarray ],
]

if sys.platform == "win32":
	REDBG = "!!!     "
	GREEN = ""
	OFF = ""
else:
	REDBG = "\033[7;31m"
	GREEN = "\033[32m"
	OFF = "\033[0m"

for t in tests:
	print 76 * '-'
	print "Running %s" % t[0].__doc__
	ret = apply(t[0], [t[1]])
	if not ret:
		print REDBG + "TEST FAILED" + OFF
	else:
		print GREEN + "Test passed" + OFF
