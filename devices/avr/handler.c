
/**************************************************************************
 *
 * (c) 2005-2011 section5 // Martin Strubel
 *
 * This file was generated by dclib/netpp. Modifications to this file will
 * be lost. Rename to handler.c and remove this warning.
 *
 * Stylesheet: userhandler.xsl    (c) 2005-2014 www.section5.ch
 *
 *
 **************************************************************************/
#include "devlib_types.h"
#include "devlib_error.h"
#include "property_protocol.h"  // for netpp_log()
#include <stdio.h> // XXX just for debugging
#include <avr/sfr_defs.h>
#include "adc.h"
#include "portmux.h"
#include "modes.h"
#include "timer.h"

struct adc_config {
	unsigned char channel;
} g_adc = {
	.channel = 0
};

struct debug g_debug;

/* Handler for property 'Data' */
	
int get_adcdata(DEVICE d, DCValue *out)
{
	out->value.u = adc_read(128);
	return 0;
}

/* Handler for property 'Mode' */
	
int get_portmux(DEVICE d, DCValue *out)
{
	return DCERR_PROPERTY_HANDLER;
}

int set_portmux(DEVICE d, DCValue *in)
{
	// if (in->value.index > NUM_PINS) return DCERR_PROPERTY_RANGE;
	return set_portfunc(in->index, in->value.i);
}

/* Handler for property 'ADC.MuxMode' */
	
int get_mux(DEVICE d, DCValue *out)
{
	out->value.i = get_adc_mux();
	return 0;
}

int set_mux(DEVICE d, DCValue *in)
{
	set_adc_mux(in->value.i);
	return 0;
}

/* Handler for property 'Level' */
	
int get_iolevel(DEVICE d, DCValue *out)
{
	int ret = get_port(out->index);
	if (ret < 0) return ret;
	out->value.i = ret;
	return 0;
}

int set_iolevel(DEVICE d, DCValue *in)
{
	return set_port(in->index, in->value.i);
}

int get_temp(DEVICE d, DCValue *out)
{
// 	uint8_t mux = get_adc_mux();
	out->value.f = (float) adc_read(39) / 10.0;
	return 0;
}

int get_tick(DEVICE d, DCValue *out)
{
	out->value.u = timer0_get();
	return 0;
}
		
////////////////////////////////////////////////////////////////////////////

int device_write(RemoteDevice *d,
		uint32_t addr, const unsigned char *buf,
		unsigned long size)
{
	uint8_t page = (addr >> 8);
	switch (page) {
		case 0:
			switch (size) {
				case 1: _SFR_MEM8(addr) = buf[0]; break;
				case 2: _SFR_MEM16(addr) = (unsigned short)
					buf[0] | (buf[1] << 8); break;
			}
			break;
		default:
			return DCERR_PROPERTY_ACCESS;
	}
	return 0;

}

int device_read(RemoteDevice *d,
		uint32_t addr, unsigned char *buf, unsigned long size)
{
	uint8_t page = (addr >> 8);
	uint8_t *p;
	uint16_t v;
	switch (page) {
		case 0:
			switch (size) {
				case 1: buf[0] = _SFR_MEM8(addr); break;
				case 2:
					v = _SFR_MEM16(addr); buf[0] = v; buf[1] = v >> 8;
					break;
			}
			break;
		case 1:
			if (size != 2) return DCERR_COMM_FRAME;
			p = (uint8_t *) &g_portmap[addr & 0xff];
			*buf++ = *p++; *buf = *p;
			break;
		default:
			return DCERR_PROPERTY_ACCESS;
	}
	return 0;
}


