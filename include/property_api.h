/* \file
 * Internal property I/O API
 *
 * This is a nasty hack to swap the internal api property_*() calls against
 * possibly external implementations of the property_*() set.
 * When PROXY_MODE set, the call family prefix is changed to local_.
 *
 * */


#ifdef PROXY_MODE
#define  property_getbuf    local_getbuf
#define  property_parsename local_parsename
#define  property_select    local_select
#define  property_get       local_get
#define  property_getname   local_getname
#define  property_getdesc   local_getdesc
#define  property_set       local_set
#else
#undef  property_getbuf
#undef  property_parsename
#undef  property_select
#undef  property_get
#undef  property_getname
#undef  property_getdesc
#undef  property_set
#endif

int         property_getbuf(DEVICE d, TOKEN p, DCValue *value);
TOKEN       property_parsename(DEVICE d, const char *propname, TOKEN parent);
TOKEN       property_select(DEVICE d, TOKEN parent, TOKEN prev);
/** Local call to get a property value. Use only on slave for direct
 * local property access (wrappers)
 *
 * Note this function may be called twice on a property transaction 
 * and must not allocate DCValue buffers on the stack.
 *
 */
int         property_get(DEVICE d, TOKEN p, DCValue *value);
int         property_getname(DEVICE d, TOKEN t, DCValue *value);
int         property_getdesc(DEVICE d, TOKEN t, DCValue *value);
/** Local call to set a property value. Use only on slave for direct
 *
 * Note this function may be called twice on a property transaction 
 * and must not allocate DCValue buffers on the stack.
 *
 * local property access (wrappers) */
int         property_set(DEVICE d, TOKEN p, DCValue *value);

