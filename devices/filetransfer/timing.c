#include "timing.h"

#if defined(__WIN32) || defined(__CYGWIN__)


float took_time(unsigned long *t0, unsigned long *t1)
{
	float t;

	int msec = *t1 - *t0;
	t = (float) msec / 1000.0;
	return t;
}

int get_time(unsigned long *t)
{
	*t = GetTickCount();
	return 0;
}

#else


float took_time(struct timeval *t0, struct timeval *t1)
{
	time_t sec;
	suseconds_t usec;
	float t;

	sec = t1->tv_sec - t0->tv_sec;
	usec = t1->tv_usec - t0->tv_usec;

	if (usec < 0) {
		usec += 1000000;
		sec--;
	}
	t = (float) sec + (float) usec / 1e6;
	// printf("sec: %ld, usec: %lu\n", sec, usec);
	// printf("took: %f\n", t);
	return t;
}

int get_time(struct timeval *t)
{
	return gettimeofday(t, 0);
}

#endif

