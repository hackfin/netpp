/** \file slave.c
 *
 * Slave property dispatcher
 * Property protocol back end (embedded system, etc.)
 *
 * (c) 2006 Martin Strubel <hackfin@section5.ch>
 *
 */

#ifdef DEBUG
#define DEB(x) x
#endif

#include "netpp.h"
#include "slave.h"
#include "example.h"
#include <stdio.h>

static unsigned char _dummy_registers[256] = {
	0xaa, 0x55, 
};

/** Device flat address register map write access.
 * For low level device access (SPI, I2C, etc.) this normally wants to
 * be implemented */

#ifdef __ZPU__

int device_write(RemoteDevice *d,
		uint32_t addr, const unsigned char *buf,
		unsigned long size)
{
	return 0;
}

int device_read(RemoteDevice *d,
		uint32_t addr, unsigned char *buf, unsigned long size)
{
	return 0;
}

#else

int device_write(RemoteDevice *d,
		uint32_t addr, const unsigned char *buf,
		unsigned long size)
{
	addr &= 0xfff;
	if (addr > 255) {
		printf("Address 0x%x out of range.\n", addr);
		return DCERR_PROPERTY_RANGE;
	}
	printf("Write to register %04x:", addr);
	memcpy(&_dummy_registers[addr & 0xff], buf, size);
	while (size--) {
		printf(" %02x", *buf++);
	}
	printf("\n");
	return 0;
}


/** Device flat address register map read access.
 * For low level device access (SPI, I2C, etc.) this normally wants to
 * be implemented */

int device_read(RemoteDevice *d,
		uint32_t addr, unsigned char *buf, unsigned long size)
{
	addr &= 0xfff;
	if (addr > 255) {
		printf("Address 0x%x out of range.\n", addr);
		return DCERR_PROPERTY_RANGE;
	}
	printf("Read from register %04x (%lu bytes)\n", addr, size);
	memcpy(buf, &_dummy_registers[addr & 0xff], size);
	return 0;
}

#endif

////////////////////////////////////////////////////////////////////////////
//
// TODO: Install a SIGPIPE handler


int main(int argc, char **argv)
{
	int error;

	// Important: Register property list
	register_proplist(g_devices, g_ndevices);
//
// This function must be called for example from MSVC
#ifndef C99_COMPLIANT
	init_properties();
#endif

	error = start_server(argc, argv);
	if (error)
		netpp_log(DCLOG_ERROR, "Server terminated with error:\n"
			"'%s'\n", dcGetErrorString(error));
	return error;
}

